import type { FC, PropsWithChildren } from "react";
import { Suspense } from "react";
import { initReactI18next } from "react-i18next";
import i18n from "i18next";
import HttpApi from "i18next-http-backend";
import useTranslate from "@hooks/useTranslate";
import LanguageSwitcher from "@components/LanguageSwitcher";
import { Language } from "@store/translateStore";

i18n
  .use(HttpApi)
  .use(initReactI18next)
  .init({
    load: "currentOnly",
    saveMissingTo: "fallback",
    fallbackLng: Language.EN,
    supportedLngs: [Language.EN, Language.ZH]
  });

const TranslateProvider: FC<PropsWithChildren> = ({ children }) => {
  const { setLanguage } = useTranslate();

  return (
    <Suspense>
      {i18n.isInitialized && children}
      <LanguageSwitcher onSwitch={setLanguage} />
    </Suspense>
  );
};

export default TranslateProvider;
