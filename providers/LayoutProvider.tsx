import type { FC, PropsWithChildren } from "react";

import ProtectedLayout from "@components/Layouts/ProtectedLayout";
import AuthLayout from "@components/Layouts/AuthLayout";

export type LayoutProviderProps = {
  isAuthPage?: boolean;
};

const ProtectedPage: FC<PropsWithChildren> = ({ children }) => {
  // TODO: pass some hooks or business logic here
  return <ProtectedLayout>{children}</ProtectedLayout>;
};

const LayoutProvider: FC<PropsWithChildren<LayoutProviderProps>> = ({
  isAuthPage,
  children
}) => {
  if (isAuthPage) return <AuthLayout>{children}</AuthLayout>;
  return <ProtectedPage>{children}</ProtectedPage>;
};

export default LayoutProvider;
