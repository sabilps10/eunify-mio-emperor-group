import apiCall, { BaseUrlVariant } from "./apiCall";

export const getClientFunctions = async (payload: any) => {
  const res = await apiCall.post(BaseUrlVariant.gateway_security, `/setting/functions`, {
    data: payload
  });
  return res.data;
};

export const getRoleAssignments = async (payload: any) => {
  const res = await apiCall.post(BaseUrlVariant.gateway_security, `/setting/roleAssignments`, {
    data: payload
  });
  return res.data;
};

export const createRoleAssignments = async (payload: any) => {
  const res = await apiCall.post(BaseUrlVariant.gateway_security, "/setting/roleAssignment", {
    data: payload
  });
  return res.data;
};

export const editRoleAssignments = async ({ id, payload }: { id: string; payload: any }) => {
  const res = await apiCall.put(BaseUrlVariant.gateway_security, `/setting/roleAssignments/${id}`, {
    data: payload
  });
  return res.data;
};

export const getUserAssignment = async (payload: any) => {
  const res = await apiCall.post(BaseUrlVariant.gateway_security, `/setting/userAssignments`, {
    data: payload
  });
  return res.data;
};

// user assignment
export const postUserAssignment = async (payload: any) => {
  const res = await apiCall.post(BaseUrlVariant.gateway_security, `/setting/userAssignment`, {
    data: payload
  });
  return res.data;
};

export const getUserName = async (email: any) => {
  const res = await apiCall.query(
    BaseUrlVariant.gateway_security,
    `/setting/UserManagement/username/${email}`
  );
  return res.data;
};

export const getEmailAvailable = async (email: any) => {
  const res = await apiCall.query(BaseUrlVariant.gateway_security, `/setting/is-email-available/${email}`);
  return res.data;
};

export const addUserManagement = async (payload: any) => {
  const res = await apiCall.post(BaseUrlVariant.gateway_security, "/setting/userAssignment", {
    data: payload
  });
  return res.data;
};

export const editUserManagement = async (id: string | number, payload: any) => {
  const res = await apiCall.put(BaseUrlVariant.gateway_security, `/setting/userAssignment/${id}`, {
    data: payload
  });
  return res.data;
};
