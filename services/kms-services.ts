import apiCall, { BaseUrlVariant } from "./apiCall";

export const encrypt = async (payload: any) => {
  const res = await apiCall.post(BaseUrlVariant.kms, "/encrypt", { data: payload });
  return res.data;
};
