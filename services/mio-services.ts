import apiCall, { BaseUrlVariant } from "./apiCall";

export const getDepositChannelPage = async (payload: any) => {
  const res = await apiCall.post(BaseUrlVariant.gateway_mio, `/setting/depositChannel/page`, {
    data: payload
  });
  return res.data;
};

export const getDepositChannelList = async (payload: any) => {
  const res = await apiCall.post(BaseUrlVariant.gateway_mio, `/setting/depositChannel/list`, {
    data: payload
  });
  return res.data;
};

export const putDepositChannel = async (id: number, payload: any) => {
  const res = await apiCall.put(BaseUrlVariant.gateway_mio, `/setting/depositChannel/${id}`, {
    data: payload
  });
  return res.data;
};

export const getWithdrawalChannelPage = async (payload: any) => {
  const res = await apiCall.post(BaseUrlVariant.gateway_mio, `/setting/withdrawalChannel/page`, {
    data: payload
  });
  return res.data;
};

export const putWithdrawalChannel = async (id: number, payload: any) => {
  const res = await apiCall.put(BaseUrlVariant.gateway_mio, `/setting/withdrawalChannel/${id}`, {
    data: payload
  });
  return res.data;
};

export const getWithdrawalChannelList = async (payload: any) => {
  const res = await apiCall.post(BaseUrlVariant.gateway_mio, "/setting/withdrawalChannel/list", {
    data: payload
  });
  return res.data;
};

export const getTransactions = async (payload: any) => {
  const res = await apiCall.post(BaseUrlVariant.gateway_mio, "/transactions", { data: payload });
  return res.data;
};
export const getTransactionsRecords = async (payload: any) => {
  const res = await apiCall.post(BaseUrlVariant.gateway_mio, "/transactions/page", { data: payload });
  return res.data;
};

export const getWithdrawalRecords = async (payload: any, query = "list") => {
  const res = await apiCall.post(BaseUrlVariant.gateway_mio, `/withdrawalInst/${query}`, {
    data: payload
  });
  return res.data;
};

export const putWithdrawalStatus = async (payload: any) => {
  const res = await apiCall.put(BaseUrlVariant.gateway_mio, "/withdrawalInst/approval", {
    data: payload
  });
  return res.data;
};

export const updateWithdrawalAML = async (payload: any) => {
  const res = await apiCall.post(BaseUrlVariant.gateway_mio, "/withdrawalInst/retryAml", {
    data: payload
  });
  return res.data;
};

export const getWithdrawalBalanceCheck = async (payload: any) => {
  const res = await apiCall.query(BaseUrlVariant.gateway_mio, `/accounts/withdrawalBalanceCheck`, {
    params: payload
  });
  return res.data;
};

export const getAccountBalanceCheck = async (payload: any) => {
  const res = await apiCall.query(BaseUrlVariant.gateway_mio, `/accounts/Balance`, {
    params: payload
  });
  return res.data;
};

export const getCcbaBankRecords = async (payload: any) => {
  const res = await apiCall.post(BaseUrlVariant.gateway_mio, "/ccbaBankRecords", {
    data: payload
  });
  return res.data;
};
export const postCcbaBankRecords = async (payload: any) => {
  const res = await apiCall.post(BaseUrlVariant.gateway_mio, "/ccbaBankRecords/page", {
    data: payload
  });
  return res.data;
};

export const getServiceCharge = async (payload: any) => {
  const res = await apiCall.query(BaseUrlVariant.gateway_mio, `/setting/depositChannel/charge`, {
    params: payload
  });
  return res.data;
};

export const getReportList = async () => {
  const res = await apiCall.query(BaseUrlVariant.gateway, "/report/reports/list");
  return res.data;
};

export const getReportData = async (payload: any) => {
  const res = await apiCall.query(
    BaseUrlVariant.gateway,
    `/report/reports/data?reportCode=${payload.reportCode}&dateFrom=${payload.dateFrom}&dateTo=${payload.dateTo}`
  );
  return res.data;
};

export const postWithdrawal = async (payload: any, token: string) => {
  const res = await apiCall.post(BaseUrlVariant.gateway_mio, "/withdrawal", {
    data: payload,
    headers: { token }
  });
  return res.data;
};

export const postWithdrawalInst = async (payload: any) => {
  const res = await apiCall.post(BaseUrlVariant.gateway_mio, "/withdrawalInst", {
    data: payload
  });
  return res.data;
};

export const postPaymentCancel = async (payload: any) => {
  const res = await apiCall.post(BaseUrlVariant.gateway_mio, "/transaction/paymentCancel", {
    data: payload
  });
  return res.data;
};

export const postPaymentFailed = async (payload: any) => {
  const res = await apiCall.post(BaseUrlVariant.gateway_mio, "/transaction/paymentFailed", {
    data: payload
  });
  return res.data;
};

export const postPaymentConfirm = async (payload: any) => {
  const res = await apiCall.post(BaseUrlVariant.gateway_mio, "/transaction/paymentConfirm", {
    data: payload
  });
  return res.data;
};

export const postUnclaimBalance = async (payload: any) => {
  const res = await apiCall.post(BaseUrlVariant.gateway_mio, "/transaction/unclaimBalance", {
    data: payload
  });
  return res.data;
};

export const postExportTransaction = async (payload: any) => {
  const res = await apiCall.post(BaseUrlVariant.gateway_mio, "/transaction/exportTransaction", {
    data: payload
  });
  return res.data;
};

export const putCcbaBankRecord = async (payload: any) => {
  const res = await apiCall.put(BaseUrlVariant.gateway_mio, "/ccbaBankRecordsMapping", {
    data: payload
  });
  return res.data;
};

export const getDepositInst = async (payload: any, query = "list") => {
  const res = await apiCall.post(BaseUrlVariant.gateway_mio, `/depositInst/${query}`, {
    data: payload
  });
  return res.data;
};

export const postDepositInst = async (payload: any) => {
  const res = await apiCall.post(BaseUrlVariant.gateway_mio, "/depositInst", {
    data: payload
  });
  return res.data;
};

export const putDepositStatus = async (payload: any) => {
  const res = await apiCall.put(BaseUrlVariant.gateway_mio, "/depositInst/approval", {
    data: payload
  });
  return res.data;
};

export const updateDepositAML = async (payload: any) => {
  const res = await apiCall.post(BaseUrlVariant.gateway_mio, "/depositInst/retryAml", {
    data: payload
  });
  return res.data;
};

export const getBlacklistChannelList = async (payload?: any) => {
  const res = await apiCall.query(BaseUrlVariant.gateway_mio, `/setting/depositChannel/blacklists`, {
    params: payload
  });
  return res.data;
};

export const updateBlacklistChannel = async (payload: any) => {
  const res = await apiCall.post(BaseUrlVariant.gateway_mio, "/setting/depositChannel/blacklists", {
    data: payload
  });
  return res.data;
};

export const postDepositExceptions = async (payload: any) => {
  const res = await apiCall.post(BaseUrlVariant.gateway_mio, "/Exception/DepositException", {
    data: payload
  });
  return res.data;
};
