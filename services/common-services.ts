import { string } from "zod";
import apiCall, { BaseUrlVariant } from "./apiCall";

export const getEntityId = async () => {
  const res = await apiCall.query(
    BaseUrlVariant.gateway_common,
    "/codeValue?tableCode=payment_channel&columnCode=entity_id&lang=en"
  );
  return res.data;
};

export const getPaymentGatewayName = async () => {
  const res = await apiCall.query(
    BaseUrlVariant.gateway_common,
    "/codeValue?tableCode=payment_channel&columnCode=gateway_id&lang=en"
  );
  return res.data;
};

export const getPaymentChannelStatus = async () => {
  const res = await apiCall.query(
    BaseUrlVariant.gateway_common,
    "/codeValue?tableCode=payment_channel&columnCode=status&lang=en"
  );
  return res.data;
};

export const getCurrency = async () => {
  const res = await apiCall.query(
    BaseUrlVariant.gateway_common,
    "/codeValue?tableCode=exchange_rate&columnCode=currency&lang=en"
  );
  return res.data;
};

export const getExchangeType = async () => {
  const res = await apiCall.query(
    BaseUrlVariant.gateway_common,
    "/codeValue?tableCode=exchange_rate&columnCode=exchange_type&lang=en"
  );
  return res.data;
};

export const getExchangeRatePage = async () => {
  const res = await apiCall.post(BaseUrlVariant.gateway_common, `/setting/exchangeRates/page`, {
    data: {}
  });
  return res.data;
};

export const getExchangeRateList = async (payload: any | null) => {
  const res = await apiCall.post(BaseUrlVariant.gateway_common, `/setting/exchangeRates/list`, {
    data: payload || {}
  });
  return res.data;
};
export const putExchangeRate = async (id: number, payload: any) => {
  const res = await apiCall.put(BaseUrlVariant.gateway_common, `/setting/exchangeRates/${id}`, {
    data: payload
  });
  return res.data;
};

export const getBankType = async () => {
  const res = await apiCall.query(
    BaseUrlVariant.gateway_common,
    "/codeValue?tableCode=customer_bank_account&columnCode=bank_type"
  );
  return res.data;
};

export const getBankStatus = async () => {
  const res = await apiCall.query(
    BaseUrlVariant.gateway_common,
    "/codeValue?tableCode=customer_bank_account&columnCode=status"
  );
  return res.data;
};

export const getCustomerBankSetting = async (payload: any) => {
  const res = await apiCall.post(BaseUrlVariant.gateway_common, `/setting/customerBankSetting`, {
    data: payload
  });
  return res.data;
};

export const getTradeType = async () => {
  const res = await apiCall.query(
    BaseUrlVariant.gateway_common,
    "/codeValue?tableCode=payment_channel&columnCode=trade_type"
  );
  return res.data;
};

export const getChannel = async () => {
  const res = await apiCall.query(
    BaseUrlVariant.gateway_common,
    "/codeValue?tableCode=payment_channel&columnCode=gateway_id"
  );
  return res.data;
};

export const getAdjusmentType = async () => {
  const res = await apiCall.query(
    BaseUrlVariant.gateway_common,
    "/codeValue?tableCode=inst_request&columnCode=adjustment_type"
  );
  return res.data;
};

export const getAdjustmentTypeList = async () => {
  const res = await apiCall.query(BaseUrlVariant.gateway_common, "/setting/adjustmentType");
  return res.data;
};

export const getPaymentNetwork = async () => {
  const res = await apiCall.query(
    BaseUrlVariant.gateway_common,
    "/codeValue?tableCode=payment_channel_currency&columnCode=network"
  );
  return res.data;
};

export const getPaymentChannelUnit = async () => {
  const res = await apiCall.query(
    BaseUrlVariant.gateway_common,
    "/codeValue?tableCode=payment_channel&columnCode=unit"
  );
  return res.data;
};

export const getNetwork = async () => {
  const res = await apiCall.query(
    BaseUrlVariant.gateway_common,
    "/codeValue?tableCode=payment_channel_currency&columnCode=network"
  );
  return res.data;
};

export const getExchangeRate = async (payload: any) => {
  const res = await apiCall.post(BaseUrlVariant.gateway_common, "/setting/exchangeRate", {
    data: payload
  });
  return res.data;
};

export const getPaymentStatus = async () => {
  const res = await apiCall.query(
    BaseUrlVariant.gateway_common,
    "/codeValue?tableCode=deposit_transaction&columnCode=payment_status"
  );
  return res.data;
};

export const getPaymentStatusRejectedDeposit = async () => {
  const res = await apiCall.query(
    BaseUrlVariant.gateway_common,
    "/codeValue?tableCode=payment_management&columnCode=payment_status_rej_mi"
  );
  return res.data;
};

export const getPaymentStatusUnclaimedDeposit = async () => {
  const res = await apiCall.query(
    BaseUrlVariant.gateway_common,
    "/codeValue?tableCode=payment_management&columnCode=payment_status_unc_mi"
  );
  return res.data;
};

export const getPaymentStatusApprovedWithdrawal = async () => {
  const res = await apiCall.query(
    BaseUrlVariant.gateway_common,
    "/codeValue?tableCode=payment_management&columnCode=payment_status_ap_mo"
  );
  return res.data;
};

export const getCcbaPaymentStatus = async () => {
  const res = await apiCall.query(
    BaseUrlVariant.gateway_common,
    "/codeValue?tableCode=ccba_bank_record&columnCode=payment_status"
  );
  return res.data;
};

export const getRegistrationLimit = async () => {
  const res = await apiCall.query(BaseUrlVariant.gateway_common, "/setting/sysSetting");
  return res.data;
};

export const editRegistrationLimit = async (payload: any) => {
  const res = await apiCall.put(BaseUrlVariant.gateway_common, "/setting/sysSetting", {
    data: payload
  });
  return res.data;
};

export const getDummyAccounts = async () => {
  const res = await apiCall.query(BaseUrlVariant.gateway_common, "/setting/dummyAccounts");
  return res.data;
};

export const postDummyAccountsPage = async (payload: any) => {
  const res = await apiCall.post(BaseUrlVariant.gateway_common, "/setting/dummyAccounts/page", {
    data: payload
  });
  return res.data;
};

export const editDummyAccount = async (payload: any, id: string | number) => {
  const res = await apiCall.put(BaseUrlVariant.gateway_common, `/setting/dummyAccounts/${id}`, {
    data: payload
  });
  return res.data;
};

export const addDummyAccount = async (payload: any) => {
  const res = await apiCall.post(BaseUrlVariant.gateway_common, "/setting/dummyAccounts", {
    data: payload
  });
  return res.data;
};

export const getEddaRecord = async (payload: any) => {
  const res = await apiCall.post(BaseUrlVariant.gateway_common, "/accounts/bankAccounts/eddaStatus", {
    data: payload
  });
  return res.data;
};

export const getEntities = async () => {
  const res = await apiCall.query(BaseUrlVariant.gateway_common, "/setting/entity");
  return res.data;
};

export const getCurrencies = async () => {
  const res = await apiCall.query(BaseUrlVariant.gateway_common, "/setting/currency");
  return res.data;
};

export const getNetworks = async () => {
  const res = await apiCall.query(BaseUrlVariant.gateway_common, "/setting/network");
  return res.data;
};

export const putCodeValue = async (id: number, payload: any) => {
  const res = await apiCall.put(BaseUrlVariant.gateway_common, `/setting/codeValue/${id}`, {
    data: payload
  });
  return res.data;
};

export const getBeneficiaryAccountRecord = async () => {
  const res = await apiCall.query(BaseUrlVariant.gateway_common, "/setting/BeneficiaryAccount");
  return res.data;
};

export const putBeneficiaryAccount = async (payload: any, bank_id: any) => {
  const res = await apiCall.put(BaseUrlVariant.gateway_common, `/setting/BeneficiaryAccount/${bank_id}`, {
    data: payload
  });
  return res.data;
};

export const getPaymentGroup = async (entityId: string) => {
  const res = await apiCall.query(BaseUrlVariant.gateway_common, `/paymentGroups?entityId=${entityId}`);
  return res.data;
};

export const getNotificationStatus = async () => {
  const res = await apiCall.query(
    BaseUrlVariant.gateway_common,
    `/codeValue?tableCode=notification_template&columnCode=status&lang=en`
  );
  return res.data;
};

export const getNotificationType = async () => {
  const res = await apiCall.query(
    BaseUrlVariant.gateway_common,
    `/codeValue?tableCode=notification_template&columnCode=type&lang=en`
  );
  return res.data;
};

export const getInstStatus = async () => {
  const res = await apiCall.query(
    BaseUrlVariant.gateway_common,
    "/codeValue?tableCode=inst_request&columnCode=status"
  );
  return res.data;
};

export const getBankAccountsByEntity = async (payload?: any) => {
  const res = await apiCall.query(
    BaseUrlVariant.gateway_common,
    `/accounts/bankAccounts?entityId=${payload.entityId}`
  );
  return res.data;
};

export const getBankAccountsByTradingAccount = async (payload?: any) => {
  const res = await apiCall.query(
    BaseUrlVariant.gateway_common,
    `/accounts/bankAccounts?tradingAccountNo=${payload.tradingAccountNo}`
  );
  return res.data;
};

export const getBankAccountsByLocation = async (payload?: any) => {
  const res = await apiCall.query(BaseUrlVariant.gateway_common, `/setting/bank/${payload.locationId}`);
  return res.data;
};

export const getVerifiedBankAccounts = async (payload?: any) => {
  const res = await apiCall.query(
    BaseUrlVariant.gateway_common,
    `/accounts/${payload.customerAccountNo}/bankAccounts?entityId=${payload.entityId}&statuses=2${
      payload.entityId == "XPro" ? `&statuses=1` : ""
    }&bankType=BK`
  );
  return res.data;
};

export const getCustomerBankCurrency = async () => {
  const res = await apiCall.query(
    BaseUrlVariant.gateway_common,
    "/codeValue?tableCode=customer_bank_account&columnCode=currency&lang=en"
  );
  return res.data;
};

export const checkEAML = async (payload: any) => {
  const res = await apiCall.post(BaseUrlVariant.gateway_common, "/eaml/validation", {
    data: payload
  });
  return res.data;
};

export const getAuditLogList = async (payload: any) => {
  const res = await apiCall.query(BaseUrlVariant.gateway_common, `/auditLog`, {
    params: payload
  });
  return res.data;
};

export const getAuditLogPage = async (payload: any) => {
  const res = await apiCall.post(BaseUrlVariant.gateway_common, `/auditLog/page`, {
    data: payload
  });
  return res.data;
};

export const postAuditLog = async (payload: any) => {
  const res = await apiCall.post(BaseUrlVariant.gateway_common, `/auditLog`, {
    data: payload
  });
  return res.data;
};

export const getClientBankRecord = async (payload: any) => {
  let queries = "";
  let counts = 0;
  Object.keys(payload).forEach(function (obj: any, index: number) {
    if (payload[obj]) {
      if (counts == 0) {
        queries += `?${obj}=${payload[obj]}`;
        counts++;
      } else {
        queries += `&${obj}=${payload[obj]}`;
        counts++;
      }
    }
  });
  const res = await apiCall.query(BaseUrlVariant.gateway_common, "/accounts/bankAccounts" + queries);
  return res.data;
};

export const postClientBankRecord = async (payload: any) => {
  const res = await apiCall.post(BaseUrlVariant.gateway_common, "/accounts/bankAccounts/page", {
    data: payload
  });
  return res.data;
};

export const getAddressMappingList = async (payload: any) => {
  const res = await apiCall.post(BaseUrlVariant.gateway_common, `/setting/addressMapping/list`, {
    data: payload
  });
  return res.data;
};

export const postProvince = async (payload: any) => {
  const res = await apiCall.post(BaseUrlVariant.gateway_common, `/setting/addressMapping/province`, {
    data: payload
  });
  return res.data;
};

export const putProvince = async (id: number, payload: any) => {
  const res = await apiCall.put(BaseUrlVariant.gateway_common, `/setting/addressMapping/province/${id}`, {
    data: payload
  });
  return res.data;
};

export const postRegion = async (payload: any) => {
  const res = await apiCall.post(BaseUrlVariant.gateway_common, `/setting/addressMapping/region`, {
    data: payload
  });
  return res.data;
};

export const putRegion = async (id: number, payload: any) => {
  const res = await apiCall.put(BaseUrlVariant.gateway_common, `/setting/addressMapping/region/${id}`, {
    data: payload
  });
  return res.data;
};

export const postCity = async (payload: any) => {
  const res = await apiCall.post(BaseUrlVariant.gateway_common, `/setting/addressMapping/city`, {
    data: payload
  });
  return res.data;
};

export const putCity = async (id: number, payload: any) => {
  const res = await apiCall.put(BaseUrlVariant.gateway_common, `/setting/addressMapping/city/${id}`, {
    data: payload
  });
  return res.data;
};

export const getTradeDate = async () => {
  const res = await apiCall.query(BaseUrlVariant.gateway_common, "/setting/TradeDate/list");
  return res.data;
};

export const postTradeDate = async (payload: any) => {
  const res = await apiCall.post(BaseUrlVariant.gateway_common, "/setting/TradeDate", {
    data: payload
  });
  return res.data;
};

export const getAccounts = async (payload: any) => {
  const res = await apiCall.query(BaseUrlVariant.gateway_common, `/accounts`, {
    params: payload
  });
  return res.data;
};

export const getLocationList = async (payload: any) => {
  const res = await apiCall.post(BaseUrlVariant.gateway_common, `/setting/addressMapping/location/list`, {
    data: payload
  });
  return res.data;
};

export const getProvinceList = async (payload: any) => {
  const res = await apiCall.post(BaseUrlVariant.gateway_common, `/setting/addressMapping/province/list`, {
    data: payload
  });
  return res.data;
};

export const getRegionList = async (payload: any) => {
  const res = await apiCall.post(BaseUrlVariant.gateway_common, `/setting/addressMapping/region/list`, {
    data: payload
  });
  return res.data;
};

export const getCityList = async (payload: any) => {
  const res = await apiCall.post(BaseUrlVariant.gateway_common, `/setting/addressMapping/city/list`, {
    data: payload
  });
  return res.data;
};

export const postAdjustmentType = async (data: any) => {
  const res = await apiCall.post(BaseUrlVariant.gateway_common, `/setting/adjustmentType`, {
    data
  });
  return res.data;
};

export const postBankAccount = async (accountNo: string, payload: any) => {
  const res = await apiCall.post(
    BaseUrlVariant.gateway_common,
    `/accounts/client/${accountNo}/bankAccounts`,
    {
      data: payload
    }
  );
  return res.data;
};
export const getBankMaster = async () => {
  const res = await apiCall.query(BaseUrlVariant.gateway_common, "/setting/banks");
  return res.data;
};

export const addBankMaster = async (payload: any) => {
  const res = await apiCall.post(BaseUrlVariant.gateway_common, "/setting/banks", { data: payload });
  return res.data;
};

export const getBankEddaCode = async () => {
  const res = await apiCall.query(
    BaseUrlVariant.gateway_common,
    "/codeValue?tableCode=bank_master&columnCode=edda_bank_code"
  );
  return res.data;
};

export const getEddaStatus = async () => {
  const res = await apiCall.query(
    BaseUrlVariant.gateway_common,
    "/codeValue?tableCode=edda_request&columnCode=status"
  );
  return res.data;
};

export const putBankAccount = async (accountNo: string, bankId: string, payload: any) => {
  const res = await apiCall.put(
    BaseUrlVariant.gateway_common,
    `/accounts/client/${accountNo}/bankAccounts/${bankId}`,
    {
      data: payload
    }
  );
  return res.data;
};
export const editBankMaster = async ({ id, payload }: { id: string; payload: any }) => {
  const res = await apiCall.put(BaseUrlVariant.gateway_common, `/setting/banks/${id}`, { data: payload });
  return res.data;
};

export const getEddaDisplayName = async () => {
  const res = await apiCall.query(BaseUrlVariant.gateway_common, "/setting/eddaDisplayName");
  return res.data;
};
