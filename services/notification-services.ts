import apiCall, { BaseUrlVariant } from "./apiCall";

export const getNotificationList = async (payload: any) => {
  const res = await apiCall.post(BaseUrlVariant.gateway_notification, "/template/list", {
    data: payload
  });

  return res.data;
};

export const getEventList = async (payload: any) => {
  const res = await apiCall.post(BaseUrlVariant.gateway_notification, "/mapping/list", {
    data: payload
  });
  return res.data;
};

export const getNotificationTemplate = async (id: string | number) => {
  const res = await apiCall.query(BaseUrlVariant.gateway_notification, `/template/${id}`);
  return res.data;
};

export const postNotificationTemplate = async (payload: any) => {
  const res = await apiCall.post(BaseUrlVariant.gateway_notification, `/template`, {
    data: payload
  });
  return res.data;
};

export const putNotificationTemplate = async ({ id, payload }: { id: string; payload: any }) => {
  const res = await apiCall.put(BaseUrlVariant.gateway_notification, `/template/${id}`, { data: payload });
  return res.data;
};

export const getNotificationVariableList = async ({ eventId }: any) => {
  const res = await apiCall.post(BaseUrlVariant.gateway_notification, "/event/variable/list", {
    data: { eventId }
  });
  return res.data;
};
