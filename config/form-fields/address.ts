import { array, object, string } from "zod";

export const ProvinceFields = [
  { id: "parentId", name: "parent_id", type: "text", disabled: false, hidden: true },
  { id: "nameEn", name: "en_display_name", type: "text", disabled: false },
  { id: "nameTc", name: "tc_display_name", type: "text", disabled: false },
  { id: "nameSc", name: "sc_display_name", type: "text", disabled: false }
];

export const ProvinceSchema = object({
  en_display_name: string().min(1, { message: "EN Display Name is required" }),
  tc_display_name: string().min(1, { message: "TC Display Name is required" }),
  sc_display_name: string().min(1, { message: "SC Display Name is required" })
});
