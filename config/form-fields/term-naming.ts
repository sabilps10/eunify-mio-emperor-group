import { object, string } from "zod";

export const EditTermNamingFields = [
  { id: "codeCode", name: "system_display_name", type: "text", disabled: true },
  { id: "nameEn", name: "en_display_name", type: "text", disabled: false },
  { id: "nameTc", name: "tc_display_name", type: "text", disabled: false },
  { id: "nameSc", name: "sc_display_name", type: "text", disabled: false }
];

export const EditTermNamingSchema = object({
  system_display_name: string().min(1, { message: "System Display Name is required" }),
  en_display_name: string().min(1, { message: "EN Display Name is required" }),
  tc_display_name: string().min(1, { message: "TC Display Name is required" }),
  sc_display_name: string().min(1, { message: "SC Display Name is required" })
});
