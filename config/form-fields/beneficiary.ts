import { array, object, string } from "zod";

export const EditBeneficiaryBankFields = [
  { name: "entity", type: "text", disabled: true },
  { name: "gateway_name", type: "text", disabled: true },
  { name: "bank_acc_number", type: "text", disabled: false },
  { name: "bank_name", type: "text", disabled: false },
  { name: "bank_account_holder", type: "text", disabled: false },
  { name: "bank_code", type: "text", disabled: false },
  { name: "swift", type: "text", disabled: false },
  { name: "bank_address", type: "text", disabled: false }
];

export const EditBeneficiaryFPSFields = [
  { name: "entity", type: "text", disabled: true },
  { name: "gateway_name", type: "text", disabled: true },
  { name: "merchant_name", type: "text", disabled: false },
  { name: "fps_id", type: "text", disabled: false },
  { name: "fps_qr_code", type: "image", disabled: false }
];


