import { object, string } from "zod";

export const AdjustmentTypeFields = [
  { name: "type", type: "text", disabled: false },
  { name: "adjustment_type", type: "text", disabled: false },
  { name: "status", type: "select", disabled: false }
];

export const AdjustmentTypeSchema = object({
  type: string().min(1, { message: "Type is required" }),
  adjustment_type: string().min(1, { message: "System Name is required" }),
  status: string().min(1, { message: "Status is required" })
});
