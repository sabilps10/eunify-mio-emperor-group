export const EXCHANGE_RATES_HEADER = [
  {
    id: "entityId",
    numeric: false,
    disablePadding: false,
    label: "Entity",
    isSort: true
  },
  {
    id: "exchangeTypeName",
    numeric: false,
    disablePadding: false,
    label: "Exchange Type",
    isSort: true
  },
  {
    id: "quoteCurrency",
    numeric: false,
    disablePadding: false,
    label: "Quote Currency",
    isSort: true
  },
  {
    id: "baseCurrency",
    numeric: false,
    disablePadding: false,
    label: "Base Currency",
    isSort: true
  },
  {
    id: "platformDepositRate",
    numeric: false,
    disablePadding: false,
    label: "Platform Deposit",

    isSort: true
  },
  {
    id: "platformWithdrawRate",
    numeric: false,
    disablePadding: false,
    label: "Platform Withdrawal",

    isSort: true
  },
  {
    id: "depositRate",
    numeric: false,
    disablePadding: false,
    label: "Deposit Rate",
    isSort: true
  },
  {
    id: "withdrawalRate",
    numeric: false,
    disablePadding: false,
    label: "Withdrawal Rate",
    isSort: true
  },
  {
    id: "lastModifiedDate",
    numeric: false,
    disablePadding: false,
    label: "Last Update",
    isSort: true
  },
  {
    id: "action",
    numeric: false,
    disablePadding: false,
    label: "Action",
    key_value: "entityId",
    idValue: "exchangeId"
  }
];
