export const DEPOSIT_CHANNEL_HEADER = [
  {
    id: "entityId",
    numeric: false,
    disablePadding: false,
    label: "Entity",
    isSort: true
  },
  {
    id: "gatewayNameEn",
    numeric: false,
    disablePadding: false,
    label: "Gateway",
    isSort: true
  },
  {
    id: "serviceFee",
    numeric: true,
    disablePadding: false,
    label: "Service Fee",
    isSort: true
  },
  {
    id: "status",
    numeric: false,
    disablePadding: false,
    label: "Status",
    isSort: true
  },
  {
    id: "enquiryInterval",
    numeric: true,
    disablePadding: false,
    label: "Enquiry Interval",
    isSort: true
  },
  {
    id: "stopEnquiryInterval",
    numeric: true,
    disablePadding: false,
    label: "Stop Enquiry Interval",
    isSort: true
  },
  {
    id: "action",
    numeric: false,
    disablePadding: false,
    label: "Action",
    key_value: "gatewayNameEn",
    idValue: "id"
  }
];

export const DEPOSIT_CHANNEL_ACTION = [{ name: "export", action: { type: "export_csv", value: "" } }];

export const DEPOSIT_CHANNEL_EXPORT_HEADING = [
  { name: "entityId", label: "Entity", width: 20, position: 1 },
  { name: "gatewayNameEn", label: "Gateway Name", width: 20, position: 2 },
  {
    name: "isOnlineGateway",
    type: "custom-value",
    width: 20,
    custom_values: [
      { value: true, label: "Online" },
      { value: false, label: "Offline" }
    ],
    label: "Online/Offline",
    position: 3
  },
  { name: "displayNameEn", label: "English Display Name", width: 20, position: 4 },
  { name: "displayNameTc", label: "Trad Chin Display Name", width: 30, position: 5 },
  { name: "displayNameSc", label: "Simp Chin Display Name", width: 30, position: 6 },
  {
    name: "currencies",
    label: "Gateway Currency",
    type: "custom-columns-0",
    key: "currency",
    width: 20,
    position: 7
  },
  {
    name: "currencies",
    label: "Network",
    type: "custom-columns-0",
    key: "cryptoNetwork",
    width: 20,
    position: 8
  },
  { name: "maxAmount", label: "Max Deposit Amount", width: 20, position: 9 },
  { name: "minAmount", label: "Min Deposit Amount", width: 20, position: 10 },
  { name: "gatewayCharge", label: "Gateway Charge", width: 20, position: 11 },
  {
    name: "serviceFee",
    type: "custom-columns-0",
    key: "serviceFeeUnit",
    label: "Service Fee Rate",
    width: 20,
    position: 12
  },
  { name: "estSla", label: "Ext SLA (min)", width: 20, position: 13 },
  { name: "frozenPeriod", label: "Frozen Period (day)", width: 20, position: 14 },
  { name: "enquiryInterval", label: "Enquiry Interval (s)", width: 20, position: 15 },
  { name: "stopEnquiryInterval", label: "Stop Enquiry Interval (s)", width: 20, position: 16 },
  {
    name: "charges",
    type: "custom-columns",
    columns: [
      { name: "ruleKey", label: "Application Group :count", width: 30, position: 17 },
      { name: "value", label: "Group :count Amount", width: 30, position: 18 },
      { name: "unit", label: "Group :count Type", width: 30, position: 19 }
    ]
  }
];
