export const KEY_DISTRICT_HEADER = [
  {
    id: "provinceNameEn",
    numeric: false,
    disablePadding: false,
    label: "Province",
    isSort: true
  },
  {
    id: "nameEn",
    numeric: false,
    disablePadding: false,
    label: "Eng Display Name",
    isSort: true
  },
  {
    id: "nameTc",
    numeric: false,
    disablePadding: false,
    label: "Trad Chi Display Name",
    isSort: true
  },
  {
    id: "nameSc",
    numeric: false,
    disablePadding: false,
    label: "Simpli Chi Display Name",
    isSort: true
  },
  {
    id: "action",
    numeric: false,
    disablePadding: false,
    label: "Action",
    key_value: "nameEn",
    idValue: "addressId"
  }
];

export const KEY_DISTRICT_ROWS = [
  "province",
  "eng_display_name",
  "trad_chi_display_name",
  "simp_chi_display_name"
];

export const KEY_DISTRICT_ACTION = [{ name: "add", action: { type: "link", value: "/add" } }];
