export const AUDIT_LOG_ROWS = ["login", "role_name", "ip", "time", "function", "content"];
export const AUDIT_LOG_HEADER = [
  {
    id: "userName",
    numeric: true,
    disablePadding: false,
    label: "User Name",
    isSort: true
  },
  {
    id: "roleName",
    numeric: false,
    disablePadding: false,
    label: "Role Name",
    isSort: false
  },
  {
    id: "ipAddr",
    numeric: false,
    disablePadding: false,
    label: "IP",
    isSort: true
  },
  {
    id: "createDate",
    numeric: true,
    disablePadding: false,
    label: "Time",
    isSort: true
  },
  {
    id: "funcName",
    numeric: true,
    disablePadding: false,
    label: "Function",
    isSort: false
  },
  {
    id: "content",
    numeric: false,
    disablePadding: false,
    label: "Content",
    isSort: false
  },
  {
    id: "detail",
    numeric: false,
    disablePadding: false,
    label: "Action",
    isSort: false
  }
];
export const AUDIT_LOG_ACTION = [{ name: "export", action: { type: "export_csv", value: "" } }];

export const AUDIT_LOG_EXPORT_HEADING = [
  { name: "userName", label: "User Name", width: 20, position: 1 },
  { name: "roleName", label: "Role Name", width: 20, position: 2 },
  { name: "ipAddr", label: "IP", width: 30, position: 4 },
  { name: "createDate", label: "Time", width: 40, position: 5 },
  { name: "funcName", label: "Function Name", width: 40, position: 6 },
  { name: "content", label: "Content", width: 40, position: 7 }
];
