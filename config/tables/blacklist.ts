export const BLACKLIST_ACTION = [
  { name: "add", action: { type: "link", value: "/add" } },
  { name: "export", action: { type: "export_csv", value: "" } }
];

export const BLACKLIST_HEADER = [
  {
    id: "no",
    numeric: false,
    disablePadding: false,
    label: "No.",
    isSort: true
  },
  {
    id: "entityId",
    numeric: false,
    disablePadding: false,
    label: "Entity",
    isSort: true
  },
  {
    id: "trading_acc_name",
    numeric: false,
    disablePadding: false,
    label: "Trading Acc Name",
    isSort: true
  },
  {
    id: "tradingAccountNo",
    numeric: false,
    disablePadding: false,
    label: "Trading Acc Number",
    isSort: true
  },
  {
    id: "edda",
    numeric: false,
    disablePadding: false,
    label: "eDDA",
    isSort: true
  },
  {
    id: "bank_transfer",
    numeric: false,
    disablePadding: false,
    label: "Bank Transfer",
    isSort: true
  },
  {
    id: "fps",
    numeric: false,
    disablePadding: false,
    label: "FPS",
    isSort: true
  },
  {
    id: "bipipay",
    numeric: false,
    disablePadding: false,
    label: "Bipipay",
    isSort: true
  },
  {
    id: "action",
    numeric: false,
    disablePadding: false,
    label: "Action",
    idValue: "60027109"
  }
];
