export const BANK_MANAGEMENT_ACTION = [
  { name: "add", action: { type: "link", value: "/add" } },
  { name: "export", action: { type: "export_csv", value: "" } }
];

export const BANK_MANAGEMENT_HEADER = [
  {
    id: "no",
    numeric: false,
    disablePadding: false,
    label: "No",
    isSort: false
  },
  {
    id: "entityId",
    numeric: false,
    disablePadding: false,
    label: "Entity",
    isSort: true
  },
  {
    id: "tradingAccountNo",
    numeric: false,
    disablePadding: false,
    label: "Trading Account Number",
    isSort: true
  },
  {
    id: "customerAccountNo",
    numeric: false,
    disablePadding: false,
    label: "Customer Account Number",
    isSort: true
  },
  {
    id: "tradingAccountName",
    numeric: false,
    disablePadding: false,
    label: "Trading Account Name",
    isSort: false
  },
  {
    id: "bankType",
    numeric: false,
    disablePadding: false,
    label: "Type",
    isSort: true
  },
  {
    id: "bankName",
    numeric: false,
    disablePadding: false,
    label: "Bank Name/Credit Card Brand Name",
    isSort: true
  },
  {
    id: "bankAccNo",
    numeric: false,
    disablePadding: false,
    label: "Bank Name/Credit Card Brand Number",
    isSort: true
  },
  {
    id: "lastModifiedDate",
    numeric: false,
    disablePadding: false,
    label: "Last Update Date",
    isSort: true
  },
  {
    id: "statusName",
    numeric: false,
    disablePadding: false,
    label: "Status",
    isSort: true,
    sortId: "status"
  },
  {
    id: "action",
    numeric: false,
    disablePadding: false,
    label: "Action",
    key_value: "bankAccNo",
    isSort: false,
    idValue: "bankAccNo"
  }
];

export const BANK_CARD_EXPORT_HEADING = [{ name: "entityId", label: "Entity", width: 20, position: 1 }];

export const BANK_ACCOUNTS_EXPORT_HEADING = [
  { name: "entityId", label: "Entity", width: 20, position: 1 },
  { name: "tradingAccountNo", label: "Trading Account Number", width: 30, position: 2 },
  { name: "tradingAccountName", label: "Trading Account Name", width: 30, position: 3 },
  { name: "bankType", label: "Type", width: 30, position: 4 },
  {
    name: "bankAccNo",
    label: "Bank Account or Credit Card No.",
    width: 40,
    position: 5
  },
  {
    name: "accHolderName",
    label: "Bank Account or Credit Card Holder Name",
    width: 40,
    position: 6
  },
  { name: "bankName", label: "Bank Name", width: 40, position: 7 },
  { name: "provinceEn", label: "Bank Branch (Province)", width: 20, position: 8 },
  { name: "regionEn", label: "Bank Branch (Region)", width: 20, position: 9 },
  { name: "cityEn", label: "Bank Branch (City)", width: 20, position: 10 },
  { name: "bankAddr", label: "Bank Address", width: 20, position: 11 },
  { name: "swift", label: "SWIFT Code", width: 20, position: 12 },
  { name: "iBANCode", label: "IBAN Code (optional)", width: 20, position: 13 },
  { name: "currency", label: "Currency", width: 20, position: 14 },
  { name: "statusName", label: "Status", width: 20, position: 15 }
];
