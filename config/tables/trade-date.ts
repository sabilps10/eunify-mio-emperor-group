export const TRADE_DATE_ACTION = [{ name: "add", action: { type: "link", value: "/add" } }];
export const TRADE_DATE_HEADER = [
  {
    id: "periodStart",
    numeric: false,
    disablePadding: false,
    label: "Start Date",
    isSort: true
  },
  {
    id: "periodEnd",
    numeric: false,
    disablePadding: false,
    label: "End Date",
    isSort: true
  },
  {
    id: "tradeStartTime",
    numeric: false,
    disablePadding: false,
    label: "Cutoff Time",
    isSort: true
  }
];
