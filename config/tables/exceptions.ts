export const EXCEPTION_MANAGEMENT_ROWS = [
  "type",
  "reference_number",
  "entity",
  "trading_acc_name",
  "trading_acc_num",
  "platformm_currency",
  "balance",
  "application_time",
  "last_update_time",
  "status"
];
export const EXCEPTION_MANAGEMENT_HEADER = [
  {
    id: "tradeTypeName",
    numeric: true,
    disablePadding: false,
    label: "Type",
    isSort: true,
    sortId: "tradeType"
  },
  {
    id: "orderNo",
    numeric: true,
    disablePadding: false,
    label: "Reference Number",
    isSort: true
  },
  {
    id: "entityName",
    numeric: true,
    disablePadding: false,
    label: "Entity",
    isSort: true
  },
  {
    id: "clientName",
    numeric: false,
    disablePadding: false,
    label: "Trading Account Name",
    isSort: true
  },
  {
    id: "tradingAccountNo",
    numeric: false,
    disablePadding: false,
    label: "Trading Account Number",
    isSort: true
  },
  {
    id: "currency",
    name: "platform_currency",
    numeric: false,
    disablePadding: false,
    label: "Platform Currency",
    isSort: true
  },
  {
    id: "balance",
    name: "balance",
    numeric: false,
    disablePadding: false,
    label: "Balance",
    isSort: true
  },
  {
    id: "createdDate",
    numeric: true,
    disablePadding: false,
    label: "Application Time",
    isSort: true
  },
  {
    id: "lastModifiedDate",
    numeric: false,
    disablePadding: false,
    label: "Last Update Time",
    isSort: true
  },
  {
    id: "paymentStatusName",
    numeric: false,
    disablePadding: false,
    label: "Status",
    isSort: true,
    sortId: "paymentStatus"
  },

  {
    id: "action",
    numeric: false,
    disablePadding: false,
    label: "Action"
  }
];

export const EXCEPTION_MANAGEMENT_ACTION = [{ name: "export", action: { type: "export_csv", value: "" } }];

export const EXPCEPTION_MANAGEMENT_EXPORT_HEADER = [
  { name: "tradeTypeName", label: "Type", width: 20, position: 1 },
  { name: "orderNo", label: "Reference ID", width: 20, position: 2 },
  { name: "entityName", label: "Entity", width: 20, position: 3 },
  { name: "clientName", label: "Trading Acc Name", width: 20, position: 4 },
  { name: "tradingAccountNo", label: "Trading Acc No", width: 20, position: 5 },
  { name: "currency", label: "Platform Currency", width: 20, position: 6 },
  { name: "balance", label: "Balance", width: 20, position: 7 },
  { name: "createdDate", label: "Application Time", width: 20, position: 8 },
  { name: "lastModifiedDate", label: "Last update time", width: 20, position: 9 },
  { name: "paymentStatusName", label: "Status", width: 20, position: 10 }
];
