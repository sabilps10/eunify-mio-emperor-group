export const DUMMY_ACCOUNT_HEADER = [
  {
    id: "no",
    numeric: true,
    disablePadding: false,
    label: "No",
    isSort: true
  },
  {
    id: "entityId",
    numeric: true,
    disablePadding: false,
    label: "Entity",
    isSort: true
  },
  {
    id: "tradingAccountNo",
    numeric: true,
    disablePadding: false,
    label: "Acc",
    isSort: true
  },
  {
    id: "action",
    numeric: true,
    disablePadding: false,
    label: "Action",
    key_value: "id",
    idValue: "id"
  }
];

export const DUMMY_ACCOUNT_ACTION = [{ name: "add", action: { type: "link", value: "/add" } }];
