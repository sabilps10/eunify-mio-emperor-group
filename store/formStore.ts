import createStore from "zustand";

type FormState = {
  data: any;
};

type FormAction = {
  setFormData: (value: any) => void;
};

export type FormStoreTypes = FormState & FormAction;

const formStore = createStore<FormStoreTypes>((set: any) => ({
  data: null,
  setFormData: async (value: any) => {
    set(() => ({
      data: value
    }));
  }
}));

export default formStore;
