import create from "zustand";
import { persist } from "zustand/middleware";

type DrawerState = {
  activeMenu: any;
  isDrawerOpen: boolean;
  functionCode: string;
};

type DrawerActions = {
  setActiveMenu: (menu: any, code: string) => void;
  setDrawer: (isDrawerOpen: boolean) => void;
};

export type DrawerStore = DrawerState & DrawerActions;

const useDrawerStore = create(
  persist<DrawerStore>(
    (set) => ({
      activeMenu: {},
      isDrawerOpen: true,
      functionCode: "",
      setActiveMenu(item: any, code: string) {
        set((state) => ({
          functionCode: code,
          activeMenu: {
            ...state.activeMenu,
            [item]: !state.activeMenu[item]
          }
        }));
      },
      setDrawer(isDrawerOpen: boolean) {
        set(() => ({
          isDrawerOpen
        }));
      }
    }),
    { name: "drawer_data" }
  )
);

export default useDrawerStore;
