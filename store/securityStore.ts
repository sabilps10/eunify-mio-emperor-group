import create from "zustand";
import { persist } from "zustand/middleware";

type SecurityState = {
  client_functions: any;
  role_code: string;
};

type SecurityAction = {
  setClientFunction: (client_functions: any, role_code: string) => void;
};
export type SecurityStoreTypes = SecurityState & SecurityAction;

const securityStore = create(
  persist<SecurityStoreTypes>(
    (set) => ({
      client_functions: {},
      role_code: "",
      setClientFunction: async (client_functions: any, role_code: string) => {
        set(() => ({
          client_functions: client_functions,
          role_code: role_code
        }));
      }
    }),
    { name: "role_access" }
  )
);

export default securityStore;
