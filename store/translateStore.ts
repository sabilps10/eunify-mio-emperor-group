import createStore from "zustand";

type TranslateState = {
  language: string;
};

type TranslateActions = {
  setLanguage: (language: string) => void;
};

export enum Language {
  EN = "en-US",
  ZH = "zh-HK"
}

export type TranslateStore = TranslateState & TranslateActions;

const createTranslateStore = createStore<TranslateStore>((set) => ({
  language: Language.ZH,

  setLanguage(language: string) {
    set(() => ({
      language
    }));
  }
}));

export default createTranslateStore;
