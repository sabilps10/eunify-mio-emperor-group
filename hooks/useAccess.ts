import { useEffect, useState } from "react";
import securityStore, { SecurityStoreTypes } from "@/store/securityStore";
import shallow from "zustand/shallow";
import convertNameToUrl from "@/utils/convertToPathUrl";

const useAccess = (menu: any, permission: "writeAccess" | "readAccess" | "approveAccess") => {
  const { client_functions } = securityStore((state) => state, shallow) as SecurityStoreTypes;
  const [access, setAccess] = useState<any[]>([]);

  useEffect(() => {
    const entities = Object.keys(client_functions);
    const list: any[] = [];
    entities.map((el: any) => {
      const entityMenu = client_functions[el]?.find((x: any) => convertNameToUrl(x.funcNameEn) == menu);
      let isAccess = entityMenu ? entityMenu[permission] : false;
      if (isAccess) {
        list.push(el);
      }
    });

    setAccess(list);
  }, [menu]);

  return {
    access
  };
};

export default useAccess;
