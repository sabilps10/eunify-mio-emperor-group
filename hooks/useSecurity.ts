import shallow from "zustand/shallow";

import securityStore, { SecurityStoreTypes } from "@/store/securityStore";

const useSecurity = () => {
  const SecurityStore = securityStore((state) => state, shallow) as SecurityStoreTypes;

  const { setClientFunction, client_functions, role_code } = SecurityStore;

  return {
    client_functions,
    role_code,
    setClientFunction
  };
};

export default useSecurity;
