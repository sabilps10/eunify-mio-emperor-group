import { LOCALSTORAGE_KEY } from "@/config/constants";
import { postAuditLog } from "@/services/common-services";
import securityStore, { SecurityStoreTypes } from "@/store/securityStore";
import { useMutation } from "react-query";
import shallow from "zustand/shallow";
import useLocalStorage from "./useLocalStorage";

const useAuditLog = () => {
  const { getItem } = useLocalStorage();
  const account = getItem(LOCALSTORAGE_KEY.ACCOUNT);
  const { client_functions, role_code } = securityStore((state) => state, shallow) as SecurityStoreTypes;

  const { mutate: mutateAuditLog, isLoading: isLoadingAuditLog } = useMutation((payload) =>
    postAuditLog(payload)
  );

  const handleLogFunction = async (
    code: string,
    link: string,
    username?: string,
    roleCode?: string,
    callback?: any
  ) => {
    await fetch("https://geolocation-db.com/json/")
      .then((data) => data.json())
      .then((data) => {
        const payload: any = {
          userName: account || username,
          ipAddr: data.IPv4,
          roleCode: role_code || roleCode,
          funcCode: code,
          content: "",
          relativePath: link,
          isError: false
        };
        mutateAuditLog(payload, {
          onSuccess: () => {
            if (callback) callback();
          }
        });
      });
  };

  return {
    mutateAuditLog,
    isLoadingAuditLog,
    handleLogFunction
  };
};

export default useAuditLog;
