import { FC } from "react";

import { Stack } from "@mui/material";
import { useRouter } from "next/router";
import useTranslate from "@/hooks/useTranslate";
import BankAccountLimit from "./BankAccountLimit";
import ReconciliationReport from "./ReconciliationReport";
import TabsTable from "./TableWithTab";
import DepositChannelListing from "./modules/deposit-channel/Listing";
import DepositChannelForm from "./modules/deposit-channel/Form";
import AuditLogListing from "./modules/audit-log/Listing";
import DepositRecordListing from "./modules/deposit/Listing";
import WithdrawalRecordListing from "./modules/withdrawal/Listing";
import TransactionListing from "./modules/transaction/Listing";
import WithdrawalChannelListing from "./modules/withdrawal-channel/Listing";
import BeneficiaryAccountListing from "./modules/beneficiary-account/Listing";
import ExchangeRatesListing from "./modules/exchange-rates/Listing";
import BlacklistListing from "./modules/blacklist/Listing";
import CcbaRecordListing from "./modules/ccba-records/Listing";
import PaymentListing from "./modules/payment/Listing";
import BankManagementListing from "./modules/bank-management/Listing";
import EddaRecordListing from "./modules/edda-record/Listing";
import ExceptionListing from "./modules/exceptions/Listing";
import UserManagementListing from "./modules/user-management/Listing";
import RoleSettingsListing from "./modules/role-settings/Listing";
import ListKeyListing from "./modules/list-keys/Listing";
import ProvinceListing from "./modules/term-naming-province/Listing";
import DistrictListing from "./modules/term-naming-district/Listing";
import CityListing from "./modules/term-naming-city/Listing";
import ManualAdjustmentTypeListing from "./modules/manual-adjustment-types/Listing";
import LocalBankEddaListing from "./modules/local-bank-edda/Listing";
import DummyAccountListing from "./modules/dummy-account/Listing";
import DepositForm from "./modules/deposit/Form";
import WithdrawalForm from "./modules/withdrawal/Form";
import TransactionForm from "./modules/transaction/Form";
import WithdrawalChannelForm from "./modules/withdrawal-channel/Form";
import BlacklistForm from "./modules/blacklist/Form";
import ExchangeRateForm from "./modules/exchange-rates/Form";
import BeneficiaryAccountForm from "./modules/beneficiary-account/Form";
import CcbaForm from "./modules/ccba-records/Form";
import PaymentForm from "./modules/payment/Form";
import BankManagementForm from "./modules/bank-management/Form";
import UserManagementForm from "./modules/user-management/Form";
import RoleSettingForm from "./modules/role-settings/Form";
import KeyTermForm from "./modules/list-keys/Form";
import ProvinceForm from "./modules/term-naming-province/Form";
import DistrictForm from "./modules/term-naming-district/Form";
import CityForm from "./modules/term-naming-city/Form";
import AdjustmentTypeForm from "./modules/manual-adjustment-types/Form";
import LocalBankForm from "./modules/local-bank-edda/Form";
import DummyAccountForm from "./modules/dummy-account/Form";
import NotificationManagementListing from "./modules/notification/Listing";
import NotificationManagementForm from "./modules/notification/Form";
import TradeDateListing from "./modules/trade-date/Listing";
import TradeDateForm from "./modules/trade-date/Form";
import NotificationManagementPreview from "./modules/notification/Preview";

const RenderContent: FC = () => {
  const {
    query: { page, feature }
  } = useRouter();
  const { translate } = useTranslate();
  if (feature == "deposit-management") {
    if (page == "management") {
      return <DepositRecordListing />;
    }
    if (page == "add" || "detail") {
      return <DepositForm />;
    }
  }
  if (feature == "withdrawal-management") {
    if (page == "management") {
      return <WithdrawalRecordListing />;
    }
    if (page == "add" || "detail") {
      return <WithdrawalForm />;
    }
  }
  if (feature == "transaction-records") {
    if (page == "management") {
      return <TransactionListing />;
    }
    if (page == "detail") {
      return <TransactionForm />;
    }
  }
  if (feature == "deposit-channel") {
    if (page == "management") {
      return <DepositChannelListing />;
    }
    if (page == "detail") {
      return <DepositChannelForm />;
    }
  }
  if (feature == "withdrawal-channel") {
    if (page == "management") {
      return <WithdrawalChannelListing />;
    }
    if (page == "detail") {
      return <WithdrawalChannelForm />;
    }
  }
  if (feature == "beneficiary-accounts") {
    if (page == "management") {
      return <BeneficiaryAccountListing />;
    }
    if (page == "detail") {
      return <BeneficiaryAccountForm />;
    }
  }
  if (feature == "exchange-rate") {
    if (page == "management") {
      return <ExchangeRatesListing />;
    }
    if (page == "detail") {
      return <ExchangeRateForm />;
    }
  }
  if (feature == "blacklist") {
    if (page == "management") {
      return <BlacklistListing />;
    }
    if (page == "detail" || page === "add") {
      return <BlacklistForm />;
    }
  }
  if (feature == "ccba-bank-records") {
    if (page == "management") {
      return <CcbaRecordListing />;
    }
    if (page == "detail") {
      return <CcbaForm />;
    }
  }
  if (feature == "payment-management") {
    if (page == "management") {
      return <PaymentListing />;
    }
    if (page == "detail") {
      return <PaymentForm />;
    }
  }
  if (feature == "exceptions-management") {
    if (page == "management") {
      return <ExceptionListing />;
    }
  }
  if (feature == "bank-&-credit-card-record") {
    if (page == "management") {
      return <BankManagementListing />;
    }
    if (page == "detail" || page === "add") {
      return <BankManagementForm />;
    }
  }
  if (feature == "edda-record") {
    if (page == "management") {
      return <EddaRecordListing />;
    }
  }
  if (feature == "reports") {
    if (page == "management") {
      return <ReconciliationReport />;
    }
  }
  if (feature == "user-management") {
    if (page == "management") {
      return <UserManagementListing />;
    }
    if (page == "detail" || page === "add") {
      return <UserManagementForm />;
    }
  }
  if (feature == "role-settings") {
    if (page == "management") {
      return <RoleSettingsListing />;
    }
    if (page == "add" || page == "detail") {
      return <RoleSettingForm />;
    }
  }
  if (feature == "list-of-key-term-naming") {
    if (page == "management") {
      return <ListKeyListing />;
    }
    if (page == "detail") {
      return <KeyTermForm />;
    }
  }
  if (feature == "prc-province") {
    if (page == "management") {
      return <ProvinceListing />;
    }
    if (page == "add" || page == "detail") {
      return <ProvinceForm />;
    }
  }
  if (feature == "prc-region-district") {
    if (page == "management") {
      return <DistrictListing />;
    }
    if (page == "add" || page == "detail") {
      return <DistrictForm />;
    }
  }
  if (feature == "prc-city") {
    if (page == "management") {
      return <CityListing />;
    }
    if (page == "add" || page == "detail") {
      return <CityForm />;
    }
  }
  if (feature == "manual-adjustments-types") {
    if (page == "management") {
      return <ManualAdjustmentTypeListing />;
    }
    if (page == "add" || page == "detail") {
      return <AdjustmentTypeForm />;
    }
  }

  if (feature == "local-bank-list-edda-mapping") {
    if (page == "management") {
      return <LocalBankEddaListing />;
    }
    if (page == "add" || page == "detail") {
      return <LocalBankForm />;
    }
  }
  if (feature == "dummy-account-list") {
    if (page == "management") {
      return <DummyAccountListing />;
    }
    if (page == "add" || page == "detail") {
      return <DummyAccountForm />;
    }
  }
  if (feature == "bank-&-cc-registration-limit") {
    if (page == "management") {
      return <BankAccountLimit />;
    }
  }

  if (feature == "audit-log") {
    if (page == "management") {
      return <AuditLogListing />;
    }
  }

  if (feature === "operator-notifications") {
    if (page === "management") {
      return <NotificationManagementListing />;
    }

    if (page === "add" || page === "detail") {
      return <NotificationManagementForm />;
    }
    if (page === "notification-preview") {
      return <NotificationManagementPreview />;
    }
  }

  if (feature === "trade-date") {
    if (page === "management") {
      return <TradeDateListing />;
    }

    if (page === "add" || page === "detail") {
      return <TradeDateForm />;
    }
  }

  return <></>;
};

const DashboardContent: FC = () => {
  return (
    <Stack sx={{ mt: 3 }}>
      <RenderContent />
    </Stack>
  );
};

export default DashboardContent;
