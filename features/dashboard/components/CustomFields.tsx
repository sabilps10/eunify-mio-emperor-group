import { FC } from "react";

import { Stack } from "@mui/material";
import AvailableOperationTime from "./AvailableOperationTime";
import useTranslate from "@/hooks/useTranslate";
import ApplicableGroup from "./ApplicableGroup";
import RoleAssignment from "./RoleAssignment";
import RolePermissions from "./RolePermissions";
import FPSQrCode from "./FPSQrCode";

type CustomFieldsProps = {
  name: string;
  translate: any;
  methods: any;
  value?: any;
};
const CustomFields = ({ name, translate, methods, value }: CustomFieldsProps) => {
  if (name == "timeslots") return <AvailableOperationTime methods={methods} translate={translate} />;
  if (name == "charges") return <ApplicableGroup methods={methods} translate={translate} value={value} />;
  if (name === "entity_role") return <RoleAssignment methods={methods} translate={translate} name={name} />;
  if (name === "role_permissions") return <RolePermissions methods={methods} name={name} value={value} />;
  if (name === "fps_qr_code") return <FPSQrCode value={value} />;
  return <></>;
};

export default CustomFields;
