import { FC, useState, useEffect } from "react";

import {
  Box,
  Paper,
  Table,
  TableCell,
  TableContainer,
  TableHead,
  TableBody,
  TableRow,
  Typography,
  FormLabel,
  Stack,
  TextField,
  MenuItem,
  Button,
  IconButton
} from "@mui/material";
import { LocalizationProvider } from "@mui/x-date-pickers";
import { AdapterDateFns } from "@mui/x-date-pickers/AdapterDateFns";
import CustomNumberFormat from "@/components/FormFields/NumberFormat";
import RemoveCircleOutlineIcon from "@mui/icons-material/RemoveCircleOutline";
import { APPLICABLE_GROUP_HEADER } from "@/config/tables/additional-tables";
import { useQuery } from "react-query";
import { get } from "http";
import { getPaymentGroup } from "@/services/common-services";
import { getPaymentChannelUnit } from "@/services/common-services";
import { PaymentUnit } from "@/config/utilities";

const RenderTableHead = ({ translate }: { translate: any }) => {
  return (
    <TableRow>
      {APPLICABLE_GROUP_HEADER.map((value: any, key: number) => {
        return (
          <TableCell key={key}>
            <Typography variant={"body"} fontWeight={700}>
              {translate(value.label)}
            </Typography>
          </TableCell>
        );
      })}
    </TableRow>
  );
};
const RenderTableBody = ({
  translate,
  paymentGroupOptions,
  paymentUnitOptions,
  fieldData,
  onChangeValue,
  unitValue,
  methods
}: {
  translate: any;
  paymentGroupOptions: any;
  paymentUnitOptions: any;
  fieldData: any;
  unitValue: any;
  onChangeValue: (name: string, value: any) => void;
  methods: any;
}) => {
  return (
    <LocalizationProvider dateAdapter={AdapterDateFns}>
      {fieldData.map((value: any, key: number) => {
        return (
          <TableRow key={key}>
            <TableCell>
              <TextField
                select
                value={value.ruleKey}
                sx={{ width: 180 }}
                onChange={(evt: any) => onChangeValue("ruleKey", { id: value.id, value: evt.target.value })}
              >
                {paymentGroupOptions.map((value: any, key: number) => {
                  return (
                    <MenuItem key={key} value={value.value}>
                      <Typography variant="body">{value.label}</Typography>
                    </MenuItem>
                  );
                })}
              </TextField>
            </TableCell>
            <TableCell>
              <TextField
                defaultValue={value.value}
                InputProps={{ inputComponent: CustomNumberFormat as any }}
                onChange={(evt: any) => onChangeValue("value", { id: value.id, value: evt.target.value })}
              />
            </TableCell>
            <TableCell width={120}>
              <Stack alignItems="center" direction="row">
                <TextField
                  select
                  value={unitValue}
                  disabled
                  sx={{ width: 120 }}
                  onChange={(evt: any) => onChangeValue("unit", { id: value.id, value: evt.target.value })}
                >
                  {paymentUnitOptions.map((value: any, key: number) => {
                    return (
                      <MenuItem key={key} value={value.value}>
                        <Typography variant="body">
                          {PaymentUnit(value.value, methods.getValues("gateway_currency"))}
                        </Typography>
                      </MenuItem>
                    );
                  })}
                </TextField>
                <IconButton sx={{ ml: 1 }} onClick={() => onChangeValue("remove", value.id)}>
                  <RemoveCircleOutlineIcon sx={(theme) => ({ color: theme.palette.brandRed[500] })} />
                </IconButton>
              </Stack>
            </TableCell>
          </TableRow>
        );
      })}
      <TableRow>
        <TableCell colSpan={3}>
          <Button sx={{ width: "100%" }} onClick={() => onChangeValue("add", null)}>
            <Typography variant="body" fontWeight={700}>
              {translate("dashboard.buttons.add_more")}
            </Typography>
          </Button>
        </TableCell>
      </TableRow>
    </LocalizationProvider>
  );
};

const ApplicableGroup = ({ translate, methods, value }: { translate: any; methods: any; value: any }) => {
  const [fieldData, setFieldData] = useState<any[]>([]);
  const [paymentGroupOptions, setPaymentGroupOptions] = useState<any[]>([]);
  const [paymentUnitOptions, setPaymentUnitOptions] = useState<any[]>([]);
  const entityId = methods.getValues("entity");

  const { data: paymentGroupData } = useQuery(["get-payment-group", entityId], async () => {
    const { response } = await getPaymentGroup(entityId);
    return response;
  });

  const { data: paymentChannelUnit } = useQuery(["get-payment-channel-unit"], getPaymentChannelUnit);

  const {
    setValue,
    getValues,
    formState: { errors },
    watch
  } = methods;

  const watchServiceChargeUnit = watch("service_fee_unit");

  useEffect(() => {
    if (value) {
      setFieldData(value);
    } else {
      setValue("charges", []);
    }
  }, [value]);

  useEffect(() => {
    setValue("charges", fieldData, { shouldValidate: true });
  }, [fieldData]);

  useEffect(() => {
    if (paymentGroupData) {
      let items: { label: string; value: any }[] = [];
      paymentGroupData?.forEach(function (val: any) {
        let item = { label: "", value: "" };
        item.label = val.paymentGroupName;
        item.value = val.paymentGroupId;
        items.push(item);
      });
      setPaymentGroupOptions(items);
    }
  }, [paymentGroupData]);

  useEffect(() => {
    if (paymentChannelUnit) {
      let items: { label: string; value: any }[] = [];
      paymentChannelUnit?.response[0].items.forEach(function (val: any) {
        let item = { label: "", value: "" };
        item.label = val.name;
        item.value = val.codeCode;
        items.push(item);
      });
      setPaymentUnitOptions(items);
    }
  }, [paymentChannelUnit]);

  const onChangeValue = (name: any, value: any) => {
    let copy = [...fieldData];
    if (name == "remove") {
      copy = copy.filter((el: any) => el.id !== value);

      setFieldData(copy);
    } else if (name == "add") {
      const newData = [
        ...copy,
        {
          id: Math.random().toString(),
          chargeType: "1",
          ruleType: "PG",
          ruleKey: "",
          value: "",
          unit: "",
          isExcluded: false,
          effectiveDateFrom: "2020-01-01 00:00:00",
          effectiveDateTo: "9997-12-31 00:00:00",
          isActive: true
        }
      ];
      setFieldData(newData);
    } else {
      const findIndex = copy.findIndex((el: any) => el.id == value.id);
      copy[findIndex] = {
        ...copy[findIndex],
        [name]: value.value
      };
      setFieldData(copy);
    }
  };

  return (
    <Box sx={{ width: "100%" }}>
      <Stack>
        <FormLabel color="secondary" error={!!errors["charges"]}>
          {translate("dashboard.forms.labels.applicable_group")}
        </FormLabel>
        {errors["charges"] && (
          <Typography variant="body" color="brandRed.500">
            {errors["charges"].message}
          </Typography>
        )}
      </Stack>
      <Paper sx={{ width: "60%", mb: 2, mt: 2 }}>
        <TableContainer>
          <Table>
            <TableHead>
              <RenderTableHead translate={translate} />
            </TableHead>
            <TableBody>
              <RenderTableBody
                methods={methods}
                fieldData={fieldData}
                translate={translate}
                onChangeValue={onChangeValue}
                paymentGroupOptions={paymentGroupOptions}
                paymentUnitOptions={paymentUnitOptions}
                unitValue={watchServiceChargeUnit}
              />
            </TableBody>
          </Table>
        </TableContainer>
      </Paper>
    </Box>
  );
};

export default ApplicableGroup;
