import { FC, useEffect } from "react";
import { FormProvider, useForm, SubmitHandler } from "react-hook-form";
import { useQuery, useMutation } from "react-query";
import { zodResolver } from "@hookform/resolvers/zod";
import { array, object, string, date, number, preprocess } from "zod";

import { Stack, Button, Typography, Paper } from "@mui/material";

import { getRegistrationLimit, editRegistrationLimit } from "@/services/common-services";
import useTranslate from "@/hooks/useTranslate";
import InputText from "@/components/FormFields/InputText";
import securityStore, { SecurityStoreTypes } from "@/store/securityStore";
import shallow from "zustand/shallow";
import convertNameToUrl from "@/utils/convertToPathUrl";
import toast from "react-hot-toast";
import { useRouter } from "next/router";
import useLocalStorage from "@/hooks/useLocalStorage";
import { LOCALSTORAGE_KEY } from "@/config/constants";

interface initialState {
  bank_registration_limit: string | number;
  cc_registration_limit: string | number;
}
const initial: initialState = {
  bank_registration_limit: "",
  cc_registration_limit: ""
};

const schema = object({
  bank_registration_limit: preprocess((a) => parseInt(a === "" ? "0" : (a as string)), number().min(1)),
  cc_registration_limit: preprocess((a) => parseInt(a === "" ? "0" : (a as string)), number().min(1))
});

const BankAccountLimit: FC = () => {
  const {
    query: { feature }
  } = useRouter();
  const { getItem } = useLocalStorage();
  const account = getItem(LOCALSTORAGE_KEY.ACCOUNT);
  const { client_functions } = securityStore((state) => state, shallow) as SecurityStoreTypes;
  const { translate } = useTranslate();
  const { data, refetch } = useQuery(["get-registration-limit"], getRegistrationLimit);
  const { mutate } = useMutation((payload: any) => editRegistrationLimit(payload));
  const methods = useForm<any>({
    defaultValues: initial,
    resolver: zodResolver(schema)
  });
  const {
    handleSubmit,
    setValue,
    formState: { errors }
  } = methods;

  const isError = Object.keys(errors).length > 0;

  useEffect(() => {
    methods.setValue("bank_registration_limit", data?.response?.noOfBank);
    methods.setValue("cc_registration_limit", data?.response?.noOfCreditCard);
  }, [data]);

  const onSubmit: SubmitHandler<initialState> = (data: any) => {
    const entities = Object.keys(client_functions);
    let writeAccess = 0;

    entities.map((el: any) => {
      const access = client_functions[el].find(
        (x: any) => convertNameToUrl(x.funcNameEn) == feature
      )?.writeAccess;
      if (access) {
        writeAccess++;
      }
    });

    if (writeAccess > 0) {
      mutate(
        {
          noOfBank: +data.bank_registration_limit,
          noOfCreditCard: +data.cc_registration_limit,
          requestBy: account
        },
        {
          onSuccess: () => {
            refetch();
          }
        }
      );
    } else {
      toast.error(`Dont have write access for this function`, {
        duration: 5000,
        position: "top-right",
        style: {
          background: "#ffe3e3",
          color: "#000000"
        }
      });
    }
  };

  return (
    <Stack direction="column" gap={2}>
      <Paper sx={{ p: 3 }}>
        <FormProvider {...methods}>
          <Stack direction="row" gap={2}>
            <InputText
              label={"No of Bank Registration"}
              name="bank_registration_limit"
              set={setValue}
              type="text-number"
              fullWidth
              required
            />
            <InputText
              label={"No of Credit Card Registration"}
              name="cc_registration_limit"
              set={setValue}
              type="text-number"
              fullWidth
              required
            />
          </Stack>
          <Button type="submit" onClick={handleSubmit(onSubmit)} disabled={isError}>
            <Typography variant="body" fontWeight={700}>
              {translate("dashboard.buttons.submit")}
            </Typography>
          </Button>
        </FormProvider>
      </Paper>
    </Stack>
  );
};

export default BankAccountLimit;
