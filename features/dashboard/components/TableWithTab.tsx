import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { Tabs, Tab, Paper, Box, Typography, Alert, Stack, MenuItem, TextField } from "@mui/material";

import TableList from "@/components/TableList";
import useMounted from "@/hooks/useMounted";
import { useMutation, useQuery } from "react-query";
import {
  getTransactions,
  getTransactionsRecords,
  postExportTransaction,
  postPaymentCancel,
  postPaymentConfirm,
  postPaymentFailed,
  postUnclaimBalance
} from "@/services/mio-services";
import moment from "moment";
import ModalDialog from "@/components/ModalDialog";
import useExportFile from "@/hooks/useExportFile";
import useDialog from "@/hooks/useDialog";
import useTranslate from "@/hooks/useTranslate";
import { getBeneficiaryAccountRecord } from "@/services/common-services";
import { ShowDataValueWithEmpty } from "@/config/utilities";
import usePagination from "@/hooks/usePagination";
import toast from "react-hot-toast";
import RejectModal from "./RejectModal";
import useLocalStorage from "@/hooks/useLocalStorage";
import { LOCALSTORAGE_KEY, AML_CODE } from "@/config/constants";
import useAccess from "@/hooks/useAccess";

const TabPannel = ({ value, index, children }: { value: any; index: number; children: any }) => {
  return (
    <div role="tabpanel" hidden={value != index}>
      {value == index && <>{children}</>}
    </div>
  );
};

const defaultFilterData = {
  orderNo: null,
  paymentChannelId: null,
  tradingAccountNo: null,
  applicationTimeFrom: null,
  applicationTimeTo: null,
  tranDateFrom: null,
  tranDateTo: null,
  includeDummyAccount: false
};

type FilterData = {
  orderNo: number | null;
  paymentChannelId: string | number | null;
  tradingAccountNo: any[] | null;
  applicationTimeFrom: string | number | null;
  applicationTimeTo: string | number | number[] | null;
  tranDateFrom: string | null;
  tranDateTo: string | null;
  status: any;
  includeDummyAccount: string | boolean | null;
  tradingAccountNoIsNull: string | boolean | null;
  tradeTypeList: string[] | null;
  entityIdList: string[];
} | null;

const TableWithTab = ({ tableAttr, useCheckbox }: { tableAttr: any; useCheckbox: boolean }) => {
  const router = useRouter();
  const [selectedRow, setSelectedRow] = useState<any[]>([]);
  const { access } = useAccess(router.query.feature!, "readAccess");

  const [filterPagination, setFilterPagination, pagination, setPagination] = usePagination<FilterData>({
    orderNo: null,
    paymentChannelId: null,
    tradingAccountNo: null,
    applicationTimeFrom: null,
    applicationTimeTo: null,
    status: [11, 15],
    tranDateFrom: null,
    tranDateTo: null,
    includeDummyAccount: false,
    tradingAccountNoIsNull: false,
    tradeTypeList: ["MO", "MOA"],
    entityIdList: access
  });

  const errorToastAttr: any = {
    duration: 5000,
    position: "top-right",
    style: {
      background: "#ffe3e3",
      color: "#000000"
    }
  };

  const [tabActive, setTabActive] = useState({
    reject_deposit: false,
    unclaimed_deposit: false,
    approved_withdrawal: false
  });
  const [errorMessage, setErrorMessage] = useState<string | null>(null);
  const [dataRecord, setDataRecord] = useState<any>([]);
  const [dataExport, setDataExport] = useState<any>([]);
  const [selectedBankAccount, setSelectedBankAccount] = useState<any>(null);

  const { isOpen: isOpenDialog, onClose: onCloseDialog, onOpen: onOpenDialog, dialogData } = useDialog();
  const [rejectData, setRejectData] = useState<any>(null);
  const { translate } = useTranslate();
  const { onGenerateFile } = useExportFile();

  const { getItem } = useLocalStorage();
  const account = getItem(LOCALSTORAGE_KEY.ACCOUNT);

  const { data: beneficiaryAccountRecord } = useQuery(
    ["get-beneficiary-record"],
    async () => {
      const { response } = await getBeneficiaryAccountRecord();
      return response;
    },
    {
      enabled: !!dialogData
    }
  );
  const { isFetching: fetchingPage, refetch: reFetchingPage } = useQuery(
    ["get-transaction-record", filterPagination],
    async () => {
      const { response } = await getTransactionsRecords(filterPagination);
      const { content, ...rest } = response;
      setDataRecord(content);
      setPagination(rest);
      return response;
    },
    {
      enabled: !!filterPagination.filter.status
    }
  );

  const { isFetching: fetchingList, refetch: refetchList } = useQuery(
    ["get-transaction-record-list", filterPagination.filter],
    async () => {
      const { response } = await getTransactions(filterPagination.filter);
      setDataExport(response);
      return response;
    },
    {
      enabled: !!filterPagination.filter.status
    }
  );

  const { mutate: requestPaymentCancel } = useMutation(({ payload }: { payload: any }) =>
    postPaymentCancel(payload)
  );
  const { mutate: requestPaymentConfirm } = useMutation(({ payload }: { payload: any }) =>
    postPaymentConfirm(payload)
  );
  const { mutate: requestPaymentFailed } = useMutation(({ payload }: { payload: any }) =>
    postPaymentFailed(payload)
  );
  const { mutate: requestExportTransaction } = useMutation(({ payload }: { payload: any }) =>
    postExportTransaction(payload)
  );
  const { mutate: requestUnclaimBalance } = useMutation(({ payload }: { payload: any }) =>
    postUnclaimBalance(payload)
  );

  const {
    push,
    query: { page, feature, tab }
  } = useRouter();
  const { isMounted } = useMounted();

  useEffect(() => {
    setSelectedRow([]);
    const key = Object.keys(tabActive).find((el) => el == tab) || "";
    if (key) {
      setTabActive({ ...tabActive, [key]: true });
      onChangeTab(key);
    } else {
      router.push({ pathname: "/payment-management/management", query: { tab: "approved_withdrawal" } });
    }
  }, [tab]);

  useEffect(() => {
    setFilterPagination({
      ...filterPagination,
      filter: { ...filterPagination.filter, entityIdList: access }
    });
  }, [access]);

  const onChangeTab = (newValue: any) => {
    if (newValue == "reject_deposit") {
      setFilterPagination({
        ...filterPagination,
        filter: {
          ...filterPagination.filter,
          status: [12, 18],
          tradingAccountNoIsNull: false,
          tradeTypeList: ["MI"]
        }
      });
    }
    if (newValue == "unclaimed_deposit") {
      setFilterPagination({
        ...filterPagination,
        filter: {
          ...filterPagination.filter,
          status: [12, 18, 21],
          tradingAccountNoIsNull: true,
          tradeTypeList: ["MI"]
        }
      });
    }
    if (newValue == "approved_withdrawal") {
      setFilterPagination({
        ...filterPagination,
        filter: {
          ...filterPagination.filter,
          status: [11, 15],
          tradingAccountNoIsNull: null,
          tradeTypeList: ["MO", "MOA"]
        }
      });
    }
    push({ pathname: `/${feature}/${page}`, query: { tab: newValue } });
  };

  const handleSelectRow = (val: boolean, row: any) => {
    if (val) {
      setSelectedRow([...selectedRow, row]);
    } else {
      setSelectedRow(selectedRow.filter((el) => el.id !== row.id));
    }
  };

  const handleCallbackAction = (name: string, values: any) => {
    if (values.length) {
      const requests: any[] = [];
      values.forEach(function (val: any) {
        requests.push({ requestId: val.id, tradingAccountNo: val.tradingAccountNo, requestBy: account });
      });
      if (name == "payment-confirm") {
        requestPaymentConfirm(
          { payload: { paymentManagementRequest: requests } },
          {
            onSuccess: (res) => {
              res?.response?.forEach((value: any) => {
                if (value.error) {
                  toast.error(value.message, errorToastAttr);
                }
              });
              setSelectedRow([]);
              reFetchingPage();
              refetchList();
            }
          }
        );
      }
      if (name == "payment-failed") {
        requestPaymentFailed(
          { payload: { paymentManagementRequest: requests } },
          {
            onSuccess: (res) => {
              res?.response?.forEach((value: any) => {
                if (value.error) {
                  toast.error(value.message, errorToastAttr);
                }
              });
              setSelectedRow([]);
              reFetchingPage();
              refetchList();
            }
          }
        );
      }
      if (name == "payment-cancel") {
        setRejectData(values);
      }
      if (name == "unclaim-balance") {
        requestUnclaimBalance(
          { payload: { paymentManagementRequest: requests } },
          {
            onSuccess: (res) => {
              res?.response?.forEach((value: any) => {
                if (value.error) {
                  toast.error(value.message, errorToastAttr);
                }
              });
              setSelectedRow([]);
              reFetchingPage();
              refetchList();
            }
          }
        );
      }
      if (name == "export-transaction") {
        onOpenDialog(values);
      }
    }
  };

  const handleModalCallback = () => {
    if (dialogData) {
      const requests: any[] = [];
      dialogData.forEach(function (val: any) {
        requests.push({ requestId: val.id, tradingAccountNo: val.tradingAccountNo, requestBy: account });
      });
      requestExportTransaction(
        { payload: { paymentManagementRequest: requests } },
        {
          onSuccess: (res) => {
            const exportedArray: number[] = [];
            res?.response?.forEach((value: any) => {
              if (value.error) {
                toast.error(value.message, errorToastAttr);
              } else {
                exportedArray.push(value.requestId);
              }
            });
            const rows: any[] = [];
            const reportFPSHeader = [
              { col: "Withdrawal Corp Name", width: 25, position: 1 },
              { col: "Withdrawal Account", width: 25, position: 2 },
              { col: "Withdrawal Currency", width: 25, position: 3 },
              { col: "Corporate No.", width: 25, position: 4 },
              { col: "Deposit Account Name", width: 25, position: 5 },
              { col: "Beneficiary Bank Code", width: 25, position: 6 },
              { col: "Deposit Account", width: 25, position: 7 },
              { col: "Amount", width: 25, position: 8 },
              { col: "Payment Reference", width: 30, position: 9 },
              { col: "Remarks", width: 25, position: 10 }
            ];
            dialogData.forEach(function (data: any) {
              if (exportedArray.includes(data.id)) {
                const isDeposit = data["tradeType"].toLowerCase().includes("mi");
                const isWithdraw = data["tradeType"].toLowerCase().includes("mo");

                let amount;
                let currency;

                if (isDeposit) {
                  currency = data["currency"];
                  amount = data["actAmount"] ? data["actAmount"] : data["tmpAmount"];
                }

                if (isWithdraw) {
                  currency = data["settleCurrency"];
                  amount = data["actDepositAmount"] ? data["actDepositAmount"] : data["tmpDepositAmount"];
                }

                rows.push({
                  1: ShowDataValueWithEmpty(selectedBankAccount.accHolderName),
                  2: ShowDataValueWithEmpty(selectedBankAccount.bankAccNo),
                  3: ShowDataValueWithEmpty(currency),
                  4: ShowDataValueWithEmpty(null),
                  5: ShowDataValueWithEmpty(data.accHolderName),
                  6: ShowDataValueWithEmpty(data.bankCode),
                  7: ShowDataValueWithEmpty(data.bankAccountNo),
                  8: ShowDataValueWithEmpty(amount),
                  9: ShowDataValueWithEmpty(data.tradingAccountNo),
                  10: ShowDataValueWithEmpty(data.comment)
                });
              }
            });
            if (exportedArray.length) {
              onGenerateFile(
                rows,
                reportFPSHeader,
                "report_" + selectedBankAccount?.bankName + "_" + new Date(),
                ".xlsx"
              );
            }

            setSelectedRow([]);
            reFetchingPage();
            refetchList();
            onCloseDialog();
          }
        }
      );
    }
  };

  const handleCancelPayment = (values: any) => {
    const data = rejectData.map((el: any) => ({
      requestId: el.id,
      tradingAccountNo: el.tradingAccountNo,
      comment: values.data.comment,
      requestBy: account
    }));
    requestPaymentCancel(
      { payload: { paymentManagementRequest: data } },
      {
        onSuccess: (res) => {
          res?.response?.forEach((value: any) => {
            if (value.error) {
              toast.error(value.message, errorToastAttr);
            }
          });
          setSelectedRow([]);
          setRejectData(null);
          reFetchingPage();
        }
      }
    );
  };

  const handleDoFilterData = (values: any) => {
    let filter: any = filterPagination.filter;
    Object.keys(values).forEach(function (key) {
      if (key == "applicationTime_start" || key == "applicationTime_end") {
        const splitString = key.split("_");
        if (splitString[1] == "start") {
          filter["applicationTimeFrom"] = values.applicationTime_start
            ? moment(values.applicationTime_start).format("Y-MM-DD HH:mm:ss")
            : null;
        }
        if (splitString[1] == "end") {
          filter["applicationTimeTo"] = values.applicationTime_end
            ? moment(values.applicationTime_end).format("Y-MM-DD HH:mm:ss")
            : null;
        }
      } else if (key == "status") {
        filter["status"] = values[key] ? [values[key]] : filterPagination?.filter["status"];
      } else if (key == "tradingAccountNo") {
        filter["tradingAccountNo"] = values[key] ? [values[key]] : null;
      } else {
        filter[key] = values[key] ? values[key] : null;
      }
    });

    setFilterPagination({ ...filterPagination, filter: { ...filterPagination.filter, ...filter } });
  };

  if (!isMounted || !tab) return <></>;
  return (
    <>
      <Box sx={{ mb: 2 }}>
        {errorMessage && (
          <Alert severity="error" sx={{ mb: 2 }}>
            <Typography variant="body">{errorMessage}</Typography>
          </Alert>
        )}

        <Paper>
          <Tabs
            value={tab}
            TabIndicatorProps={{
              style: {
                backgroundColor: "hsla(45, 99%, 69%, 1)"
              }
            }}
          >
            {tableAttr.map(({ title, id }: any, index: number) => (
              <Tab
                value={id}
                key={index}
                onClick={() => onChangeTab(id)}
                label={
                  <Typography variant="body" sx={{ fontWeight: 600 }} color={"#111"}>
                    {title}
                  </Typography>
                }
                sx={{ backgroundColor: tab == id ? `hsla(45, 99%, 69%, 0.3)` : null }}
              />
            ))}
          </Tabs>
        </Paper>
      </Box>
      {tableAttr.map((item: any, key: number) => {
        return (
          <TabPannel key={key} value={tab} index={item.id}>
            <TableList
              header={item.header}
              filter={item.filter}
              action={item.action}
              exportDataObject={{ heading: item.export, data: dataExport, loading: fetchingList }}
              callbackAction={handleCallbackAction}
              data={dataRecord}
              isLoading={fetchingPage}
              useCheckbox={useCheckbox}
              onSelectRow={handleSelectRow}
              selectedData={selectedRow}
              onFilterData={(values: any) => handleDoFilterData(values)}
              isPagination={true}
              isAccess={true}
              pagination={pagination}
              handlePagination={(val) => {
                setFilterPagination({ ...filterPagination, ...val });
              }}
            />
          </TabPannel>
        );
      })}
      {dialogData && (
        <ModalDialog
          data={dialogData}
          isOpen={isOpenDialog}
          onClose={onCloseDialog}
          onCallback={handleModalCallback}
          title={translate(`dashboard.dialog.export_fps`)}
          buttons={{
            continue: translate("dashboard.buttons.confirm"),
            cancel: translate("dashboard.buttons.cancel")
          }}
        >
          <Stack sx={{ width: 600 }}>
            <TextField
              select
              required={true}
              fullWidth={true}
              onChange={(event: any) => setSelectedBankAccount(event.target.value)}
            >
              {beneficiaryAccountRecord?.map((value: any, key: number) => {
                return (
                  <MenuItem key={key} value={value}>
                    {value.bankName}
                  </MenuItem>
                );
              })}
            </TextField>
          </Stack>
        </ModalDialog>
      )}
      {rejectData && (
        <RejectModal
          data={rejectData}
          isOpen={!!rejectData}
          onClose={() => setRejectData(null)}
          onSubmit={handleCancelPayment}
        />
      )}
    </>
  );
};

export default TableWithTab;
