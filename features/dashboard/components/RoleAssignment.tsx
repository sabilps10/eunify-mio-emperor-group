import { useEffect, useState } from "react";
import { useQuery } from "react-query";

import {
  Box,
  MenuItem,
  Paper,
  Select,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Typography
} from "@mui/material";
import { merge, keyBy, values } from "lodash";
import InputSelect from "@/components/FormFields/InputSelect";

import { getEntityId } from "@/services/common-services";
import { getRoleAssignments } from "@/services/security-services";

type roleResponse = {
  roleId: string;
  roleNameEn: string;
};

const RoleAssignment = ({ translate, methods, name }: { translate: any; methods: any; name: string }) => {
  const {
    setValue,
    getValues,
    formState: { errors }
  } = methods;
  const { data } = useQuery(["get-entity"], getEntityId);

  const { data: dataRole, isLoading } = useQuery(["get-role-assignment"], () =>
    getRoleAssignments({ roleId: null })
  );

  const [defaultLists, setDefaultLists] = useState([
    { entityId: "EBL", roleId: "" },
    { entityId: "EIEHK", roleId: "" },
    { entityId: "XPro", roleId: "" }
  ]);

  const [listRole, setListRole] = useState([]);

  useEffect(() => {
    if (dataRole?.response?.length > 0) {
      setListRole(dataRole?.response);
    }
  }, [dataRole?.response]);

  useEffect(() => {
    if (errors?.entity_role) {
      getValues()?.entity_role.forEach(({ roleId }: any) => {
        if (roleId === null || roleId === "") {
          methods.setError("entity_role", {
            type: "custom",
            message: "All entity's role must be filled, at least choose NO_ACCESS"
          });
          return;
        }
      });
    }
  }, [errors, methods]);

  useEffect(() => {
    if (data?.response[0].items?.length > 0) {
      const list = data?.response[0].items;
      let newLists = [...defaultLists];
      const mergingData = values(merge(keyBy(list, "codeCode"), keyBy(newLists, "entityId")));
      newLists = [...defaultLists, list];
      if (!getValues(name))
        setValue(
          name,
          mergingData.map(({ entityId, roleId }) => {
            return { entityId, roleId };
          })
        );
      else {
        const list = getValues(name);
        let newLists = [...mergingData];
        const mergingData2 = values(merge(keyBy(newLists, "codeCode"), keyBy(list, "entityId")));
        setDefaultLists(mergingData2);

        setValue(
          name,
          mergingData2.map(({ entityId, roleId }) => {
            return { entityId, roleId };
          })
        );
      }
    }
  }, [data]);

  // useEffect(() => {
  //   if (!getValues(name)) {
  //     setValue(name, defaultLists);
  //   } else {
  //     const list = getValues(name);
  //     let newLists = [...defaultLists];
  //     const mergingData = values(merge(keyBy(list, "entityId"), keyBy(newLists, "entityId")));
  //     newLists = [...defaultLists, list];
  //     setDefaultLists(mergingData);
  //   }
  // }, []);

  const handleChange = (entity: string, value: any) => {
    let index = 0;
    let lists = defaultLists;
    index = defaultLists.findIndex((data: any) => data.entityId === entity);
    lists[index].roleId = value;

    setValue(
      name,
      [...lists].map(({ entityId, roleId }) => {
        return { entityId, roleId };
      })
    );
  };

  return (
    <Paper sx={{ width: "50%" }}>
      <Box p="12px">
        {errors["entity_role"] && (
          <Typography variant="body" color="brandRed.500">
            {errors["entity_role"].message}
          </Typography>
        )}
      </Box>
      <TableContainer>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>
                <Typography variant={"body"} fontWeight={700}>
                  Entity
                </Typography>
              </TableCell>
              <TableCell>
                <Typography variant={"body"} fontWeight={700}>
                  Role
                </Typography>
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {defaultLists.length > 0 &&
              defaultLists.map((el: any) => {
                return (
                  <TableRow key={el.codeInt}>
                    <TableCell>{el.entityId}</TableCell>
                    <TableCell>
                      <Select
                        defaultValue={getValues(name)
                          ?.find((e: any) => e.entityId === el.entityId)
                          ?.roleId?.toString()}
                        onChange={(e) => handleChange(el.entityId, e.target.value)}
                        fullWidth
                      >
                        <MenuItem disabled value="placeholder">
                          Please Select
                        </MenuItem>
                        {listRole.length > 0
                          ? listRole?.map(({ roleNameEn, roleId }: roleResponse) => {
                              return (
                                <MenuItem key={roleId} value={roleId.toString()}>
                                  {roleNameEn}
                                </MenuItem>
                              );
                            })
                          : null}
                      </Select>
                    </TableCell>
                  </TableRow>
                );
              })}
          </TableBody>
        </Table>
      </TableContainer>
    </Paper>
  );
};

export default RoleAssignment;
