import { FC } from "react";
import { useRouter } from "next/router";
import { Stack, Button, Typography, Paper, TextField } from "@mui/material";
import { FormProvider, useForm, SubmitHandler } from "react-hook-form";
import useTranslate from "@/hooks/useTranslate";
import DateRange from "@/components/FormFields/DateRange";
import InputSelect from "@/components/FormFields/InputSelect";
import { DateTimePicker } from "@mui/x-date-pickers/DateTimePicker";
import { DesktopDatePicker, LocalizationProvider } from "@mui/x-date-pickers";
import { AdapterDateFns } from "@mui/x-date-pickers/AdapterDateFns";
import { useMutation, useQuery } from "react-query";
import { getReportData, getReportList } from "@/services/mio-services";
import { GenerateSelectOptions, ShowDataValueWithEmpty } from "@/config/utilities";
import moment from "moment";
import useExportFile from "@/hooks/useExportFile";
import convertNameToUrl from "@/utils/convertToPathUrl";
import securityStore, { SecurityStoreTypes } from "@/store/securityStore";
import shallow from "zustand/shallow";

interface initialState {
  reportCode: string;
  dateFrom: string;
  dateTo: string;
}
const initial: initialState = {
  reportCode: "",
  dateFrom: "",
  dateTo: ""
};

const reportHeader = [
  { col: "Entity", width: 20 },
  { col: "Trading Account Name", width: 20 },
  { col: "Trading Account No.", width: 20 },
  { col: "Reference No.", width: 20 },
  { col: "Deposit Channel", width: 20 },
  { col: "Actual Currency", width: 20 },
  { col: "Actual Amount", width: 20 },
  { col: "Account Settlement Currency", width: 20 },
  { col: "Platform Amount", width: 20 },
  { col: "Exchange Rate", width: 20 },
  { col: "IB Code", width: 20 },
  { col: "Trade Date", width: 20 },
  { col: "Payment Time", width: 20 },
  { col: "Frozen Period (day)", width: 20 }
];

const ReconsiliationReport: FC = () => {
  const {
    query: { feature }
  } = useRouter();
  const { client_functions } = securityStore((state) => state, shallow) as SecurityStoreTypes;

  const { translate } = useTranslate();
  const methods = useForm<any>({ defaultValues: initial });
  const { data: reportList } = useQuery(["get-report-list"], getReportList);
  const { mutate: generateReport } = useMutation(({ payload }: { payload: any }) => getReportData(payload));
  const { handleSubmit } = methods;
  const { onGenerateFile } = useExportFile();

  const checkAccess = (row: any, permission: "writeAccess" | "readAccess" | "approveAccess") => {
    if (client_functions) {
      if (row.entityName) {
        return client_functions[row.entityName]?.find(
          (menu: any) => convertNameToUrl(menu.funcNameEn) == feature
        )[permission];
      } else {
        return true;
      }
    }
    return true;
  };

  const onSubmit: SubmitHandler<initialState> = (data: any) => {
    const payload = {
      reportCode: data.reportCode,
      dateFrom: moment(data.dateFrom).format("Y-MM-DD HH:mm:ss"),
      dateTo: moment(data.dateTo).format("Y-MM-DD HH:mm:ss")
    };
    if (data.reportCode && data.dateFrom && data.dateTo) {
      generateReport(
        { payload },
        {
          onSuccess: (res) => {
            console.log(res);
            const rows: any[] = [];
            res.response.forEach(function (value: any) {
              if (checkAccess(value, "readAccess")) {
                rows.push({
                  1: ShowDataValueWithEmpty(value.entityName),
                  2: ShowDataValueWithEmpty(value.clientName),
                  3: ShowDataValueWithEmpty(value.tradingAccountNo),
                  4: ShowDataValueWithEmpty(value.orderNo),
                  5: ShowDataValueWithEmpty(value.paymentChannelName),
                  6: ShowDataValueWithEmpty(value.currency),
                  7: ShowDataValueWithEmpty(value.actAmount),
                  8: ShowDataValueWithEmpty(value.settlementAccCurrency),
                  9: ShowDataValueWithEmpty(value.platformAmount),
                  10: ShowDataValueWithEmpty(value.exchangeRate),
                  11: ShowDataValueWithEmpty(value.IBAccountNo),
                  12: ShowDataValueWithEmpty(moment(value.tradeDate).format("YYYY-MM-DD")),
                  13: ShowDataValueWithEmpty(value.settleDate),
                  14: ShowDataValueWithEmpty(value.frozenToDate)
                });
              }
            });
            onGenerateFile(rows, reportHeader, "report_" + data.reportCode + "_" + new Date(), ".xlsx");
          }
        }
      );
    }
  };

  return (
    <Stack direction="column" gap={2}>
      <Paper sx={{ p: 3 }}>
        <FormProvider {...methods}>
          <Stack
            direction="row"
            gap={2}
            sx={{
              ".MuiFormControl-root": {
                width: "200px",
                height: "43px"
              }
            }}
          >
            <InputSelect
              required
              set={methods.setValue}
              defaultValue={methods.getValues("reportCode") || ""}
              options={GenerateSelectOptions(reportList?.response, "reportCode", ["reportName"])}
              name="reportCode"
              label="Report Name"
              fullWidth={true}
              size="small"
            />
            <LocalizationProvider dateAdapter={AdapterDateFns}>
              <Stack direction="column" gap={1}>
                <Typography variant="h4">{translate(`dashboard.forms.labels.period`)}</Typography>
                <Stack direction="row" gap={2}>
                  <DateTimePicker
                    inputFormat="yyyy-MM-dd HH:mm:ss"
                    views={["year", "day", "hours", "minutes", "seconds"]}
                    value={methods.watch(`dateFrom`) || null}
                    onAccept={(value) => {
                      methods.setValue("dateFrom", value);
                    }}
                    onChange={() => null}
                    renderInput={(params) => (
                      <TextField
                        size="small"
                        {...params}
                        inputProps={{
                          ...params.inputProps,
                          placeholder: "Select Date Time"
                        }}
                      />
                    )}
                  />
                  <Stack alignItems="center" justifyContent="center">
                    {"-"}
                  </Stack>
                  <DateTimePicker
                    inputFormat="yyyy-MM-dd HH:mm:ss"
                    views={["year", "day", "hours", "minutes", "seconds"]}
                    value={methods.watch(`dateTo`) || null}
                    onAccept={(value) => {
                      methods.setValue("dateTo", value);
                    }}
                    onChange={() => null}
                    renderInput={(params) => (
                      <TextField
                        size="small"
                        {...params}
                        inputProps={{
                          ...params.inputProps,
                          placeholder: "Select Date Time"
                        }}
                      />
                    )}
                  />
                </Stack>
              </Stack>
            </LocalizationProvider>
          </Stack>
          <Button
            type="submit"
            onClick={handleSubmit(onSubmit)}
            sx={{
              mt: "24px"
            }}
          >
            <Typography variant="body" fontWeight={700}>
              {translate("dashboard.buttons.submit")}
            </Typography>
          </Button>
        </FormProvider>
      </Paper>
    </Stack>
  );
};

export default ReconsiliationReport;
