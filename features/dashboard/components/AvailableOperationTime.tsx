import { FC, useEffect, useState } from "react";

import {
  Box,
  Paper,
  Table,
  TableCell,
  TableContainer,
  TableHead,
  TableBody,
  TableRow,
  Typography,
  FormLabel,
  Checkbox,
  TextField,
  Stack
} from "@mui/material";
import { TimePicker } from "@mui/x-date-pickers/TimePicker";
import { LocalizationProvider } from "@mui/x-date-pickers";
import { AdapterDateFns } from "@mui/x-date-pickers/AdapterDateFns";
import { AVAILABLE_OPERATION_TIME_DAY } from "@/config/constants";
import { useFormContext } from "react-hook-form";
import { AVAILABLE_OPERATION_TIME_HEADER } from "@/config/tables/additional-tables";
import { watch } from "fs/promises";
import { valuesIn } from "lodash";

type AvailableOperationTimeProps = {
  translate: any;
  methods: any;
};

const RenderTableBody = ({
  onChangeValue,
  getValues,
  translate,
  watch
}: {
  onChangeValue: any;
  getValues: any;
  translate: any;
  watch: any;
}) => {
  const [fieldData, setFieldData] = useState<any[]>([]);
  const columnTimeField = ["gatewayTimeStart", "gatewayTimeEnd", "breakTimeStart", "breakTimeEnd"];

  useEffect(() => {
    const subscription = watch((value: any, { name }: { name: string }) => {
      if (name === "timeslots") {
        setFieldData(value["timeslots"]);
      }
    });
    return () => subscription.unsubscribe();
  }, [watch]);

  return (
    <LocalizationProvider dateAdapter={AdapterDateFns}>
      {AVAILABLE_OPERATION_TIME_DAY.map((day: any, key: number) => {
        let indexOfObject = -1;

        if (fieldData) {
          fieldData.forEach((object: any, key: number) => {
            if (object.dayCode === day.value) {
              indexOfObject = key;
            }
          });
        }

        let gatewayTimeStart = new Date();
        let gatewayTimeEnd = new Date();
        let breakTimeStart = new Date();
        let breakTimeEnd = new Date();

        if (indexOfObject > -1) {
          columnTimeField.forEach(function (value: any) {
            let [hours, minutes, seconds] = fieldData[indexOfObject][value].split(":");
            eval(value).setHours(+hours);
            eval(value).setMinutes(minutes);
            eval(value).setSeconds(seconds);
          });
        } else {
          columnTimeField.forEach(function (value: any) {
            eval(value).setHours(0);
            eval(value).setMinutes(0);
            eval(value).setSeconds(0);
          });
        }

        return (
          <TableRow key={key}>
            <TableCell>
              <RenderCheckbox value={indexOfObject > -1} onChangeValue={onChangeValue} dayValue={day} />
            </TableCell>
            <TableCell>
              <Typography>{translate(`dashboard.labels.days.${day.name}`)}</Typography>
            </TableCell>
            <TableCell>
              <TimePicker
                views={["hours", "minutes", "seconds"]}
                inputFormat="HH:mm:ss"
                ampm={false}
                disabled={indexOfObject > -1 ? false : true}
                value={gatewayTimeStart}
                onChange={(ts) => {
                  onChangeValue(day, "gatewayTimeStart", ts);
                }}
                renderInput={(params) => <TextField {...params} />}
              />
            </TableCell>
            <TableCell>
              <TimePicker
                views={["hours", "minutes", "seconds"]}
                inputFormat="HH:mm:ss"
                ampm={false}
                disabled={indexOfObject > -1 ? false : true}
                value={gatewayTimeEnd}
                onChange={(ts) => onChangeValue(day, "gatewayTimeEnd", ts)}
                renderInput={(params) => <TextField {...params} />}
              />
            </TableCell>
            <TableCell>
              <TimePicker
                views={["hours", "minutes", "seconds"]}
                inputFormat="HH:mm:ss"
                ampm={false}
                disabled={indexOfObject > -1 ? false : true}
                value={breakTimeStart}
                onChange={(ts) => onChangeValue(day, "breakTimeStart", ts)}
                renderInput={(params) => <TextField {...params} />}
              />
            </TableCell>
            <TableCell>
              <TimePicker
                views={["hours", "minutes", "seconds"]}
                inputFormat="HH:mm:ss"
                ampm={false}
                disabled={indexOfObject > -1 ? false : true}
                value={breakTimeEnd}
                onChange={(ts) => onChangeValue(day, "breakTimeEnd", ts)}
                renderInput={(params) => <TextField {...params} />}
              />
            </TableCell>
          </TableRow>
        );
      })}
    </LocalizationProvider>
  );
};

const RenderCheckbox = ({
  value,
  onChangeValue,
  dayValue
}: {
  value: boolean;
  onChangeValue: any;
  dayValue: any;
}) => {
  const [checked, setIsChecked] = useState(false);
  useEffect(() => {
    setIsChecked(value);
  }, [value]);
  return (
    <Checkbox
      checked={checked}
      onChange={(ev) => {
        onChangeValue(dayValue, "selected", ev.target.checked);
      }}
      color="primary"
    />
  );
};

const RenderTableHead = ({ translate }: { translate: any }) => {
  return (
    <TableRow>
      {AVAILABLE_OPERATION_TIME_HEADER.map((value: any, key: number) => {
        return (
          <TableCell key={key}>
            <Typography variant={"body"} fontWeight={700}>
              {translate(value.label)}
            </Typography>
          </TableCell>
        );
      })}
    </TableRow>
  );
};

const AvailableOperationTime: FC<AvailableOperationTimeProps> = ({ translate, methods }) => {
  const {
    setValue,
    getValues,
    formState: { errors },
    watch
  } = methods;

  useEffect(() => {
    if (!getValues("timeslots")) {
      setValue("timeslots", []);
    }
  }, []);

  const onChangeValue = (day: any, name: any, value: any) => {
    const existingValue = getValues("timeslots");
    const objectToInsert = existingValue ? [...existingValue] : [];

    let indexOfObject = -1;
    objectToInsert.forEach((object: any, key: number) => {
      if (object.dayCode === day.value) {
        indexOfObject = key;
      }
    });

    if (name == "selected") {
      if (value == false) {
        if (indexOfObject > -1) {
          objectToInsert.splice(indexOfObject, 1);
        }
      }
      if (value == true) {
        const item: any = {
          dayCode: day.value,
          gatewayTimeStart: "00:00:00",
          gatewayTimeEnd: "00:00:00",
          breakTimeStart: "00:00:00",
          breakTimeEnd: "00:00:00"
        };
        objectToInsert.push(item);
      }
    } else {
      let date = new Date(value);
      const dateString = date.toTimeString();
      objectToInsert[indexOfObject][name] = dateString.split(" ")[0];
    }
    setValue("timeslots", objectToInsert);
  };

  return (
    <Box sx={{ width: "100%" }}>
      <Stack>
        <FormLabel color="secondary" error={!!errors["timeslots"]}>
          {translate("dashboard.forms.labels.timeslots")}
        </FormLabel>
        {errors["timeslots"] && (
          <Typography variant="body" color="brandRed.500">
            At least one or more Available Operation Time required
          </Typography>
        )}
      </Stack>

      <Paper sx={{ width: "100%", mb: 2, mt: 2 }}>
        <TableContainer>
          <Table>
            <TableHead>
              <RenderTableHead translate={translate} />
            </TableHead>
            <TableBody>
              <RenderTableBody
                watch={watch}
                onChangeValue={onChangeValue}
                translate={translate}
                getValues={getValues}
              />
            </TableBody>
          </Table>
        </TableContainer>
      </Paper>
    </Box>
  );
};

export default AvailableOperationTime;
