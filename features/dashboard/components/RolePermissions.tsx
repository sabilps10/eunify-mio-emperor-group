import { useEffect, useState, useCallback } from "react";

import {
  Paper,
  Tab,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Checkbox
} from "@mui/material";
import { useQuery } from "react-query";
import { LOCALSTORAGE_KEY } from "@/config/constants";
import useLocalStorage from "@/hooks/useLocalStorage";
import { getClientFunctions } from "@/services/security-services";

const RolePermissions = ({ methods, name, value = [] }: { methods: any; name: string; value: any }) => {
  const { setValue, getValues } = methods;
  const header = ["Function", "Read", "Edit", "Approve"];
  const { getItem } = useLocalStorage();
  const account = getItem(LOCALSTORAGE_KEY.ACCOUNT);
  const { data, isLoading } = useQuery(["get-client-functions"], () =>
    getClientFunctions({ account: account, appCode: "MIO_ADMIN_PORTAL" })
  );

  const [list, setList] = useState<any>([]);

  const [_, updateState] = useState<any>();
  const forceUpdate = useCallback(() => updateState({}), []);

  const sort = (arr: any = []) => {
    const newArr = arr?.sort((a: any, b: any) => (a.displayOrder > b.displayOrder ? 1 : -1));
    return newArr;
  };

  useEffect(() => {
    if (data) {
      const menus: any[] = [];
      sort(data.response).map((item: any) => {
        if (item.child.length > 0) {
          return sort(item.child).map((el: any) =>
            menus.push({
              approveAccess: false,
              readAccess: false,
              writeAccess: false,
              funcId: el.id,
              funcName: el.funcNameEn
            })
          );
        }
        menus.push({
          approveAccess: false,
          readAccess: false,
          writeAccess: false,
          funcId: item.id,
          funcName: item.funcNameEn
        });
      });

      if (value.length > 0) {
        const newMenus = menus.map((el: any) => {
          const row = value.find((menu: any) => menu.id == el.funcId);
          return {
            ...el,
            approveAccess: row?.approveAccess,
            readAccess: row?.readAccess,
            writeAccess: row?.writeAccess
          };
        });
        setList(newMenus);
        setValue(name, newMenus);
      } else {
        setList(menus);
        setValue(name, menus);
      }
    }
  }, [data]);

  const RenderTableContent = ({ menu }: any) => {
    return (
      <TableRow key={menu.funcId}>
        <TableCell>{menu.funcName}</TableCell>
        <TableCell>
          <Checkbox
            checked={menu.readAccess}
            onChange={(e) => handleChange(e.target.checked, menu.funcId, "readAccess")}
          />
        </TableCell>
        <TableCell>
          <Checkbox
            checked={menu.writeAccess}
            onChange={(e) => handleChange(e.target.checked, menu.funcId, "writeAccess")}
          />
        </TableCell>
        <TableCell>
          <Checkbox
            checked={menu.approveAccess}
            onChange={(e) => handleChange(e.target.checked, menu.funcId, "approveAccess")}
          />
        </TableCell>
      </TableRow>
    );
  };

  const handleChange = (value: any, menu: string, access: string) => {
    const listMenus = getValues(name);
    const index = listMenus.findIndex((item: any) => item.funcId === menu);

    listMenus[index] = {
      ...listMenus[index],
      [access]: value
    };
    setList(listMenus);
    setValue(name, listMenus);
    forceUpdate();
  };

  return (
    <Paper>
      <TableContainer>
        <Table>
          <TableHead>
            <TableRow>
              {header.map((el: string) => (
                <TableCell key={el}>{el}</TableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
            {list.length > 0 &&
              list.map((item: any) => {
                return <RenderTableContent key={item.id} menu={item} />;
              })}
          </TableBody>
        </Table>
      </TableContainer>
    </Paper>
  );
};

export default RolePermissions;
