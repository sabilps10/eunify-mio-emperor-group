import { FC, useEffect, useState } from "react";

import useFormContent from "@/features/dashboard/hooks/useFormContent";
import { useRouter } from "next/router";
import DetailContentForm from "../../DetailContentForm";
import { object, string } from "zod";
import { postAdjustmentType, putCodeValue } from "@/services/common-services";
import { useMutation } from "react-query";
import useFormData from "@/hooks/useFormData";
import useLocalStorage from "@/hooks/useLocalStorage";
import { LOCALSTORAGE_KEY } from "@/config/constants";

const SelectDropdown = [
  {
    value: "MI",
    label: "Deposit"
  },
  {
    value: "MO",
    label: "Withdrawal"
  }
];

const StatusDropdown = [
  {
    value: "true",
    label: "Active"
  },
  {
    value: "false",
    label: "Inactive"
  }
];

export const AdjustmentTypeFieldsAdd = [
  { id: "type", name: "type", type: "select", options: SelectDropdown, disabled: false },
  { id: "nameEn", name: "adjustment_type", type: "text", disabled: false },
  { id: "nameTc", name: "adjustment_type", type: "text", disabled: false, hidden: true },
  { id: "nameSc", name: "adjustment_type", type: "text", disabled: false, hidden: true },
  { id: "active", name: "status", type: "select", options: StatusDropdown, disabled: false }
];

export const AdjustmentTypeFieldsEdit = [
  { id: "cv_id", name: "id", type: "text", disabled: true, hidden: true, payload_skipped: true },
  {
    id: "codeCode",
    name: "codeCode",
    type: "text",
    disabled: true,
    hidden: true
  },
  { id: "type", name: "type", type: "select", options: SelectDropdown, disabled: true },
  { id: "nameEn", name: "adjustment_type", type: "text", disabled: true },
  { id: "nameTc", name: "adjustment_type", type: "text", disabled: true, hidden: true },
  { id: "nameSc", name: "adjustment_type", type: "text", disabled: true, hidden: true },
  { id: "codeInt", name: "codeInt", type: "text", disabled: true, hidden: true },
  { id: "displayOrder", name: "displayOrder", type: "text", disabled: true, hidden: true },
  { id: "active", name: "status", type: "select", options: StatusDropdown, disabled: false }
];

const AdjustmentTypeSchemaAdd = object({
  type: string().min(1, { message: "Type is required" }),
  adjustment_type: string().min(1, { message: "System Name is required" }),
  status: string().min(1, { message: "Status is required" })
});

const AdjustmentTypeSchemaEdit = object({
  type: string().min(1, { message: "Type is required" }),
  adjustment_type: string().min(1, { message: "System Name is required" }),
  status: string().min(1, { message: "Status is required" }),
  id: string(),
  codeCode: string(),
  codeInt: string().min(0),
  displayOrder: string()
});

const AdjustmentTypeForm: FC = () => {
  const {
    query: { feature, page },
    push
  } = useRouter();
  const { getItem } = useLocalStorage();
  const account = getItem(LOCALSTORAGE_KEY.ACCOUNT);
  const { formFields, formValidations, setFormValue } = useFormContent();

  const { data: formData, setFormData } = useFormData();

  const [initialData, setInitialData] = useState(null);

  useEffect(() => {
    if (page === "add") setFormData(null);
    if (!formFields) {
      setFormValue("fields", page === "detail" ? AdjustmentTypeFieldsEdit : AdjustmentTypeFieldsAdd);
      setFormValue("validations", page === "detail" ? AdjustmentTypeSchemaEdit : AdjustmentTypeSchemaAdd);
    }

    if (formData) {
      const current: any = {};
      AdjustmentTypeFieldsEdit?.forEach((data) => {
        let value = "";
        if (data.name === "status") {
          value = formData["active"].toString();
        } else if (data.name === "type") {
          value = formData["codeCode"].includes("MI") ? "MI" : "MO";
        } else {
          value = formData[data.id]?.toString();
        }

        Object.assign(current, { [data.name]: value || "" });
      });
      setInitialData(current);
    }
  }, [page]);

  const { mutate: createAdjustmentType } = useMutation((payload: any) => postAdjustmentType(payload));
  const { mutate: editAdjustmentType } = useMutation(({ id, payload }: any) => putCodeValue(id, payload));

  const handleSubmitForm = (values: any) => {
    const data = {};
    const fields = page === "detail" ? AdjustmentTypeFieldsEdit : AdjustmentTypeFieldsAdd;
    fields.forEach(function (field: any) {
      let value = null;
      if (!field?.payload_skipped) {
        if (!values[field.name] && initialData) {
          value = initialData[field.name];
        } else {
          if (field.name == "status" || field.name === "active") {
            value = values[field.name] === "true";
          } else {
            value = values[field.name]?.toString();
          }
        }
        if (value || typeof value === "boolean") {
          Object.assign(data, { [field.id]: value });
        }
      }
    });

    if (page === "detail") {
      editAdjustmentType(
        { id: values.id, payload: { ...data, requestBy: account } },
        {
          onSuccess: () => {
            setFormData(null);
            push(`/${feature}/management`);
          }
        }
      );
    } else {
      createAdjustmentType(
        { ...data, requestBy: account },
        {
          onSuccess: () => {
            setFormData(null);
            push(`/${feature}/management`);
          }
        }
      );
    }
  };

  return (
    <DetailContentForm
      fields={formFields}
      validations={formValidations}
      initialData={initialData}
      onSubmit={handleSubmitForm}
      onCancel={() => setFormData(null)}
    />
  );
};

export default AdjustmentTypeForm;
