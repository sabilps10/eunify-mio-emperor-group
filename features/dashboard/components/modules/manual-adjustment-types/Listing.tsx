import { FC } from "react";
import TableList from "@/components/TableList";
import { ADJUSTMENT_TYPE_ACTION, ADJUSTMENT_TYPE_HEADER } from "@/config/tables/adjustment-type";
import { getAdjustmentTypeList } from "@/services/common-services";
import { useQuery } from "react-query";

const ManualAdjustmentTypeListing: FC = () => {
  const { data: adjustmentType, isLoading } = useQuery(
    ["get-adjustment-type"],
    getAdjustmentTypeList
  );

  return (
    <TableList
      header={ADJUSTMENT_TYPE_HEADER}
      action={ADJUSTMENT_TYPE_ACTION}
      data={adjustmentType?.response}
      isLoading={isLoading}
    />
  );
};

export default ManualAdjustmentTypeListing;
