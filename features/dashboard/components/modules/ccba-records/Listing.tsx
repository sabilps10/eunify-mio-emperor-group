import { FC, useCallback, useEffect, useState } from "react";
import { useMutation, useQuery } from "react-query";

import { getCcbaBankRecords, postCcbaBankRecords } from "@/services/mio-services";
import { getEntityId, getCcbaPaymentStatus, getBeneficiaryAccountRecord } from "@/services/common-services";
import TableList from "@/components/TableList";
import {
  CCBA_MANAGEMENT_ACTION,
  CCBA_MANAGEMENT_HEADER,
  CCBA_EXPORT_HEADING
} from "@/config/tables/ccba-record";
import moment from "moment";
import usePagination from "@/hooks/usePagination";
import { filter } from "lodash";
import toast from "react-hot-toast";

export const CCBA_MANAGEMENT_FILTER = [
  { name: "entityId", id: "entity", type: "dropdown", options: [] },
  { name: "efsgBank", id: "efsg_bank", type: "dropdown", options: [] },
  { name: "efsgAccNo", id: "efsg_bank_account_num", type: "text" },
  { name: "bankTxnRefNo", id: "bank_transaction_ref_num", type: "text" },
  { name: "refNo", id: "reference_number", type: "textbox" },
  { name: "tranDate", id: "transaction_date", type: "time-range" },
  { name: "customerBankAccNo", id: "bank_account_num", type: "text" },
  { name: "customerBankAccHolderName", id: "bank_account_holder", type: "text" },
  { name: "tradingAccountNo", id: "trading_account_num", type: "text" },
  { name: "tradingAccountName", id: "trading_account_holder", type: "text" },
  { name: "status", id: "status", type: "dropdown", options: [] },
  {
    name: "includeDummyAccount",
    id: "dummy_account",
    type: "dropdown",
    options: [
      { value: "true", label: "Include" },
      { value: "false", label: "Exclude" }
    ]
  }
];

type FilterData = {
  entityId: string | number | null;
  efsgBank: string | number | null;
  efsgAccNo: string | number | null;
  bankTxnRefNo: string | number | null;
  refNo: string | number | null;
  customerBankAccNo: string | number | null;
  customerBankAccHolderName: string | number | null;
  status: string | number | null;
  includeDummyAccount: boolean;
  tradingAccountNo: string | number | null;
  tradingAccountName: string | number | null;
  tranDateFrom: string | number | null;
  tranDateTo: string | number | null;
};

const CcbaRecordListing: FC = () => {
  const [, updateState] = useState<any>();
  const forceUpdate = useCallback(() => updateState({}), []);

  const [filterPagination, setFilterPagination, pagination, setPagination] = usePagination<FilterData>({
    entityId: null,
    efsgBank: null,
    efsgAccNo: null,
    bankTxnRefNo: null,
    refNo: null,
    customerBankAccNo: null,
    customerBankAccHolderName: null,
    status: null,
    includeDummyAccount: false,
    tradingAccountNo: null,
    tradingAccountName: null,
    tranDateFrom: null,
    tranDateTo: null
  });

  const { data: entityData } = useQuery(["get-entity"], getEntityId);
  const { data: bankStatus } = useQuery(["get-payment-status"], getCcbaPaymentStatus);
  const { data: efsgBank } = useQuery(["get-efsg-bank"], getBeneficiaryAccountRecord);

  useEffect(() => {
    if (entityData) {
      let items: { label: string; value: any }[] = [];
      entityData?.response[0].items.forEach(function (val: any) {
        let item = { label: "", value: "" };
        item.label = val.codeCode;
        item.value = val.codeCode;
        items.push(item);
      });
      CCBA_MANAGEMENT_FILTER[0].options = items;
      forceUpdate();
    }
  }, [entityData]);

  useEffect(() => {
    if (bankStatus) {
      const options = bankStatus?.response[0].items.map((el: any) => ({
        label: el.name,
        value: el.codeInt.toString()
      }));
      CCBA_MANAGEMENT_FILTER[10].options = options;
      forceUpdate();
    }
  }, [bankStatus]);

  useEffect(() => {
    if (efsgBank) {
      const options = efsgBank?.response.map((el: any) => ({
        label: el.bankName,
        value: el.bankId.toString()
      }));
      CCBA_MANAGEMENT_FILTER[1].options = options;
    }
  }, [efsgBank]);

  const [dataExport, setDataExport] = useState<any>([]);
  const [dataRecord, setDataRecord] = useState<any>([]);

  const { mutate: ccbaPage, isLoading: loadingPage } = useMutation((payload: any) =>
    postCcbaBankRecords(payload)
  );

  const { mutate: ccbaList, isLoading: loadingList } = useMutation((payload: any) =>
    getCcbaBankRecords(payload)
  );

  useEffect(() => {
    ccbaPage(filterPagination, {
      onSuccess: ({ response }) => {
        if (response) {
          const { content, ...rest } = response;
          setDataRecord(content || []);
          setPagination(rest);
        }
      },
      onError: () => {
        setDataRecord([]);
      }
    });
  }, [filterPagination]);

  useEffect(() => {
    ccbaList(filterPagination.filter, {
      onSuccess: ({ response }) => {
        setDataExport(response);
      }
    });
  }, [filterPagination.filter]);

  const handleFilterData = (values: any) => {
    let filter: any = filterPagination.filter;
    Object.keys(values).forEach(function (key) {
      if (key == "tranDate_start" || key == "tranDate_end") {
        const splitString = key.split("_");
        if (splitString[1] == "start") {
          filter["tranDateFrom"] = values.tranDate_start
            ? moment(values.tranDate_start).format("Y-MM-DD HH:mm:ss")
            : null;
        }
        if (splitString[1] == "end") {
          filter["tranDateTo"] = values.tranDate_end
            ? moment(values.tranDate_end).format("Y-MM-DD HH:mm:ss")
            : null;
        }
      } else if (key == "status") {
        filter["status"] = values[key] ? values[key] : null;
      } else if (key == "isValidAml") {
        filter["isValidAml"] =
          values[key] !== "" && typeof values[key] !== "undefined"
            ? values[key] == "true"
              ? true
              : false
            : null;
      } else {
        filter[key] = values[key] ? values[key] : null;
      }
    });
    console.log({ ...filterPagination, filter: { ...filterPagination.filter, ...filter } });
    setFilterPagination({ ...filterPagination, filter: { ...filterPagination.filter, ...filter } });
  };

  return (
    <TableList
      header={CCBA_MANAGEMENT_HEADER}
      filter={CCBA_MANAGEMENT_FILTER}
      action={CCBA_MANAGEMENT_ACTION}
      rowActionCallback={() => null}
      data={dataRecord}
      isLoading={loadingPage}
      onFilterData={(values: any) => handleFilterData(values)}
      filterData={filterPagination.filter}
      exportDataObject={{ heading: CCBA_EXPORT_HEADING, data: dataExport, loading: loadingList }}
      isPagination={true}
      pagination={pagination}
      handlePagination={(val) => {
        setFilterPagination({ ...filterPagination, ...val });
      }}
    />
  );
};

export default CcbaRecordListing;
