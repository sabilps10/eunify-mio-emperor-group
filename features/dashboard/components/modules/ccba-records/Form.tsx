import { FC, useEffect, useState, useCallback, useMemo } from "react";
import { any, object, string, number } from "zod";
import { useMutation, useQuery } from "react-query";
import { useRouter } from "next/router";

import useFormContent from "@/features/dashboard/hooks/useFormContent";
import useFormData from "@/hooks/useFormData";
import DetailContentForm from "../../DetailContentForm";
import { getEntityId } from "@/services/common-services";
import { putCcbaBankRecord } from "@/services/mio-services";
import { getAccounts } from "@/services/common-services";
import debouce from "lodash.debounce";
import useLocalStorage from "@/hooks/useLocalStorage";
import { LOCALSTORAGE_KEY } from "@/config/constants";

const AddCCBARecord = [
  {
    id: "efsgBankName",
    name: "efsg_bank",
    type: "text",
    disabled: true,
    payload_skipped: true
  },
  {
    id: "efsgBankAccNo",
    name: "efsg_bank_acc_no",
    type: "text",
    disabled: true,
    payload_skipped: true
  },
  {
    id: "refId",
    name: "bank_txn_reference_no",
    type: "text",
    disabled: true,
    payload_skipped: true
  },
  {
    id: "txnDate",
    name: "txn_date_time",
    type: "text",
    disabled: true,
    payload_skipped: true
  },
  {
    id: "bankAccountNo",
    name: "client_bank_acc_no",
    type: "text",
    disabled: true,
    payload_skipped: true
  },
  {
    id: "clientName",
    name: "client_bank_acc_holder_name",
    type: "text",
    disabled: true,
    payload_skipped: true
  },
  {
    id: "currency",
    name: "currency",
    type: "text",
    disabled: true,
    payload_skipped: true
  },
  {
    id: "actAmount",
    name: "amount",
    type: "text",
    disabled: true,
    payload_skipped: true
  },
  {
    id: "entityId",
    name: "entity",
    type: "select",
    options: [],
    disabled: false
  },
  {
    id: "tradingAccountNo",
    name: "trading_acc_no",
    callback: {
      enabled: ["entity", "trading_acc_no"],
      action: "parentCallback",
      function: "debouncedTradingAccountInput(values, methods)"
    },
    type: "text",
    disabled: true,
    disabled_condition: {
      name: "entity",
      value: "has_value",
      disable: false
    }
  },
  {
    id: "tradingAccName",
    name: "trading_acc_name",
    type: "text",
    disabled: true,
    payload_skipped: true
  },
  { id: "requestId", name: "request_id", hidden: true }
];

const AddCCBARecordSchema = object({
  efsg_bank: string().optional(),
  efsg_bank_acc_no: string().optional(),
  bank_txn_reference_no: string().optional(),
  txn_date_time: string().optional(),
  client_bank_acc_no: string().optional(),
  currency: string().optional(),
  amount: string().optional(),
  entity: string().min(1, { message: "Entity is required" }),
  trading_acc_no: string().min(1, { message: "Trading Account No. is required" }),
  trading_acc_name: string().min(1, { message: "Trading Account Name is required" }),
  request_id: string().min(1, { message: "Request ID is required" })
});

const CcbaForm: FC = () => {
  const router = useRouter();
  const [_, updateState] = useState<any>();
  const forceUpdate = useCallback(() => updateState({}), []);
  const { formFields, formValidations, setFormValue } = useFormContent();
  const { data: formData, setFormData, emptyFields } = useFormData();

  const [initialData, setInitialData] = useState<any>(null);
  const [requestError, setRequestError] = useState(null);

  const { mutate: fetchCcbaBankRecord, isLoading: fetchingCcbaBankRecord } = useMutation((payload: any) =>
    putCcbaBankRecord({ ...payload.payload })
  );
  const { data: entityData } = useQuery(["get-entity"], getEntityId);
  const { data: getAccountDetail } = useQuery(["get-account-detail"], () =>
    getAccounts({ entityId: formData["entityId"], tradingAccountNo: formData["tradingAccountNo"] })
  );
  const { mutate: fetchAccountDetail, isLoading: fetchingAccount } = useMutation((payload: any) =>
    getAccounts({ ...payload.payload })
  );

  const getAccountDetails = (formValues: any, formMethods: any) => {
    formMethods.clearErrors("trading_acc_no");
    formMethods.setValue("trading_acc_name", "");
    fetchAccountDetail(
      { payload: { entityId: formValues.entity, tradingAccountNo: formValues.trading_acc_no } },
      {
        onSuccess: (res) => {
          if (res.result == true) {
            formMethods.setValue("trading_acc_name", res.response.realName, {
              shouldValidate: true
            });
          } else {
            formMethods.setError("trading_acc_no", {
              type: "custom",
              message: res.errorDescription
            });
          }
        }
      }
    );
  };

  const debouncedTradingAccountInput = useMemo(() => {
    return debouce(getAccountDetails, 2000);
  }, []);

  useEffect(() => {
    return () => {
      debouncedTradingAccountInput.cancel();
    };
  });

  useEffect(() => {
    if (entityData) {
      const options = entityData?.response[0].items.map((el: any) => ({
        label: el.name,
        value: el.codeCode
      }));
      AddCCBARecord[8].options = options;
      forceUpdate();
    }
  }, [entityData]);

  useEffect(() => {
    if (!formFields) {
      setFormValue("fields", AddCCBARecord);
      setFormValue("validations", AddCCBARecordSchema);
    }
  }, []);

  useEffect(() => {
    if (formData) {
      let current: any = {};
      AddCCBARecord.map((field: any) => {
        if (field.name == "trading_acc_name" && getAccountDetail) {
          current["trading_acc_name"] = getAccountDetail?.response?.realName;
        } else if (field.name == "request_id") {
          console.log(field.name);
          current["request_id"] = formData["id"]
            ? formData["id"].toString()
            : formData["requestId"].toString();
        } else {
          current[field.name] = formData[field.id]?.toString() || "";
        }
      });
      setInitialData(current);
    }
  }, [formData, getAccountDetail]);

  useEffect(() => {
    forceUpdate();
  }, [initialData]);

  const handleSubmit = (values: any) => {
    const data = {};

    AddCCBARecord.forEach(function (field: any) {
      if (!field.payload_skipped) {
        let value = values[field.name] || "";
        const key = field.id;
        Object.assign(data, { [key]: value });
      }
    });

    const { getItem } = useLocalStorage();
    const account = getItem(LOCALSTORAGE_KEY.ACCOUNT);
    Object.assign(data, { requestBy: account });
    fetchCcbaBankRecord(
      { payload: data },
      {
        onSuccess: (res) => {
          console.log(res);
          if (res.result) {
            router.push(`/${router.query.feature}/management`);
          }
        }
      }
    );
  };

  const handleCancel = () => {
    emptyFields();
    setInitialData(null);
  };

  return (
    <DetailContentForm
      initialData={initialData}
      fields={formFields}
      validations={formValidations}
      onSubmit={handleSubmit}
      onCancel={handleCancel}
      parentCallback={(func: any, values: any, methods: any) => {
        eval(func);
      }}
      fetchingRequest={fetchingCcbaBankRecord}
      requestError={requestError}
    />
  );
};

export default CcbaForm;
