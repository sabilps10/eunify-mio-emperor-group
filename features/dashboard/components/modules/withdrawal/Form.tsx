import { FC, useCallback, useEffect, useMemo, useState } from "react";
import { object, string, number, TypeOf, preprocess } from "zod";
import { useQuery, useMutation } from "react-query";
import { useRouter } from "next/router";

import {
  getEntityId,
  getNetwork,
  getAdjustmentTypeList,
  getExchangeRate as getExchangeRateAmount,
  getCurrency,
  getBankAccountsByEntity,
  getVerifiedBankAccounts
} from "@/services/common-services";
import {
  getWithdrawalChannelList,
  getWithdrawalBalanceCheck,
  getServiceCharge as getServiceChargeAmount,
  postWithdrawalInst,
  putWithdrawalStatus
} from "@/services/mio-services";
import { getAccounts } from "@/services/common-services";
import { encrypt } from "@/services/kms-services";
import useFormData from "@/hooks/useFormData";
import useFormContent from "@/features/dashboard/hooks/useFormContent";
import DetailContentForm from "../../DetailContentForm";
import { formatNumber, RoundNumber } from "@/config/utilities";
import debouce from "lodash.debounce";
import useLocalStorage from "@/hooks/useLocalStorage";
import { LOCALSTORAGE_KEY } from "@/config/constants";

export const DetailWithdrawalFields: any = [
  { id: "tradeTypeName", name: "type", type: "text", disabled: true },
  { id: "adjustmentTypeName", name: "adjustment_type", type: "text", disabled: true },
  { id: "orderNo", name: "reference_no", type: "text", disabled: true },
  { id: "entityId", name: "entity", type: "text", disabled: true },
  { id: "tradingAccountNo", name: "trading_acc_no", type: "text", disabled: true },
  { id: "clientName", name: "acc_holder", type: "text", disabled: true },
  { id: "isValidAml", name: "aml_status", type: "text", value_type: "aml-status", disabled: true },
  {
    ids: [
      { data: "static", value: "USD" },
      { data: "formData", value: "balance" }
    ],
    name: "withdrawal_balance",
    type: "text",
    disabled: true
  },
  { id: "tmpAmount", name: "platform_amount", type: "text", disabled: true },
  {
    id: "custom_value",
    name: "ref_exchange_rate",
    decimal_scale: 4,
    type: "text",
    value_type: "number-percentage",
    additional_value: "gateway_charge_unit",
    disabled: true
  },
  { id: "custom_value", name: "actual_currency", type: "text", disabled: true },
  { id: "custom_value", name: "actual_amount", type: "text", disabled: true },
  {
    id: "custom_value",
    name: "service_charge",
    additional_value: "currency",
    type: "text",
    disabled: true
  },
  { id: "bankName", name: "bank_name", type: "text", disabled: true },
  { id: "accHolderName", name: "bank_acc_holder", type: "text", disabled: true },
  { id: "bankAccountNo", name: "bank_acc_no", type: "text", disabled: true },
  { id: "bankLocation", name: "bank_location", type: "text", disabled: true },
  { id: "bankProvince", name: "bank_province", type: "text", disabled: true },
  { id: "bankRegion", name: "bank_region", type: "text", disabled: true },
  { id: "bankCity", name: "bank_city", type: "text", disabled: true },
  { id: "bankAddress", name: "bank_address", type: "text", disabled: true },
  { id: "IBAccountNo", name: "IB_code", type: "text", disabled: true },
  { id: "cryptoNetwork", name: "crypto_network", type: "text", disabled: true },
  { id: "cryptoAddress", name: "crypto_address", type: "text", disabled: true },
  { id: "createDate", name: "application_time", type: "text", disabled: true },
  { id: "approvedDate", name: "approval_time", type: "text", disabled: true },
  { id: "requestBy", name: "request_by", type: "text", disabled: true },
  { id: "approvedBy", name: "approved_by", type: "text", disabled: true },
  { id: "comment", name: "remark", type: "text", disabled: true },
  { id: "status", name: "status", type: "text", disabled: true, hidden: true },
  { id: "statusName", name: "status_name", type: "text", disabled: true },
  { id: "approvedComment", name: "reject_reason", type: "text", disabled: true }
];

const AddWithdrawalFields = [
  {
    id: "entityId",
    name: "entity",
    type: "select",
    options: [],
    on_select_callback: "onSelectEntity(value, methods)",
    disabled: false
  },
  {
    id: "tradingAccountNo",
    name: "trading_acc_no",
    callback: {
      enabled: ["entity", "trading_acc_no"],
      action: "parentCallback",
      function: "debouncedTradingAccInput(values, methods)"
    },
    type: "text",
    disabled: true,
    disabled_condition: {
      name: "entity",
      value: "has_value",
      disable: false
    }
  },
  {
    id: "tradingAccountName",
    name: "trading_acc_name",
    type: "text",
    disabled: true,
    payload_skipped: true
  },
  {
    id: "balance",
    name: "withdrawable_balance",
    type: "text",
    value_type: "number-currency",
    allow_negative: true,
    additional_value: "USD",
    additional_type: "static",
    disabled: true,
    callback: {
      enabled: ["trading_acc_name"],
      action: "parentCallback",
      function: "getAccountWithdrawalBalance(values, methods)"
    }
  },
  {
    id: "involvePayment",
    name: "involve_payment",
    type: "radio",
    options: [
      { value: "involve", label: "Involve" },
      { value: "not_involve", label: "Not Involve" }
    ],
    disabled: false,
    default_value: "involve",
    callback: {
      enabled: ["involve_payment"],
      action: "parentCallback",
      function: "onChangeInvolveField(values, methods)"
    }
  },
  {
    id: "adjustmentType",
    name: "adjustment_type",
    type: "select",
    options: [],
    disabled: true,
    disabled_condition: {
      name: "entity",
      value: "has_value",
      disable: false
    },
    callback: {
      enabled: ["entity"],
      action: "parentCallback",
      function: "getAdjustmentType()"
    }
  },
  {
    id: "paymentChannelId",
    name: "withdrawal_channel",
    callback: {
      enabled: ["entity"],
      action: "parentCallback",
      function: "getWithdrawalChannel(values)"
    },
    condition: {
      name: "involve_payment",
      value: "involve"
    },
    type: "select",
    options: [],
    selected_option_value: "id",
    on_select_callback: "onSelectWithdrawalChannel(values, value, methods)",
    disabled: true,
    disabled_condition: {
      name: "entity",
      value: "has_value",
      disable: false
    }
  },
  {
    id: "bankAccountNo",
    name: "bank_acc_list",
    condition: {
      name: "involve_payment",
      value: "involve"
    },
    type: "select",
    disabled: true,
    disabled_condition: {
      type: "array",
      names: ["entity", "is_crypto", "customer_acc_no"],
      value: "has_value",
      disable: false
    },
    options: [],
    selected_option_value: "bankAccNo",
    callback: {
      enabled: ["entity", "is_crypto", "customer_acc_no"],
      action: "parentCallback",
      function: "getBankAccounts(values, methods)"
    },
    on_select_callback: "onSelectBankAccount(value, methods)"
  },
  {
    id: "network",
    name: "network",
    condition: {
      name: "involve_payment",
      value: "involve"
    },
    type: "select",
    options: [],
    disabled: true,
    disabled_condition: {
      name: "crypto_currency",
      value: "has_value",
      disable: false
    },
    selected_option_value: "codeCode",
    on_select_callback: "onSelectNetwork(value, methods)"
  },
  {
    id: "cryptoAddress",
    name: "wallet_address",
    condition: {
      name: "involve_payment",
      value: "involve"
    },
    type: "text",
    disabled: true,
    disabled_condition: {
      name: "crypto_currency",
      value: "has_value",
      disable: false
    }
  },
  {
    id: "amount",
    name: "withdrawal_amount",
    additional_value: "platform_currency",
    type: "text",
    value_type: "number-currency",
    disabled: false,
    disabled_condition: {
      type: "array",
      names: ["withdrawal_channel", "currency", "trading_acc_no", "withdrawal_channel"],
      value: "has_value",
      disable: false
    },
    default_value: "",
    condition: {
      name: "involve_payment",
      value: "involve"
    },
    callback: {
      enabled: ["withdrawal_amount"],
      action: "parentCallback",
      function: "debouncedAmountInput(values, methods)"
    }
  },
  {
    id: "amount",
    name: "withdrawal_amount",
    type: "text",
    value_type: "number-currency",
    additional_value: "platform_currency",
    default_value: "",
    disabled: false,
    condition: {
      name: "involve_payment",
      value: "not_involve"
    }
  },
  {
    id: "exchangeRate",
    name: "ref_exchange_rate",
    type: "text",
    disabled: true,
    payload_skipped: true
  },
  {
    id: "refAmount",
    name: "ref_withdrawal_amount",
    type: "text",
    value_type: "number-currency",
    allow_negative: true,
    additional_value: "currency",
    disabled: true,
    payload_skipped: true
  },
  {
    id: "charge",
    name: "ref_service_fee",
    condition: {
      name: "involve_payment",
      value: "involve"
    },
    type: "text",
    value_type: "number-currency",
    additional_value: "currency",
    disabled: true,
    payload_skipped: true
  },
  { id: "comment", name: "remark", type: "text", disabled: false },
  {
    id: "settlementCurrency",
    type: "text",
    name: "settlement_currency",
    hidden: true,
    payload_skipped: true
  },
  { id: "currency", name: "currency", type: "text", hidden: true },
  {
    id: "cryptoCurrency",
    type: "text",
    name: "crypto_currency",
    hidden: true,
    payload_skipped: true
  },
  { id: "requestBy", type: "text", name: "request_by", hidden: true },
  { id: "tradeType", type: "text", name: "trade_type", default_value: "MOA", hidden: true },
  { id: "bankName", type: "text", name: "bank_name", hidden: true },
  { id: "plaformCurrency", type: "text", name: "platform_currency", hidden: true, payload_skipped: true },
  { id: "clientName", type: "text", name: "client_name", hidden: true },
  { id: "userId", name: "customer_acc_no", type: "text", hidden: true, payload_skipped: true }
];

const AddWithdrawalSchema = object({
  entity: string().min(1, { message: "Entity is required" }),
  trading_acc_no: string().min(1, { message: "Trading Account No. is required" }),
  trading_acc_name: string().min(1, { message: "Trading Account Name is required" }),
  withdrawable_balance: string().min(1, { message: "Withdrawal Balance is required" }),
  adjustment_type: string().min(1, { message: "Adjustment Type is required" }),
  involve_payment: string().min(1, { message: "Involve Payment is required" }),
  remark: string().max(20, { message: "Remark must contain at most 20 character(s)" }).optional(),
  withdrawal_amount: string().min(1, { message: "Withdrawal Amount is required" }),
  ref_withdrawal_amount: preprocess(
    (a: any) => parseFloat(string().parse(a)),
    number({
      required_error: "Ref. Withdrawal Amount is required",
      invalid_type_error: "Ref. Withdrawal Amount is required"
    }).positive()
  ),
  currency: string().min(0),
  platform_currency: string().min(0),
  request_by: string().min(0),
  trade_type: string().min(0)
});

const InvolvesSchema = object({
  adjustment_type: string().min(1, { message: "Adjustment Type is required" }),
  withdrawal_channel: string().min(1, { message: "Withdrawal Channel is Required" }),
  ref_exchange_rate: string().min(1, { message: "Exchange Rate is required" }),
  ref_service_fee: string().min(1, { message: "Ref Service Fee is required" })
});

const CryptoSchema = object({
  network: string().min(1, { message: "Network is required" }),
  wallet_address: string().min(1, { message: "Wallet Address is required" })
});

const BankTransferSchema = object({
  bank_acc_list: string().min(1, { message: "Bank Account is Required" }),
  bank_name: string().min(1, { message: "Bank Name is Required" })
});

const WithdrawalForm: FC = () => {
  const {
    push,
    query: { feature, page }
  } = useRouter();
  const [_, updateState] = useState<any>();
  const { formFields, formValidations, setFormValue } = useFormContent();

  const { data: entityId } = useQuery(["get-entity"], getEntityId);
  const { data: networkList } = useQuery(["get-network"], getNetwork);

  const { mutate: fetchWithdrawalChannel } = useMutation((payload: any) =>
    getWithdrawalChannelList({ ...payload.payload })
  );
  const { mutate: fetchAdjustmentType } = useMutation((payload: any) => getAdjustmentTypeList());
  const { mutate: fetchAccountDetail, isLoading: fetchingAccount } = useMutation((payload: any) =>
    getAccounts({ ...payload.payload })
  );
  const { mutate: fetchBankAccounts, isLoading: fetchingBankAccounts } = useMutation((payload: any) =>
    getVerifiedBankAccounts({ ...payload.payload })
  );
  const { mutate: fetchWithdrawalBalance, isLoading: fetchingWithdrawalBalance } = useMutation(
    (payload: any) => getWithdrawalBalanceCheck({ ...payload.payload })
  );
  const { mutate: fetchServiceCharge } = useMutation((payload: any) =>
    getServiceChargeAmount({ ...payload.payload })
  );
  const { mutate: fetchExchangeRate } = useMutation((payload: any) =>
    getExchangeRateAmount({ ...payload.payload })
  );
  const { mutate: fetchManualWithdrawal, isLoading: fetchingWitdrawal } = useMutation(({ payload }: any) =>
    postWithdrawalInst(payload)
  );

  const { mutate: editStatus, isLoading: updateWithdrawalStatus } = useMutation((payload: any) =>
    putWithdrawalStatus(payload)
  );

  const [initialData, setInitialData] = useState(null);
  const [withdrawalChannelList, setWithdrawalChannelList] = useState<any>(null);
  const [adjustmentTypeList, setAdjustmentTypeList] = useState<any>(null);
  const [bankAccountList, setBankAccountList] = useState<any>(null);
  const [requestError, setRequestError] = useState(null);

  const forceUpdate = useCallback(() => updateState({}), []);
  const { data: formData, setFormData, emptyFields } = useFormData();
  const { getItem } = useLocalStorage();

  useEffect(() => {
    if (!formFields) {
      if (page == "add") {
        setFormValue("fields", AddWithdrawalFields);
        setFormValue("validations", AddWithdrawalSchema);
      } else {
        setFormValue("fields", DetailWithdrawalFields);
      }
    }
  }, []);

  useEffect(() => {
    if (formFields) {
      if (formData && page == "detail") {
        let current: any = {};
        formFields.map((val: any) => {
          if (formData[val.id] || typeof formData[val.id] === "boolean" || val.id == "custom_value") {
            let value = "";
            if (val.name == "ref_exchange_rate") {
              if (formData["actExchangeRate1"] && formData["actExchangeRate2"]) {
                value = (formData["actExchangeRate1"] * formData["actExchangeRate2"]).toString();
              } else {
                value = (formData["tmpExchangeRate1"] * formData["tmpExchangeRate2"]).toString();
              }
            } else if (val.name == "actual_amount") {
              value = formData["actDepositAmount"]
                ? formData["actDepositAmount"]?.toString()
                : formData["tmpDepositAmount"]?.toString();
            } else if (val.name == "actual_currency") {
              value = formData["cryptoCurrency"]
                ? formData["cryptoCurrency"]
                : formData["settlementCurrency"];
            } else if (val.name == "service_charge") {
              value += formData["serviceChargeCurrency"];
              value += " ";
              value += formData["actCharge"]
                ? formData["actCharge"]?.toString()
                : formData["tmpCharge"]?.toString();
            } else if (val.name == "bank_location") {
              value = formData["bankLocation"]?.nameEn;
            } else if (val.name == "bank_province") {
              value = formData["bankProvince"]?.nameEn;
            } else if (val.name == "bank_region") {
              value = formData["bankRegion"]?.nameEn;
            } else if (val.name == "bank_city") {
              value = formData["bankCity"]?.nameEn;
            } else {
              value =
                typeof formData[val.id] === "number" || typeof formData[val.id] === "boolean"
                  ? formData[val.id].toString()
                  : formData[val.id];
            }
            Object.assign(current, { [val.name]: value });
          }
        });
        setInitialData(current);
      } else {
        let current: any = {};
        formFields.map((val: any) => {
          if (val.default_value) {
            Object.assign(current, { [val.name]: val.default_value });
          } else {
            Object.assign(current, { [val.name]: "" });
          }
        });
        setInitialData(current);
      }
    }
  }, [formFields]);

  useEffect(() => {
    if (entityId) {
      const options = entityId?.response[0].items.map((el: any) => ({
        label: el.name,
        value: el.codeCode
      }));
      AddWithdrawalFields[0].options = options.filter((el: any) => el.value !== "EBL");
      forceUpdate();
    }
  }, [entityId]);

  useEffect(() => {
    if (networkList) {
      const options = networkList?.response[0].items.map((el: any) => ({
        label: el.name,
        value: el.codeCode,
        origin: el
      }));
      AddWithdrawalFields[8].options = options;
      forceUpdate();
    }
  }, [networkList]);

  useEffect(() => {
    if (adjustmentTypeList) {
      const options = adjustmentTypeList?.map((el: any) => ({
        label: el.nameEn,
        value: el.codeCode
      }));
      AddWithdrawalFields[5].options = options;
      forceUpdate();
    }
  }, [adjustmentTypeList]);

  useEffect(() => {
    if (bankAccountList) {
      const options = bankAccountList?.map((el: any) => ({
        value: el.bankAccNo,
        label:
          el.location == 2
            ? `${el?.bankCode ?? ""} ${el?.bankName ?? ""} ${el?.currency ?? ""} ${el?.bankAccNo ?? ""}`
            : `${el?.bankName ?? ""} ${el?.currency ?? ""} ${el?.bankAccNo ?? ""}`,
        origin: el
      }));
      AddWithdrawalFields[7].options = options;
      forceUpdate();
    }
  }, [bankAccountList]);

  useEffect(() => {
    if (withdrawalChannelList) {
      const options = withdrawalChannelList.map((el: any) => ({
        label: el.displayNameEn,
        value: el.id,
        origin: el
      }));
      AddWithdrawalFields[6].options = options;
      forceUpdate();
    }
  }, [withdrawalChannelList]);

  const getWithdrawalChannel = (formValues: any, formMethods: any) => {
    fetchWithdrawalChannel(
      { payload: { entityId: formValues.entity } },
      {
        onSuccess: (res) => {
          if (res.result) {
            setWithdrawalChannelList(res.response);
          }
        }
      }
    );
  };

  const onSelectWithdrawalChannel = (formValues: any, fieldValue: any, formMethods: any) => {
    formMethods.setValue("bank_acc_list", "");
    formMethods.setValue("bank_name", "");
    formMethods.setValue("currency", "");
    formMethods.setValue("ref_exchange_rate", "");
    formMethods.clearErrors("ref_exchange_rate");
    formMethods.setValue("ref_service_fee", "");
    formMethods.clearErrors("ref_service_fee");
    const currency = fieldValue.currencies[0].cryptoCurrency ? "USDT" : fieldValue.currencies[0].currency;
    const isCrypto = fieldValue.currencies[0].cryptoCurrency ? true : false;

    let Schema = null;
    if (formValues.involve_payment == "involve") {
      Schema = AddWithdrawalSchema.merge(InvolvesSchema);
    } else {
      Schema = AddWithdrawalSchema;
    }

    if (isCrypto) {
      const CombinedCryptoSchema = Schema.merge(CryptoSchema);
      setFormValue("validations", CombinedCryptoSchema);
    } else {
      const CombinedBankTransferSchema = Schema.merge(BankTransferSchema);
      setFormValue("validations", CombinedBankTransferSchema);
      removeCryptoField(formMethods);
    }
    formMethods.setValue("withdrawal_channel", fieldValue.id.toString(), { shouldValidate: true });
    formMethods.setValue("currency", currency, { shouldValidate: true });
    formMethods.setValue("crypto_currency", fieldValue.currencies[0].cryptoCurrency, {
      shouldValidate: true
    });
    formMethods.setValue("is_crypto", !fieldValue.currencies[0].cryptoCurrency);
    if (isCrypto) {
      formMethods.setValue("bank_acc_list", "");
      formMethods.setValue("currency", "USDT");
    } else {
      formMethods.setValue("network", "");
      formMethods.setValue("currency", "");
    }
    formMethods.setValue("withdrawal_amount", "");
    formMethods.setValue("ref_withdrawal_amount", "");
    forceUpdate();
  };

  const onSelectEntity = (fieldValue: any, formMethods: any) => {
    formMethods.setValue("entity", fieldValue, { shouldValidate: true });
    formMethods.setValue("platform_currency", "USD");
    removeCryptoField(formMethods);
  };

  const getBankAccounts = (formValues: any) => {
    fetchBankAccounts(
      { payload: { customerAccountNo: formValues.customer_acc_no, entityId: formValues.entity } },
      {
        onSuccess: (res) => {
          if (res.result) {
            setBankAccountList(res.response);
          }
        }
      }
    );
  };

  const onSelectBankAccount = (fieldValue: any, formMethods: any) => {
    formMethods.setValue("bank_acc_list", fieldValue.bankAccNo, { shouldValidate: true });
    formMethods.setValue("bank_name", fieldValue.bankName);
    formMethods.setValue("currency", fieldValue.currency);
  };

  const onSelectNetwork = (fieldValue: any, formMethods: any) => {
    formMethods.setValue("network", fieldValue.codeCode);
    getExchangeRate(fieldValue, formMethods);
  };

  const getAdjustmentType = () => {
    fetchAdjustmentType(
      {},
      {
        onSuccess: (res) => {
          if (res.result) {
            const items = res.response.filter(function (item: any) {
              return item.codeCode.toLowerCase().includes("mo") && item.active == true;
            });
            setAdjustmentTypeList(items);
          }
        }
      }
    );
  };

  const getAccountDetails = (formValues: any, formMethods: any) => {
    const account = getItem(LOCALSTORAGE_KEY.ACCOUNT);
    formMethods.setValue("request_by", account);

    formMethods.clearErrors("trading_acc_no");
    formMethods.setValue("trading_acc_name", "");
    formMethods.setValue("customer_acc_no", "");
    formMethods.setValue("withdrawable_balance", "");
    fetchAccountDetail(
      { payload: { entityId: formValues.entity, tradingAccountNo: formValues.trading_acc_no } },
      {
        onSuccess: (res) => {
          console.log(res);
          if (res.result == true) {
            formMethods.setValue("trading_acc_name", res.response.realName, {
              shouldValidate: true
            });
            formMethods.setValue("customer_acc_no", res.response.userId, {
              shouldValidate: true
            });
            formMethods.setValue("payment_group", res.response.paymentGroupCode, {
              shouldValidate: true
            });
          } else {
            formMethods.setError("trading_acc_no", {
              type: "custom",
              message: res.errorDescription
            });
          }
        }
      }
    );
  };

  const getAccountWithdrawalBalance = (formValues: any, formMethods: any) => {
    fetchWithdrawalBalance(
      { payload: { entityId: formValues.entity, tradingAccountNo: formValues.trading_acc_no } },
      {
        onSuccess: (res) => {
          if (res.result) {
            formMethods.setValue("withdrawable_balance", res.response.balance.toString(), {
              shouldValidate: true
            });
          } else {
            formMethods.setError("withdrawable_balance", {
              type: "custom",
              message: res.errorDescription ? res.errorDescription : "Request error, please try again"
            });
          }
        }
      }
    );
  };

  const onChangeInvolveField = (formValues: any, formMethods: any) => {
    formMethods.setValue("platform_currency", "USD");
    formMethods.setValue("currency", "USD");
    formMethods.setValue("crypto_currency", "");
    formMethods.setValue("wallet_address", "");
    formMethods.setValue("adjustment_type", "");
    formMethods.setValue("withdrawal_channel", "");
    formMethods.setValue("bank_acc_list", "");
    formMethods.setValue("ref_withdrawal_amount", "");
    formMethods.clearErrors("ref_withdrawal_amount");
    formMethods.setValue("ref_exchange_rate", "");
    formMethods.setValue("ref_service_fee", "");
    formMethods.setValue("remark", "");
    if (formValues.involve_payment == "involve") {
      formMethods.setValue("currency", "");
      const CombinedSchema = AddWithdrawalSchema.merge(InvolvesSchema);
      setFormValue("validations", CombinedSchema);
    } else if (formValues.involve_payment == "not_involve") {
      setFormValue("validations", AddWithdrawalSchema);
    }
  };

  const getExchangeRate = (formValues: any, formMethods: any) => {
    formMethods.setValue(
      "withdrawal_amount",
      Number(formValues.withdrawal_amount) > -1 ? formValues.withdrawal_amount : ""
    );
    formMethods.clearErrors("withdrawal_amount");
    if (formValues.withdrawal_amount == 0) {
      formMethods.setError("withdrawal_amount", {
        type: "custom",
        message: "Amount cannot be 0"
      });
    }
    formMethods.setValue("ref_withdrawal_amount", "");
    formMethods.setValue("ref_exchange_rate", "");
    formMethods.clearErrors("ref_exchange_rate");
    formMethods.setValue("ref_service_fee", "");
    formMethods.clearErrors("ref_service_fee");

    if (formValues.withdrawal_amount == 0 || formValues.withdrawal_amount == "") return;
    const isCrypto = formValues.crypto_currency ? true : false;

    const payload = {
      entityId: formValues.entity,
      paymentGroup: formValues?.paymentGroup,
      tradingAccountNo: formValues.trading_acc_no,
      baseCurrency: isCrypto ? formValues.network : formValues?.currency,
      quoteCurrency: "USD",
      exchangeType: "PER"
    };

    fetchExchangeRate(
      { payload },
      {
        onSuccess: (res) => {
          if (res.result) {
            formMethods.setValue("ref_exchange_rate", res.response.withdrawalRate.toString());
            formMethods.setValue("ref_service_fee", "");
            if (formValues.involve_payment == "not_involve") {
              const settlement_amount = Number(formValues.withdrawal_amount) * res.response.withdrawalRate!;
              formMethods.setValue("ref_withdrawal_amount", RoundNumber(settlement_amount).toString(), {
                shouldValidate: true
              });
            } else if (formValues.involve_payment == "involve") {
              getServiceCharge(formValues, formMethods, res.response.withdrawalRate);
            }
          } else {
            formMethods.setError("display_exchange_rate", {
              type: "custom",
              message: res.errorDescription
            });
          }
        }
      }
    );
  };

  const getServiceCharge = (formValues: any, formMethods: any, withdrawalRate: number) => {
    const payload = {
      entityId: formValues.entity || "",
      paymentChannelId: formValues.withdrawal_channel.toString() || "",
      paymentGroup: formValues?.payment_group || "",
      tradingAccountNo: formValues?.trading_acc_no || "",
      amount: formValues.withdrawal_amount || 0
    };
    fetchServiceCharge(
      { payload },
      {
        onSuccess: (res) => {
          if (res.result) {
            const settlement_amount =
              (Number(formValues.withdrawal_amount) - res.response.charge) * withdrawalRate!;
            formMethods.setValue("ref_withdrawal_amount", RoundNumber(settlement_amount), {
              shouldValidate: true
            });
            formMethods.setValue(
              "ref_service_fee",
              RoundNumber(res.response.charge * withdrawalRate).toString()
            );
          } else {
            formMethods.setError("ref_service_fee", {
              type: "custom",
              message: res.errorDescription
            });
          }
        }
      }
    );
  };

  const debouncedTradingAccInput = useMemo(() => {
    return debouce(getAccountDetails, 1000);
  }, []);

  const debouncedAmountInput = useMemo(() => {
    return debouce(getExchangeRate, 1000);
  }, []);

  useEffect(() => {
    return () => {
      debouncedAmountInput.cancel();
      debouncedTradingAccInput.cancel();
    };
  });

  const removeCryptoField = (formMethods: any) => {
    formMethods.clearErrors("wallet_address");
    formMethods.setValue("wallet_address", "");
    formMethods.clearErrors("network");
    formMethods.setValue("network", "");
    formMethods.setValue("crypto_currency", "");
  };

  useEffect(() => {
    forceUpdate();
  }, [initialData]);

  const handleSubmitForm = (values: any) => {
    const data = {};
    AddWithdrawalFields.forEach(function (field: any) {
      if (!field.payload_skipped) {
        const isCrypto = values["network"] ? true : false;
        const currency = isCrypto ? values["network"] : values["currency"];
        let value = field.name == "currency" ? currency : values[field.name] || "";
        if (field.name == "involve_payment") {
          value = values[field.name] == "involve" ? true : false;
        }
        const key = field.id == "network" ? field.name : field.id;
        Object.assign(data, { [key]: value });
      }
    });
    fetchManualWithdrawal(
      { payload: data },
      {
        onSuccess: (res) => {
          if (res.result) {
            push(`/${feature}/management`);
          } else {
            setRequestError(res);
          }
        }
      }
    );
  };

  const handleCancel = async () => {
    emptyFields();
    setInitialData(null);
  };
  const handleAction = (values: any) => {
    const { getItem } = useLocalStorage();
    const account = getItem(LOCALSTORAGE_KEY.ACCOUNT);
    switch (values.action) {
      case "approve":
        return editStatus(
          [
            {
              requestBy: account,
              instRequestId: formData.instRequestId,
              status: 1
            }
          ],
          {
            onSuccess: (res) => {
              if (res.result) {
                push(`/${feature}/management`);
              }
            }
          }
        );
      case "reject":
        return editStatus(
          [
            {
              requestBy: account,
              instRequestId: formData.instRequestId,
              comment: values.data.comment,
              status: 2
            }
          ],
          {
            onSuccess: (res) => {
              if (res.result) {
                push(`/${feature}/management`);
              }
            }
          }
        );
    }
  };

  return (
    <DetailContentForm
      formData={formData}
      initialData={initialData}
      fields={formFields}
      validations={formValidations}
      onSubmit={handleSubmitForm}
      onCancel={() => setFormData(null)}
      parentCallback={(func: any, values: any, methods: any) => {
        eval(func);
      }}
      onSelectCallback={(func: any, values: any, value: any, methods: any) => {
        eval(func);
      }}
      fetchingRequest={fetchingWitdrawal || updateWithdrawalStatus}
      requestError={requestError}
      detailAction={handleAction}
    />
  );
};

export default WithdrawalForm;
