import { FC, useCallback, useEffect, useState } from "react";
import TableList from "@/components/TableList";
import useListingContent from "@/features/dashboard/hooks/useListingContent";
import {
  EXCEPTION_MANAGEMENT_ACTION,
  EXCEPTION_MANAGEMENT_HEADER,
  EXPCEPTION_MANAGEMENT_EXPORT_HEADER
} from "@/config/tables/exceptions";
import { useMutation, useQuery } from "react-query";
import { getTransactions, postDepositExceptions } from "@/services/mio-services";
import { getEntityId, getTradeType } from "@/services/common-services";
import moment from "moment";

export const EXCEPTION_MANAGEMENT_FILTER = [
  { name: "tradeType", id: "type", type: "dropdown", options: [] },
  { name: "orderNo", id: "reference_number", type: "text" },
  { name: "entityId", id: "entity", type: "dropdown", options: [] },
  { name: "tradingAccountNo", id: "trading_acc_no", type: "text" },
  { name: "applicationTime", id: "application_time", type: "time-range" }
];

const ExceptionListing: FC = () => {
  const [, updateState] = useState<any>();
  const forceUpdate = useCallback(() => updateState({}), []);
  const [filterData, setFilterData] = useState({
    tradeType: null,
    orderNo: null,
    entityId: null,
    applicationTimeFrom: null,
    applicationTimeTo: null,
    status: [5],
    includeDummyAccount: false
  });
  const { data: entity } = useQuery(["get-entity"], getEntityId);
  const { data: tradetype } = useQuery(["get-trade-type"], getTradeType);
  const {
    data: exceptionRecord,
    isFetching: fetchingRecord,
    refetch: refetchExceptionRecord
  } = useQuery(["get-exception-record", filterData], async () => {
    const { response } = await getTransactions(filterData);
    return response;
  });
  const { mutate: depositExceptions, isLoading: fetchingRetryRequest } = useMutation((payload: any) =>
    postDepositExceptions(payload)
  );

  useEffect(() => {
    if (entity) {
      const options = entity?.response[0].items.map((el: any) => ({
        label: el.codeCode,
        value: el.codeCode
      }));
      EXCEPTION_MANAGEMENT_FILTER[2].options = options;
      forceUpdate();
    }
  }, [entity]);

  useEffect(() => {
    if (tradetype) {
      const options = tradetype?.response[0].items.map((el: any) => ({
        label: el.name,
        value: el.codeCode
      }));
      EXCEPTION_MANAGEMENT_FILTER[0].options = options;
      forceUpdate();
    }
  }, [tradetype]);

  const handleDoFilterData = (values: any) => {
    let filter: any = filterData;
    Object.keys(values).forEach(function (key) {
      if (key == "applicationTime_start" || key == "applicationTime_end") {
        const splitString = key.split("_");
        if (splitString[1] == "start") {
          console.log(values);
          filter["applicationTimeFrom"] = moment(values.applicationTime_start).format("Y-MM-DD HH:mm:ss");
        }
        if (splitString[1] == "end") {
          filter["applicationTimeTo"] = moment(values.applicationTime_end).format("Y-MM-DD") + " 23:59:59";
        }
      } else if (key == "status") {
        filter["status"] = values[key] ? values[key] : filterData["status"];
      } else if (key == "tradingAccountNo") {
        filter["tradingAccountNo"] = values[key] ? [values[key]] : null;
      } else {
        filter[key] = values[key] ? values[key] : null;
      }
    });
    setFilterData({ ...filterData, ...filter });
  };

  const handleRowAction = (values: any) => {
    depositExceptions(
      { requestId: values.id },
      {
        onSuccess(res) {
          refetchExceptionRecord();
        }
      }
    );
  };

  return (
    <TableList
      header={EXCEPTION_MANAGEMENT_HEADER}
      filter={EXCEPTION_MANAGEMENT_FILTER}
      action={EXCEPTION_MANAGEMENT_ACTION}
      data={exceptionRecord}
      isLoading={fetchingRecord}
      actionButtonLoading={fetchingRetryRequest}
      onFilterData={(values: any) => handleDoFilterData(values)}
      rowActionCallback={handleRowAction}
      exportDataObject={{ heading: EXPCEPTION_MANAGEMENT_EXPORT_HEADER }}
    />
  );
};

export default ExceptionListing;
