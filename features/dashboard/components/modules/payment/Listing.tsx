import { FC, useCallback, useEffect, useState } from "react";
import { useQuery } from "react-query";

import { PAYMENT_REJECT_DEPOSIT_ACTION } from "@/config/tables/deposit-payment-reject";
import { PAYMENT_UNCLAIMED_DEPOSIT_ACTION } from "@/config/tables/deposit-payment-unclaimed";
import { PAYMENT_APPROVED_WITHDRAWAL_ACTION } from "@/config/tables/withdrawal-payment-approved";
import {
  getChannel,
  getPaymentStatus,
  getPaymentStatusRejectedDeposit,
  getPaymentStatusUnclaimedDeposit,
  getPaymentStatusApprovedWithdrawal,
  getEntityId
} from "@/services/common-services";
import TableWithTab from "../../TableWithTab";
import { useRouter } from "next/router";
import { getDepositChannelList, getWithdrawalChannelList } from "@/services/mio-services";
import { FindIndexFormField } from "@/config/utilities";

export const PAYMENT_EXPORT_HEADING = [
  { name: "tradeTypeName", label: "Type", width: 20, position: 1 },
  { name: "adjustmentTypeName", label: "Adjustment Type", width: 20, position: 2 },
  { name: "orderNo", label: "Reference No.", width: 20, position: 3 },
  { name: "entityName", label: "Entity", width: 30, position: 4 },
  { name: "tradingAccountNo", label: "Trading Account No.", width: 30, position: 5 },
  { name: "clientName", label: "Acc Holder Name", width: 30, position: 6 },
  {
    name: "validAml",
    type: "custom-value",
    custom_values: [
      { value: true, label: "Pass" },
      { value: false, label: "Fail" }
    ],
    label: "AML Status",
    width: 30,
    position: 7
  },
  { name: "settlementAccCurrency", label: "Account Settlement Currency", width: 20, position: 8 },
  { type: "custom-data", key: "exchange-rate", label: "Exchange Rate", width: 20, position: 9 },
  { type: "custom-data", key: "platform-currency", label: "Platform Currency", width: 20, position: 10 },
  { type: "custom-data", key: "platform-amount", label: "Platform Amount", width: 20, position: 11 },
  { type: "custom-data", key: "actual-currency", label: "Actual Currency", width: 20, position: 12 },
  { type: "custom-data", key: "actual-amount", label: "Actual Amount", width: 20, position: 13 },
  {
    type: "custom-data",
    key: "service-charge",
    label: "Service Charge",
    width: 20,
    position: 14,
    isSort: true
  },
  {
    type: "custom-data",
    key: "calculated-actual-deposit",
    label: "Calculated Actual Deposit Amount",
    width: 20,
    position: 15
  },
  { name: "refId", label: "Payment gateway ref no", width: 20, position: 16, isSort: true },
  { name: "bankName", label: "Bank name", width: 20, position: 17, isSort: true },
  {
    type: "custom-data",
    key: "bank-location",
    name: "bankLocation",
    label: "Bank Location",
    width: 20,
    position: 18
  },
  {
    type: "custom-data",
    key: "bank-province",
    name: "bankProvince",
    label: "Bank Province",
    width: 18,
    position: 19
  },
  {
    type: "custom-data",
    key: "bank-region",
    name: "bankRegion",
    label: "Bank Region",
    width: 19,
    position: 20
  },
  { type: "custom-data", key: "bank-city", name: "bankCity", label: "Bank City", width: 20, position: 21 },
  { name: "bankAddress", label: "Bank Address", width: 20, position: 22 },
  { name: "accHolderName", label: "Bank Account Name", width: 20, position: 23 },
  { name: "bankAccountNo", label: "Bank Account Number", width: 20, position: 24 },
  { name: "IBAccountNo", label: "IB Code", width: 20, position: 25 },
  { name: "cryptoNetwork", label: "Network", width: 20, position: 26 },
  { name: "cryptoAddress", label: "Crypto Address", width: 20, position: 27 },
  { name: "tradeDate", label: "Trade Date", width: 20, position: 28 },
  { name: "createdDate", label: "Application Time", width: 20, position: 29 },
  { name: "approvedDate", label: "Approval Time", width: 20, position: 30 },
  { name: "txnDate", label: "Transaction Time", width: 20, position: 31 },
  { name: "lastModifiedBy", label: "Requested By", width: 20, position: 32 },
  { name: "approvedBy", label: "Approved By", width: 20, position: 33 },
  { name: "paymentStatusName", label: "Status", width: 20, position: 34 },
  { name: "comment", label: "Remark", width: 20, position: 35 }
];

export const DEPOSIT_PAYMENT_MANAGEMENT_HEADER = [
  {
    id: "checkbox",
    numeric: true,
    disablePadding: false,
    isSort: false
  },
  {
    id: "orderNo",
    numeric: true,
    disablePadding: false,
    label: "Reference No.",
    isSort: false
  },
  {
    id: "entityId",
    numeric: false,
    disablePadding: false,
    label: "Entity",
    isSort: true
  },
  {
    id: "paymentChannelName",
    numeric: true,
    disablePadding: false,
    label: "Deposit Channel",
    isSort: false
  },
  {
    id: "custom_value",
    name: "actual_currency",
    numeric: false,
    disablePadding: false,
    label: "Actual Currency",
    isSort: true,
    sortId: "currency"
  },
  {
    id: "custom_value",
    name: "actual_amount",
    numeric: true,
    disablePadding: false,
    label: "Actual Amount",
    isSort: true,
    sortId: "amount"
  },
  {
    id: "settlementAccCurrency",
    name: "account_settlement_currency",
    numeric: true,
    disablePadding: false,
    label: "Account Settlement Currency",
    isSort: true
  },
  {
    id: "createdDate",
    numeric: false,
    disablePadding: false,
    label: "Application Time",
    isSort: true
  },
  {
    id: "tradeDate",
    numeric: false,
    disablePadding: false,
    label: "Trade Date",
    isSort: true
  },
  {
    id: "tradingAccountNo",
    numeric: true,
    disablePadding: false,
    label: "Trading Account Number",
    isSort: true
  },
  {
    id: "paymentStatusName",
    numeric: false,
    disablePadding: false,
    label: "Status",
    isSort: true,
    sortId: "paymentStatus"
  },

  {
    id: "exported",
    numeric: false,
    disablePadding: false,
    label: "Export for FPS?",
    row_condition: [
      {
        type: "contain-string",
        key: "true",
        value: "Y"
      },
      {
        type: "contain-string",
        key: "false",
        value: "N"
      }
    ],
    isSort: false
  },
  {
    id: "exportedDate",
    numeric: false,
    disablePadding: false,
    label: "FPS Export Date",
    isSort: true,
    sortId: "exportedDate"
  },
  {
    id: "IBAccountNo",
    numeric: false,
    disablePadding: false,
    label: "IB Code",
    isSort: true
  },
  {
    id: "comment",
    numeric: false,
    disablePadding: false,
    label: "Remarks",
    isSort: true,
    sortId: "comment"
  },
  {
    id: "action",
    numeric: true,
    disablePadding: false,
    label: "Action",
    key_value: "orderNo",
    isSort: false
  }
];

export const WITHDRAW_PAYMENT_MANAGEMENT_HEADER = [
  {
    id: "checkbox",
    numeric: true,
    disablePadding: false,
    isSort: false
  },
  {
    id: "orderNo",
    numeric: true,
    disablePadding: false,
    label: "Reference No.",
    isSort: false
  },
  {
    id: "entityId",
    numeric: false,
    disablePadding: false,
    label: "Entity",
    isSort: true
  },
  {
    id: "paymentChannelName",
    numeric: true,
    disablePadding: false,
    label: "Withdrawal Channel",
    isSort: false
  },
  {
    id: "custom_value",
    name: "actual_currency",
    numeric: false,
    disablePadding: false,
    label: "Actual Currency",
    isSort: false
  },
  {
    id: "custom_value",
    name: "actual_amount",
    numeric: true,
    disablePadding: false,
    label: "Actual Amount",
    isSort: true,
    sortId: "depositAmount"
  },
  {
    id: "settlementAccCurrency",
    name: "account_settlement_currency",
    numeric: true,
    disablePadding: false,
    label: "Account Settlement Currency",
    isSort: true
  },
  {
    id: "createdDate",
    numeric: false,
    disablePadding: false,
    label: "Application Time",
    isSort: true
  },
  {
    id: "tradeDate",
    numeric: false,
    disablePadding: false,
    label: "Trade Date",
    isSort: true
  },
  {
    id: "tradingAccountNo",
    numeric: true,
    disablePadding: false,
    label: "Trading Account Number",
    isSort: true
  },
  {
    id: "paymentStatusName",
    numeric: false,
    disablePadding: false,
    label: "Status",
    isSort: true,
    sortId: "paymentStatus"
  },

  {
    id: "exported",
    numeric: false,
    disablePadding: false,
    label: "Export for FPS?",
    row_condition: [
      {
        type: "contain-string",
        key: "true",
        value: "Y"
      },
      {
        type: "contain-string",
        key: "false",
        value: "N"
      }
    ],
    isSort: false
  },
  {
    id: "exportedDate",
    numeric: false,
    disablePadding: false,
    label: "FPS Export Date",
    isSort: true,
    sortId: "exportedDate"
  },
  {
    id: "IBAccountNo",
    numeric: false,
    disablePadding: false,
    label: "IB Code",
    isSort: true
  },
  {
    id: "comment",
    numeric: false,
    disablePadding: false,
    label: "Remarks",
    isSort: true,
    sortId: "comment"
  },
  {
    id: "action",
    numeric: true,
    disablePadding: false,
    label: "Action",
    key_value: "orderNo",
    isSort: false
  }
];

const actualCurrencies = [
  { value: "HKD", label: "HKD" },
  { value: "RMB", label: "RMB" },
  { value: "USD", label: "USD" },
  { value: "USDTERC20", label: "USDTERC20" },
  { value: "USDTTRC20", label: "USDTTRC20" }
];

export const REJECT_DEPOSIT_PAYMENT_MANAGEMENT_FILTER = [
  { name: "orderNo", id: "reference_id", type: "text" },
  { name: "paymentChannelId", id: "deposit_channel", type: "dropdown", options: [] },
  {
    name: "currency",
    id: "actual_currency",
    type: "dropdown",
    options: actualCurrencies
  },
  { name: "applicationTime", id: "application_time", type: "time-range" },
  { name: "tradingAccountNo", id: "trading_acc_no", type: "text" },
  { name: "status", id: "status", type: "dropdown", options: [] },
  { id: "entity", name: "entityId", type: "dropdown", options: [] },
  {
    id: "expotForFPS",
    name: "exported",
    type: "dropdown",
    options: [
      {
        label: "Y",
        value: "true"
      },
      {
        label: "N",
        value: "false"
      }
    ]
  }
];

export const UNCLAIMED_DEPOSIT_PAYMENT_MANAGEMENT_FILTER = [
  { name: "orderNo", id: "reference_id", type: "text" },
  { name: "paymentChannelId", id: "deposit_channel", type: "dropdown", options: [] },
  {
    name: "currency",
    id: "actual_currency",
    type: "dropdown",
    options: actualCurrencies
  },
  { name: "applicationTime", id: "application_time", type: "time-range" },
  { name: "tradingAccountNo", id: "trading_acc_no", type: "text" },
  { name: "status", id: "status", type: "dropdown", options: [] },
  { id: "entity", name: "entityId", type: "dropdown", options: [] },
  {
    id: "expotForFPS",
    name: "exported",
    type: "dropdown",
    options: [
      {
        label: "Y",
        value: "true"
      },
      {
        label: "N",
        value: "false"
      }
    ]
  }
];

export const WITHDRAWAL_PAYMENT_MANAGEMENT_FILTER = [
  { name: "orderNo", id: "reference_id", type: "text" },
  { name: "paymentChannelId", id: "withdrawal_channel", type: "dropdown", options: [] },
  {
    name: "settleCurrency",
    id: "actual_currency",
    type: "dropdown",
    options: actualCurrencies
  },
  { name: "applicationTime", id: "application_time", type: "time-range" },
  { name: "tradingAccountNo", id: "trading_acc_no", type: "text" },
  { name: "status", id: "status", type: "dropdown", options: [] },
  { id: "entity", name: "entityId", type: "dropdown", options: [] },
  {
    id: "expotForFPS",
    name: "exported",
    type: "dropdown",
    options: [
      {
        label: "Y",
        value: "true"
      },
      {
        label: "N",
        value: "false"
      }
    ]
  }
];

export const TABLE_WITH_TAB_PAYMENT = [
  {
    id: "approved_withdrawal",
    title: "Approved Withdrawal",
    filter: WITHDRAWAL_PAYMENT_MANAGEMENT_FILTER,
    action: PAYMENT_APPROVED_WITHDRAWAL_ACTION,
    header: WITHDRAW_PAYMENT_MANAGEMENT_HEADER,
    export: PAYMENT_EXPORT_HEADING,
    isLoading: false
  },
  {
    id: "unclaimed_deposit",
    title: "Unclaimed Deposit",
    filter: UNCLAIMED_DEPOSIT_PAYMENT_MANAGEMENT_FILTER,
    action: PAYMENT_UNCLAIMED_DEPOSIT_ACTION,
    header: DEPOSIT_PAYMENT_MANAGEMENT_HEADER,
    export: PAYMENT_EXPORT_HEADING,
    isLoading: false
  },
  {
    id: "reject_deposit",
    title: "Reject Deposit",
    filter: REJECT_DEPOSIT_PAYMENT_MANAGEMENT_FILTER,
    action: PAYMENT_REJECT_DEPOSIT_ACTION,
    header: DEPOSIT_PAYMENT_MANAGEMENT_HEADER,
    export: PAYMENT_EXPORT_HEADING,
    isLoading: false
  }
];

const PaymentListing: FC = () => {
  const router = useRouter();
  const [_, updateState] = useState<any>();
  const { data: entityData } = useQuery(["get-entity"], getEntityId);
  const { data: depositChannel } = useQuery(["get-deposit-channel-option"], getDepositChannelList);
  const { data: withdrawalChannel } = useQuery(["get-withdrawal-channel-option"], getWithdrawalChannelList);
  const { data: rejectedDepositStatus } = useQuery(
    ["get-rejected-mi-status"],
    getPaymentStatusRejectedDeposit
  );
  const { data: unclaimedDepositStatus } = useQuery(
    ["get-unclaimed-mi-status"],
    getPaymentStatusUnclaimedDeposit
  );
  const { data: approvedWithdrawalStatus } = useQuery(
    ["get-approve-mo-status"],
    getPaymentStatusApprovedWithdrawal
  );

  const forceUpdate = useCallback(() => updateState({}), []);

  useEffect(() => {
    if (entityData) {
      let items: { label: string; value: any }[] = [];
      entityData?.response[0].items.forEach(function (val: any) {
        let item = { label: "", value: "" };
        item.label = val.name;
        item.value = val.codeCode;
        items.push(item);
      });
      WITHDRAWAL_PAYMENT_MANAGEMENT_FILTER[
        FindIndexFormField(WITHDRAWAL_PAYMENT_MANAGEMENT_FILTER, "entityId")
      ].options = items;
      UNCLAIMED_DEPOSIT_PAYMENT_MANAGEMENT_FILTER[
        FindIndexFormField(UNCLAIMED_DEPOSIT_PAYMENT_MANAGEMENT_FILTER, "entityId")
      ].options = items;
      REJECT_DEPOSIT_PAYMENT_MANAGEMENT_FILTER[
        FindIndexFormField(REJECT_DEPOSIT_PAYMENT_MANAGEMENT_FILTER, "entityId")
      ].options = items;
      forceUpdate();
    }
  }, [entityData]);

  useEffect(() => {
    if (depositChannel) {
      const options = depositChannel?.response?.map((el: any) => ({
        label: el.gatewayNameEn,
        value: el.id
      }));
      REJECT_DEPOSIT_PAYMENT_MANAGEMENT_FILTER[
        FindIndexFormField(REJECT_DEPOSIT_PAYMENT_MANAGEMENT_FILTER, "paymentChannelId")
      ].options = options;
      UNCLAIMED_DEPOSIT_PAYMENT_MANAGEMENT_FILTER[
        FindIndexFormField(UNCLAIMED_DEPOSIT_PAYMENT_MANAGEMENT_FILTER, "paymentChannelId")
      ].options = options;
      forceUpdate();
    }
  }, [depositChannel]);

  console.log(UNCLAIMED_DEPOSIT_PAYMENT_MANAGEMENT_FILTER);

  useEffect(() => {
    if (withdrawalChannel) {
      const options = withdrawalChannel?.response.map((el: any) => ({
        label: el.gatewayNameEn,
        value: el.id
      }));
      WITHDRAWAL_PAYMENT_MANAGEMENT_FILTER[
        FindIndexFormField(WITHDRAWAL_PAYMENT_MANAGEMENT_FILTER, "paymentChannelId")
      ].options = options;
      forceUpdate();
    }
  }, [withdrawalChannel]);

  useEffect(() => {
    if (rejectedDepositStatus) {
      const options = rejectedDepositStatus?.response[0].items.map((el: any) => ({
        label: el.name,
        value: el.codeInt
      }));
      REJECT_DEPOSIT_PAYMENT_MANAGEMENT_FILTER[
        FindIndexFormField(REJECT_DEPOSIT_PAYMENT_MANAGEMENT_FILTER, "status")
      ].options = options;
      forceUpdate();
    }
  }, [rejectedDepositStatus]);

  useEffect(() => {
    if (unclaimedDepositStatus) {
      const options = unclaimedDepositStatus?.response[0].items.map((el: any) => ({
        label: el.name,
        value: el.codeInt
      }));
      UNCLAIMED_DEPOSIT_PAYMENT_MANAGEMENT_FILTER[
        FindIndexFormField(UNCLAIMED_DEPOSIT_PAYMENT_MANAGEMENT_FILTER, "status")
      ].options = options;
      forceUpdate();
    }
  }, [unclaimedDepositStatus]);

  useEffect(() => {
    if (approvedWithdrawalStatus) {
      const options = approvedWithdrawalStatus?.response[0].items.map((el: any) => ({
        label: el.name,
        value: el.codeInt
      }));
      WITHDRAWAL_PAYMENT_MANAGEMENT_FILTER[
        FindIndexFormField(WITHDRAWAL_PAYMENT_MANAGEMENT_FILTER, "status")
      ].options = options;
      forceUpdate();
    }
  }, [approvedWithdrawalStatus]);

  return <TableWithTab tableAttr={TABLE_WITH_TAB_PAYMENT} useCheckbox={true} />;
};

export default PaymentListing;
