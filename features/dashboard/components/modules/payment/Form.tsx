import { FC, useEffect, useState } from "react";
import { useRouter } from "next/router";

import useFormData from "@/hooks/useFormData";
import useFormContent from "@/features/dashboard/hooks/useFormContent";
import DetailContentForm from "../../DetailContentForm";

export const DetailTransactionFields = [
  { id: "tradeTypeName", name: "type", type: "text", disabled: true },
  { id: "adjustmentTypeName", name: "adjustment_type", type: "text", disabled: true },
  {
    id: "orderNo",
    name: "reference_no",
    type: "text",
    disabled: true
  },
  { id: "entityId", name: "entity", type: "text", disabled: true },
  { id: "tradingAccountNo", name: "trading_acc_no", type: "text", disabled: true },
  { id: "clientName", name: "acc_holder", type: "text", disabled: true },
  { id: "validAml", name: "aml_status", type: "text", disabled: true },
  {
    id: "currency",
    name: "currency",
    type: "text",
    hidden: true
  },
  {
    id: "tmpExchangeRate1",
    name: "exchange_rate",
    type: "text",
    disabled: true
  },
  { id: "static_value", name: "platform_currency", defaultValue: "USD", type: "text", disabled: true },
  {
    id: "custom_value",
    name: "platform_amount",
    type: "text",
    value_type: "number",
    disabled: true
  },
  { id: "custom_value", name: "actual_currency", type: "text", disabled: true },
  {
    id: "custom_value",
    name: "actual_amount",
    value_type: "number",
    type: "text",
    disabled: true
  },
  {
    id: "settlementAccCurrency",
    name: "account_settlement_currency",
    type: "text",
    disabled: true
  },
  {
    id: "custom_value",
    name: "service_charge",
    type: "text",
    value_type: "number-currency",
    additional_value: "currency",
    disabled: true
  },
  { id: "bankName", name: "bank_name", type: "text", disabled: true },
  { id: "accHolderName", name: "bank_acc_holder", type: "text", disabled: true },
  { id: "bankAccountNo", name: "bank_acc_no", type: "text", disabled: true },
  { id: "bankLocation", name: "bank_location", type: "text", disabled: true },
  { id: "bankProvince", name: "bank_province", type: "text", disabled: true },
  { id: "bankRegion", name: "bank_region", type: "text", disabled: true },
  { id: "bankCity", name: "bank_city", type: "text", disabled: true },
  { id: "bankAddress", name: "bank_address", type: "text", disabled: true },
  { id: "IBAccountNo", name: "IB_code", type: "text", disabled: true },
  { id: "cryptoNetwork", name: "crypto_network", type: "text", disabled: true },
  { id: "cryptoAddress", name: "crypto_address", type: "text", disabled: true },
  { id: "createdDate", name: "application_time", type: "text", disabled: true },
  { id: "tradeDate", name: "trade_date", type: "text", disabled: true },
  { id: "approvedDate", name: "approval_time", type: "text", disabled: true },
  { id: "txnDate", name: "transaction_time", type: "text", disabled: true },
  { id: "createBy", name: "request_by", type: "text", disabled: true },
  { id: "approvedBy", name: "approved_by", type: "text", disabled: true },
  { id: "comment", name: "remark", type: "text", disabled: true },
  { id: "paymentStatusName", name: "status", type: "text", disabled: true },
  { id: "approvedComment", name: "reject_reason", type: "text", disabled: true }
];

const PaymentForm: FC = () => {
  const {
    query: { page }
  } = useRouter();
  const { formFields, formValidations, setFormValue } = useFormContent();
  const { data: formData, setFormData } = useFormData();

  const [initialData, setInitialData] = useState({});

  useEffect(() => {
    if (!formFields) {
      if (page == "detail") {
        setFormValue("fields", DetailTransactionFields);
      }
    }

    if (formData) {
      let current: any = {};
      DetailTransactionFields.map((field: any) => {
        if (formData[field.id] || typeof formData[field.id] === "boolean" || field.id == "custom_value") {
          let value = "";
          let isDeposit = formData["tradeType"].toLowerCase().includes("mi");
          let isWithdrawal = formData["tradeType"].toLowerCase().includes("mo");

          if (field.name == "exchange_rate") {
            if (formData["actExchangeRate1"] && formData["actExchangeRate2"]) {
              value = (formData["actExchangeRate1"] * formData["actExchangeRate2"]).toString();
            } else {
              value = (formData["tmpExchangeRate1"] * formData["tmpExchangeRate2"]).toString();
            }
          } else if (field.name == "actual_amount" || field.name == "actual_currency") {
            if (isDeposit) {
              if (field.name == "actual_amount") {
                value = formData["actAmount"] ? formData["actAmount"] : formData["tmpAmount"];
              }
              if (field.name == "actual_currency") {
                value = formData["currency"];
              }
            }
            if (isWithdrawal) {
              if (field.name == "actual_amount") {
                value = formData["actDepositAmount"]
                  ? formData["actDepositAmount"]
                  : formData["tmpDepositAmount"];
              }
              if (field.name == "actual_currency") {
                value = formData["settleCurrency"];
              }
            }
          } else if (field.name == "platform_amount") {
            if (isDeposit) {
              value = formData["actDepositAmount"]
                ? formData["actDepositAmount"]
                : formData["tmpDepositAmount"];
            }
            if (isWithdrawal) {
              value = formData["actAmount"] ? formData["actAmount"] : formData["tmpAmount"];
            }
          } else if (field.name == "service_charge") {
            value = formData["actCharge"] ? formData["actCharge"] : formData["tmpCharge"];
          } else if (field.name == "bank_location") {
            value = formData["bankLocation"]?.nameEn;
          } else if (field.name == "bank_province") {
            value = formData["bankProvince"]?.nameEn;
          } else if (field.name == "bank_region") {
            value = formData["bankRegion"]?.nameEn;
          } else if (field.name == "bank_city") {
            value = formData["bankCity"]?.nameEn;
          } else {
            value =
              typeof formData[field.id] === "number" || typeof formData[field.id] === "boolean"
                ? formData[field.id].toString()
                : formData[field.id];
          }
          Object.assign(current, { [field.name]: value });
        }
      });
      setInitialData(current);
    }
  }, []);

  return (
    <DetailContentForm
      formData={formData}
      initialData={initialData}
      fields={formFields}
      validations={formValidations}
      onCancel={() => setFormData(null)}
    />
  );
};

export default PaymentForm;
