import { FC, useCallback, useEffect, useState } from "react";
import TableList from "@/components/TableList";
import { EDDA_BANK_ACTION, EDDA_BANK_EXPORT_HEADING, EDDA_BANK_HEADER } from "@/config/tables/edda-records";
import { useQuery } from "react-query";
import {
  getEddaRecord,
  getEntityId,
  getBankEddaCode,
  getBankMaster,
  getEddaStatus
} from "@/services/common-services";
import moment from "moment";
import { FindIndexFormField } from "@/config/utilities";
import toast from "react-hot-toast";

export const EDDA_BANK_FILTER: any = [
  { id: "entity", name: "entityId", type: "dropdown", options: [] },
  { id: "bank_name", name: "bankName", type: "dropdown", options: [] },
  { id: "trading_acc_no", name: "tradingAccountNo", type: "text" },
  { id: "last_update_date", name: "lastModifiedDate", type: "time-range", options: [] },
  { id: "edda_recepient_bank", name: "eddaRecipientBank", type: "dropdown", options: [] },
  { id: "status", name: "status", type: "dropdown", options: [] },
  {
    name: "includeDummyAccount",
    id: "dummy_account",
    type: "dropdown",
    options: [
      { value: "true", label: "Include" },
      { value: "false", label: "Exclude" }
    ]
  }
];

const EddaListing: FC = () => {
  const [, updateState] = useState<any>();
  const forceUpdate = useCallback(() => updateState({}), []);
  const [filterData, setFilterData] = useState({
    entityId: null,
    customerAccountNo: null,
    eddaRecipientBank: null,
    bankName: null,
    tradingAccountNo: null,
    lastModifiedDateFrom: null,
    lastModifiedDateTo: null,
    status: null,
    includeDummyAccount: false
  });

  const { data: entityData } = useQuery(["get-entity"], getEntityId);
  const { data: bankMaster } = useQuery(["get-bank-master"], getBankMaster);
  const { data: eddBankCode } = useQuery(["get-edda-bank-code"], getBankEddaCode);
  const { data: status } = useQuery(["get-status"], getEddaStatus);
  const {
    data: eddaRecord,
    isFetching: fetchingRecord,
    refetch
  } = useQuery(["get-edda-record", filterData], async () => {
    const { response } = await getEddaRecord(filterData);
    return response;
  });

  useEffect(() => {
    if (entityData) {
      let items: { label: string; value: any }[] = [];
      entityData?.response[0].items.forEach(function (val: any) {
        let item = { label: "", value: "" };
        item.label = val.name;
        item.value = val.codeCode;
        items.push(item);
      });
      EDDA_BANK_FILTER[0].options = items;
      forceUpdate();
    }
  }, [entityData]);

  useEffect(() => {
    if (eddBankCode) {
      const options = eddBankCode.response[0].items.map((el: any) => ({
        value: el.codeCode,
        label: el.name
      }));
      EDDA_BANK_FILTER[FindIndexFormField(EDDA_BANK_FILTER, "eddaRecipientBank")].options = options;
      forceUpdate();
    }
  }, [eddBankCode]);

  useEffect(() => {
    if (bankMaster) {
      const options = bankMaster.response.map((el: any) => ({
        value: el.bankNameSys,
        label: el.bankNameSys
      }));
      EDDA_BANK_FILTER[FindIndexFormField(EDDA_BANK_FILTER, "bankName")].options = options;
      forceUpdate();
    }
  }, [bankMaster]);

  useEffect(() => {
    if (status) {
      const options = status.response[0].items.map((el: any) => ({
        value: el.codeInt,
        label: el.name
      }));
      EDDA_BANK_FILTER[FindIndexFormField(EDDA_BANK_FILTER, "status")].options = options;
      forceUpdate();
    }
  }, [status]);

  const handleDoFilterData = (values: any) => {
    let filter: any = filterData;
    Object.keys(values).forEach(function (key) {
      if (key == "lastModifiedDate_start") {
        filter["lastModifiedDateFrom"] = moment(values[key]).format("Y-MM-DD HH:mm:ss");
      } else if (key == "lastModifiedDate_end") {
        filter["lastModifiedDateTo"] = moment(values[key]).format("Y-MM-DD HH:mm:ss");
      } else if (key == "includeDummyAccount") {
        filter["includeDummyAccount"] = values[key] == "true" ? true : false;
      } else if (key == "status") {
        filter["status"] = typeof values[key] == "number" ? values[key] : null;
      } else {
        filter[key] = values[key] || typeof values[key] == "number" ? values[key] : null;
      }
    });
    setFilterData({
      ...filter
    });
    refetch();
  };

  return (
    <TableList
      header={EDDA_BANK_HEADER}
      filter={EDDA_BANK_FILTER}
      filterData={filterData}
      action={EDDA_BANK_ACTION}
      data={eddaRecord}
      exportDataObject={{ heading: EDDA_BANK_EXPORT_HEADING }}
      isLoading={fetchingRecord}
      onFilterData={(values: any) => handleDoFilterData(values)}
    />
  );
};

export default EddaListing;
