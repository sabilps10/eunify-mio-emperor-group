import { FC, useState } from "react";

import TableList from "@/components/TableList";
import { useQuery } from "react-query";
import { getAddressMappingList, getRegionList } from "@/services/common-services";
import { KEY_DISTRICT_ACTION, KEY_DISTRICT_HEADER } from "@/config/tables/key-district";

const RegionListing: FC = () => {
  const [filterData] = useState({
    isActive: true
  });
  const { data: regionRecord, isFetching: fetchingRecord } = useQuery(
    ["get-address-map", filterData],
    async () => {
      const { response } = await getRegionList(filterData);
      return response;
    },
    { keepPreviousData: true }
  );

  return (
    <TableList
      header={KEY_DISTRICT_HEADER}
      action={KEY_DISTRICT_ACTION}
      data={regionRecord}
      isLoading={fetchingRecord}
    />
  );
};

export default RegionListing;
