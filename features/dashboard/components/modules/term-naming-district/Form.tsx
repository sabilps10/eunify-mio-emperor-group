import { FC, useCallback, useEffect, useState } from "react";

import useFormContent from "@/features/dashboard/hooks/useFormContent";
import { Router, useRouter } from "next/router";
import DetailContentForm from "../../DetailContentForm";
import useFormData from "@/hooks/useFormData";
import { useMutation, useQuery } from "react-query";
import { getAddressMappingList, getProvinceList, postRegion, putRegion } from "@/services/common-services";
import useLocalStorage from "@/hooks/useLocalStorage";
import { LOCALSTORAGE_KEY } from "@/config/constants";
import { array, object, string, number } from "zod";

export const EditRegionFields: any = [
  { id: "addressId", name: "address_id", type: "text", disabled: false, hidden: true, payload_skipped: true },
  { id: "provinceId", name: "province_id", type: "text", disabled: true, hidden: true },
  { id: "provinceNameEn", name: "linked_province", type: "text", disabled: true },
  { id: "nameEn", name: "en_display_name", type: "text", disabled: false },
  { id: "nameTc", name: "tc_display_name", type: "text", disabled: false },
  { id: "nameSc", name: "sc_display_name", type: "text", disabled: false }
];

export const AddRegionFields: any = [
  {
    id: "provinceId",
    name: "linked_province",
    type: "select",
    autocomplete: true,
    options: [],
    disabled: false
  },
  { id: "nameEn", name: "en_display_name", type: "text", disabled: false },
  { id: "nameTc", name: "tc_display_name", type: "text", disabled: false },
  { id: "nameSc", name: "sc_display_name", type: "text", disabled: false }
];

export const AddRegionSchema = object({
  linked_province: string().min(1, { message: "Linked Province is required" }),
  en_display_name: string().min(1, { message: "EN Display Name is required" }),
  tc_display_name: string().min(1, { message: "TC Display Name is required" }),
  sc_display_name: string().min(1, { message: "SC Display Name is required" })
});

export const EditRegionSchema = object({
  address_id: number().min(1),
  province_id: number().min(1),
  linked_province: string().min(1, { message: "Linked Province is required" }),
  en_display_name: string().min(1, { message: "EN Display Name is required" }),
  tc_display_name: string().min(1, { message: "TC Display Name is required" }),
  sc_display_name: string().min(1, { message: "SC Display Name is required" })
});

type createRegionVariables = {
  payload: any;
};

type updateRegionVariables = {
  id: any;
  payload: any;
};

const RegionForm: FC = () => {
  const router = useRouter();
  const [, updateState] = useState<any>();
  const { getItem } = useLocalStorage();
  const account = getItem(LOCALSTORAGE_KEY.ACCOUNT);
  const forceUpdate = useCallback(() => updateState({}), []);
  const [initialData, setInitialData] = useState(null);
  const { formFields, formValidations, setFormValue } = useFormContent();
  const { data: formData, setFormData } = useFormData();

  const { data: provinceRecord, isFetching: fetchingRecord } = useQuery(["get-province-list"], async () => {
    const { response } = await getProvinceList({
      isActive: true,
      locationId: "1"
    });
    return response;
  });

  const { mutate: createRegion } = useMutation(({ payload }: createRegionVariables) => postRegion(payload));
  const { mutate: updateRegion } = useMutation(({ id, payload }: updateRegionVariables) =>
    putRegion(id, payload)
  );

  useEffect(() => {
    if (router.query.page == "add" && provinceRecord) {
      let items: { label: string; value: any }[] = [];
      provinceRecord.forEach(function (val: any) {
        let item = { label: "", value: "" };
        item.label = val.nameEn || "null";
        item.value = String(val.addressId);
        items.push(item);
      });
      AddRegionFields[0].options = items;
      forceUpdate();
    }
  }, [provinceRecord]);

  useEffect(() => {
    if (!formFields) {
      if (router.query.page == "add") {
        setFormValue("fields", AddRegionFields);
        setFormValue("validations", AddRegionSchema);
      }
      if (router.query.page == "detail") {
        setFormValue("fields", EditRegionFields);
        setFormValue("validations", EditRegionSchema);
      }
    }
    if (formData && router.query.page == "detail") {
      let current: any = {};
      EditRegionFields.map((val: any) => {
        if (formData[val.id] || val.id == "custom_value") {
          let value = null;
          if (val.name == "location_id") {
            value = 1;
          } else {
            value = formData[val.id];
          }
          Object.assign(current, { [val.name]: value });
        }
      });
      setInitialData(current);
    }
  }, []);

  const handleSubmitForm = (values: any) => {
    const data = {};
    const Fields = router.query.page == "add" ? AddRegionFields : EditRegionFields;
    Fields.forEach(function (field: any) {
      let value = null;
      if (!values[field.name] && initialData) {
        value = initialData[field.name];
      } else {
        value = values[field.name];
      }
      if (value && !field.payload_skipped) {
        const key = field.id;
        Object.assign(data, { [key]: value });
      }
    });
    Object.assign(data, { isActive: true });
    if (router.query.page == "add") {
      createRegion(
        { payload: { ...data, requestBy: account } },
        {
          onSuccess: (data) => {
            router.push(`/${router.query.feature}/management`);
          }
        }
      );
    }
    if (router.query.page == "detail") {
      updateRegion(
        { id: values.address_id, payload: { ...data, requestBy: account } },
        {
          onSuccess: (data) => {
            router.push(`/${router.query.feature}/management`);
          }
        }
      );
    }
  };

  return (
    <DetailContentForm
      formData={formData}
      initialData={initialData}
      fields={formFields}
      validations={formValidations}
      onSubmit={handleSubmitForm}
      onCancel={() => setFormData(null)}
    />
  );
};

export default RegionForm;
