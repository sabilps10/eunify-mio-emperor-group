import { FC, useCallback, useEffect, useState } from "react";

import useFormContent from "@/features/dashboard/hooks/useFormContent";
import { Router, useRouter } from "next/router";
import DetailContentForm from "../../DetailContentForm";
import useFormData from "@/hooks/useFormData";
import { useMutation, useQuery } from "react-query";
import { getAddressMappingList, getCityList, postCity, putCity } from "@/services/common-services";
import useLocalStorage from "@/hooks/useLocalStorage";
import { LOCALSTORAGE_KEY } from "@/config/constants";
import { array, object, string, number } from "zod";

export const EditCityFields: any = [
  { id: "addressId", name: "address_id", type: "text", disabled: false, hidden: true, payload_skipped: true },
  { id: "regionId", name: "region_id", type: "text", hidden: true },
  { id: "regionNameEn", name: "linked_district", type: "text", disabled: true, payload_skipped: true },
  { id: "nameEn", name: "en_display_name", type: "text", disabled: false },
  { id: "nameTc", name: "tc_display_name", type: "text", disabled: false },
  { id: "nameSc", name: "sc_display_name", type: "text", disabled: false }
];

export const AddCityFields: any = [
  {
    id: "regionId",
    name: "linked_district",
    type: "select",
    autocomplete: true,
    options: [],
    disabled: false
  },
  { id: "nameEn", name: "en_display_name", type: "text", disabled: false },
  { id: "nameTc", name: "tc_display_name", type: "text", disabled: false },
  { id: "nameSc", name: "sc_display_name", type: "text", disabled: false }
];

export const AddCitySchema = object({
  linked_district: number().min(1, { message: "Linked Region/District is required" }),
  en_display_name: string().min(1, { message: "EN Display Name is required" }),
  tc_display_name: string().min(1, { message: "TC Display Name is required" }),
  sc_display_name: string().min(1, { message: "SC Display Name is required" })
});

export const EditCitySchema = object({
  region_id: number().min(1),
  address_id: number().min(1),
  linked_district: string().min(1, { message: "Linked Region/District is required" }),
  en_display_name: string().min(1, { message: "EN Display Name is required" }),
  tc_display_name: string().min(1, { message: "TC Display Name is required" }),
  sc_display_name: string().min(1, { message: "SC Display Name is required" })
});

type createCityVariables = {
  payload: any;
};

type updateCityVariables = {
  id: number;
  payload: any;
};

const CityForm: FC = () => {
  const router = useRouter();
  const [, updateState] = useState<any>();
  const { getItem } = useLocalStorage();
  const account = getItem(LOCALSTORAGE_KEY.ACCOUNT);
  const forceUpdate = useCallback(() => updateState({}), []);
  const [initialData, setInitialData] = useState(null);
  const { formFields, formValidations, setFormValue } = useFormContent();
  const { data: formData, setFormData } = useFormData();

  const { data: regionRecord, isFetching: fetchingRecord } = useQuery(["get-address-list"], async () => {
    const { response } = await getCityList({
      isActive: true,
      locationId: "1"
    });
    return response;
  });
  const { mutate: createCity } = useMutation(({ payload }: createCityVariables) => postCity(payload));
  const { mutate: updateCity } = useMutation(({ id, payload }: updateCityVariables) => putCity(id, payload));

  useEffect(() => {
    if (regionRecord) {
      let items: { label: string; value: any }[] = [];
      regionRecord.forEach(function (val: any) {
        let item = { label: "", value: "" };
        item.label = val.nameEn || "null";
        item.value = val.addressId;
        items.push(item);
      });
      AddCityFields[0].options = items;
      forceUpdate();
    }
  }, [regionRecord]);

  useEffect(() => {
    if (!formFields) {
      if (router.query.page == "add") {
        setFormValue("fields", AddCityFields);
        setFormValue("validations", AddCitySchema);
      }
      if (router.query.page == "detail") {
        setFormValue("fields", EditCityFields);
        setFormValue("validations", EditCitySchema);
      }
    }
    if (formData) {
      let current: any = {};
      EditCityFields.map((val: any) => {
        if (formData[val.id]) {
          let value = null;
          value = formData[val.id];
          Object.assign(current, { [val.name]: value });
        }
      });
      setInitialData(current);
    }
  }, []);

  const handleSubmitForm = (values: any) => {
    const data = {};
    const Fields = router.query.page == "add" ? AddCityFields : EditCityFields;
    Fields.forEach(function (field: any) {
      let value = null;
      if (!values[field.name] && initialData) {
        value = initialData[field.name];
      } else {
        value = values[field.name];
      }
      if (value && !field.payload_skipped) {
        const key = field.id;
        Object.assign(data, { [key]: value });
      }
    });
    Object.assign(data, { isActive: true });
    if (router.query.page == "add") {
      createCity(
        { payload: { ...data, requestBy: account } },
        {
          onSuccess: (data) => {
            router.push(`/${router.query.feature}/management`);
          }
        }
      );
    }
    if (router.query.page == "detail") {
      updateCity(
        { id: values.address_id, payload: { ...data, requestBy: account } },
        {
          onSuccess: (data) => {
            router.push(`/${router.query.feature}/management`);
          }
        }
      );
    }
  };

  return (
    <DetailContentForm
      formData={formData}
      initialData={initialData}
      fields={formFields}
      validations={formValidations}
      onSubmit={handleSubmitForm}
      onCancel={() => setFormData(null)}
    />
  );
};

export default CityForm;
