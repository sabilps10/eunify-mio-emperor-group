import { FC, useState } from "react";

import TableList from "@/components/TableList";
import { useQuery } from "react-query";
import { getAddressMappingList, getCityList } from "@/services/common-services";
import { KEY_DISTRICT_ACTION, KEY_DISTRICT_HEADER } from "@/config/tables/key-district";
import { KEY_CITY_ACTION, KEY_CITY_HEADER } from "@/config/tables/key-city";

const CityListing: FC = () => {
  const [filterData] = useState({
    isActive: true
  });

  const { data: cityRecord, isFetching: fetchingRecord } = useQuery(
    ["get-city-list", filterData],
    async () => {
      const { response } = await getCityList(filterData);
      return response;
    }
  );

  return (
    <TableList
      header={KEY_CITY_HEADER}
      action={KEY_CITY_ACTION}
      data={cityRecord}
      isLoading={fetchingRecord}
    />
  );
};

export default CityListing;
