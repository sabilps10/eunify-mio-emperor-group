import { FC, useEffect, useState } from "react";
import { useQuery } from "react-query";

import { getPaymentChannelStatus } from "@/services/common-services";
import TableList from "@/components/TableList";

import {
  USER_MANAGEMENT_ACTION,
  USER_MANAGEMENT_HEADER,
  USER_MANAGEMENT_EXPORT_HEADING
} from "@/config/tables/user-management";
import { getUserAssignment } from "@/services/security-services";

const USER_MANAGEMENT_FILTER = [
  { id: "name", name: "userName", type: "text" },
  {
    id: "status",
    name: "status",
    type: "dropdown",
    options: [
      { label: "Active", value: "true" },
      { label: "Inactive", value: "false" }
    ]
  }
];

const exportToFileObject = { heading: USER_MANAGEMENT_EXPORT_HEADING };

const UserManagementListing: FC = () => {
  const [filterData, setFilterData] = useState({
    userName: null,
    status: null
  });
  const { data: userRecord, isFetching: fetchingRecord } = useQuery(
    ["get-user-record", filterData],
    async () => {
      const { response } = await getUserAssignment({ ...filterData });
      return response;
    },
    { keepPreviousData: true }
  );

  const handleDoFilterData = (values: any) => {
    let filter: any = filterData;
    Object.keys(values).forEach(function (key) {
      if (key == "status") {
        filter[key] = values[key] === "true" ? true : values[key] === "false" ? false : null;
      } else {
        filter[key] = values[key] ? values[key] : null;
      }
    });
    console.log(filter);
    setFilterData({ ...filter });
  };

  return (
    <TableList
      header={USER_MANAGEMENT_HEADER}
      filter={USER_MANAGEMENT_FILTER}
      action={USER_MANAGEMENT_ACTION}
      data={userRecord}
      isLoading={fetchingRecord}
      onFilterData={(values: any) => handleDoFilterData(values)}
      exportDataObject={exportToFileObject}
    />
  );
};

export default UserManagementListing;
