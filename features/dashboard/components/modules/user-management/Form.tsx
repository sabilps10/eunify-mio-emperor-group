import { FC, useEffect, useMemo, useState } from "react";
import { useRouter } from "next/router";
import { useMutation, useQuery } from "react-query";
import { any, array, object, string, boolean } from "zod";
import debouce from "lodash.debounce";

import { getPaymentChannelStatus } from "@/services/common-services";
import { addUserManagement, editUserManagement } from "@/services/security-services";

import useFormData from "@/hooks/useFormData";
import useFormContent from "@/features/dashboard/hooks/useFormContent";
import DetailContentForm from "../../DetailContentForm";
import { getUserName, getEmailAvailable } from "@/services/security-services";
import { LOCALSTORAGE_KEY } from "@/config/constants";
import useLocalStorage from "@/hooks/useLocalStorage";

type updateUserManagementVariables = {
  id: number;
  payload: any;
};

const UserManagementForm: FC = () => {
  const {
    push,
    query: { page, feature, id }
  } = useRouter();
  const { data: status } = useQuery(["get-status-payment-channel"], getPaymentChannelStatus);
  const { mutate } = useMutation((payload: any) => addUserManagement(payload));
  const { mutate: edit } = useMutation(({ id, payload }: updateUserManagementVariables) =>
    editUserManagement(id, payload)
  );
  const { formFields, formValidations, setFormValue } = useFormContent();
  const { data: formData, setFormData } = useFormData();
  const [initialData, setInitialData] = useState({
    name: "",
    email: "",
    status: ""
  });
  const [disableButton, setDisableButton] = useState(false);

  const editUserManagementFields = [
    { id: "userName", name: "name", type: "text", disabled: true },
    { id: "email", name: "email", type: "text", disabled: true, },
    {
      id: "isActive",
      name: "status",
      type: "select",
      options: status?.response[0].items.map((el: any) => ({
        value: el.codeInt,
        label: el.name
      })),
      disabled: false
    },
    { id: "roleList", name: "entity_role", type: "custom_fields", disabled: false }
  ];

  const addUserManagementFields = [
    { id: "userName", name: "name", type: "text", disabled: true },
    {
      id: "email",
      name: "email",
      type: "text",
      disabled: false,
      callback: {
        enabled: ["email"],
        action: "parentCallback",
        function: "debouncedAmountInput(values, methods)"
      }
    },
    {
      id: "isActive",
      name: "status",
      type: "select",
      options: [],
      disabled: false
    },
    { id: "roleList", name: "entity_role", type: "custom_fields", disabled: false }
  ];

  const UserManagementSchema = object({
    name: string().min(1, { message: "Username is required" }),
    email: string().min(1, { message: "Email is required" }),
    status: string().min(1, { message: "Status is required" }),
    entity_role: array(
      object({
        entityId: string().min(1, { message: "entity is required" }),
        roleId: string().min(1, { message: "Role is required" })
      })
    )
  });

  useEffect(() => {
    if (!formFields && status) {
      let fields = [];

      if (page === "add") fields = [...addUserManagementFields];
      else fields = [...editUserManagementFields];

      fields = fields.map((data) => {
        return data.id === "isActive"
          ? {
              ...data,
              options: status?.response[0].items.map((el: any) => ({
                value: String(el.codeInt),
                // value: String(el.codeInt),
                label: el.name
              }))
            }
          : data;
      });

      setFormValue("fields", fields);

      setFormValue("validations", UserManagementSchema);
    }

    if (formData) {
      let current: any = initialData;
      editUserManagementFields.map((field: any) => {
        if (field.id === "isActive") {
          current[field.name] = formData[field.id] ? "0" : "1";
        } else {
          current[field.name] = formData[field.id];
        }
      });
      setInitialData(current);
    }
  }, [page, status]);

  const { mutate: fetchEmail } = useMutation(async (email: any) => getUserName(email));
  const { mutate: fetchEmailAvailable } = useMutation((email: any) => getEmailAvailable(email));

  const handleGetUserName = (formValue: any, formMethods: any) => {
    fetchEmail(formValue?.email, {
      onSuccess: (data) => {
        if (data?.errorReasonCode === "400") {
          formMethods.setValue("name", "");
          formMethods.setError("email", {
            type: "custom",
            message: data.errorDescription
          });
          return;
        }
        fetchEmailAvailable(formValue?.email, {
          onSuccess: (data) => {
            if (!data?.response) {
              formMethods.setError("email", {
                type: "custom",
                message: "This email has already been registered"
              });
              setDisableButton(true);
            } else {
              formMethods.clearErrors("email");
              setDisableButton(false);
            }
          }
        });
        formMethods.setValue("name", data?.response?.userName);
      }
    });
  };

  const debouncedAmountInput = useMemo(() => {
    return debouce(handleGetUserName, 800);
  }, []);

  useEffect(() => {
    return () => {
      debouncedAmountInput.cancel();
    };
  });

  const handleSubmit = (values: any, methods: any) => {
    let isStatus = values.status === "0" ? true : false;
    const { getItem } = useLocalStorage();
    const account = getItem(LOCALSTORAGE_KEY.ACCOUNT);
    if (page === "add") {
      mutate(
        {
          adAcc: values.email.split("@")[0],
          userName: values.name,
          email: values.email,
          status: isStatus,
          department: "",
          company: "",
          roleList: values.entity_role,
          requestBy: account
        },
        {
          onSuccess: () => {
            push(`/${feature}/management`);
          }
        }
      );
    } else {
      edit(
        {
          id: formData.userId,
          payload: {
            adAcc: values.email.split("@")[0],
            department: formData?.company || "",
            company: formData?.department || "",
            userName: values.name,
            email: values.email,
            isActive: isStatus,
            roleList: values.entity_role,
            requestBy: account
          }
        },
        {
          onSuccess: () => {
            push(`/${feature}/management`);
          }
        }
      );
    }
  };

  return (
    <DetailContentForm
      initialData={initialData}
      fields={formFields}
      validations={formValidations}
      isDisabledButton={disableButton}
      onSubmit={handleSubmit}
      onCancel={() => setFormData(null)}
      parentCallback={(func: any, values: any, methods: any) => {
        eval(func);
      }}
      onSelectCallback={(func: any, values: any, value: any, methods: any) => {
        eval(func);
      }}
    />
  );
};

export default UserManagementForm;
