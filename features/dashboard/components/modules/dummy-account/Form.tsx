import { FC, useEffect, useState, useCallback } from "react";
import { useRouter } from "next/router";
import { useMutation, useQuery } from "react-query";
import { object, string } from "zod";
import debounce from "lodash.debounce";

import { addDummyAccount as postDummyAccount, getAccounts } from "@/services/common-services";
import { getEntityId } from "@/services/common-services";
import useFormContent from "@/features/dashboard/hooks/useFormContent";
import DetailContentForm from "../../DetailContentForm";
import useLocalStorage from "@/hooks/useLocalStorage";
import { LOCALSTORAGE_KEY } from "@/config/constants";

const DummyAccountForm: FC = () => {
  const {
    push,
    query: { page, feature }
  } = useRouter();
  const [, updateState] = useState<any>();
  const forceUpdate = useCallback(() => updateState({}), []);
  const { data } = useQuery(["get-entity"], getEntityId);
  const { mutate } = useMutation((payload: any) => postDummyAccount(payload));
  const { mutate: fetchAccountDetail, isLoading: fetchingAccount } = useMutation((payload: any) =>
    getAccounts(payload)
  );
  const { getItem } = useLocalStorage();
  const account = getItem(LOCALSTORAGE_KEY.ACCOUNT);
  const { formFields, formValidations, setFormValue } = useFormContent();
  const [initialData, setInitialData] = useState({});

  const AddDummyAccountField = [
    {
      name: "entity",
      type: "select",
      options: [],
      disabled: false
    },
    {
      name: "dummy_acc_no",
      type: "text",
      disabled: true,
      disabled_condition: {
        name: "entity",
        value: "has_value",
        disable: false
      },
      callback: {
        enabled: ["entity", "dummy_acc_no"],
        action: "parentCallback",
        function: "getAccountDetails(values, methods)"
      }
    }
  ];

  const AddDummyAccountSchema = object({
    entity: string().min(1, { message: "Entity is required" }),
    dummy_acc_no: string().min(1, { message: "Dummy Account Number is required" })
  });

  useEffect(() => {
    if (!formFields) {
      setFormValue("fields", AddDummyAccountField);
      setFormValue("validations", AddDummyAccountSchema);
    }
  }, []);

  useEffect(() => {
    if (data) {
      const options = data?.response[0].items.map((el: any) => ({
        value: el.codeCode,
        label: el.codeCode
      }));
      AddDummyAccountField[0].options = options;
      setFormValue("fields", AddDummyAccountField);
      forceUpdate();
    }
  }, [data, forceUpdate]);

  const getAccountDetails = debounce((formValues: any, formMethods: any) => {
    fetchAccountDetail(
      {
        entityId: formValues.entity,
        tradingAccountNo: formValues.dummy_acc_no
      },
      {
        onSuccess: (res) => {
          if (!res.result) {
            formMethods.setError("dummy_acc_no", {
              type: "custom",
              message: "Account does not exist under this entity"
            });
          }
        }
      }
    );
  }, 1000);

  const handleSubmit = (values: any) => {
    mutate(
      {
        tradingAccountNo: values.dummy_acc_no,
        entityId: values.entity,
        requestBy: account
      },
      {
        onSuccess: (data) => {
          push(`/${feature}/management`);
        }
      }
    );
  };

  return (
    <DetailContentForm
      fields={formFields}
      validations={formValidations}
      onSubmit={handleSubmit}
      parentCallback={(func: any, values: any, methods: any) => {
        eval(func);
      }}
      fetchingRequest={fetchingAccount}
    />
  );
};

export default DummyAccountForm;
