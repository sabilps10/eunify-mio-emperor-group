import { FC, useState } from "react";
import { useMutation, useQuery } from "react-query";

import { getDummyAccounts, editDummyAccount, postDummyAccountsPage } from "@/services/common-services";
import TableList from "@/components/TableList";
import useLocalStorage from "@/hooks/useLocalStorage";
import { LOCALSTORAGE_KEY } from "@/config/constants";
import { DUMMY_ACCOUNT_ACTION, DUMMY_ACCOUNT_HEADER } from "@/config/tables/dummy-account";
import usePagination from "@/hooks/usePagination";

const DummyAccountListing: FC = () => {
  const { data: dummyAccount, isLoading, refetch } = useQuery(["get-dummy-account"], getDummyAccounts);
  const { mutate } = useMutation((payload: any) => editDummyAccount(payload, payload.id));
  const { getItem } = useLocalStorage();
  const account = getItem(LOCALSTORAGE_KEY.ACCOUNT);

  const [filterPagination, setFilterPagination, pagination, setPagination] = usePagination({});
  const [dataRecord, setDataRecord] = useState<any>([]);

  const { isFetching: fetchingRecord } = useQuery(
    ["get-dummy-account-page", filterPagination],
    ({ queryKey }) => {
      return postDummyAccountsPage(queryKey[1]);
    },
    {
      onSuccess({ response }) {
        const { content, ...rest } = response;
        setDataRecord(content);
        setPagination(rest);
      }
    }
  );

  const handleRemove = (data: any) => {
    mutate(
      {
        id: data.id,
        entityId: data.entityId,
        tradingAccountNo: data.tradingAccountNo,
        isActive: false,
        requestBy: account
      },
      {
        onSuccess: () => {
          refetch();
        }
      }
    );
  };

  return (
    <TableList
      header={DUMMY_ACCOUNT_HEADER}
      action={DUMMY_ACCOUNT_ACTION}
      data={dataRecord}
      rowActionCallback={(values: any) => handleRemove(values)}
      isLoading={fetchingRecord}
      isPagination={true}
      pagination={pagination}
      handlePagination={(val) => {
        setFilterPagination({ ...filterPagination, ...val });
      }}
    />
  );
};

export default DummyAccountListing;
