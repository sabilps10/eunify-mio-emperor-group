import { FC } from "react";
import { useQuery } from "react-query";

import { getRoleAssignments } from "@/services/security-services";
import TableList from "@/components/TableList";

import { ROLE_SETTINGS_ACTION, ROLE_SETTINGS_HEADER } from "@/config/tables/role-settings";

const RoleSettingsListing: FC = () => {
  const { data, isLoading } = useQuery(["get-role-assignment"], () => getRoleAssignments({ roleId: null }));

  return (
    <TableList
      header={ROLE_SETTINGS_HEADER}
      action={ROLE_SETTINGS_ACTION}
      data={data?.response}
      isLoading={isLoading}
    />
  );
};

export default RoleSettingsListing;
