import { FC, useEffect, useState } from "react";
import { any, array, object, string } from "zod";
import { useMutation } from "react-query";

import useFormData from "@/hooks/useFormData";
import useFormContent from "@/features/dashboard/hooks/useFormContent";
import { useRouter } from "next/router";
import DetailContentForm from "../../DetailContentForm";
import { createRoleAssignments, editRoleAssignments } from "@/services/security-services";

type updateEditRoleAssignmentVariables = {
  id: number;
  payload: any;
};

const RoleSettingForm: FC = () => {
  const {
    push,
    query: { page, feature, id }
  } = useRouter();

  const { mutate: add, isLoading: isLoadingAdd } = useMutation((payload: any) =>
    createRoleAssignments(payload)
  );

  const { mutate: edit, isLoading: isLoadingEdit } = useMutation((payload: any) =>
    editRoleAssignments(payload)
  );

  const { formFields, formValidations, setFormValue } = useFormContent();
  const { data: formData, setFormData } = useFormData();
  const [initialData, setInitialData] = useState({
    role_name: "",
    role_permissions: []
  });

  const AddRoleSettingsFields = [
    { name: "role_name", type: "text", disabled: false },
    { name: "role_permissions", type: "custom_fields", disabled: false }
  ];

  const EditRoleSettingsFields = [
    { id: "roleNameEn", name: "role_name", type: "text", disabled: true },
    { id: "funcList", name: "role_permissions", type: "custom_fields", disabled: false }
  ];

  const RoleSettingSchema = object({
    role_name: string().min(1, { message: "Role Name is required" }),
    role_permissions: any().array()
  });

  useEffect(() => {
    if (!formFields) {
      if (page === "add") {
        setFormValue("fields", AddRoleSettingsFields);
      } else {
        setFormValue("fields", EditRoleSettingsFields);
      }
      setFormValue("validations", RoleSettingSchema);
    }

    if (formData) {
      let current: any = initialData;
      EditRoleSettingsFields.map((field: any) => {
        current[field.name] = formData[field.id];
      });
      setInitialData(current);
    }
  }, []);

  const handleSubmit = (data: any) => {
    const payload = {
      roleNameEn: data.role_name,
      roleNameTc: data.role_name,
      roleNameSc: data.role_name,
      funcList: data.role_permissions
    };
    if (page === "add") {
      add(payload, {
        onSuccess: (res) => {
          if (res.result) {
            push(`/${feature}/management`);
          }
        }
      });
    } else {
      edit(
        { id: formData.roleId, payload },
        {
          onSuccess: (res) => {
            if (res.result) {
              push(`/${feature}/management`);
            }
          }
        }
      );
    }
  };

  return (
    <DetailContentForm
      initialData={initialData}
      fields={formFields}
      validations={formValidations}
      onSubmit={handleSubmit}
      fetchingRequest={isLoadingAdd}
      onCancel={() => setFormData(null)}
    />
  );
};

export default RoleSettingForm;
