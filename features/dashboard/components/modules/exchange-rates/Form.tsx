import { FC, useCallback, useEffect, useState } from "react";

import useFormContent from "@/features/dashboard/hooks/useFormContent";
import DetailContentForm from "../../DetailContentForm";
import { object, string, number } from "zod";
import { useMutation, useQuery } from "react-query";
import { getEntityId, getExchangeType, putExchangeRate } from "@/services/common-services";
import useFormData from "@/hooks/useFormData";
import { useRouter } from "next/router";
import useLocalStorage from "@/hooks/useLocalStorage";
import { LOCALSTORAGE_KEY } from "@/config/constants";

export const EditExchangeRatesFields: any = [
  { id: "entityId", name: "entity", type: "select", disabled: true },
  { id: "quoteCurrency", name: "quote_currency", type: "text", disabled: true },
  { id: "baseCurrency", name: "base_currency", type: "text", disabled: true },
  { id: "exchangeType", name: "exchange_type", options: [], type: "select", disabled: true },
  {
    id: "platformDepositRate",
    condition: {
      name: "exchange_type",
      value: "PER"
    },
    name: "platform_deposit_rate",
    type: "text",
    disabled: false
  },
  {
    id: "platformWithdrawRate",
    condition: {
      name: "exchange_type",
      value: "PER"
    },
    name: "platform_withdrawal_rate",
    type: "text",
    disabled: false
  },
  {
    id: "depositRate",
    condition: {
      name: "exchange_type",
      value: "BER"
    },
    name: "bank_deposit_rate",
    type: "text",
    disabled: false
  },
  {
    id: "withdrawalRate",
    condition: {
      name: "exchange_type",
      value: "BER"
    },
    name: "bank_withdrawal_rate",
    type: "text",
    disabled: false
  }
];

export const EditExchangeRatesSchema: any = object({
  entity: string().min(1, { message: "Entity is required" }),
  base_currency: string().min(1, { message: "Base Currency is required" }),
  quote_currency: string().min(1, { message: "Quote Currency is required" }),
  exchange_type: string().min(1, { message: "Exchange Type Rate is required" })
});

export const PlatformRatesSchema: any = object({
  platform_deposit_rate: string().min(1, { message: "Plaform Deposit Rate is required" }),
  platform_withdrawal_rate: string().min(1, { message: "Plaform Withdrawal Rate is required" })
});

export const BankRatesSchema: any = object({
  bank_deposit_rate: string().min(1, { message: "Bank Deposit Rate is required" }),
  bank_withdrawal_rate: string().min(1, { message: "Bank Withdrawal Rate is required" })
});

type updateExchangeRateAccountVariables = {
  id: number;
  payload: any;
};

const ExchangeRateForm: FC = () => {
  const [, updateState] = useState<any>();
  const { getItem } = useLocalStorage();
  const account = getItem(LOCALSTORAGE_KEY.ACCOUNT);
  const { formFields, formValidations, setFormValue } = useFormContent();
  const { data: entityData } = useQuery(["get-entity"], getEntityId);
  const { data: exchangeType } = useQuery(["get-exchange_type"], getExchangeType);
  const { mutate: updateExchangeRate } = useMutation(({ id, payload }: updateExchangeRateAccountVariables) =>
    putExchangeRate(id, payload)
  );
  const { data: formData, setFormData } = useFormData();
  const forceUpdate = useCallback(() => updateState({}), []);

  const [initialData, setInitialData] = useState({});
  const {
    query: { feature, id },
    push
  } = useRouter();

  useEffect(() => {
    if (entityData) {
      let items: { label: string; value: any }[] = [];
      entityData?.response[0].items.forEach(function (val: any) {
        let item = { label: "", value: "" };
        item.label = val.name;
        item.value = val.codeCode;
        items.push(item);
      });
      EditExchangeRatesFields[0].options = items;
      forceUpdate();
    }
  }, [entityData]);

  useEffect(() => {
    if (exchangeType) {
      let items: { label: string; value: any }[] = [];
      exchangeType.response[0].items.forEach(function (val: any) {
        let item = { label: "", value: "" };
        item.label = val.name;
        item.value = val.codeCode;
        items.push(item);
      });
      EditExchangeRatesFields[3].options = items;
      forceUpdate();
    }
  }, [exchangeType]);

  useEffect(() => {
    if (!formFields && formData) {
      let CombinedSchema = null;
      if (formData.exchangeType.toLowerCase() == "per") {
        CombinedSchema = EditExchangeRatesSchema.merge(PlatformRatesSchema);
      }
      if (formData.exchangeType.toLowerCase() == "ber") {
        CombinedSchema = EditExchangeRatesSchema.merge(BankRatesSchema);
      }

      setFormValue("fields", EditExchangeRatesFields);
      setFormValue("validations", CombinedSchema);

      let current: any = initialData;
      EditExchangeRatesFields.map((val: any) => {
        if (formData[val.id] || typeof formData[val.id] === "boolean") {
          let value =
            typeof formData[val.id] === "number" || typeof formData[val.id] === "boolean"
              ? formData[val.id].toString()
              : formData[val.id];
          Object.assign(current, { [val.name]: value });
        }
      });

      setInitialData(current);
    }
  }, []);

  const handleSubmitForm = (values: any) => {
    let depositRate = "";
    let withdrawalRate = "";

    if (values.exchange_type.toLowerCase() == "ber") {
      depositRate = values.bank_deposit_rate;
      withdrawalRate = values.bank_withdrawal_rate;
    }
    if (values.exchange_type.toLowerCase() == "per") {
      depositRate = values.platform_deposit_rate;
      withdrawalRate = values.platform_withdrawal_rate;
    }
    const data = {
      depositRate: depositRate,
      withdrawalRate: withdrawalRate,
      exchangeType: values.exchange_type,
      isActive: true,
      additional: [],
      requestBy: account
    };
    updateExchangeRate(
      { id: formData.exchangeId, payload: data },
      {
        onSuccess: (data) => {
          push(`/${feature}/management`);
        }
      }
    );
  };

  return (
    <DetailContentForm
      initialData={initialData}
      fields={formFields}
      validations={formValidations}
      onSubmit={handleSubmitForm}
      onCancel={() => setFormData(null)}
    />
  );
};

export default ExchangeRateForm;
