import { FC, useEffect, useState } from "react";
import TableList from "@/components/TableList";
import useListingContent from "@/features/dashboard/hooks/useListingContent";

import { EXCHANGE_RATES_HEADER } from "@/config/tables/exchange-rates";
import { useQuery } from "react-query";
import { getExchangeRateList } from "@/services/common-services";
import { AnyNode } from "domhandler";

const ExchangeRatesListing: FC = () => {
  const [exchangeRateData, setExchangeRateData] = useState<any[]>([]);
  const [isLoading, setIsLoading] = useState(true);
  const { data: exchangeRate } = useQuery(
    ["get-exchange-rate"],
    async () => {
      const { response } = await getExchangeRateList(null);
      return response;
    },
    { keepPreviousData: true }
  );
  useEffect(() => {
    if (exchangeRate) {
      let items: any[] = [];
      exchangeRate.forEach((val: any, key: number) => {
        let item = { ...exchangeRate[key] };
        if (val.exchangeType.toLowerCase() == "per") {
          item["platformDepositRate"] = val.depositRate;
          item["platformWithdrawRate"] = val.withdrawalRate;
          item["depositRate"] = null;
          item["withdrawalRate"] = null;
        } else {
          item["platformDepositRate"] = null;
          item["platformWithdrawRate"] = null;
        }

        items.push(item);
      });
      setExchangeRateData(items);
      setIsLoading(false);
    }
  }, [exchangeRate]);
  return <TableList header={EXCHANGE_RATES_HEADER} data={exchangeRateData} isLoading={isLoading} />;
};

export default ExchangeRatesListing;
