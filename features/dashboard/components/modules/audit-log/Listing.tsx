import { FC, useEffect, useState, useCallback } from "react";
import TableList from "@/components/TableList";
import { AUDIT_LOG_HEADER, AUDIT_LOG_ACTION, AUDIT_LOG_EXPORT_HEADING } from "@/config/tables/audit-log";
import { useMutation, useQuery } from "react-query";
import { getAuditLogList, getAuditLogPage } from "@/services/common-services";
import { getClientFunctions, getRoleAssignments } from "@/services/security-services";
import { LOCALSTORAGE_KEY } from "@/config/constants";
import useLocalStorage from "@/hooks/useLocalStorage";
import moment from "moment";
import usePagination from "@/hooks/usePagination";

export const AUDIT_LOG_FILTER: any[] = [
  { name: "userName", id: "login", type: "text" },
  { name: "roleCode", id: "role_name", type: "dropdown", options: [] },
  { name: "ipAddr", id: "ip", type: "text" },
  { name: "dateFrom", id: "period", type: "time-range" },
  { name: "funcCode", id: "function", type: "dropdown", options: [] },
  { name: "content", id: "keyword", type: "text" }
];

type FilterData = {
  userName: string | number | null;
  funcCode: string | number | null;
  dateFrom: string | number | null;
  dateTo: string | number | null;
  content: string | number | null;
};

const defaultFilter = {
  userName: null,
  funcCode: null,
  dateFrom: null,
  dateTo: null,
  content: null
}

const AuditLogListing: FC = () => {
  const [, updateState] = useState<any>();
  const forceUpdate = useCallback(() => updateState({}), []);

  const [filterPagination, setFilterPagination, pagination, setPagination] = usePagination<FilterData>({
    userName: null,
    funcCode: null,
    dateFrom: null,
    dateTo: null,
    content: null
  });

  const { getItem } = useLocalStorage();
  const account = getItem(LOCALSTORAGE_KEY.ACCOUNT);
  const [dataRecord, setDataRecord] = useState<any>([]);
  const [dataExport, setDataExport] = useState<any>([]);

  const { mutate: mutatePage, isLoading: isLoadingPage } = useMutation((payload: any) =>
    getAuditLogPage(payload)
  );

  useEffect(() => {
    if (filterPagination?.filter && !isLoadingPage) {
      mutatePage(filterPagination, {
        onSuccess: ({ response }) => {
          const { content, ...rest } = response;
          setDataRecord(content);
          setPagination(rest);
        }
      });
    }
  }, [filterPagination]);

  const { isFetching: fetchingList } = useQuery(
    ["get-auditlog-record-list", filterPagination.filter],
    ({ queryKey }) => {
      return getAuditLogList(queryKey[1]);
    },
    {
      onSuccess({ response }) {
        setDataExport(response);
      }
    }
  );

  const { data: clientFunctions } = useQuery(
    ["get-client-function", account],
    async () => {
      const payload = { account: account, appCode: "MIO_ADMIN_PORTAL" };
      const { response } = await getClientFunctions(payload);
      return response;
    },
    { keepPreviousData: true }
  );
  const { data: roleNames } = useQuery(["get-role-names"], getRoleAssignments);

  const exportToFileObject = { heading: AUDIT_LOG_EXPORT_HEADING, data: dataExport, loading: fetchingList };

  useEffect(() => {
    if (clientFunctions) {
      let items: { label: string; value: any }[] = [];
      clientFunctions?.forEach(function (val: any) {
        if (val.child.length > 0) {
          val.child.map((x: any) => {
            let item = { label: "", value: "" };
            item.label = x.funcNameEn;
            item.value = x.funcCode;
            items.push(item);
          });
        } else {
          let item = { label: "", value: "" };
          item.label = val.funcNameEn;
          item.value = val.funcCode;
          items.push(item);
        }
      });
      AUDIT_LOG_FILTER[4].options = items;
      forceUpdate();
    }
  }, [clientFunctions]);

  useEffect(() => {
    if (roleNames) {
      let items: { label: string; value: any }[] = [];
      roleNames?.response.forEach(function (val: any) {
        let item = { label: "", value: "" };
        item.label = val.roleNameEn;
        item.value = val.roleCode;
        items.push(item);
      });
      AUDIT_LOG_FILTER[1].options = items;
      forceUpdate();
    }
  }, [roleNames]);

  const handleDoFilterData = (values: any) => {
    let filter: any = filterPagination.filter;
    Object.keys(values).forEach(function (key) {
      if (key == "dateFrom_start" || key == "dateFrom_end") {
        const splitString = key.split("_");
        if (splitString[1] == "start") {
          filter["dateFrom"] = values.dateFrom_start
            ? moment(values.dateFrom_start).format("Y-MM-DD HH:mm:ss")
            : null;
        }
        if (splitString[1] == "end") {
          filter["dateTo"] = values.dateFrom_end
            ? moment(values.dateFrom_end).format("Y-MM-DD HH:mm:ss")
            : null;
        }
      } else {
        filter[key] = values[key] ? values[key] : null;
      }
    });
    setFilterPagination({ ...filterPagination, page: 0, filter: { ...filterPagination.filter, ...filter } });
  };

  return (
    <TableList
      header={AUDIT_LOG_HEADER}
      filter={AUDIT_LOG_FILTER}
      action={AUDIT_LOG_ACTION}
      data={dataRecord}
      onFilterData={(values: any) => handleDoFilterData(values)}
      isLoading={isLoadingPage}
      exportDataObject={exportToFileObject}
      isPagination={true}
      pagination={pagination}
      handlePagination={(val) => {
        setFilterPagination({ ...filterPagination, ...val });
      }}
    />
  );
};

export default AuditLogListing;
