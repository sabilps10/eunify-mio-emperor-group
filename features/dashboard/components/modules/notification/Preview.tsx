import { FC, useState, useEffect, useCallback } from "react";
import { Button, Card, Stack, Typography } from "@mui/material";
import { useRouter } from "next/router";
import useFormData from "@/hooks/useFormData";
import useTranslate from "@/hooks/useTranslate";
import { useQuery } from "react-query";
import { getNotificationTemplate } from "@/services/notification-services";
import parse from "html-react-parser";

const NotificationManagementPreview: FC = () => {
  const router = useRouter();
  const { translate } = useTranslate();
  const { data: formData, setFormData } = useFormData();

  console.log(formData);

  const { data: notificationTemplate, isFetching } = useQuery(["get-deposit-channel", formData], async () => {
    const { response } = await getNotificationTemplate(formData.id);
    return response;
  });

  return (
    <Stack>
      <Card sx={{ mb: 10 }}>
        <Stack sx={{ pl: 3, pr: 3, mt: 2 }}>
          {notificationTemplate && notificationTemplate.type.toLowerCase() == "sms" && (
            <Typography variant={"h4"} fontWeight={700} sx={{ mb: 1 }}>
              SMS Text:{" "}
            </Typography>
          )}
          {notificationTemplate && parse(notificationTemplate?.content)}
        </Stack>
        <Stack direction={"row"} gap={2} justifyContent={"flex-end"} sx={{ pr: 2 }}>
          <Button
            onClick={function () {
              setFormData(null);
              router.push(`/${router.query.feature}/management`);
            }}
            sx={{ mt: 3, mb: 2 }}
            variant={"outlined"}
          >
            <Typography variant="body" fontWeight={700}>
              {translate("dashboard.buttons.back")}
            </Typography>
          </Button>
        </Stack>
      </Card>
    </Stack>
  );
};

export default NotificationManagementPreview;
