import { FC, useEffect, useState } from "react";
import { useQuery, useMutation } from "react-query";
import {
  NOTIFICATION_MANAGEMENT_HEADER,
  NOTIFICATION_MANAGEMENT_ACTION
} from "@/config/tables/notification-management";
import TableList from "@/components/TableList";
import { getNotificationList } from "@/services/notification-services";

const NotificationManagementListing: FC = () => {
  const [filterData, setFilterData] = useState({
    id: null,
    eventId: null,
    description: null,
    type: null
  });

  const { data: notificationRecord, isFetching: fetchingRecord } = useQuery(
    ["get-deposit-channel", filterData],
    async () => {
      const { response } = await getNotificationList(filterData);
      return response;
    },
    { keepPreviousData: true }
  );

  return (
    <TableList
      header={NOTIFICATION_MANAGEMENT_HEADER}
      action={NOTIFICATION_MANAGEMENT_ACTION}
      data={notificationRecord}
      isLoading={fetchingRecord}
    />
  );
};

export default NotificationManagementListing;
