import { FC, useState, useEffect, useCallback } from "react";
import { useRouter } from "next/router";
import { useMutation, useQuery } from "react-query";
import useFormContent from "@/features/dashboard/hooks/useFormContent";
import useFormData from "@/hooks/useFormData";
import DetailContentForm from "../../DetailContentForm";
import {
  getEventList,
  postNotificationTemplate,
  putNotificationTemplate,
  getNotificationVariableList
} from "@/services/notification-services";
import { getNotificationStatus, getNotificationType } from "@/services/common-services";
import { object, string, number } from "zod";
import TableList from "@/components/TableList";
import { Dialog, DialogContent, DialogTitle, IconButton } from "@mui/material";
import { MdClear as ClearIcon } from "react-icons/md";

const NotificationFields = [
  { name: "event_id", id: "eventId", type: "select", options: [], disabled: false },
  { name: "description", id: "description", type: "text", disabled: false },
  { name: "type", id: "type", type: "select", options: [], disabled: false },
  { name: "subject", id: "subject", type: "text", disabled: false },
  { name: "status", id: "enabled", type: "select", options: [], disabled: false },
  {
    name: "content",
    id: "content",
    type: "editor",
    options: [],
    disabled: false,
    condition: { name: "type", value: "EMAIL" }
  },
  {
    name: "content",
    id: "content",
    type: "text",
    options: [],
    disabled: false,
    condition: { name: "type", value: "SMS" }
  }
];

export const NotificationSchema: any = object({
  event_id: string().min(1, { message: "Event Id is required" }),
  description: string().min(1, { message: "Description is required" }),
  type: string().min(1, { message: "Type is required" }),
  subject: string().min(0),
  status: string().min(1, { message: "Subject is required" }),
  content: string().min(1, { message: "Content is required" })
});

const HINTS_HEADER = [
  {
    id: "varName",
    numeric: false,
    disablePadding: false,
    label: "Variable Name"
  },
  {
    id: "displayName",
    numeric: false,
    disablePadding: false,
    label: "Display Name"
  },
  {
    id: "remark",
    numeric: false,
    disablePadding: false,
    label: "Remark"
  }
];

const NotificationManagementForm: FC = () => {
  const {
    push,
    query: { page, feature, id }
  } = useRouter();
  const [_, updateState] = useState<any>();
  const forceUpdate = useCallback(() => updateState({}), []);

  const { formFields, formValidations, setFormValue } = useFormContent();
  const { data: formData, setFormData } = useFormData();

  const { data: eventList } = useQuery(
    ["get-deposit-channel"],
    async () => {
      const { response } = await getEventList({ eventId: null, description: null });
      return response;
    },
    { keepPreviousData: true }
  );
  const { data: status } = useQuery(["get-status-event"], getNotificationStatus);
  const { data: typeEvent } = useQuery(["get-type-event"], getNotificationType);
  const { data: variableList } = useQuery(["get-variable-list"], () =>
    getNotificationVariableList({ eventId: formData?.eventId })
  );

  const { mutate: createNotificationTemplate } = useMutation(({ payload }: { payload: any }) =>
    postNotificationTemplate(payload)
  );

  const { mutate: editNotificationTemplate } = useMutation((payload: any) =>
    putNotificationTemplate(payload)
  );

  const [isOpen, setIsOpen] = useState<boolean>(false);
  const [initialData, setInitialData] = useState(null);

  useEffect(() => {
    if (!formFields) {
      if (page === "detail") {
        NotificationFields[0].disabled = true;
      }
      setFormValue("fields", NotificationFields);
      setFormValue("validations", NotificationSchema);
    }
    if (formData) {
      let current: any = {};
      NotificationFields.map((val: any) => {
        if (formData[val.id] || typeof formData[val.id] === "boolean") {
          let value =
            typeof formData[val.id] === "boolean" ? (formData[val.id] ? "1" : "0") : formData[val.id];
          Object.assign(current, { [val.name]: value });
        }
      });
      setInitialData(current);
    } else {
      let current: any = {};
      NotificationFields.map((val: any) => {
        Object.assign(current, { [val.name]: "" });
      });

      setInitialData(current);
    }
  }, []);

  useEffect(() => {
    if (status) {
      const options = status?.response[0].items.map((el: any) => ({
        value: String(el.codeCode),
        label: el.name
      }));
      NotificationFields[4].options = options;
      forceUpdate();
    }
  }, [status]);

  useEffect(() => {
    if (eventList) {
      const options = eventList?.map((el: any) => ({
        value: el.eventId,
        label: el.eventId
      }));
      NotificationFields[0].options = options;
      forceUpdate();
    }
  }, [eventList]);

  useEffect(() => {
    if (typeEvent) {
      const options = typeEvent?.response[0].items.map((el: any) => ({
        value: el.codeCode,
        label: el.name
      }));
      NotificationFields[2].options = options;
      forceUpdate();
    }
  }, [typeEvent]);

  const handleSubmitForm = (values: any) => {
    const data = {
      eventId: values.event_id,
      description: values.description,
      type: values.type,
      subject: values.subject,
      enabled: values.status == "1" ? true : false,
      content: values.content
    };
    if (page == "add") {
      createNotificationTemplate(
        { payload: data },
        {
          onSuccess: (data) => {
            setFormData(data.response);
            push({ pathname: `/${feature}/notification-preview`, query: { id: data.response.eventId } });
          }
        }
      );
    } else {
      console.log(formData);
      editNotificationTemplate(
        {
          id: formData.id,
          payload: data
        },
        {
          onSuccess: (data) => {
            setFormData(data.response);
            push({ pathname: `/${feature}/notification-preview`, query: { id: data.response.eventId } });
          }
        }
      );
      // console.log(data);
    }
  };

  return (
    <>
      <DetailContentForm
        initialData={initialData}
        fields={formFields}
        validations={formValidations}
        onSubmit={handleSubmitForm}
        onCancel={() => setFormData(null)}
        detailAction={() => setIsOpen(true)}
      />
      <Dialog open={isOpen} onClose={() => setIsOpen(false)}>
        <DialogTitle sx={{ display: "flex", justifyContent: "flex-end" }}>
          <IconButton onClick={() => setIsOpen(false)}>
            <ClearIcon />
          </IconButton>
        </DialogTitle>
        <DialogContent>
          <TableList header={HINTS_HEADER} data={variableList?.response} />
        </DialogContent>
      </Dialog>
    </>
  );
};

export default NotificationManagementForm;
