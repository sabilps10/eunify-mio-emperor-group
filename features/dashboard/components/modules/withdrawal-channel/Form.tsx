import { FC, useCallback, useEffect, useState } from "react";

import useFormContent from "@/features/dashboard/hooks/useFormContent";
import DetailContentForm from "../../DetailContentForm";

import { FindIndexFormField, snakeToCamel } from "@/config/utilities";
import useFormData from "@/hooks/useFormData";
import {
  getCurrency,
  getEntityId,
  getPaymentGatewayName,
  getPaymentNetwork
} from "@/services/common-services";
import { putWithdrawalChannel } from "@/services/mio-services";
import { useRouter } from "next/router";
import { useMutation, useQuery } from "react-query";
import { array, boolean, object, string } from "zod";
import useLocalStorage from "@/hooks/useLocalStorage";
import { LOCALSTORAGE_KEY } from "@/config/constants";

export const EditWithdrawalChannelFields: any = [
  { id: "entityId", name: "entity", options: [], type: "select", disabled: true },
  { id: "gatewayNameEn", name: "gateway_name", type: "text", disabled: true },
  { id: "gatewayId", name: "gateway_id", type: "text", disabled: true, hidden: true },
  {
    id: "isOnlineGateway",
    name: "isOnlineGateway",
    options: [
      { label: "Online", value: "true" },
      { label: "Offline", value: "false" }
    ],
    type: "select",
    disabled: true
  },
  { id: "displayNameEn", name: "english_display_name", type: "text", disabled: false },
  { id: "displayNameSc", name: "simp_chi_display_name", type: "text", disabled: false },
  { id: "displayNameTc", name: "trad_chi_display_name", type: "text", disabled: false },
  {
    id: "currencies",
    custom_prefill: {
      value_type: "index_keys_array",
      source: "form_data",
      source_key: "currencies",
      source_index: 0,
      value_keys: [
        { value: "other", key: "currency" },
        { value: "USDT", key: "cryptoCurrency" }
      ]
    },
    name: "gateway_currency",
    type: "text",
    disabled: true
  },
  {
    id: "network",
    name: "currencies",
    condition: {
      source: "form_data",
      name: "paymentMethod",
      value: "crypto"
    },
    custom_prefill: {
      value_type: "array",
      source: "form_data",
      source_key: "currencies"
    },
    type: "checkbox",
    options: [],
    disabled: false
  },
  {
    id: "maxAmount",
    name: "max_withdrawal_amount",
    additional_value: "gateway_currency",
    type: "text",
    value_type: "number-currency",
    disabled: false,
    callback: {
      enabled: ["max_withdrawal_amount"],
      action: "parentCallback",
      function: "checkMaxDepositAmount(values, methods)"
    }
  },
  {
    id: "minAmount",
    name: "min_withdrawal_amount",
    additional_value: "gateway_currency",
    type: "text",
    value_type: "number-currency",
    disabled: false,
    callback: {
      enabled: ["min_withdrawal_amount"],
      action: "parentCallback",
      function: "checkMaxDepositAmount(values, methods)"
    }
  },
  {
    id: "gatewayCharge",
    name: "gateway_charge",
    type: "text",
    value_type: "number-percentage",
    additional_value: "gateway_charge_unit",
    disabled: false
  },
  {
    id: "gatewayChargeUnit",
    name: "gateway_charge_unit",
    type: "text",
    hidden: true
  },
  {
    id: "serviceFee",
    name: "service_fee_charge",
    type: "text",
    additional_value: "service_fee_unit",
    value_type: "number-percentage",
    disabled: false
  },
  {
    id: "serviceFeeUnit",
    name: "service_fee_unit",
    type: "text",
    hidden: true
  },
  { id: "charges", name: "charges", type: "custom_fields", disabled: false },
  {
    id: "status",
    name: "status",
    type: "radio",
    options: [
      { value: "true", label: "Active" },
      { value: "false", label: "Inactive" }
    ],
    disabled: false
  }
];

export const EditWithdrawalChannelSchema = object({
  entity: string().min(1, { message: "Entity is required" }),
  gateway_name: string().min(1, { message: "Gateway Name is required" }),
  currencies: array(string()),
  english_display_name: string().min(1, { message: "English Display Name is required" }),
  trad_chi_display_name: string().min(1, { message: "Trad. Chi Display Name is required" }),
  simp_chi_display_name: string().min(1, { message: "Simp. Chi Display Name is required" }),
  max_withdrawal_amount: string().min(1, { message: "Max Withdrawal Amount is required" }),
  min_withdrawal_amount: string().min(1, { message: "Min Withdrawal Amount is required" }),
  gateway_charge: string().min(1, { message: "Gateway Charges is required" }),
  gateway_charge_unit: string().min(1, { message: "Gateway Charge Unit is required" }),
  service_fee_charge: string().min(1, { message: "Service Fee Charge is required" }),
  service_fee_unit: string().min(1, { message: "Service Fee Charge Unit is required" }),
  status: string().min(1, { message: "Status is required" }),
  charges: array(
    object({
      chargeType: string(),
      effectiveDateFrom: string(),
      effectiveDateTo: string(),
      isActive: boolean(),
      isExcluded: boolean(),
      ruleKey: string(),
      ruleType: string(),
      unit: string(),
      value: string()
    })
  ).nullable()
});

type updateWithdrawalChannelVariables = {
  id: number;
  payload: any;
};

const WithdrawalChannelForm: FC = () => {
  const [, updateState] = useState<any>();
  const { getItem } = useLocalStorage();
  const account = getItem(LOCALSTORAGE_KEY.ACCOUNT);
  const forceUpdate = useCallback(() => updateState({}), []);
  const { formFields, formValidations, setFormValue } = useFormContent();
  const { data: entityData } = useQuery(["get-entity"], getEntityId);
  const { data: gatewayData } = useQuery(["get-gateway-name"], getPaymentGatewayName);
  const { data: currencyData } = useQuery(["get-currency"], getCurrency);
  const { data: paymentNetworkData } = useQuery(["get-payment-network"], getPaymentNetwork);

  const { mutate: updateWithdrawalChannel } = useMutation(
    ({ id, payload }: updateWithdrawalChannelVariables) => putWithdrawalChannel(id, payload)
  );
  const { data: formData, setFormData } = useFormData();
  const [initialData, setInitialData] = useState(null);
  const {
    query: { feature, id },
    push
  } = useRouter();

  useEffect(() => {
    if (entityData) {
      let items: { label: string; value: any }[] = [];
      entityData?.response[0].items.forEach(function (val: any) {
        let item = { label: "", value: "" };
        item.label = val.name;
        item.value = val.codeCode;
        items.push(item);
      });
      EditWithdrawalChannelFields[0].options = items;
      forceUpdate();
    }
  }, [entityData]);

  useEffect(() => {
    if (gatewayData) {
      let items: { label: string; value: any }[] = [];
      gatewayData.response[0].items.forEach(function (val: any) {
        let item = { label: "", value: "" };
        item.label = val.name;
        item.value = val.codeCode;
        items.push(item);
      });

      EditWithdrawalChannelFields[1].options = items;
      forceUpdate();
    }
  }, [gatewayData]);

  useEffect(() => {
    if (currencyData) {
      let items: { label: string; value: any }[] = [];
      currencyData.response[0].items.forEach(function (val: any) {
        let item = { label: "", value: "" };
        item.label = val.name;
        item.value = val.codeCode;
        items.push(item);
      });

      EditWithdrawalChannelFields[5].options = items;
      forceUpdate();
    }
  }, [currencyData]);

  useEffect(() => {
    if (paymentNetworkData) {
      let items: { label: string; value: any }[] = [];
      paymentNetworkData.response[0].items.forEach(function (val: any) {
        let item = { label: "", value: "" };
        item.label = val.name;
        item.value = val.codeCode;
        items.push(item);
      });

      EditWithdrawalChannelFields[FindIndexFormField(EditWithdrawalChannelFields, "network", "id")].options =
        items;
      forceUpdate();
    }
  }, [paymentNetworkData]);

  useEffect(() => {
    if (!formFields) {
      setFormValue("fields", EditWithdrawalChannelFields);
      setFormValue("validations", EditWithdrawalChannelSchema);
    }
    if (formData) {
      let current: any = {};
      EditWithdrawalChannelFields.map((field: any) => {
        if (formData[field.id] || typeof formData[field.id] === "boolean") {
          let value = null;
          if (field.name == "charges") {
            const items: any[] = [];
            formData[field.id].forEach(function (objs: any) {
              let item: any = {};
              Object.keys(objs).forEach((obj) => {
                item[obj] = typeof objs[obj] === "number" ? objs[obj].toString() : objs[obj];
              });

              items.push(item);
            });
            value = items;
          } else if (field.id == "currencies") {
            let currencies: any[] = [];
            formData["currencies"].forEach(function (curr: any) {
              currencies.push(curr.currency);
            });
            value = currencies;
          } else {
            value =
              typeof formData[field.id] === "number" || typeof formData[field.id] === "boolean"
                ? formData[field.id].toString()
                : formData[field.id];
          }
          Object.assign(current, { [field.name]: value });
        }
      });
      setInitialData(current);
    }
  }, []);

  const checkMaxDepositAmount = (formValues: any, formMethods: any) => {
    if (+formValues.max_withdrawal_amount < +formValues.min_withdrawal_amount) {
      setTimeout(() => {
        formMethods.setError("max_withdrawal_amount", {
          type: "custom",
          message: "The max deposit amount should be greater than the min deposit amount"
        });
      }, 100);
    } else {
      setTimeout(() => {
        formMethods.clearErrors("max_withdrawal_amount");
      }, 100);
    }
  };

  const handleSubmitForm = (values: any, methods: any) => {
    const data: any = {};
    EditWithdrawalChannelFields.forEach(function (field: any) {
      let value = null;
      if (!values[field.name] && initialData) {
        value = initialData[field.name];
      } else {
        if (field.name == "status") {
          value = values[field.name] === "true";
        } else {
          value = values[field.name];
        }
      }
      if (value || typeof value === "boolean") {
        const key = field.id == "network" ? field.name : field.id;
        Object.assign(data, { [key]: value });
      }
    });

    var chargesValue = data.charges.map(function (item: any) {
      return item.ruleKey;
    });
    var isDuplicateCharges = chargesValue.some(function (item: any, idx: any) {
      return chargesValue.indexOf(item) != idx;
    });
    if (isDuplicateCharges) {
      methods.setError("charges", {
        type: "custom",
        message: "Each group can only be added once"
      });
      return;
    }

    const newData = {
      ...data,
      charges: data.charges.map((el: any) => ({
        ...el,
        unit: data.serviceFeeUnit
      }))
    };

    values.charges.forEach(({ value }: any) => {
      if (value === null || value === "") {
        methods.setError("charges", {
          type: "custom",
          message: "At least input a number in order to proceed"
        });
        return;
      }
    });
    if (values?.charges?.some(({ value }: any) => value === null || value === "")) return;
    updateWithdrawalChannel(
      { id: formData.id, payload: { ...newData, requestBy: account } },
      {
        onSuccess: (data) => {
          push(`/${feature}/management`);
        }
      }
    );
  };
  return (
    <DetailContentForm
      formData={formData}
      initialData={initialData}
      fields={formFields}
      validations={formValidations}
      onSubmit={handleSubmitForm}
      onCancel={() => setFormData(null)}
      parentCallback={(func: any, values: any, methods: any) => {
        eval(func);
      }}
    />
  );
};

export default WithdrawalChannelForm;
