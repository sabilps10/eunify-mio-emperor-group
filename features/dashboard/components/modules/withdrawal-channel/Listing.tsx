import { FC, useEffect, useState } from "react";

import TableList from "@/components/TableList";
import {
  WITHDRAWAL_CHANNEL_HEADER,
  WITHDRAWAL_CHANNEL_ACTION,
  WITHDRAW_CHANNEL_EXPORT_HEADING
} from "@/config/tables/withdrawal-channel";
import { useQuery } from "react-query";
import { getWithdrawalChannelList } from "@/services/mio-services";
import { getEntityId, getPaymentGatewayName, getPaymentChannelStatus } from "@/services/common-services";

export const WITHDRAWAL_CHANNEL_FILTER: any = [
  { id: "entity", name: "entityId", type: "dropdown", options: [] },
  { id: "gateway", name: "gatewayId", type: "dropdown", options: [] },
  { id: "status", name: "gatewayStatus", type: "dropdown", options: [] }
];

const defaultFilterData = {
  paymentChannelId: null,
  entityId: null,
  gatewayId: null,
  gatewayStatus: null
};

const WithdrawalChannelListing: FC = () => {
  const [filterData, setFilterData] = useState({
    paymentChannelId: null,
    entityId: null,
    gatewayId: null,
    gatewayStatus: null
  });
  const getWithdrawalChannel = useQuery(
    ["get-withdrawal-channel", filterData],
    async () => {
      const { response } = await getWithdrawalChannelList(filterData);
      return response;
    },
    { keepPreviousData: true }
  );
  const { data: entityData } = useQuery(["get-entity"], getEntityId);
  const { data: statusData } = useQuery(["get-status"], getPaymentChannelStatus);
  const { data: withdrawalChannelData } = useQuery(
    ["get-withdrawal-channel"],
    async () => {
      const { response } = await getWithdrawalChannelList(defaultFilterData);
      return response;
    },
    { keepPreviousData: true }
  );

  const { data: withdrawalChannel, isFetching } = getWithdrawalChannel;
  const exportToFileObject = { heading: WITHDRAW_CHANNEL_EXPORT_HEADING };

  useEffect(() => {
    if (entityData) {
      let items: { label: string; value: any }[] = [];
      entityData?.response[0].items.forEach(function (val: any) {
        let item = { label: "", value: "" };
        item.label = val.name;
        item.value = val.codeCode;
        items.push(item);
      });
      WITHDRAWAL_CHANNEL_FILTER[0].options = items;
    }
  }, [entityData]);

  useEffect(() => {
    if (withdrawalChannelData) {
      let items: { label: string; value: any }[] = [];
      withdrawalChannelData.forEach(function (val: any) {
        let item = { label: "", value: "" };
        item.label = val.gatewayNameEn;
        item.value = val.gatewayId;
        items.push(item);
      });

      WITHDRAWAL_CHANNEL_FILTER[1].options = items;
    }
  }, [withdrawalChannelData]);

  useEffect(() => {
    if (statusData) {
      let items: { label: string; value: any }[] = [];
      statusData.response[0].items.forEach(function (val: any) {
        let item = { label: "", value: "" };
        item.label = val.name;
        item.value = val.codeInt ? "false" : "true";
        items.push(item);
      });
      WITHDRAWAL_CHANNEL_FILTER[2].options = items;
    }
  }, [statusData]);

  const handleDoFilterData = (values: any) => {
    let filter: any = filterData;
    Object.keys(values).forEach(function (key) {
      if (key == "gatewayStatus") {
        filter[key] = values[key] === "true" ? true : values[key] === "false" ? false : null;
      } else {
        filter[key] = values[key] ? values[key] : null;
      }
    });
    setFilterData({ ...filterData, ...filter });
  };

  return (
    <TableList
      header={WITHDRAWAL_CHANNEL_HEADER}
      filter={WITHDRAWAL_CHANNEL_FILTER}
      action={WITHDRAWAL_CHANNEL_ACTION}
      data={withdrawalChannel}
      isLoading={isFetching}
      onFilterData={(values: any) => handleDoFilterData(values)}
      exportDataObject={exportToFileObject}
    />
  );
};

export default WithdrawalChannelListing;
