import { FC, useCallback, useEffect, useMemo, useState } from "react";
import { useRouter } from "next/router";
import { object, string, number, any, optional } from "zod";
import { useQuery, useMutation } from "react-query";

import {
  getEntityId,
  getBankStatus,
  getLocationList,
  getProvinceList,
  getRegionList,
  getCityList,
  getBankAccountsByLocation,
  getCustomerBankCurrency,
  getAccounts,
  postBankAccount,
  putBankAccount,
  getCustomerBankSetting
} from "@/services/common-services";
import useFormData from "@/hooks/useFormData";
import useFormContent from "@/features/dashboard/hooks/useFormContent";
import DetailContentForm from "../../DetailContentForm";
import { FindIndexFormField } from "@/config/utilities";
import useLocalStorage from "@/hooks/useLocalStorage";
import { LOCALSTORAGE_KEY } from "@/config/constants";
import { debounce } from "lodash";
import toast from "react-hot-toast";

const BankCardFields: any[] = [
  {
    id: "entityId",
    name: "entity",
    type: "select",
    options: [],
    on_select_callback: "onSelectEntity(values, value, methods)",
    disabled: false
  },
  {
    id: "tradingAccountNo",
    name: "trading_acc_no",
    type: "text",
    disabled: false,
    callback: {
      enabled: ["entity", "trading_acc_no"],
      action: "parentCallback",
      function: "debouncedAccountInput(values, methods)"
    }
  },
  { id: "tradingAccountName", name: "trading_acc_name", type: "text", disabled: true },
  {
    id: "bankType",
    name: "type",
    type: "text",
    defaultValue: "BK",
    disabled: true
  },
  {
    id: "location",
    name: "location",
    type: "select",
    options: [],
    on_select_callback: "onSelectLocation(values, value, methods)",
    disabled: true,
    disabled_condition: {
      type: "array",
      names: ["entity", "trading_acc_no"],
      value: "has_value",
      disable: false
    }
  },
  {
    id: "province",
    name: "province",
    type: "select",
    autocomplete: true,
    options: [],
    on_select_callback: "onSelectProvince(value, methods)",
    condition: {
      name: "location",
      value: "1"
    },
    disabled: true,
    disabled_condition: {
      name: "location",
      value: "has_value",
      disable: false
    }
  },
  {
    id: "region",
    name: "region",
    type: "select",
    autocomplete: true,
    options: [],
    on_select_callback: "onSelectRegion(value, methods)",
    condition: {
      name: "location",
      value: "1"
    },
    disabled: true,
    disabled_condition: {
      name: "province",
      value: "has_value",
      disable: false
    }
  },
  {
    id: "city",
    name: "city",
    type: "select",
    options: [],
    autocomplete: true,
    on_select_callback: "onSelectCity(value, methods)",
    disabled: true,
    hidden: true,
    disabled_condition: {
      name: "region",
      value: "has_value",
      disable: false
    }
  },
  {
    id: "bankAddr",
    name: "branch_address",
    type: "text",
    condition: {
      name: "location",
      value: "1"
    },
    disabled: true,
    disabled_condition: {
      name: "region",
      value: "has_value",
      disable: false
    }
  },
  {
    id: "bankName",
    name: "bank_name",
    selected_option_value: "bankName",
    type: "select",
    on_select_callback: "onSelectBankName(values, value, methods)",
    options: [],
    condition: {
      name: "location",
      value: "2"
    },
    disabled: true,
    disabled_condition: {
      name: "trading_acc_no",
      value: "has_value",
      disable: false
    }
  },
  {
    id: "bankName",
    name: "bank_name",
    type: "text",
    condition: {
      name: "location",
      value: "3"
    },
    disabled: false
  },
  {
    id: "bankAddr",
    name: "bank_address",
    type: "text",
    condition: {
      name: "location",
      value: "3"
    },
    disabled: false
  },
  {
    id: "swift",
    name: "swift",
    type: "text",
    condition: {
      name: "location",
      value: "3"
    },
    disabled: false
  },
  {
    id: "iBANCode",
    name: "iban_code",
    type: "text",
    condition: {
      name: "location",
      value: "3"
    },
    disabled: false
  },
  {
    id: "bankName",
    name: "bank_name",
    type: "text",
    condition: {
      name: "location",
      value: "1"
    },
    disabled: false
  },
  {
    id: "bankAccNo",
    name: "bank_acc_no",
    type: "text",
    disabled: false
  },
  {
    id: "accHolderName",
    name: "bank_acc_holder",
    type: "text",
    disabled: false
  },
  {
    id: "currency",
    name: "currency",
    type: "select",
    options: [],
    disabled: true,
    disabled_condition: {
      name: "location",
      value: "has_value",
      disable: false
    }
  },
  {
    id: "status",
    name: "status",
    type: "select",
    options: [],
    disabled: false
  },
  { id: "customerAccountNo", name: "customer_acc_no", type: "text", hidden: true },
  { id: "bankCode", name: "bank_code", type: "text", hidden: true }
];

const CreditCardDetailFields: any[] = [
  { id: "bankId", name: "bank_id", type: "text", disabled: true, payload_skipped: true, hidden: true },
  { id: "customerAccountNo", name: "customer_acc_no", type: "text", payload_skipped: true, hidden: true },
  { id: "entityId", name: "entity", type: "text", disabled: true },
  { id: "tradingAccountNo", name: "trading_acc_no", type: "text", disabled: true },
  { id: "tradingAccountName", name: "trading_acc_name", type: "text", disabled: true, payload_skipped: true },
  { id: "bankType", name: "type", type: "text", defaultValue: "CC", disabled: true },
  { id: "bankAccNo", name: "bank_acc_no", type: "text", disabled: true },
  { id: "accHolderName", name: "bank_acc_holder", type: "text", disabled: true },
  { id: "status", name: "status", type: "select", options: [], disabled: false },
  { id: "checkoutSourceId", name: "checkout_source", type: "text", hidden: true }
];

const BankCardDetailFields: any[] = [
  { id: "bankId", name: "bank_id", type: "text", disabled: true, payload_skipped: true, hidden: true },
  { id: "customerAccountNo", name: "customer_acc_no", type: "text", payload_skipped: true, hidden: true },
  { id: "entityId", name: "entity", type: "text", disabled: true },
  { id: "tradingAccountNo", name: "trading_acc_no", type: "text", disabled: true },
  { id: "tradingAccountName", name: "trading_acc_name", type: "text", disabled: true, payload_skipped: true },
  { id: "bankType", name: "type", type: "text", defaultValue: "BK", disabled: true },
  { id: "location", name: "location", type: "select", options: [], disabled: true },
  {
    id: "provinceEn",
    name: "bank_province",
    type: "text",
    disabled: true,
    hidden: false,
    payload_skipped: true
  },
  { id: "province", name: "province", type: "text", disabled: true, hidden: true },
  { id: "regionEn", name: "bank_region", type: "text", disabled: true, hidden: false, payload_skipped: true },
  { id: "region", name: "region", type: "text", disabled: true, hidden: true },
  { id: "cityEn", name: "bank_city", type: "text", disabled: true, hidden: false, payload_skipped: true },
  { id: "city", name: "city", type: "text", disabled: true, hidden: true },
  { id: "branchNo", name: "branch_no", type: "text", disabled: true, hidden: true },
  { id: "bankAddr", name: "bank_address", type: "text", disabled: true, hidden: false },
  { id: "bankName", name: "bank_name", type: "text", disabled: true },
  { id: "bankAccNo", name: "bank_acc_no", type: "text", disabled: true },
  { id: "accHolderName", name: "bank_acc_holder", type: "text", disabled: false },
  { id: "status", name: "status", type: "select", options: [], disabled: false },
  { id: "bankCode", name: "bank_code", type: "text", disabled: true, hidden: true },
  { id: "currency", name: "currency", type: "text", disabled: true, hidden: true },
  { id: "swift", name: "swift", type: "text", disabled: true, hidden: true },
  { id: "iBANCode", name: "iban_code", type: "text", disabled: true, hidden: true }
];

const CreditCardSchema = object({
  bank_id: string().min(1, { message: "Bank Id is required" }),
  customer_acc_no: string().min(1, { message: "Customer Account No. is required" }),
  entity: string().min(1, { message: "Entity is required" }),
  trading_acc_no: string().optional(),
  trading_acc_name: string().optional(),
  type: string().min(1, { message: "Branch Type is required" }),
  bank_acc_no: string().min(1, { message: "Bank Account Number is required" }),
  bank_acc_holder: string().min(1, { message: "Bank Account Holder is required" }),
  status: string().min(1),
  checkout_source: string().min(1)
});

const BankCardDetailSchema = object({
  bank_id: string().min(1, { message: "Bank Id is required" }),
  customer_acc_no: string().min(1, { message: "Customer Account No. is required" }),
  entity: string().min(1, { message: "Entity is required" }),
  trading_acc_no: string().optional(),
  trading_acc_name: string().optional(),
  type: string().min(1, { message: "Branch Type is required" }),
  location: string().min(1, { message: "Location Type is required" }),
  bank_name: string().min(1, { message: "Bank Name is required" }),
  bank_code: string().optional(),
  bank_acc_no: string().min(1, { message: "Bank Account Number is required" }),
  bank_acc_holder: string().min(1, { message: "Bank Account Holder is required" }),
  bank_address: string().optional(),
  status: string().min(1, { message: "Status is required" }),
  province: string().optional(),
  region: string().optional(),
  city: string().optional(),
  branchNo: string().optional(),
  bankAddr: string().optional(),
  bankCode: string().optional(),
  currency: string().optional(),
  swift: string().optional(),
  iban_code: string().optional()
});

const BaseBankFieldSchema = object({
  entity: string().min(1, { message: "Entity is required" }),
  trading_acc_no: string().min(1, { message: "Trading Account No. is required" }),
  trading_acc_name: string().optional(),
  type: string().min(1, { message: "Bank Type is required" }),
  location: string().min(1, { message: "Location is required" }),
  bank_name: string().min(1, { message: "Bank Name is required" }),
  bank_acc_no: string().min(1, { message: "Bank Account Number is required" }),
  bank_acc_holder: string().min(1, { message: "Bank Account Holder Name is required" }),
  currency: string().min(1, { message: "Currency Name is required" }),
  status: string().min(1, { message: "Currency Name is required" }),
  customer_acc_no: string().min(1)
});

const PRCBankFieldSchema = object({
  province: string().min(1, { message: "Province is required" }),
  region: string().min(1, { message: "Region is required" }),
  city: string().optional(),
  branch_address: string().min(1, { message: "Branch Name is required" })
});

const OverseasBankFieldSchema = object({
  bank_address: string().min(1, { message: "Bank Address is required" }),
  swift: string().min(1, { message: "Swift Code is required" }),
  iban_code: string().optional()
});

const HKBankFieldSchema = object({
  bank_code: string().optional()
});

const BankManagementForm: FC = () => {
  const {
    push,
    query: { page, type, feature }
  } = useRouter();
  const { formFields, formValidations, setFormValue } = useFormContent();
  const [, updateState] = useState<any>();
  const forceUpdate = useCallback(() => updateState({}), []);
  const [Fields, setFields] = useState<any>([]);

  const { data: entityId } = useQuery(["get-entity"], getEntityId);
  const { data: locationList } = useQuery(["get-location-list"], getLocationList);
  const { data: currencyList } = useQuery(["get-currency-list"], getCustomerBankCurrency);
  const { data: statusList } = useQuery(["get-bank-status"], getBankStatus);

  const { mutate: bankSetting } = useMutation((payload: any) => getCustomerBankSetting(payload));
  const { mutate: fetchAccountDetail, isLoading: fetchingAccount } = useMutation((payload: any) =>
    getAccounts({ ...payload.payload })
  );
  const { mutate: fetchProvinceList } = useMutation((payload: any) =>
    getProvinceList({ ...payload.payload })
  );
  const { mutate: fetchRegionList } = useMutation((payload: any) => getRegionList({ ...payload.payload }));
  const { mutate: fetchCityList } = useMutation((payload: any) => getCityList({ ...payload.payload }));
  const { mutate: fetchBankList } = useMutation((payload: any) =>
    getBankAccountsByLocation({ ...payload.payload })
  );
  const { mutate: createBankAccount, isLoading: submittingBankAccount } = useMutation(
    ({ accountNo, payload }: any) => postBankAccount(accountNo, payload)
  );
  const { mutate: updateBankAccount, isLoading: updatingBankAccount } = useMutation(
    ({ accountNo, bankId, payload }: any) => putBankAccount(accountNo, bankId, payload)
  );

  const [provinceOption, setProvinceOption] = useState<any[]>([]);
  const [regionOption, setRegionOption] = useState<any[]>([]);
  const [cityOption, setCityOption] = useState<any[]>([]);
  const [bankOption, setBankOption] = useState<any[]>([]);

  const { data: formData, setFormData } = useFormData();
  const [initialData, setInitialData] = useState(null);

  const onSelectEntity = (formValues: any, fieldValue: string, formMethods: any) => {
    formMethods.setValue("entity", fieldValue, { shouldValidate: true });
    formMethods.setValue("location", "");
    formMethods.setValue("currency", "");
    formMethods.setValue("swift", "");
    formMethods.setValue("iban_code", "");
    formMethods.setValue("province", "");
    formMethods.setValue("region", "");
    formMethods.setValue("city", "");
    formMethods.setValue("branch_code", "");

    const locations: any[] = [];
    let locationIds: any[] = [];
    if (fieldValue.toLowerCase() === "xpro") {
      locationIds.push(1);
    }
    if (fieldValue.toLowerCase() === "eiehk") {
      locationIds.push(2);
    }
    if (fieldValue.toLowerCase() === "ebl") {
      locationIds.push(2, 3);
    }
    locationList?.response?.forEach(function (val: any) {
      if (locationIds.indexOf(val.addressId) > -1) {
        const location = { label: "", value: "" };

        location.label = val.nameEn;
        location.value = val.addressId.toString();
        locations.push(location);
      }
    });
    BankCardFields[4].options = locations;
  };

  const getAccountDetails = (formValues: any, formMethods: any) => {
    formMethods.clearErrors("trading_acc_no");
    formMethods.setValue("trading_acc_name", "");
    fetchAccountDetail(
      { payload: { entityId: formValues.entity, tradingAccountNo: formValues.trading_acc_no } },
      {
        onSuccess: (res) => {
          if (res.result == true) {
            formMethods.setValue("trading_acc_name", res.response.realName, {
              shouldValidate: true
            });
            formMethods.setValue("customer_acc_no", res.response.userId);
          } else {
            formMethods.setError("trading_acc_no", {
              type: "custom",
              message: res.errorDescription
            });
          }
        }
      }
    );
  };

  const onSelectLocation = (formValues: any, fieldValue: number, formMethods: any) => {
    formMethods.setValue("location", fieldValue, { shouldValidate: true });
    let Schema = BaseBankFieldSchema;
    bankSetting(
      { entityId: formValues.entity },
      {
        onSuccess(res: any) {
          let locationCurrencies: any[] | undefined;
          const currencies: any[] = [];
          locationCurrencies = res?.response.filter((element: any) => element.location == fieldValue);
          locationCurrencies?.forEach(function (val: any) {
            const currency = { label: val.currency, value: val.currency };
            currencies.push(currency);
          });
          BankCardFields[FindIndexFormField(BankCardFields, "currency")].options = currencies;
          forceUpdate();
        }
      }
    );

    if (fieldValue == 1) {
      const CombinedSchema = Schema.merge(PRCBankFieldSchema);
      setFormValue("validations", CombinedSchema);

      formMethods.setValue("province", "", { shouldValidate: true });
      formMethods.setValue("region", "", { shouldValidate: true });
      formMethods.setValue("city", "", { shouldValidate: true });

      fetchProvinceList(
        { payload: { locationId: fieldValue, isActive: true } },
        {
          onSuccess: (res) => {
            if (res.result) {
              setProvinceOption(res.response);
            }
          }
        }
      );
    }
    if (fieldValue == 2) {
      fetchBankList(
        { payload: { locationId: fieldValue, bankType: "BK" } },
        {
          onSuccess: (res) => {
            if (res.result) {
              let bankLists: any[] = [];
              res.response.forEach(function (val: any) {
                const bankList = {
                  label: val.bankName || "null",
                  value: val.bankName || "null",
                  origin: val
                };
                bankLists.push(bankList);
              });
              BankCardFields[9].options = bankLists;
              forceUpdate();
            }
          }
        }
      );
      const CombinedSchema = Schema.merge(HKBankFieldSchema);
      setFormValue("validations", CombinedSchema);
    }
    if (fieldValue == 3) {
      const CombinedSchema = Schema.merge(OverseasBankFieldSchema);
      setFormValue("validations", CombinedSchema);
    }
  };

  const onSelectProvince = (fieldValue: number, formMethods: any) => {
    formMethods.setValue("province", fieldValue, { shouldValidate: true });

    formMethods.resetField("region");
    setRegionOption([]);

    BankCardFields[FindIndexFormField(BankCardFields, "city")].hidden = true;
    formMethods.setValue("city", "");
    setCityOption([]);
    forceUpdate();

    fetchRegionList(
      { payload: { provinceId: fieldValue, isActive: true } },
      {
        onSuccess: (res) => {
          if (res.result) {
            setRegionOption(res.response);
          }
        }
      }
    );
  };

  const onSelectRegion = (fieldValue: number, formMethods: any) => {
    formMethods.setValue("region", fieldValue, { shouldValidate: true });
    formMethods.setValue("city", "");
    fetchCityList(
      { payload: { regionId: fieldValue, isActive: true } },
      {
        onSuccess: (res) => {
          if (res.result && res?.response?.length) {
            BankCardFields[FindIndexFormField(BankCardFields, "city")].hidden = false;
            setCityOption(res.response);
            forceUpdate();
          } else {
            BankCardFields[FindIndexFormField(BankCardFields, "city")].hidden = true;
            setCityOption([]);
            forceUpdate();
          }
        }
      }
    );
  };

  const onSelectCity = (fieldValue: number, formMethods: any) => {
    formMethods.setValue("city", fieldValue, { shouldValidate: true });
  };

  const onSelectBankName = (formValues: any, fieldValue: any, formMethods: any) => {
    formMethods.setValue("bank_name", fieldValue.bankName, { shouldValidate: true });
    formMethods.setValue("bank_code", fieldValue.bankCode);
  };

  const debouncedAccountInput = useMemo(() => {
    return debounce(getAccountDetails, 800);
  }, []);

  useEffect(() => {
    return () => {
      debouncedAccountInput.cancel();
    };
  });

  useEffect(() => {
    if (entityId) {
      const options = entityId?.response[0].items.map((el: any) => ({
        label: el.name,
        value: el.codeCode
      }));
      BankCardFields[0].options = options;
      forceUpdate();
    }
  }, [entityId]);

  useEffect(() => {
    if (locationList && Fields.length && page == "detail" && formData?.bankType.toLowerCase() !== "cc") {
      const options = locationList?.response?.map((el: any) => ({
        label: el.nameEn,
        value: el.addressId.toString()
      }));
      Fields[FindIndexFormField(Fields, "location")].options = options;
      forceUpdate();
    }
  }, [locationList, Fields]);

  useEffect(() => {
    if (provinceOption) {
      const options = provinceOption?.map((el: any) => ({
        label: el.nameEn,
        value: el.addressId.toString()
      }));
      BankCardFields[5].options = options;
      forceUpdate();
    }
  }, [provinceOption]);

  useEffect(() => {
    if (regionOption) {
      const options = regionOption?.map((el: any) => ({
        label: el.nameEn,
        value: el.addressId.toString()
      }));
      BankCardFields[6].options = options;
      forceUpdate();
    }
  }, [regionOption]);

  useEffect(() => {
    if (cityOption) {
      const options = cityOption?.map((el: any) => ({
        label: el.nameEn,
        value: el.addressId.toString()
      }));
      BankCardFields[7].options = options;
      forceUpdate();
    }
  }, [cityOption]);

  useEffect(() => {
    if (statusList && Fields.length) {
      const options = statusList?.response[0].items?.map((el: any) => ({
        label: el.name,
        value: el.codeInt.toString()
      }));
      Fields[FindIndexFormField(Fields, "status")].options = options;
      forceUpdate();
    }
  }, [statusList, Fields]);

  useEffect(() => {
    if (!formFields) {
      if (page == "detail" && formData && formData?.bankType.toLowerCase() == "cc") {
        setFields(CreditCardDetailFields);
        setFormValue("fields", CreditCardDetailFields);
        setFormValue("validations", CreditCardSchema);
      }
      if (page == "detail" && formData && formData?.bankType.toLowerCase() == "bk") {
        setFields(BankCardDetailFields);
        setFormValue("fields", BankCardDetailFields);
        setFormValue("validations", BankCardDetailSchema);
      }
      if (page == "add") {
        setFields(BankCardFields);
        setFormValue("fields", BankCardFields);
        setFormValue("validations", BaseBankFieldSchema);
        let current: any = {};
        BankCardFields.map((val: any) => {
          let value = null;
          Object.assign(current, { [val.name]: "" });
        });
        setInitialData(current);
      }
    }
  }, []);

  useEffect(() => {
    if (Fields && formData && page == "detail") {
      let current: any = {};
      Fields.map((val: any) => {
        let value = null;
        value = formData[val.id]?.toString() || "";
        Object.assign(current, { [val.name]: value });
      });
      setInitialData(current);
    }
  }, [Fields]);

  useEffect(() => {
    BankCardFields[FindIndexFormField(BankCardFields, "city")].hidden = true;
    forceUpdate();
  }, []);

  const handleSubmit = (values: any) => {
    const data = {};
    Fields.forEach(function (field: any) {
      if (!field.payload_skipped && values[field.name]) {
        Object.assign(data, { [field.id]: values[field.name] });
      }
    });
    const { getItem } = useLocalStorage();
    const account = getItem(LOCALSTORAGE_KEY.ACCOUNT);
    Object.assign(data, { requestBy: account });
    if (page == "add") {
      createBankAccount(
        { payload: data, accountNo: values.customer_acc_no },
        {
          onSuccess: (res: any) => {
            console.log(res);
            if (res.result) {
              push(`/${feature}/management`);
            } else {
              toast.dismiss();
              const toastAttr: any = {
                duration: 5000,
                position: "top-right",
                style: {
                  background: "#ffe3e3",
                  color: "#000000"
                },
                id: "error"
              };
              toast.error(
                "Bank account already registered (Ref. No: [" + res.errorReasonCode + "])",
                toastAttr
              );
            }
          }
        }
      );
    }
    if (page == "detail") {
      updateBankAccount(
        {
          payload: { ...data, bankCode: formData?.bankCode },
          accountNo: values.customer_acc_no,
          bankId: values.bank_id
        },
        {
          onSuccess: (res: any) => {
            if (res.result) {
              push(`/${feature}/management`);
            }
          }
        }
      );
    }
  };

  return (
    <DetailContentForm
      initialData={initialData}
      fields={formFields}
      validations={formValidations}
      onSubmit={handleSubmit}
      fetchingRequest={submittingBankAccount}
      onCancel={() => setFormData(null)}
      parentCallback={(func: any, values: any, methods: any) => {
        eval(func);
      }}
      onSelectCallback={(func: any, values: any, value: any, methods: any) => {
        eval(func);
      }}
    />
  );
};

export default BankManagementForm;
