import { FC, useCallback, useEffect, useState } from "react";
import { useMutation, useQuery } from "react-query";

import TableList from "@/components/TableList";
import {
  BANK_ACCOUNTS_EXPORT_HEADING,
  BANK_MANAGEMENT_ACTION,
  BANK_MANAGEMENT_HEADER
} from "@/config/tables/bank-management";
import {
  getEntityId,
  getBankStatus,
  getBankType,
  postClientBankRecord,
  getClientBankRecord
} from "@/services/common-services";
import moment from "moment";
import { FindIndexFormField } from "@/config/utilities";
import usePagination from "@/hooks/usePagination";
import toast from "react-hot-toast";

export const BANK_MANAGEMENT_FILTER = [
  {
    id: "entity",
    name: "entityId",
    type: "dropdown",
    options: []
  },
  {
    id: "customer_account_no",
    name: "customerAccountNo",
    type: "text"
  },
  {
    id: "trading_account_no",
    name: "tradingAccountNo",
    type: "text"
  },
  {
    id: "type",
    name: "bankType",
    type: "dropdown",
    options: []
  },
  {
    id: "status",
    name: "status",
    type: "dropdown",
    options: []
  },
  {
    id: "last_modified",
    name: "lastModified",
    type: "time-range"
  }
];

type FilterData = {
  entityId: string | number | null;
  customerAccountNo: string | number | null;
  tradingAccountNo: string | number | null;
  bankType: string | number | null;
  status: string | number | null;
  lastModifiedDateFrom: string | number | null;
  lastModifiedDateTo: string | number | null;
};

const BankManagementListing: FC = () => {
  const [, updateState] = useState<any>();
  const forceUpdate = useCallback(() => updateState({}), []);

  const [filterPagination, setFilterPagination, pagination, setPagination] = usePagination<FilterData>({
    entityId: null,
    customerAccountNo: null,
    tradingAccountNo: null,
    bankType: null,
    status: null,
    lastModifiedDateFrom: null,
    lastModifiedDateTo: null
  });

  const { data: entity } = useQuery(["get-entity"], getEntityId);
  const { data: bankType } = useQuery(["get-bank-type"], getBankType);
  const { data: bankStatus } = useQuery(["get-bank-status"], getBankStatus);

  const [clientBankRecord, setClientBankRecord] = useState<any>([]);
  const [clientBankExport, setClientBankExport] = useState<any>([]);
  const { mutate: mutatePage, isLoading: loadingpage } = useMutation((payload: any) =>
    postClientBankRecord(payload)
  );

  const { mutate: mutateList, isLoading: loadingList } = useMutation((payload: any) =>
    getClientBankRecord(payload)
  );

  const exportToFileObject = {
    heading: BANK_ACCOUNTS_EXPORT_HEADING,
    data: clientBankExport,
    loading: loadingList
  };

  useEffect(() => {
    mutatePage(filterPagination, {
      onSuccess: ({ response }) => {
        const { content, ...rest } = response;
        setClientBankRecord(content);
        setPagination(rest);
      }
    });
  }, [filterPagination]);

  useEffect(() => {
    mutateList(filterPagination.filter, {
      onSuccess: ({ response }) => {
        setClientBankExport(response);
      }
    });
  }, [filterPagination.filter]);

  useEffect(() => {
    if (entity) {
      const options = entity?.response[0]?.items.map((el: any) => ({
        label: el.name,
        value: el.codeCode
      }));
      BANK_MANAGEMENT_FILTER[0].options = options;
      forceUpdate();
    }
  }, [entity]);

  useEffect(() => {
    if (bankType) {
      const options = bankType?.response[0].items.map((el: any) => ({
        label: el.name,
        value: el.codeCode
      }));
      BANK_MANAGEMENT_FILTER[FindIndexFormField(BANK_MANAGEMENT_FILTER, "bankType")].options = options;
      forceUpdate();
    }
  }, [bankType]);

  useEffect(() => {
    if (bankStatus) {
      const options = bankStatus?.response[0].items.map((el: any) => ({
        label: el.name,
        value: el.codeInt
      }));
      BANK_MANAGEMENT_FILTER[FindIndexFormField(BANK_MANAGEMENT_FILTER, "status")].options = options;
      forceUpdate();
    }
  }, [bankStatus]);

  const handleDoFilterData = (values: any) => {
    let filter: any = filterPagination.filter;
    Object.keys(values).forEach(function (key) {
      if (key == "lastModified_start" || key == "lastModified_end") {
        const splitString = key.split("_");
        if (splitString[1] == "start") {
          filter["lastModifiedDateFrom"] = values.lastModified_start
            ? moment(values.lastModified_start).format("Y-MM-DD HH:mm:ss")
            : null;
        }
        if (splitString[1] == "end") {
          filter["lastModifiedDateTo"] = values.lastModified_end
            ? moment(values.lastModified_end).format("Y-MM-DD HH:mm:ss")
            : null;
        }
      }
      if (key === "tradingAccountNo") {
        filter["tradingAccountNo"] = values[key];
      } else {
        filter[key] = values[key] ? values[key] : null;
      }
    });
    setFilterPagination({ ...filterPagination, filter: { ...filter } });
  };

  return (
    <TableList
      header={BANK_MANAGEMENT_HEADER}
      filter={BANK_MANAGEMENT_FILTER}
      action={BANK_MANAGEMENT_ACTION}
      data={clientBankRecord}
      isLoading={loadingpage}
      onFilterData={(values: any) => handleDoFilterData(values)}
      exportDataObject={exportToFileObject}
      isPagination={true}
      pagination={pagination}
      handlePagination={(val) => {
        setFilterPagination({ ...filterPagination, ...val });
      }}
    />
  );
};

export default BankManagementListing;
