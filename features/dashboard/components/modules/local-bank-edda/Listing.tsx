import { FC, useEffect, useState, useCallback } from "react";
import { useQuery, useMutation } from "react-query";
import { getBankMaster, getBankEddaCode, editBankMaster } from "@/services/common-services";
import TableList from "@/components/TableList";
import { LOCAL_BANK_ACTION, LOCAL_BANK_HEADER } from "@/config/tables/local-banks";
import { Typography } from "@mui/material";
import { useRouter } from "next/router";
import securityStore, { SecurityStoreTypes } from "@/store/securityStore";
import shallow from "zustand/shallow";
import convertNameToUrl from "@/utils/convertToPathUrl";
import toast from "react-hot-toast";

const LocalBankEddaListing: FC = () => {
  const {
    query: { feature }
  } = useRouter();
  const { client_functions } = securityStore((state) => state, shallow) as SecurityStoreTypes;

  const [, updateState] = useState<any>();
  const forceUpdate = useCallback(() => updateState({}), []);
  const { data, isLoading, refetch } = useQuery(["get-bank-master"], getBankMaster);

  return (
    <TableList
      isLoading={isLoading}
      action={LOCAL_BANK_ACTION}
      header={LOCAL_BANK_HEADER}
      data={data?.response}
    />
  );
};

export default LocalBankEddaListing;
