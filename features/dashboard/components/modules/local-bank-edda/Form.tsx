import { FC, useEffect, useState, useCallback } from "react";
import { useRouter } from "next/router";
import { object, string } from "zod";
import { useMutation, useQuery } from "react-query";

import {
  getPaymentChannelStatus,
  addBankMaster,
  getBankEddaCode,
  editBankMaster
} from "@/services/common-services";
import useFormContent from "@/features/dashboard/hooks/useFormContent";
import useFormData from "@/hooks/useFormData";
import DetailContentForm from "../../DetailContentForm";

export const LocalBankFields = [
  { id: "isActive", name: "status", type: "select", options: [], disabled: false },
  { id: "bankCode", name: "bank_code", type: "text", disabled: false },
  { id: "bankNameSys", name: "system_display_name", type: "text", disabled: false },
  { id: "bankNameEn", name: "english_display_name", type: "text", disabled: false },
  { id: "bankNameTc", name: "trad_chi_display_name", type: "text", disabled: false },
  { id: "bankNameSc", name: "simp_chi_display_name", type: "text", disabled: false },
  { id: "eddaBankCode", name: "edda_bank", type: "radio", options: [], disabled: false }
];

export const LocalBankSchema = object({
  status: string().min(1, { message: "Status is required" }),
  bank_code: string().min(1, { message: "Bank Code is required" }),
  system_display_name: string().min(1, { message: "Sytem Display Name is required" }),
  english_display_name: string().min(1, { message: "Eng. Display Name is required" }),
  trad_chi_display_name: string().min(1, { message: "Trad. Ch Display Name is required" }),
  simp_chi_display_name: string().min(1, { message: "Simp. Ch Display Name is required" }),
  edda_bank: string().min(1, { message: "Edda Bank is required" })
});

const LocalBankForm: FC = () => {
  const {
    push,
    query: { feature, page }
  } = useRouter();
  const { formFields, formValidations, setFormValue } = useFormContent();
  const { data: formData, setFormData, emptyFields } = useFormData();
  const [, updateState] = useState<any>();
  const forceUpdate = useCallback(() => updateState({}), []);

  const { data: status } = useQuery(["get-status"], getPaymentChannelStatus);
  const { data: bankEdda } = useQuery(["get-edda-bank-code"], getBankEddaCode);

  const { mutate: add, isLoading: loadingAdd } = useMutation((payload: any) => addBankMaster(payload));

  const { mutate: edit, isLoading: loadingEdit } = useMutation((payload: any) => editBankMaster(payload));

  const [initialData, setInitialData] = useState<any>(null);

  useEffect(() => {
    if (status) {
      let items: any = [];
      status.response[0].items.forEach(function (val: any) {
        let item = { label: "", value: "" };
        item.label = val.name;
        item.value = val.codeInt ? "false" : "true";
        items.push(item);
      });
      LocalBankFields[0].options = items;
      forceUpdate();
    }
  }, [status]);

  useEffect(() => {
    if (bankEdda) {
      const options = bankEdda?.response[0].items.map((el: any) => ({
        value: el.codeCode,
        label: el.name
      }));
      LocalBankFields[6].options = options;
      forceUpdate();
    }
  }, [bankEdda]);

  useEffect(() => {
    if (!formFields) {
      if (page === "add") {
        setFormData(null);
        setFormValue("fields", LocalBankFields);
      } else {
        LocalBankFields[1].disabled = true;
        LocalBankFields[2].disabled = true;
        setFormValue("fields", LocalBankFields);
      }
      setFormValue("validations", LocalBankSchema);
    }
  }, []);

  useEffect(() => {
    if (formFields) {
      if (formData) {
        let current: any = {};
        formFields.map((el: any) => {
          if (el.name === "status") {
            Object.assign(current, { [el.name]: formData[el.id] ? "true" : "false" });
          } else {
            Object.assign(current, { [el.name]: formData[el.id] });
          }
        });
        setInitialData(current);
      }
    }
  }, [formFields]);

  const handleSubmit = (val: any) => {
    const payload = {
      bankCode: val.bank_code,
      bankNameSys: val.system_display_name,
      bankNameEn: val.english_display_name,
      bankNameTc: val.trad_chi_display_name,
      bankNameSc: val.simp_chi_display_name,
      eddaBankCode: val.edda_bank,
      isActive: val.status === "true" ? true : false
    };

    if (page === "add") {
      add(payload, {
        onSuccess: (res) => {
          if (res.result) {
            push(`/${feature}/management`);
          }
        }
      });
    } else {
      edit(
        { id: formData.bankId, payload },
        {
          onSuccess: (res) => {
            if (res.result) {
              push(`/${feature}/management`);
            }
          }
        }
      );
    }
  };

  return (
    <DetailContentForm
      initialData={initialData}
      fields={formFields}
      validations={formValidations}
      onSubmit={handleSubmit}
      fetchingRequest={loadingAdd || loadingEdit}
    />
  );
};

export default LocalBankForm;
