import { FC, useEffect, useState } from "react";

import useFormContent from "@/features/dashboard/hooks/useFormContent";
import { Router, useRouter } from "next/router";
import DetailContentForm from "../../DetailContentForm";
import useFormData from "@/hooks/useFormData";
import { useMutation, useQuery } from "react-query";
import { postTradeDate, getTradeDate } from "@/services/common-services";
import { array, object, string, date, number } from "zod";
import moment from "moment";

export const TradeDateFields = [
  {
    id: "periodStart",
    name: "period_start",
    type: "date-time",
    input_format: "yyyy-MM-dd",
    views_format: ["year", "day"],
    format: { type: "moment", value: "Y-MM-DD 00:00:00" },
    disabled: true
  },
  {
    id: "periodEnd",
    name: "period_end",
    type: "date-time",
    input_format: "yyyy-MM-dd",
    views_format: ["year", "day"],
    format: { type: "moment", value: "Y-MM-DD 23:59:59" },
    disabled: false
  },
  {
    id: "tradeEndTime",
    name: "trade_start_time",
    type: "time",
    format: { type: "moment", value: "HH:mm:ss " },
    disabled: false
  }
];

export const TradeDateSchema = object({
  period_start: date({
    required_error: "Trade Date Start is required",
    invalid_type_error: "Please select a date"
  }).or(string().min(1).or(number().min(1))),
  period_end: date({
    required_error: "Trade Date End is required",
    invalid_type_error: "Please select a date"
  }),
  trade_start_time: date({
    required_error: "Cutoff Start is required",
    invalid_type_error: "Please select a time"
  }).or(string().min(1))
});

const defaultValue = {
  period_start: "",
  period_end: "",
  trade_start_time: ""
};

const TradeDateForm: FC = () => {
  const router = useRouter();
  const [requestError, setRequestError] = useState(null);
  const [initialData, setInitialData] = useState<any>(defaultValue);
  const { formFields, formValidations, setFormValue } = useFormContent();
  const { data: formData, setFormData } = useFormData();

  const { data: tradeDateRecord, isFetching: fetchingRecord } = useQuery(["get-trade-date"], async () => {
    const { response } = await getTradeDate();
    return response;
  });

  const { mutate: createTradedate } = useMutation(({ payload }: { payload: any }) => postTradeDate(payload));

  useEffect(() => {
    if (!formFields) {
      setFormValue("fields", TradeDateFields);
      setFormValue("validations", TradeDateSchema);
    }
    if (formData) {
      let current: any = {};
      TradeDateFields.map((val: any) => {
        if (formData[val.id]) {
          let value = null;
          value = formData[val.id];
          Object.assign(current, { [val.name]: value });
        }
      });
      setInitialData(current);
    }
  }, []);

  useEffect(() => {
    if (tradeDateRecord) {
      const date = new Date(tradeDateRecord[tradeDateRecord.length - 1]?.periodEnd);
      let startDate = date.setSeconds(date.getSeconds() + 1);
      let endDate = date.setMinutes(date.getMinutes() + 1);

      setInitialData({
        period_start: startDate,
        period_end: "",
        trade_start_time: `2022-10-10T${tradeDateRecord[tradeDateRecord.length - 1]?.tradeEndTime}`
      });
    }
  }, [tradeDateRecord]);

  const handleSubmitForm = (values: any) => {
    const data: any = {};
    TradeDateFields.forEach(function (field: any) {
      let value = null;
      if (!values[field.name] && initialData) {
        value = initialData[field.name];
      } else {
        value = values[field.name];
      }
      if (value) {
        const key = field.id;
        Object.assign(data, { [key]: moment(value).format(field.format.value) });
      }
    });
    createTradedate(
      { payload: { ...data, tradeStartTime: data.tradeEndTime } },
      {
        onSuccess: (res) => {
          if (res?.result) {
            router.push(`/${router.query.feature}/management`);
          } else {
            setRequestError(res);
          }
        },
        onError: (err) => {
          console.log(err);
        }
      }
    );
  };

  return (
    <DetailContentForm
      initialData={initialData}
      formData={formData}
      fields={formFields}
      validations={formValidations}
      onSubmit={handleSubmitForm}
      onCancel={() => setFormData(null)}
      requestError={requestError}
    />
  );
};

export default TradeDateForm;
