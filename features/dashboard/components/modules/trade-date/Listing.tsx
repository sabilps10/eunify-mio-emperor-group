import { FC, useState } from "react";

import TableList from "@/components/TableList";
import { useQuery } from "react-query";
import { TRADE_DATE_ACTION, TRADE_DATE_HEADER } from "@/config/tables/trade-date";
import { getTradeDate } from "@/services/common-services";

const TradeDateListing: FC = () => {
  const [filterData] = useState({
    addressLevel: 2
  });
  const { data: tradeDateRecord, isFetching: fetchingRecord } = useQuery(
    ["get-trade-date"],
    async () => {
      const { response } = await getTradeDate();
      return response;
    }
  );

  return (
    <TableList
      header={TRADE_DATE_HEADER}
      action={TRADE_DATE_ACTION}
      data={tradeDateRecord}
      isLoading={fetchingRecord}
    />
  );
};

export default TradeDateListing;
