import { FC, useCallback, useEffect, useMemo, useState } from "react";
import { object, string, number, TypeOf, preprocess } from "zod";
import { useQuery, useMutation } from "react-query";
import { useRouter } from "next/router";

import {
  getEntityId,
  getAdjustmentTypeList,
  getExchangeRate as getExchangeRateAmount,
  getExchangeRateList
} from "@/services/common-services";
import {
  getDepositChannelList,
  getServiceCharge as getServiceChargeAmount,
  getAccountBalanceCheck,
  postDepositInst,
  putDepositStatus
} from "@/services/mio-services";
import { getAccounts } from "@/services/common-services";
import useFormData from "@/hooks/useFormData";
import useFormContent from "@/features/dashboard/hooks/useFormContent";
import DetailContentForm from "../../DetailContentForm";
import { FindIndexFormField, formatNumber, RoundNumber } from "@/config/utilities";
import debouce from "lodash.debounce";
import useLocalStorage from "@/hooks/useLocalStorage";
import { LOCALSTORAGE_KEY } from "@/config/constants";

export const DetailDepositFields: any = [
  { id: "tradeTypeName", name: "type", type: "text", disabled: true },
  { id: "paymentChannelName", name: "payment_channel", type: "text", disabled: true },
  { id: "adjustmentTypeName", name: "adjustment_type", type: "text", disabled: true },
  { id: "orderNo", name: "reference_no", type: "text", disabled: true },
  { id: "entityId", name: "entity", type: "text", disabled: true },
  { id: "tradingAccountNo", name: "trading_acc_no", type: "text", disabled: true },
  { id: "clientName", name: "acc_holder", type: "text", disabled: true },
  { id: "isValidAml", name: "aml_status", type: "text", value_type: "aml-status", disabled: true },
  {
    ids: [
      { data: "static", value: "USD" },
      { data: "formData", value: "balance" }
    ],
    name: "balance",
    type: "text",
    disabled: true
  },
  {
    id: "custom_value",
    name: "exchange_rate",
    type: "text",
    disabled: true
  },
  { name: "platform_currency", defaultValue: "USD", type: "text", disabled: true },
  {
    id: "custom_value",
    name: "platform_amount",
    type: "text",
    disabled: true
  },
  { id: "currency", name: "actual_currency", type: "text", disabled: true },
  { id: "custom_value", name: "actual_amount", type: "text", disabled: true },
  {
    id: "custom_value",
    name: "service_charge",
    type: "text",
    disabled: true
  },
  { id: "bankName", name: "bank_name", type: "text", disabled: true },
  { id: "accHolderName", name: "bank_acc_holder", type: "text", disabled: true },
  { id: "bankAccountNo", name: "credit_card_no", type: "text", disabled: true },
  { id: "cryptoAddress", name: "crypto_address", type: "text", disabled: true },
  { id: "cryptoNetwork", name: "crypto_network", type: "text", disabled: true },
  { id: "createDate", name: "application_time", type: "text", disabled: true },
  { id: "approvedDate", name: "approval_time", type: "text", disabled: true },
  { id: "requestBy", name: "request_by", type: "text", disabled: true },
  { id: "approvedBy", name: "approved_by", type: "text", disabled: true },
  { id: "comment", name: "remark", type: "text", disabled: true },
  { id: "status", name: "status", type: "text", disabled: true, hidden: true },
  { id: "statusName", name: "status_name", type: "text", disabled: true },
  { id: "approvedComment", name: "reject_reason", type: "text", disabled: true }
];

const AddDepositFields: any = [
  {
    id: "entityId",
    name: "entity",
    type: "select",
    options: [],
    on_select_callback: "onSelectEntity(value, methods)",
    disabled: false
  },
  {
    id: "tradingAccountNo",
    name: "trading_acc_no",
    callback: {
      enabled: ["entity", "trading_acc_no"],
      action: "parentCallback",
      function: "debouncedAccountInput(values, methods)"
    },
    type: "text",
    disabled: true,
    disabled_condition: {
      name: "entity",
      value: "has_value",
      disable: false
    }
  },
  {
    id: "tradingAccountName",
    name: "trading_acc_name",
    type: "text",
    disabled: true,
    payload_skipped: true
  },
  {
    id: "involvePayment",
    name: "involve_payment",
    type: "radio",
    options: [
      { value: "involve", label: "Involve" },
      { value: "not_involve", label: "Not Involve" }
    ],
    disabled: false,
    default_value: "involve",
    callback: {
      enabled: ["involve_payment"],
      action: "parentCallback",
      function: "onChangeInvolveField(values, methods)"
    }
  },
  {
    id: "adjustmentType",
    name: "adjustment_type",
    type: "select",
    options: [],
    disabled: true,
    disabled_condition: {
      name: "entity",
      value: "has_value",
      disable: false
    },
    callback: {
      enabled: ["entity"],
      action: "parentCallback",
      function: "getAdjustmentType()"
    }
  },
  {
    id: "paymentChannelId",
    name: "deposit_channel",
    callback: {
      enabled: ["entity"],
      action: "parentCallback",
      function: "getDepositChannel(values)"
    },
    condition: {
      name: "involve_payment",
      value: "involve"
    },
    type: "select",
    options: [],
    selected_option_value: "id",
    on_select_callback: "onSelectDepositChannel(values, value, methods)",
    disabled: true,
    disabled_condition: {
      name: "entity",
      value: "has_value",
      disable: false
    }
  },
  {
    id: "network",
    name: "network",
    condition: {
      name: "involve_payment",
      value: "involve"
    },
    type: "select",
    options: [],
    disabled: true,
    disabled_condition: {
      name: "crypto_currency",
      value: "has_value",
      disable: false
    },
    selected_option_value: "currency",
    on_select_callback: "onSelectNetwork(value, methods)"
  },
  {
    id: "cryptoAddress",
    name: "wallet_address",
    condition: {
      name: "involve_payment",
      value: "involve"
    },
    type: "text",
    disabled: true,
    disabled_condition: {
      name: "crypto_currency",
      value: "has_value",
      disable: false
    }
  },
  {
    id: "amount",
    name: "deposit_amount",
    type: "text",
    value_type: "number-currency",
    additional_value: "currency",
    disabled: true,
    disabled_condition: {
      type: "array",
      names: ["trading_acc_no", "deposit_channel"],
      value: "has_value",
      disable: false
    },
    default_value: "",
    condition: {
      name: "involve_payment",
      value: "involve"
    },
    callback: {
      enabled: ["deposit_amount"],
      action: "parentCallback",
      function: "debouncedAmountInput(values, methods)"
    }
  },
  {
    id: "amount",
    name: "deposit_amount",
    type: "text",
    value_type: "number-currency",
    additional_options: [],
    disabled: true,
    disabled_condition: {
      type: "array",
      names: ["trading_acc_no", "entity"],
      value: "has_value",
      disable: false
    },
    default_value: "",
    condition: {
      name: "involve_payment",
      value: "not_involve"
    },
    callback: {
      enabled: ["deposit_amount"],
      action: "parentCallback",
      function: "debouncedAmountInput(values, methods, name)"
    }
  },
  {
    id: "exchangeRate",
    name: "ref_exchange_rate",
    type: "text",
    disabled: true,
    payload_skipped: true
  },
  {
    id: "refAmount",
    name: "ref_deposit_amount",
    type: "text",
    value_type: "number-currency",
    allow_negative: true,
    validate_input: false,
    additional_value: "settlement_currency",
    disabled: true,
    payload_skipped: true
  },
  {
    id: "charge",
    name: "ref_service_fee",
    condition: {
      name: "involve_payment",
      value: "involve"
    },
    type: "text",
    value_type: "number-currency",
    additional_value: "currency",
    disabled: true,
    payload_skipped: true
  },
  { id: "comment", name: "remark", type: "text", disabled: false },
  {
    id: "settlementCurrency",
    type: "text",
    name: "settlement_currency",
    hidden: true,
    payload_skipped: true
  },
  {
    id: "balance",
    name: "account_balance",
    type: "text",
    disabled: true,
    allow_negative: true,
    hidden: true,
    callback: {
      enabled: ["trading_acc_name"],
      action: "parentCallback",
      function: "getAccountBalance(values, methods)"
    }
  },
  {
    id: "cryptoCurrency",
    type: "text",
    name: "crypto_currency",
    hidden: true,
    payload_skipped: true
  },
  { id: "currency", type: "text", name: "currency", hidden: true },
  { id: "requestBy", type: "text", name: "request_by", hidden: true },
  { id: "tradeType", type: "text", name: "trade_type", default_value: "MIA", hidden: true },
  { id: "paymentGroup", type: "text", name: "payment_group", hidden: true, payload_skipped: true }
];

const AddDepositSchema = object({
  entity: string().min(1, { message: "Entity is required" }),
  trading_acc_no: string().min(1, { message: "Trading Account No. is required" }),
  trading_acc_name: string().min(1, { message: "Trading Account Name is required" }),
  account_balance: string().min(1, { message: "Account Balance is required" }),
  adjustment_type: string().min(1, { message: "Adjustment Type is required" }),
  involve_payment: string().min(1, { message: "Involve Payment is required" }),
  remark: string().max(20, { message: "Remark must contain at most 20 character(s)" }).optional(),
  deposit_amount: string().min(1, { message: "Deposit Amount is required" }),
  ref_deposit_amount: preprocess(
    (a: any) => parseFloat(string().parse(a)),
    number({
      required_error: "Ref. Withdrawal Amount is required",
      invalid_type_error: "Ref. Withdrawal Amount is required"
    }).positive()
  ),
  settlement_currency: string().min(0),
  currency: string().min(0),
  request_by: string().min(0),
  trade_type: string().min(0)
});

const InvolvesSchema = object({
  adjustment_type: string().min(1, { message: "Adjustment Type is required" }),
  deposit_channel: string().min(1, { message: "Deposit Channel is Required" }),
  ref_exchange_rate: string().min(1, { message: "Exchange Rate is required" }),
  ref_service_fee: string().min(1, { message: "Ref Service Fee is required" })
});

const CryptoSchema = object({
  network: string().min(1, { message: "Network is required" }),
  wallet_address: string().min(1, { message: "Wallet Address is required" })
});

const DepositForm: FC = () => {
  const {
    push,
    query: { feature, page }
  } = useRouter();
  const [_, updateState] = useState<any>();
  const forceUpdate = useCallback(() => updateState({}), []);
  const { formFields, formValidations, setFormValue } = useFormContent();

  const { data: entityId } = useQuery(["get-entity"], getEntityId);

  const { mutate: fetchDepositChannel } = useMutation((payload: any) =>
    getDepositChannelList({ ...payload.payload })
  );
  const { mutate: fetchAdjustmentType } = useMutation((payload: any) => getAdjustmentTypeList());
  const { mutate: fetchAccountDetail, isLoading: fetchingAccount } = useMutation((payload: any) =>
    getAccounts({ ...payload.payload })
  );
  const { mutate: fetchAccountBalance, isLoading: fetchingAccountBalance } = useMutation((payload: any) =>
    getAccountBalanceCheck({ ...payload.payload })
  );
  const { mutate: fetchServiceCharge } = useMutation((payload: any) =>
    getServiceChargeAmount({ ...payload.payload })
  );
  const { mutate: fetchExchangeRateList } = useMutation((payload: any) =>
    getExchangeRateList({ ...payload.payload })
  );
  const { mutate: fetchExchangeRate } = useMutation((payload: any) =>
    getExchangeRateAmount({ ...payload.payload })
  );
  const { mutate: fetchManualDeposit, isLoading: fetchingDeposit } = useMutation(({ payload }: any) =>
    postDepositInst(payload)
  );

  const { mutate: editStatus, isLoading: updateDepositStatus } = useMutation((payload: any) =>
    putDepositStatus(payload)
  );

  const [initialData, setInitialData] = useState(null);
  const [depositChannelList, setDepositChannelList] = useState<any>(null);
  const [adjustmentTypeList, setAdjustmentTypeList] = useState<any>(null);
  const [bankAccountList, setBankAccountList] = useState<any>(null);
  const [requestError, setRequestError] = useState(null);

  const { data: formData, setFormData, emptyFields } = useFormData();
  const { getItem } = useLocalStorage();

  useEffect(() => {
    if (!formFields) {
      if (page == "add") {
        setFormValue("fields", AddDepositFields);
        setFormValue("validations", AddDepositSchema);
      } else {
        setFormValue("fields", DetailDepositFields);
      }
    }
  }, []);

  useEffect(() => {
    if (formFields) {
      if (formData && page == "detail") {
        let current: any = {};
        formFields.map((field: any) => {
          if (
            formData[field.id] ||
            typeof formData[field.id] === "boolean" ||
            field.id == "custom_value" ||
            field?.ids?.length
          ) {
            let value = "";
            if (field.name == "exchange_rate") {
              if (formData["actExchangeRate1"] && formData["actExchangeRate2"]) {
                value = (formData["actExchangeRate1"] * formData["actExchangeRate2"]).toString();
              } else {
                value = (formData["tmpExchangeRate1"] * formData["tmpExchangeRate2"]).toString();
              }
            } else if (field.name == "platform_amount") {
              value = formData["actDepositAmount"]
                ? formData["actDepositAmount"]?.toString()
                : formData["tmpDepositAmount"]?.toString();
            } else if (field.name == "actual_amount") {
              value = formData["actAmount"]
                ? formData["actAmount"]?.toString()
                : formData["tmpAmount"]?.toString();
            } else if (field.name == "service_charge") {
              value += formData["serviceChargeCurrency"];
              value += " ";
              value += formData["actCharge"]
                ? formData["actCharge"]?.toString()
                : formData["tmpCharge"]?.toString();
            } else {
              value =
                typeof formData[field.id] === "number" || typeof formData[field.id] === "boolean"
                  ? formData[field.id].toString()
                  : formData[field.id];
            }
            Object.assign(current, { [field.name]: value });
          }
        });
        setInitialData(current);
      } else {
        let current: any = {};
        formFields.map((val: any) => {
          if (val.default_value) {
            Object.assign(current, { [val.name]: val.default_value });
          } else {
            Object.assign(current, { [val.name]: "" });
          }
        });
        setInitialData(current);
      }
    }
  }, [formFields]);

  useEffect(() => {
    if (entityId) {
      const options = entityId?.response[0].items.map((el: any) => ({
        label: el.name,
        value: el.codeCode
      }));
      AddDepositFields[0].options = options.filter((el: any) => el.value !== "EBL");
      forceUpdate();
    }
  }, [entityId]);

  useEffect(() => {
    if (adjustmentTypeList) {
      const options = adjustmentTypeList?.map((el: any) => ({
        label: el.nameEn,
        value: el.codeCode
      }));
      AddDepositFields[4].options = options;
      forceUpdate();
    }
  }, [adjustmentTypeList]);

  useEffect(() => {
    if (depositChannelList) {
      const options = depositChannelList.map((el: any) => ({
        label: el.gatewayNameEn,
        value: el.id,
        origin: el
      }));
      AddDepositFields[5].options = options;
      forceUpdate();
    }
  }, [depositChannelList]);

  const getDepositChannel = (formValues: any, formMethods: any) => {
    fetchDepositChannel(
      { payload: { entityId: formValues.entity } },
      {
        onSuccess: (res) => {
          if (res.result) {
            setDepositChannelList(res.response);
          }
        }
      }
    );
  };

  const onSelectDepositChannel = (formValues: any, fieldValue: any, formMethods: any) => {
    const currency = fieldValue.currencies[0].cryptoCurrency
      ? fieldValue.currencies[0].cryptoCurrency
      : fieldValue.currencies[0].currency;
    const isCrypto = fieldValue.currencies[0].cryptoCurrency ? true : false;

    let Schema = null;
    if (formValues.involve_payment == "involve") {
      Schema = AddDepositSchema.merge(InvolvesSchema);
    } else {
      Schema = AddDepositSchema;
    }
    setFormValue("validations", Schema);

    if (isCrypto) {
      const CombinedCryptoSchema = Schema.merge(CryptoSchema);
      setFormValue("validations", CombinedCryptoSchema);
    } else {
      removeCryptoField(formMethods);
    }

    formMethods.setValue("deposit_channel", fieldValue.id.toString(), { shouldValidate: true });
    formMethods.setValue("currency", currency, { shouldValidate: true });
    formMethods.setValue("crypto_currency", fieldValue.currencies[0].cryptoCurrency, {
      shouldValidate: true
    });
    formMethods.setValue("is_crypto", !fieldValue.currencies[0].cryptoCurrency);
    if (formValues.deposit_amount) {
      getExchangeRate(formValues, formMethods);
    }
    if (isCrypto) {
      const networks: { label: string; value: string }[] = [];
      fieldValue.currencies.forEach((val: any) => {
        const network = { label: val.cryptoNetwork, value: val.currency, origin: val };
        networks.push(network);
      });
      AddDepositFields[FindIndexFormField(AddDepositFields, "network")].options = networks;
    }
    forceUpdate();
  };

  const onSelectNetwork = (fieldValue: any, formMethods: any) => {
    formMethods.setValue("network", fieldValue.currency);
  };

  const removeCryptoField = (formMethods: any) => {
    formMethods.clearErrors("wallet_address");
    formMethods.setValue("wallet_address", "");
    formMethods.clearErrors("network");
    formMethods.setValue("network", "");
    formMethods.setValue("network", "");
    formMethods.setValue("crypto_currency", "");
  };

  const onSelectEntity = (fieldValue: any, formMethods: any) => {
    formMethods.setValue("entity", fieldValue, { shouldValidate: true });
    formMethods.setValue("trading_acc_no", "");
    formMethods.setValue("trading_acc_name", "");
    formMethods.setValue("deposit_amount", "");
    formMethods.setValue("ref_exchange_rate", "");
    formMethods.setValue("ref_service_fee", "");
    formMethods.setValue("ref_deposit_amount", "", { shouldValidate: false });
    formMethods.setValue("settlement_currency", "USD", {
      shouldValidate: true
    });
    removeCryptoField(formMethods);

    fetchExchangeRateList(
      { payload: { exchangeType: null, entityId: fieldValue } },
      {
        onSuccess: (res: any) => {
          const amountCurrencies: { value: string; label: string }[] = [];
          res.response.forEach((value: any) => {
            if (!amountCurrencies.some((el) => el.value === value.baseCurrency)) {
              amountCurrencies.push({ value: value.baseCurrency, label: value.baseCurrency });
            }
          });
          AddDepositFields[FindIndexFormField(AddDepositFields, "deposit_amount") + 1].additional_options =
            amountCurrencies;
          forceUpdate();
        }
      }
    );
  };

  const getAdjustmentType = () => {
    fetchAdjustmentType(
      {},
      {
        onSuccess: (res) => {
          if (res.result) {
            const items = res.response.filter(function (item: any) {
              return item.codeCode.toLowerCase().includes("mi") && item.active == true;
            });
            setAdjustmentTypeList(items);
          }
        }
      }
    );
  };

  const getAccountDetails = (formValues: any, formMethods: any) => {
    const account = getItem(LOCALSTORAGE_KEY.ACCOUNT);
    formMethods.setValue("request_by", account);
    formMethods.clearErrors("trading_acc_no");
    formMethods.setValue("trading_acc_name", "");
    formMethods.setValue("account_balance", "");
    fetchAccountDetail(
      { payload: { entityId: formValues.entity, tradingAccountNo: formValues.trading_acc_no } },
      {
        onSuccess: (res) => {
          if (res.result == true) {
            formMethods.setValue("trading_acc_name", res.response.realName, {
              shouldValidate: true
            });
            formMethods.setValue("payment_group", res.response.paymentGroupCode, {
              shouldValidate: true
            });
          } else {
            formMethods.setError("trading_acc_no", {
              type: "custom",
              message: res.errorDescription
            });
          }
        }
      }
    );
  };

  const getAccountBalance = (formValues: any, formMethods: any) => {
    fetchAccountBalance(
      { payload: { entityId: formValues.entity, tradingAccountNo: formValues.trading_acc_no } },
      {
        onSuccess: (res) => {
          if (res.result) {
            formMethods.setValue("account_balance", res.response.balance.toString(), {
              shouldValidate: true
            });
          } else {
            formMethods.setError("account_balance", {
              type: "custom",
              message: res.errorDescription ? res.errorDescription : "Request error, please try again"
            });
          }
        }
      }
    );
  };

  const onChangeInvolveField = (formValues: any, formMethods: any) => {
    formMethods.setValue("ref_deposit_amount", "", { shouldValidate: false });
    formMethods.setValue("currency", "");
    formMethods.setValue("deposit_channel", "");
    formMethods.setValue("bank_acc_list", "");
    formMethods.setValue("ref_exchange_rate", "");
    formMethods.setValue("ref_service_fee", "");
    if (formValues.involve_payment == "involve") {
      const CombinedSchema = AddDepositSchema.merge(InvolvesSchema);
      setFormValue("validations", CombinedSchema);
    } else if (formValues.involve_payment == "not_involve") {
      const Schema = AddDepositSchema;
      setFormValue("validations", Schema);
    }
  };

  const getExchangeRate = (formValues: any, formMethods: any) => {
    formMethods.setValue(
      "deposit_amount",
      Number(formValues.deposit_amount) > -1 ? formValues.deposit_amount : "0"
    );
    formMethods.clearErrors("deposit_amount");
    if (formValues.deposit_amount == 0) {
      formMethods.setError("deposit_amount", {
        type: "custom",
        message: "Amount cannot be 0"
      });
    }
    formMethods.setValue("ref_exchange_rate", "");
    formMethods.clearErrors("ref_exchange_rate");
    formMethods.setValue("ref_service_fee", "");
    formMethods.clearErrors("ref_service_fee");
    formMethods.setValue("ref_deposit_amount", "", { shouldValidate: false });
    formMethods.clearErrors("ref_deposit_amount");

    if (formValues.deposit_amount == 0 || formValues.deposit_amount == "") return;
    const isCrypto = formValues.crypto_currency ? true : false;

    const payload = {
      entityId: formValues.entity,
      paymentGroup: formValues?.payment_group,
      tradingAccountNo: formValues.trading_acc_no,
      baseCurrency: isCrypto ? formValues.network : formMethods.getValues("currency"),
      quoteCurrency: "USD",
      exchangeType: "PER"
    };

    fetchExchangeRate(
      { payload },
      {
        onSuccess: (res) => {
          if (res.result) {
            formMethods.setValue("ref_exchange_rate", res.response.depositRate.toString());
            if (formValues.involve_payment == "not_involve") {
              const settlement_amount = Number(formValues.deposit_amount) / res.response.depositRate!;
              formMethods.setValue("ref_deposit_amount", RoundNumber(settlement_amount).toString(), {
                shouldValidate: true
              });
            } else if (formValues.involve_payment == "involve") {
              getServiceCharge(formValues, formMethods, res.response.depositRate);
            }
          } else {
            formMethods.setError("ref_exchange_rate", {
              type: "custom",
              message: res.errorDescription
            });
          }
        }
      }
    );
  };

  const getServiceCharge = (formValues: any, formMethods: any, depositRate: number) => {
    const payload = {
      entityId: formValues.entity || "",
      paymentChannelId: formMethods.getValues("deposit_channel").toString() || "",
      paymentGroup: formMethods.getValues("payment_group") || "",
      tradingAccountNo: formValues?.trading_acc_no || "",
      amount: formValues.deposit_amount || 0
    };
    fetchServiceCharge(
      { payload },
      {
        onSuccess: (res) => {
          if (res.result) {
            const settlement_amount =
              (Number(formValues.deposit_amount) - res.response.charge) / depositRate!;
            formMethods.setValue("ref_deposit_amount", RoundNumber(settlement_amount).toString(), {
              shouldValidate: true
            });
            formMethods.setValue("ref_service_fee", res.response.charge.toString());
          } else {
            formMethods.setError("ref_service_fee", {
              type: "custom",
              message: res.errorDescription
            });
          }
        }
      }
    );
  };

  const debouncedAmountInput = useMemo(() => {
    return debouce(getExchangeRate, 800);
  }, []);

  useEffect(() => {
    return () => {
      debouncedAmountInput.cancel();
    };
  });

  const debouncedAccountInput = useMemo(() => {
    return debouce(getAccountDetails, 800);
  }, []);

  useEffect(() => {
    return () => {
      debouncedAccountInput.cancel();
    };
  });

  useEffect(() => {
    forceUpdate();
  }, [initialData]);

  const handleSubmitForm = (values: any) => {
    const data = {};
    AddDepositFields.forEach(function (field: any) {
      if (!field.payload_skipped) {
        let value = values[field.name] || "";
        if (field.name == "involve_payment") {
          value = values[field.name] == "involve" ? true : false;
        }
        if (field.name == "currency" && values.network) {
          value = values.network;
        }
        const key = field.id;
        Object.assign(data, { [key]: value });
      }
    });
    fetchManualDeposit(
      { payload: data },
      {
        onSuccess: (res) => {
          if (res.result) {
            push(`/${feature}/management`);
          } else {
            setRequestError(res);
          }
        }
      }
    );
  };

  const handleCancel = async () => {
    emptyFields();
    setInitialData(null);
  };

  const handleDetailAction = (values: any) => {
    const { getItem } = useLocalStorage();
    const account = getItem(LOCALSTORAGE_KEY.ACCOUNT);
    switch (values.action) {
      case "approve":
        return editStatus(
          [
            {
              requestBy: account,
              instRequestId: formData.instRequestId,
              status: 1
            }
          ],
          {
            onSuccess: ({ response }) => {
              if (response[0].status !== "false") {
                push(`/${feature}/management`);
              }
            }
          }
        );

      case "reject":
        return editStatus(
          [
            {
              requestBy: account,
              instRequestId: formData.instRequestId,
              comment: values.data.comment,
              status: 2
            }
          ],
          {
            onSuccess: ({ response }) => {
              if (response[0].status !== "false") {
                push(`/${feature}/management`);
              }
            }
          }
        );
    }
  };

  return (
    <DetailContentForm
      formData={formData}
      initialData={initialData}
      fields={formFields}
      validations={formValidations}
      onSubmit={handleSubmitForm}
      onCancel={() => setFormData(null)}
      parentCallback={(func: any, values: any, methods: any) => {
        eval(func);
      }}
      onSelectCallback={(func: any, values: any, value: any, methods: any) => {
        eval(func);
      }}
      fetchingRequest={fetchingDeposit || updateDepositStatus}
      requestError={requestError}
      detailAction={handleDetailAction}
    />
  );
};

export default DepositForm;
