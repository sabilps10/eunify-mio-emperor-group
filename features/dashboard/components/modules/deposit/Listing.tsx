import { FC, useCallback, useEffect, useState } from "react";
import moment from "moment";
import { useMutation, useQuery } from "react-query";

import TableList from "@/components/TableList";
import { DEPOSIT_ACTION, DEPOSIT_HEADER, DEPOSIT_EXPORT_HEADING } from "@/config/tables/deposit-record";
import {
  getAdjusmentType,
  getCurrency,
  getEntityId,
  getInstStatus,
  getTradeType,
  checkEAML
} from "@/services/common-services";
import {
  getDepositChannelList,
  getDepositInst,
  putDepositStatus,
  updateDepositAML
} from "@/services/mio-services";
import { getAccounts } from "@/services/common-services";
import { FindIndexFormField } from "@/config/utilities";
import useLocalStorage from "@/hooks/useLocalStorage";
import { AML_CODE, LOCALSTORAGE_KEY } from "@/config/constants";
import { FilterPagination, Pagination } from "@/types";
import usePagination from "@hooks/usePagination";
import toast from "react-hot-toast";
import useAccess from "@/hooks/useAccess";
import { useRouter } from "next/router";

export const DEPOSIT_FILTER: any = [
  { name: "tradeType", id: "type", type: "dropdown", options: [] },
  { name: "adjustmentType", id: "adjustment_type", type: "dropdown", options: [] },
  { name: "entityId", id: "entity", type: "dropdown", options: [] },
  { name: "orderNo", id: "reference_no", type: "text" },
  {
    name: "isValidAml",
    id: "aml_status",
    type: "dropdown",
    options: [
      { value: "true", label: "Pass" },
      { value: "false", label: "Fail" }
    ]
  },
  { name: "currency", id: "actual_currency", type: "dropdown", options: [] },
  { name: "tradingAccountNo", id: "trading_acc", type: "text" },
  { name: "paymentChannelId", id: "deposit_channel", type: "dropdown", options: [] },
  { name: "applicationTime", id: "application_time", type: "time-range" },
  { name: "status", id: "status", type: "dropdown", options: [] },
  {
    name: "includeDummyAccount",
    id: "dummy_account",
    type: "dropdown",
    options: [
      { value: "true", label: "Include" },
      { value: "false", label: "Exclude" }
    ]
  }
];

type updateApprovalStatusVariables = {
  payload: any;
};
type FilterData = {
  entityId: string | number | null;
  intRequestId: string | number | null;
  adjustmentType: string | number | null;
  orderNo: string | number | null;
  tradingAccountNo: string | number | null;
  status: string | number | null;
  applicationTimeFrom: string | number | null;
  applicationTimeTo: string | number | null;
  tranDateFrom: string | number | null;
  tranDateTo: string | number | null;
  includeDummyAccount: boolean;
  isValidAml: boolean | null;
  paymentChannelId: string | number | null;
  tradeType: string | number | null;
  currency: string | number | null;
  entityIdList: string[];
};

type RowRetry = {
  row: any;
  status: string;
} | null;

const DepositRecordListing: FC = () => {
  const {
    query: { feature }
  } = useRouter();
  const [, updateState] = useState<any>();
  const [rowRetry, setIsRetryAML] = useState<RowRetry>(null);
  const forceUpdate = useCallback(() => updateState({}), []);
  const { access } = useAccess(feature!, "readAccess");

  const [depositRecord, setDepositRecord] = useState<any>([]);
  const [depositExport, setDepositExport] = useState<any>([]);
  const [filterPagination, setFilterPagination, pagination, setPagination] = usePagination<FilterData>({
    entityId: null,
    intRequestId: null,
    adjustmentType: null,
    orderNo: null,
    tradingAccountNo: null,
    status: null,
    applicationTimeFrom: null,
    applicationTimeTo: null,
    tranDateFrom: null,
    tranDateTo: null,
    includeDummyAccount: false,
    isValidAml: null,
    paymentChannelId: null,
    currency: null,
    tradeType: null,
    entityIdList: access
  });

  const { data: entityData } = useQuery(["get-entity"], getEntityId);
  const { data: adjustmentTypeData } = useQuery(["get-adjustment-type"], getAdjusmentType);
  const { data: currencyData } = useQuery(["get-currency"], getCurrency);
  const { data: statusData } = useQuery(["get-inst-status"], getInstStatus);
  const { data: typeData } = useQuery(["get-type"], getTradeType);
  const { data: depositChannelData } = useQuery(["get-deposit-channel"], async () => {
    const { response } = await getDepositChannelList({});
    return response;
  });

  const { mutate: mutatePage, isLoading: isLoadingPage } = useMutation((payload: any) =>
    getDepositInst(payload, "page")
  );

  const { mutate: mutateList, isLoading: isLoadingList } = useMutation((payload: any) =>
    getDepositInst(payload, "list")
  );

  const { mutate: editStatus, isLoading: loadingAction } = useMutation((payload: any) =>
    putDepositStatus(payload)
  );

  const { mutate: retryAML, isLoading: loadingAML } = useMutation((payload: any) =>
    updateDepositAML(payload)
  );

  useEffect(() => {
    if (filterPagination?.filter?.entityIdList?.length && !isLoadingPage) {
      mutatePage(filterPagination, {
        onSuccess: ({ response }) => {
          const { content, ...rest } = response;
          setDepositRecord(content);
          setPagination(rest);
        }
      });
    }
  }, [filterPagination]);

  useEffect(() => {
    if (filterPagination?.filter?.entityIdList?.length && !isLoadingList) {
      mutateList(filterPagination.filter, {
        onSuccess: ({ response }: any) => {
          setDepositExport(response);
        }
      });
    }
  }, [filterPagination.filter]);

  useEffect(() => {
    setFilterPagination({
      ...filterPagination,
      filter: { ...filterPagination.filter, entityIdList: access }
    });
  }, [access]);

  useEffect(() => {
    if (entityData) {
      let items: { label: string; value: any }[] = [];
      entityData?.response[0].items.forEach(function (val: any) {
        let item = { label: "", value: "" };
        item.label = val.name;
        item.value = val.codeCode;
        items.push(item);
      });
      DEPOSIT_FILTER[2].options = items;
      forceUpdate();
    }
  }, [entityData]);

  useEffect(() => {
    if (adjustmentTypeData) {
      let items: { label: string; value: any }[] = [];
      adjustmentTypeData?.response[0]?.items?.forEach(function (val: any) {
        if (val.codeCode.toLowerCase().includes("mi")) {
          let item = { label: "", value: "" };
          item.value = val.codeCode;
          item.label = val.name;
          items.push(item);
        }
      });
      DEPOSIT_FILTER[1].options = items;
      forceUpdate();
    }
  }, [adjustmentTypeData]);

  useEffect(() => {
    if (typeData) {
      let items: { label: string; value: any }[] = [];
      typeData.response[0].items.forEach(function (val: any) {
        if (val.codeCode.toLowerCase().includes("mi")) {
          let item = { label: "", value: "" };
          item.value = val.codeCode;
          item.label = val.name;
          items.push(item);
        }
      });
      DEPOSIT_FILTER[0].options = items;
      forceUpdate();
    }
  }, [typeData]);

  useEffect(() => {
    if (currencyData) {
      let items: { label: string; value: any }[] = [];
      currencyData.response[0].items.forEach(function (val: any) {
        let item = { label: "", value: "" };
        item.label = val.name;
        item.value = val.codeCode;
        items.push(item);
      });
      DEPOSIT_FILTER[5].options = items;
      forceUpdate();
    }
  }, [currencyData]);

  useEffect(() => {
    if (statusData) {
      let items: { label: string; value: any }[] = [];
      statusData.response[0].items.forEach(function (val: any) {
        let item = { label: "", value: "" };
        item.label = val.name;
        item.value = val.codeInt.toString();
        items.push(item);
      });
      DEPOSIT_FILTER[FindIndexFormField(DEPOSIT_FILTER, "status")].options = items;
      forceUpdate();
    }
  }, [statusData]);

  useEffect(() => {
    if (depositChannelData?.length > 0) {
      let items: { label: string; value: any }[] = [];
      depositChannelData?.forEach(function (val: any) {
        let item = { label: "", value: "" };
        item.label = val.gatewayNameEn;
        item.value = val.id.toString();
        items.push(item);
      });
      DEPOSIT_FILTER[FindIndexFormField(DEPOSIT_FILTER, "paymentChannelId")].options = items;
      forceUpdate();
    }
  }, [depositChannelData]);

  const handleDoFilterData = (values: any) => {
    let filter: any = filterPagination.filter;
    Object.keys(values).forEach(function (key) {
      if (key == "applicationTime_start" || key == "applicationTime_end") {
        const splitString = key.split("_");
        if (splitString[1] == "start") {
          filter["applicationTimeFrom"] = values.applicationTime_start
            ? moment(values.applicationTime_start).format("Y-MM-DD HH:mm:ss")
            : null;
        }
        if (splitString[1] == "end") {
          filter["applicationTimeTo"] = values.applicationTime_end
            ? moment(values.applicationTime_end).format("Y-MM-DD HH:mm:ss")
            : null;
        }
      } else if (key == "status") {
        filter["status"] = values[key] ? values[key] : null;
      } else if (key == "trading_acc") {
        filter["tradingAccountNo"] = values[key] ? [values[key]] : null;
      } else if (key == "includeDummyAccount") {
        filter["includeDummyAccount"] = values[key] == "true" ? true : false;
      } else if (key == "isValidAml") {
        filter["isValidAml"] =
          values[key] !== "" && typeof values[key] !== "undefined"
            ? values[key] == "true"
              ? true
              : false
            : null;
      } else {
        filter[key] = values[key] ? values[key] : null;
      }
    });

    setFilterPagination({ ...filterPagination, filter: { ...filterPagination.filter, ...filter } });
  };

  const handleRowAction = (values: any) => {
    const { getItem } = useLocalStorage();
    const account = getItem(LOCALSTORAGE_KEY.ACCOUNT);
    switch (values.action) {
      case "retry-aml":
        return retryAML(
          {
            instRequestId: values.data.instRequestId,
            requestBy: account
          },
          {
            onSuccess: ({ response }) => {
              mutatePage(filterPagination, {
                onSuccess: ({ response }) => {
                  const { content, ...rest } = response;
                  setDepositRecord(content);
                  setPagination(rest);
                }
              });
              setIsRetryAML({ row: values.data.id, status: response.validAml == true ? "Pass" : "Fail" });
            }
          }
        );
      case "approve":
        return editStatus(
          [
            {
              requestBy: account,
              instRequestId: values.data.instRequestId,
              status: 1
            }
          ],
          {
            onSuccess: ({ response }) => {
              if (response[0].status !== "false") {
                mutatePage(filterPagination, {
                  onSuccess: ({ response }) => {
                    const { content, ...rest } = response;
                    setDepositRecord(content);
                    setPagination(rest);
                  }
                });
              }
            }
          }
        );

      case "reject":
        return editStatus(
          [
            {
              requestBy: account,
              instRequestId: values.data.instRequestId,
              comment: values.data.comment,
              status: 2
            }
          ],
          {
            onSuccess: ({ response }) => {
              if (response[0].status !== "false") {
                mutatePage(filterPagination, {
                  onSuccess: ({ response }) => {
                    const { content, ...rest } = response;
                    setDepositRecord(content);
                    setPagination(rest);
                  }
                });
              }
            }
          }
        );
    }
  };

  return (
    <TableList
      header={DEPOSIT_HEADER}
      filter={DEPOSIT_FILTER}
      action={DEPOSIT_ACTION}
      rowActionCallback={handleRowAction}
      data={depositRecord}
      isLoading={isLoadingPage}
      actionButtonLoading={loadingAML || loadingAction}
      filterData={filterPagination.filter}
      onFilterData={(values: any) => handleDoFilterData(values)}
      exportDataObject={{ heading: DEPOSIT_EXPORT_HEADING, data: depositExport, loading: isLoadingList }}
      rowRetry={rowRetry}
      isAccess={true}
      isPagination={true}
      pagination={pagination}
      handlePagination={(val) => {
        setFilterPagination({ ...filterPagination, ...val });
      }}
    />
  );
};

export default DepositRecordListing;
