import { FC, useEffect, useState, useCallback } from "react";
import { useQuery, useMutation } from "react-query";

import { getEntityId, getPaymentStatus, getTradeType, getChannel } from "@/services/common-services";
import {
  getTransactions,
  getDepositChannelList,
  getWithdrawalChannelList,
  getTransactionsRecords
} from "@/services/mio-services";
import TableList from "@/components/TableList";

import {
  TRANSACTIONS_ACTION,
  TRANSACTIONS_HEADER,
  TRANSACTION_EXPORT_HEADING
} from "@/config/tables/transactions";
import moment from "moment";
import usePagination from "@/hooks/usePagination";
import { filter } from "lodash";
import toast from "react-hot-toast";
import useAccess from "@/hooks/useAccess";
import { useRouter } from "next/router";

const channelDefaultFilter = {
  paymentChannelId: null,
  entityId: null,
  gatewayId: null,
  gatewayStatus: null
};

const TRANSACTIONS_FILTER = [
  { name: "entityId", id: "entity", type: "dropdown", options: [] },
  { name: "paymentChannelId", id: "channel", type: "dropdown", options: [] },
  { name: "orderNo", id: "reference_no", type: "text" },
  { name: "tradingAccountNo", id: "trading_acc_no", type: "text" },
  { name: "tradeType", id: "type", type: "dropdown", options: [] },
  { name: "status", id: "status", type: "dropdown", options: [] },
  { name: "applicationTime", id: "application_time", type: "time-range" },
  { name: "tranDate", id: "transaction_time", type: "time-range" },
  {
    name: "includeDummyAccount",
    id: "dummy_account",
    type: "dropdown",
    options: [
      { value: "true", label: "Include" },
      { value: "false", label: "Exclude" }
    ]
  }
];

type FilterData = {
  entityId: number | null;
  paymentChannelId: string | number | null;
  orderNo: string | number | null;
  tradingAccountNo: any[] | null;
  tradeType: string | number | null;
  status: string | number | number[] | null;
  applicationTimeFrom: string | null;
  applicationTimeTo: string | null;
  tranDateFrom: string | null;
  tranDateTo: string | null;
  includeDummyAccount: boolean | null;
  applicationPeriod: string | null;
  entityIdList: string[];
} | null;

const TransactionListing: FC = () => {
  const {
    query: { feature }
  } = useRouter();
  const { access } = useAccess(feature!, "readAccess");
  const [_, updateState] = useState<any>();
  const [transactionExport, setTransactionExport] = useState<any>([]);
  const [transactionList, setTransactionList] = useState([]);
  const { data: entity } = useQuery(["get-entity"], getEntityId);
  const { data: bankStatus } = useQuery(["get-payment-status"], getPaymentStatus);
  const { data: tradetype } = useQuery(["get-trade-type"], getTradeType);
  const { data: depositChannel } = useQuery(["get-deposit-channel-option-list"], () =>
    getDepositChannelList(channelDefaultFilter)
  );
  const { data: withdrawalChannel } = useQuery(["get-withdrawal-channel-option-list"], () =>
    getWithdrawalChannelList(channelDefaultFilter)
  );
  const { mutate: mutateList, isLoading: loadingList } = useMutation((payload: any) =>
    getTransactions(payload)
  );
  const { mutate: mutatePage, isLoading: loading } = useMutation((payload: any) =>
    getTransactionsRecords(payload)
  );

  const [filterPagination, setFilterPagination, pagination, setPagination] = usePagination<FilterData>({
    entityId: null,
    paymentChannelId: null,
    tradingAccountNo: null,
    tradeType: null,
    status: null,
    applicationTimeFrom: null,
    applicationTimeTo: null,
    tranDateFrom: null,
    tranDateTo: null,
    includeDummyAccount: false,
    orderNo: null,
    applicationPeriod: null,
    entityIdList: access
  });

  const exportToFileObject = {
    heading: TRANSACTION_EXPORT_HEADING,
    data: transactionExport,
    loading: loadingList
  };

  const forceUpdate = useCallback(() => updateState({}), []);

  // console.log(loading);

  useEffect(() => {
    if (filterPagination?.filter?.entityIdList?.length && !loading) {
      mutatePage(filterPagination, {
        onSuccess: ({ response }) => {
          const { content, ...rest } = response;
          setTransactionList(content);
          setPagination(rest);
        }
      });
    }
  }, [filterPagination]);

  useEffect(() => {
    if (filterPagination?.filter?.entityIdList?.length && !loading) {
      mutateList(filterPagination.filter, {
        onSuccess: ({ response }) => {
          setTransactionExport(response);
        }
      });
    }
  }, [filterPagination.filter]);

  useEffect(() => {
    setFilterPagination({
      ...filterPagination,
      filter: { ...filterPagination.filter, entityIdList: access }
    });
  }, [access]);

  useEffect(() => {
    if (entity) {
      const options = entity?.response[0]?.items.map((el: any) => ({
        label: el.name,
        value: el.codeCode
      }));
      TRANSACTIONS_FILTER[0].options = options;
      forceUpdate();
    }
  }, [entity]);

  useEffect(() => {
    if (bankStatus) {
      const options = bankStatus?.response[0].items.map((el: any) => ({
        label: el.name,
        value: el.codeInt.toString()
      }));
      TRANSACTIONS_FILTER[5].options = options;
      forceUpdate();
    }
  }, [bankStatus]);

  useEffect(() => {
    if (tradetype) {
      const options = tradetype?.response[0].items.map((el: any) => ({
        label: el.name,
        value: el.codeCode
      }));

      TRANSACTIONS_FILTER[4].options = options;
      forceUpdate();
    }
  }, [tradetype]);

  useEffect(() => {
    if (depositChannel && withdrawalChannel) {
      const deposit = depositChannel?.response?.map((el: any) => ({
        label: el.gatewayNameEn,
        value: el.id.toString()
      }));

      const withdraw = withdrawalChannel?.response?.map((el: any) => ({
        label: el.gatewayNameEn,
        value: el.id.toString()
      }));
      TRANSACTIONS_FILTER[1].options = [...deposit, ...withdraw];
      forceUpdate();
    }
  }, [depositChannel, withdrawalChannel]);

  const handleFilterData = (values: any) => {
    let filter: any = filterPagination.filter;

    Object.keys(values).forEach(function (key) {
      if (key == "applicationTime_start" || key == "applicationTime_end") {
        const splitString = key.split("_");
        if (splitString[1] == "start") {
          filter["applicationTimeFrom"] = values.applicationTime_start
            ? moment(values.applicationTime_start).format("Y-MM-DD HH:mm:ss")
            : null;
        }
        if (splitString[1] == "end") {
          filter["applicationTimeTo"] = values.applicationTime_end
            ? moment(values.applicationTime_end).format("Y-MM-DD HH:mm:ss")
            : null;
        }
      } else if (key == "tranDate_start" || key == "tranDate_end") {
        const splitString = key.split("_");
        if (splitString[1] == "start") {
          filter["tranDateFrom"] = values.tranDate_start
            ? moment(values.tranDate_start).format("Y-MM-DD HH:mm:ss")
            : null;
        }
        if (splitString[1] == "end") {
          filter["tranDateTo"] = values.tranDate_end
            ? moment(values.tranDate_end).format("Y-MM-DD HH:mm:ss")
            : null;
        }
      } else if (key == "status") {
        filter["status"] = values[key] ? [values[key]] : null;
      } else if (key == "tradingAccountNo") {
        filter["tradingAccountNo"] = values[key] ? [values[key]] : null;
      } else {
        filter[key] = values[key] ? values[key] : null;
      }
    });

    setFilterPagination({ ...filterPagination, filter: { ...filterPagination.filter, ...filter } });
  };

  // console.log(transactionList);
  return (
    <TableList
      header={TRANSACTIONS_HEADER}
      filter={TRANSACTIONS_FILTER}
      action={TRANSACTIONS_ACTION}
      data={transactionList}
      isLoading={loading}
      onFilterData={(values: any) => handleFilterData(values)}
      filterData={filterPagination.filter}
      exportDataObject={exportToFileObject}
      isPagination={true}
      isAccess={true}
      pagination={pagination}
      handlePagination={(val) => {
        setFilterPagination({ ...filterPagination, ...val });
      }}
    />
  );
};

export default TransactionListing;
