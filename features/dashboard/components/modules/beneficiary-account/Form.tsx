import { FC, useEffect, useState } from "react";

import useFormContent from "@/features/dashboard/hooks/useFormContent";
import DetailContentForm from "../../DetailContentForm";
import { useRouter } from "next/router";
import useFormData from "@/hooks/useFormData";
import { object, string } from "zod";
import { getEntityId } from "@/services/common-services";
import { useMutation, useQuery } from "react-query";
import { putBeneficiaryAccount } from "@/services/common-services";

export const EditBeneficiaryFPSSchema = object({
  entity: string().min(1, { message: "Entity is required" }),
  gateway_name: string().min(1, { message: "Gateway Name is required" }),
  merchant_name: string().min(1, { message: "Merchant Name is required" }),
  fps_id: string().min(1, { message: "FPS ID is required" })
});

export const EditBeneficiaryBankSchema = object({
  entity: string().min(1, { message: "Entity is required" }),
  gateway_name: string().min(1, { message: "Gateway Name is required" }),
  bank_acc_number: string().min(1, { message: "Bank Account Number is required" }),
  bank_name: string().min(1, { message: "Bank Name is required" }),
  bank_account_holder: string().min(1, { message: "Bank Account Holder is required" }),
  bank_code: string().min(1, { message: "Bank Code is required" }),
  swift: string().min(1, { message: "SWIFT is required" }),
  bank_address: string().min(1, { message: "Bank Address is required" })
});

let EditBeneficiaryBankFields: any = [
  { id: "entityName", name: "entity", type: "text", disabled: true },
  { id: "paymentGatewayName", name: "gateway_name", type: "text", disabled: true },
  { id: "bankAccNo", name: "bank_acc_number", type: "text", disabled: false },
  { id: "bankName", name: "bank_name", type: "text", disabled: false },
  { id: "accHolderName", name: "bank_account_holder", type: "text", disabled: false },
  { id: "bankCode", name: "bank_code", type: "text", disabled: false },
  { id: "swift", name: "swift", type: "text", disabled: false },
  { id: "bankAddr", name: "bank_address", type: "text", disabled: false }
];

let EditBeneficiaryFPSFields: any = [
  { id: "entityName", name: "entity", type: "text", disabled: true },
  { id: "paymentGatewayName", name: "gateway_name", type: "text", disabled: true },
  { id: "accHolderName", name: "merchant_name", type: "text", disabled: false },
  { id: "bankAccNo", name: "fps_id", type: "text", disabled: false },
  { name: "fps_qr_code", type: "custom_fields", disabled: false }
];

type updateBeneficiaryVariables = {
  id: number;
  payload: any;
};

const BeneficiaryAccountForm: FC = () => {
  const {
    query: { feature, id },
    push
  } = useRouter();
  const { formFields, formValidations, setFormValue } = useFormContent();
  const { mutate: updateBeneficiaryAccount } = useMutation(({ payload, id }: updateBeneficiaryVariables) =>
    putBeneficiaryAccount(payload, id)
  );
  const { data: formData, setFormData } = useFormData();
  const [initialData, setInitialData] = useState(null);

  useEffect(() => {
    if (formData) {
      if (!formFields) {
        //@ts-ignore
        if (formData.paymentGatewayName.toLowerCase() == "ccba bank transfer") {
          setFormValue("fields", EditBeneficiaryBankFields);
          setFormValue("validations", EditBeneficiaryBankSchema);
        }
        //@ts-ignore
        if (formData.paymentGatewayName.toLowerCase() == "ccba fps") {
          setFormValue("fields", EditBeneficiaryFPSFields);
          setFormValue("validations", EditBeneficiaryFPSSchema);
        }
      }
      //@ts-ignore
      if (formData.paymentGatewayName.toLowerCase() == "ccba bank transfer") {
        let current: any = {};
        let value = null;

        EditBeneficiaryBankFields.map((val: any) => {
          value = formData[val.id];
          Object.assign(current, { [val.name]: value });
        });
        setInitialData(current);
      }

      //@ts-ignore
      if (formData.paymentGatewayName.toLowerCase() == "ccba fps") {
        let current: any = {};
        let value = null;

        EditBeneficiaryFPSFields.map((val: any) => {
          value = formData[val.id];
          Object.assign(current, { [val.name]: value });
        });
        setInitialData(current);
      }
    }
  }, []);

  const handleSubmitForm = (values: any) => {
    const data = {};
    if (formData.paymentGatewayName.toLowerCase() == "ccba bank transfer") {
      EditBeneficiaryBankFields.forEach(function (field: any) {
        let value = null;
        if (!values[field.name] && initialData) {
          value = initialData[field.name];
        } else {
          value = values[field.name];
        }
        if (value) {
          Object.assign(data, { [field.id]: value });
        }
      });
    } else {
      console.log(formData);
      EditBeneficiaryFPSFields.forEach(function (field: any) {
        let value = null;
        if (!values[field.name] && initialData) {
          value = initialData[field.name];
        } else {
          value = values[field.name];
        }
        if (value) {
          Object.assign(data, { [field.id]: value });
        }
      });
      Object.assign(data, {
        bankCode: formData.bankCode,
        bankName: formData.bankName
      });
    }

    updateBeneficiaryAccount(
      { id: formData.bankId, payload: data },
      {
        onSuccess: (data) => {
          push(`/${feature}/management`);
        }
      }
    );
  };

  return (
    <DetailContentForm
      formData={formData}
      initialData={initialData}
      fields={formFields}
      validations={formValidations}
      onSubmit={handleSubmitForm}
      onCancel={() => setFormData(null)}
    />
  );
};

export default BeneficiaryAccountForm;
