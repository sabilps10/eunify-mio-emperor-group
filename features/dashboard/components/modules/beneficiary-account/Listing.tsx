import { FC, useEffect } from "react";

import TableList from "@/components/TableList";
import useListingContent from "@/features/dashboard/hooks/useListingContent";
import { BENEFICIARY_ACCOUNT_HEADER } from "@/config/tables/beneficiary-channel";
import { useQuery } from "react-query";
import { getBeneficiaryAccountRecord } from "@/services/common-services";

const BeneficiaryAccountListing: FC = () => {
  const { data: beneficiaryAccountData, isLoading } = useQuery(
    ["get-beneficiary-account"],
    async () => {
      const { response } = await getBeneficiaryAccountRecord();
      return response;
    },
    { keepPreviousData: true }
  );

  return (
    <TableList
      header={BENEFICIARY_ACCOUNT_HEADER}
      data={beneficiaryAccountData}
      isLoading={isLoading}
    />
  );
};

export default BeneficiaryAccountListing;
