import { FC, useEffect, useState } from "react";

import useFormContent from "@/features/dashboard/hooks/useFormContent";
import { Router, useRouter } from "next/router";
import DetailContentForm from "../../DetailContentForm";
import useFormData from "@/hooks/useFormData";
import { useMutation } from "react-query";
import { postProvince, putProvince } from "@/services/common-services";
import useLocalStorage from "@/hooks/useLocalStorage";
import { LOCALSTORAGE_KEY } from "@/config/constants";
import { object, string, number } from "zod";

export const EditProvinceFields = [
  {
    id: "custom_value",
    name: "location_id",
    type: "text",
    disabled: false,
    hidden: true,
    payload_skipped: true
  },
  { id: "addressId", name: "address_id", type: "text", disabled: false, hidden: true, payload_skipped: true },
  { id: "nameEn", name: "en_display_name", type: "text", disabled: false },
  { id: "nameTc", name: "tc_display_name", type: "text", disabled: false },
  { id: "nameSc", name: "sc_display_name", type: "text", disabled: false }
];

export const AddProvinceFields = [
  { id: "nameEn", name: "en_display_name", type: "text", disabled: false },
  { id: "nameTc", name: "tc_display_name", type: "text", disabled: false },
  { id: "nameSc", name: "sc_display_name", type: "text", disabled: false }
];

export const EditProvinceSchema = object({
  location_id: number().min(1),
  address_id: number().min(1),
  en_display_name: string().min(1, { message: "EN Display Name is required" }),
  tc_display_name: string().min(1, { message: "TC Display Name is required" }),
  sc_display_name: string().min(1, { message: "SC Display Name is required" })
});

export const AddProvinceSchema = object({
  en_display_name: string().min(1, { message: "EN Display Name is required" }),
  tc_display_name: string().min(1, { message: "TC Display Name is required" }),
  sc_display_name: string().min(1, { message: "SC Display Name is required" })
});

type createProvinceVariables = {
  payload: any;
};

type updateProvinceVariables = {
  id: number;
  payload: any;
};

const ProvinceForm: FC = () => {
  const router = useRouter();
  const { getItem } = useLocalStorage();
  const account = getItem(LOCALSTORAGE_KEY.ACCOUNT);
  const [initialData, setInitialData] = useState(null);
  const { formFields, formValidations, setFormValue } = useFormContent();
  const { data: formData, setFormData } = useFormData();

  const { mutate: createProvince } = useMutation(({ payload }: createProvinceVariables) =>
    postProvince(payload)
  );

  const { mutate: updateProvince } = useMutation(({ id, payload }: updateProvinceVariables) =>
    putProvince(id, payload)
  );

  useEffect(() => {
    if (!formFields) {
      if (router.query.page == "add") {
        setFormValue("fields", AddProvinceFields);
        setFormValue("validations", AddProvinceSchema);
      }
      if (router.query.page == "detail") {
        setFormValue("fields", EditProvinceFields);
        setFormValue("validations", EditProvinceSchema);
      }
    }
    if (formData && router.query.page == "detail") {
      let current: any = {};
      EditProvinceFields.map((val: any) => {
        if (formData[val.id] || val.id == "custom_value") {
          let value = null;
          if (val.name == "location_id") {
            value = 1;
          } else {
            value = formData[val.id];
          }
          Object.assign(current, { [val.name]: value });
        }
      });
      setInitialData(current);
    }
  }, []);

  const handleSubmitForm = (values: any) => {
    const data = {};
    const Fields = router.query.page == "detail" ? AddProvinceFields : EditProvinceFields;
    Fields.forEach(function (field: any) {
      let value = null;
      if (!values[field.name] && initialData) {
        value = initialData[field.name];
      } else {
        value = values[field.name];
      }
      if (value && !field.payload_skipped) {
        const key = field.id;
        Object.assign(data, { [key]: value });
      }
    });
    Object.assign(data, { isActive: true, locationId: 1 });

    if (router.query.page == "add") {
      createProvince(
        { payload: { ...data, requestBy: account } },
        {
          onSuccess: (data) => {
            router.push(`/${router.query.feature}/management`);
          }
        }
      );
    }
    if (router.query.page == "detail") {
      updateProvince(
        { id: values.address_id, payload: { ...data, requestBy: account } },
        {
          onSuccess: (data) => {
            router.push(`/${router.query.feature}/management`);
          }
        }
      );
    }
  };

  return (
    <DetailContentForm
      formData={formData}
      initialData={initialData}
      fields={formFields}
      validations={formValidations}
      onSubmit={handleSubmitForm}
      onCancel={() => setFormData(null)}
    />
  );
};

export default ProvinceForm;
