import { FC, useState } from "react";

import TableList from "@/components/TableList";
import useListingContent from "@/features/dashboard/hooks/useListingContent";

import { KEY_PROVINCE_ACTION, KEY_PROVINCE_HEADER } from "@/config/tables/key-province";
import { use } from "i18next";
import { useQuery } from "react-query";
import { getProvinceList } from "@/services/common-services";

const ProvinceListing: FC = () => {
  const [filterData] = useState({
    isActive: true,
    locationId: "1"
  });
  const { data: provinceRecord, isFetching: fetchingRecord } = useQuery(
    ["get-province-list", filterData],
    async () => {
      const { response } = await getProvinceList(filterData);
      return response;
    }
  );

  return (
    <TableList
      header={KEY_PROVINCE_HEADER}
      action={KEY_PROVINCE_ACTION}
      data={provinceRecord}
      isLoading={fetchingRecord}
    />
  );
};

export default ProvinceListing;
