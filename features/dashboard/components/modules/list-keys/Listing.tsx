import { FC, useState, useEffect, useCallback } from "react";
import TableList from "@/components/TableList";
import useListingContent from "@/features/dashboard/hooks/useListingContent";
import { MULTIPLE_LIST_KEY_TABLE } from "@/config/tables/list-keys";
import { Stack, Typography } from "@mui/material";
import useTranslate from "@/hooks/useTranslate";
import { getCurrencies, getEntities, getNetworks, getEddaDisplayName } from "@/services/common-services";
import { useQuery } from "react-query";

const ListKeyListing: FC = () => {
  const [, updateState] = useState<any>();
  const forceUpdate = useCallback(() => updateState({}), []);
  const [entities, setEntities] = useState({ data: [], isLoading: false });
  const [eddaDisplayNames, setEddaDisplayNames] = useState({ data: [], isLoading: false });

  const { data: entitiesData, isLoading: isLoadingEntities } = useQuery(["get-entities"], getEntities);
  const { data: eddaDisplayNamesData, isLoading: isLoadingEddaDisplayNames } = useQuery(
    ["get-edda-name"],
    getEddaDisplayName
  );
  const { translate } = useTranslate();

  useEffect(() => {
    if (entitiesData || isLoadingEntities) {
      setEntities({ data: entitiesData?.response, isLoading: isLoadingEntities });
    }
  }, [entitiesData, isLoadingEntities]);

  useEffect(() => {
    if (eddaDisplayNamesData || isLoadingEddaDisplayNames) {
      setEddaDisplayNames({ data: eddaDisplayNamesData?.response, isLoading: isLoadingEddaDisplayNames });
    }
  }, [eddaDisplayNamesData, isLoadingEddaDisplayNames]);

  return (
    <Stack gap={3}>
      {MULTIPLE_LIST_KEY_TABLE.map((val: any, key: any) => {
        return (
          <Stack gap={2} key={key}>
            <Typography>{translate(`dashboard.labels.${val.name}`)}</Typography>
            <TableList
              action={val.tables.action}
              header={val.tables.header}
              data={eval(val.tables.tableData)}
              isLoading={eval(val.tables.isLoadingData)}
            />
          </Stack>
        );
      })}
    </Stack>
  );
};

export default ListKeyListing;
