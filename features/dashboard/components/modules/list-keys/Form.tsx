import { FC, useEffect, useState } from "react";

import useFormContent from "@/features/dashboard/hooks/useFormContent";
import { useRouter } from "next/router";
import DetailContentForm from "../../DetailContentForm";
import { EditTermNamingFields, EditTermNamingSchema } from "@/config/form-fields/term-naming";
import useFormData from "@/hooks/useFormData";
import { useMutation } from "react-query";
import { putCodeValue } from "@/services/common-services";
import useLocalStorage from "@/hooks/useLocalStorage";
import { LOCALSTORAGE_KEY } from "@/config/constants";

type updateCodeValueVariables = {
  id: number;
  payload: any;
};

const KeyTermForm: FC = () => {
  const {
    query: { page }
  } = useRouter();
  const { formFields, formValidations, setFormValue } = useFormContent();
  const { data: formData, setFormData } = useFormData();
  const [initialData, setInitialData] = useState(null);
  const {
    query: { feature, id },
    push
  } = useRouter();

  const { mutate: updateCodeValue } = useMutation(({ id, payload }: updateCodeValueVariables) =>
    putCodeValue(id, payload)
  );

  useEffect(() => {
    if (!formFields) {
      setFormValue("fields", EditTermNamingFields);
      setFormValue("validations", EditTermNamingSchema);
    }
    if (formData) {
      let current: any = {};
      EditTermNamingFields.map((val: any) => {
        if (formData[val.id] || typeof formData[val.id] === "boolean") {
          let value =
            typeof formData[val.id] === "number" || typeof formData[val.id] === "boolean"
              ? formData[val.id].toString()
              : formData[val.id];
          Object.assign(current, { [val.name]: value });
        }
      });
      setInitialData(current);
    }
  }, []);

  const handleSubmitForm = (values: any) => {
    const data = {};

    EditTermNamingFields.forEach(function (field: any) {
      let value = null;
      value = values[field.name];
      if (value) {
        Object.assign(data, { [field.id]: value });
      }
    });

    const { getItem } = useLocalStorage();
    const account = getItem(LOCALSTORAGE_KEY.ACCOUNT);
    Object.assign(data, { requestBy: account });
    updateCodeValue(
      { id: formData.cv_id, payload: data },
      {
        onSuccess: (data) => {
          push(`/${feature}/management`);
        }
      }
    );
  };

  return (
    <DetailContentForm
      initialData={initialData}
      fields={formFields}
      validations={formValidations}
      onSubmit={handleSubmitForm}
      onCancel={() => setFormData(null)}
    />
  );
};

export default KeyTermForm;
