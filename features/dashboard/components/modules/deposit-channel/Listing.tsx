import { FC, useCallback, useEffect, useState } from "react";
import TableList from "@/components/TableList";
import {
  DEPOSIT_CHANNEL_ACTION,
  DEPOSIT_CHANNEL_HEADER,
  DEPOSIT_CHANNEL_EXPORT_HEADING
} from "@/config/tables/deposit-channel";
import { getDepositChannelList } from "@/services/mio-services";
import { useQuery } from "react-query";
import { getEntityId, getPaymentGatewayName, getPaymentChannelStatus } from "@/services/common-services";

export const DEPOSIT_CHANNEL_FILTER: any = [
  { id: "entity", name: "entityId", type: "dropdown", options: [] },
  { id: "gateway", name: "gatewayId", type: "dropdown", options: [] },
  { id: "status", name: "gatewayStatus", type: "dropdown", options: [] }
];

const defaultFilterData = { paymentChannelId: null, entityId: null, gatewayId: null, gatewayStatus: null };

const DepositChannelListing: FC = () => {
  const [, updateState] = useState<any>();
  const forceUpdate = useCallback(() => updateState({}), []);
  const [filterData, setFilterData] = useState({
    paymentChannelId: null,
    entityId: null,
    gatewayId: null,
    gatewayStatus: null
  });

  const getDepositChannel = useQuery(
    ["get-deposit-channel", filterData],
    async () => {
      const { response } = await getDepositChannelList(filterData);
      return response;
    },
    { keepPreviousData: true }
  );
  const getDepositChannelData = useQuery(
    ["get-deposit-channel", defaultFilterData],
    async () => {
      const { response } = await getDepositChannelList(defaultFilterData);
      return response;
    },
    { keepPreviousData: true }
  );
  const { data: entityData } = useQuery(["get-entity"], getEntityId);
  const { data: gatewayData } = useQuery(["get-gateway-name"], getPaymentGatewayName);
  const { data: statusData } = useQuery(["get-status"], getPaymentChannelStatus);

  const { data: depositChannel, isLoading, isFetching } = getDepositChannel;
  const { data: depositChannelData } = getDepositChannelData;
  const exportToFileObject = { heading: DEPOSIT_CHANNEL_EXPORT_HEADING };

  useEffect(() => {
    if (entityData) {
      let items: { label: string; value: any }[] = [];
      entityData?.response[0].items.forEach(function (val: any) {
        let item = { label: "", value: "" };
        item.label = val.name;
        item.value = val.codeCode;
        items.push(item);
      });
      DEPOSIT_CHANNEL_FILTER[0].options = items;
      forceUpdate();
    }
  }, [entityData]);

  useEffect(() => {
    if (depositChannelData) {
      let items: { label: string; value: any }[] = [];
      depositChannelData.forEach(function (val: any) {
        let item = { label: "", value: "" };
        item.label = val.gatewayNameEn;
        item.value = val.gatewayId;
        items.push(item);
      });

      DEPOSIT_CHANNEL_FILTER[1].options = items;
      forceUpdate();
    }
  }, [depositChannelData]);

  useEffect(() => {
    if (statusData) {
      let items: { label: string; value: any }[] = [];
      statusData.response[0].items.forEach(function (val: any) {
        let item = { label: "", value: "" };
        item.label = val.name;
        item.value = val.codeInt ? "false" : "true";
        items.push(item);
      });
      DEPOSIT_CHANNEL_FILTER[2].options = items;
      forceUpdate();
    }
  }, [statusData]);

  const handleDoFilterData = (values: any) => {
    let filter: any = filterData;
    Object.keys(values).forEach(function (key) {
      if (key == "gatewayStatus") {
        filter[key] = values[key] === "true" ? true : values[key] === "false" ? false : null;
      } else {
        filter[key] = values[key] ? values[key] : null;
      }
    });
    setFilterData({ ...filterData, ...filter });
  };

  return (
    <TableList
      header={DEPOSIT_CHANNEL_HEADER}
      filter={DEPOSIT_CHANNEL_FILTER}
      action={DEPOSIT_CHANNEL_ACTION}
      data={depositChannel}
      isLoading={isFetching}
      onFilterData={(values: any) => handleDoFilterData(values)}
      exportDataObject={exportToFileObject}
    />
  );
};

export default DepositChannelListing;
