import useFormContent from "@/features/dashboard/hooks/useFormContent";
import { FC, useCallback, useEffect, useState } from "react";
import DetailContentForm from "../../DetailContentForm";

import useFormData from "@/hooks/useFormData";
import {
  getCurrency,
  getEntityId,
  getPaymentGatewayName,
  getPaymentNetwork
} from "@/services/common-services";
import { putDepositChannel } from "@/services/mio-services";
import { useRouter } from "next/router";
import { useMutation, useQuery } from "react-query";
import { array, boolean, number, object, string } from "zod";
import useLocalStorage from "@/hooks/useLocalStorage";
import { LOCALSTORAGE_KEY } from "@/config/constants";

export const EditDepositChannelFields: any = [
  { id: "entityId", name: "entity", options: [], type: "select", disabled: true },
  { id: "gatewayNameEn", name: "gateway_name", type: "text", disabled: true },
  { id: "gatewayId", name: "gateway_id", type: "text", disabled: true, hidden: true },
  {
    id: "isOnlineGateway",
    name: "isOnlineGateway",
    options: [
      { label: "Online", value: "true" },
      { label: "Offline", value: "false" }
    ],
    type: "select",
    disabled: true
  },
  { id: "displayNameEn", name: "english_display_name", type: "text", disabled: false },
  { id: "displayNameSc", name: "simp_chi_display_name", type: "text", disabled: false },
  { id: "displayNameTc", name: "trad_chi_display_name", type: "text", disabled: false },
  {
    id: "currencies",
    custom_prefill: {
      value_type: "index_keys_array",
      source: "form_data",
      source_key: "currencies",
      source_index: 0,
      value_keys: [
        { value: "other", key: "currency" },
        { value: "USDT", key: "cryptoCurrency" }
      ]
    },
    name: "gateway_currency",
    type: "text",
    disabled: true
  },
  {
    id: "network",
    name: "currencies",
    condition: {
      source: "form_data",
      name: "paymentMethod",
      value: "crypto"
    },
    custom_prefill: {
      value_type: "array",
      source: "form_data",
      source_key: "currencies"
    },
    type: "checkbox",
    options: [],
    disabled: false
  },
  {
    id: "maxAmount",
    name: "max_deposit_amount",
    additional_value: "gateway_currency",
    type: "text",
    value_type: "number-currency",
    disabled: false,
    callback: {
      enabled: ["max_deposit_amount"],
      action: "parentCallback",
      function: "checkMaxDepositAmount(values, methods)"
    }
  },
  {
    id: "minAmount",
    name: "min_deposit_amount",
    additional_value: "gateway_currency",
    type: "text",
    value_type: "number-currency",
    disabled: false,
    callback: {
      enabled: ["min_deposit_amount"],
      action: "parentCallback",
      function: "checkMaxDepositAmount(values, methods)"
    }
  },
  {
    id: "gatewayCharge",
    name: "gateway_charge",
    type: "text",
    value_type: "number-percentage",
    additional_value: "gateway_charge_unit",
    disabled: false
  },
  {
    id: "gatewayChargeUnit",
    name: "gateway_charge_unit",
    type: "text",
    hidden: true
  },
  {
    id: "serviceFee",
    name: "service_fee_charge",
    type: "text",
    additional_value: "service_fee_unit",
    value_type: "number-percentage",
    disabled: false
  },
  {
    id: "serviceFeeUnit",
    name: "service_fee_unit",
    type: "text",
    hidden: true
  },
  {
    id: "estSla",
    name: "est_sla",
    type: "text",
    value_type: "number",
    disabled: false
  },
  {
    id: "frozenPeriod",
    name: "frozen_period",
    type: "text",
    value_type: "number",
    disabled: false
  },
  {
    id: "enquiryInterval",
    name: "enquiry_interval",
    type: "text",
    value_type: "number",
    disabled: false
  },
  {
    id: "stopEnquiryInterval",
    name: "stop_enquiry_interval",
    type: "text",
    value_type: "number",
    disabled: false
  },
  { id: "timeslots", name: "timeslots", type: "custom_fields", disabled: false },
  { id: "charges", name: "charges", type: "custom_fields", disabled: false },
  {
    id: "status",
    name: "status",
    type: "radio",
    options: [
      { value: "true", label: "Active" },
      { value: "false", label: "Inactive" }
    ],
    disabled: false
  }
];

export const EditDepositChannelSchema = object({
  entity: string().min(1, { message: "Entity is required" }),
  gateway_name: string().min(1, { message: "Gateway Name is required" }),
  currencies: array(string()),
  english_display_name: string().min(1, { message: "English Display Name is required" }),
  trad_chi_display_name: string().min(1, { message: "Trad. Chi Display Name is required" }),
  simp_chi_display_name: string().min(1, { message: "Simp. Chi Display Name is required" }),
  frozen_period: string().min(1, { message: "Frozed Period is required" }),
  enquiry_interval: string().min(1, { message: "Enquiry Interval is required" }),
  stop_enquiry_interval: string().min(1, { message: "Stop Enquiry Interval is required" }),
  max_deposit_amount: string().min(1, { message: "Max Deposit Amount is required" }),
  min_deposit_amount: string().min(1, { message: "Min Deposit Amount is required" }),
  gateway_charge: string().min(1, { message: "Gateway Charges is required" }),
  gateway_charge_unit: string().min(1, { message: "Gateway Charge Unit is required" }),
  est_sla: string().min(1, { message: "Est. SLA (min) Charge is required" }),
  service_fee_charge: string().min(1, { message: "Service Fee Charge is required" }),
  service_fee_unit: string().min(1, { message: "Service Fee Charge Unit is required" }),
  status: string().min(1, { message: "Status is required" }),
  timeslots: array(
    object({
      dayCode: number(),
      gatewayTimeStart: string(),
      gatewayTimeEnd: string(),
      breakTimeStart: string(),
      breakTimeEnd: string()
    })
  ).nullable(),
  charges: array(
    object({
      chargeType: string(),
      effectiveDateFrom: string(),
      effectiveDateTo: string(),
      isActive: boolean(),
      isExcluded: boolean(),
      ruleKey: string(),
      ruleType: string(),
      unit: string(),
      value: string()
    })
  ).nullable()
});

type updateDepositChannelVariables = {
  id: number;
  payload: any;
};

const DepositChannelForm: FC = () => {
  const { formFields, formValidations, setFormValue } = useFormContent();
  const [, updateState] = useState<any>();
  const { getItem } = useLocalStorage();
  const account = getItem(LOCALSTORAGE_KEY.ACCOUNT);
  const forceUpdate = useCallback(() => updateState({}), []);
  const { data: entityData } = useQuery(["get-entity"], getEntityId);
  const { data: gatewayData } = useQuery(["get-gateway-name"], getPaymentGatewayName);
  const { data: paymentNetworkData } = useQuery(["get-payment-network"], getPaymentNetwork);
  const { data: currencyData } = useQuery(["get-currency"], getCurrency);

  const { mutate: updateDepositChannel } = useMutation(({ id, payload }: updateDepositChannelVariables) =>
    putDepositChannel(id, payload)
  );
  const { data: formData, setFormData } = useFormData();
  const [initialData, setInitialData] = useState(null);
  const {
    query: { feature, id },
    push
  } = useRouter();

  useEffect(() => {
    if (entityData) {
      let items: { label: string; value: any }[] = [];
      entityData?.response[0].items.forEach(function (val: any) {
        let item = { label: "", value: "" };
        item.label = val.name;
        item.value = val.codeCode;
        items.push(item);
      });
      EditDepositChannelFields[0].options = items;
      forceUpdate();
    }
  }, [entityData]);

  useEffect(() => {
    if (paymentNetworkData) {
      let items: { label: string; value: any }[] = [];
      paymentNetworkData.response[0].items.forEach(function (val: any) {
        let item = { label: "", value: "" };
        item.label = val.name;
        item.value = val.codeCode;
        items.push(item);
      });

      EditDepositChannelFields[8].options = items;
      forceUpdate();
    }
  }, [paymentNetworkData]);

  useEffect(() => {
    if (currencyData) {
      let items: { label: string; value: any }[] = [];
      currencyData.response[0].items.forEach(function (val: any) {
        let item = { label: "", value: "" };
        item.label = val.name;
        item.value = val.codeCode;
        items.push(item);
      });

      EditDepositChannelFields[6].options = items;
      forceUpdate();
    }
  }, [currencyData]);

  useEffect(() => {
    if (!formFields) {
      setFormValue("fields", EditDepositChannelFields);
      setFormValue("validations", EditDepositChannelSchema);
    }
    if (formData) {
      let current: any = {};
      EditDepositChannelFields.map((field: any) => {
        if (
          formData[field.id] ||
          typeof formData[field.id] === "boolean" ||
          field.id == "network" ||
          typeof formData[field.id] === "number"
        ) {
          let value = null;
          if (field.name == "charges") {
            const items: any[] = [];
            formData[field.id].forEach(function (objs: any) {
              let item: any = {};
              Object.keys(objs).forEach((obj) => {
                item[obj] = typeof objs[obj] === "number" ? objs[obj].toString() : objs[obj];
              });

              items.push(item);
            });
            value = items;
          } else if (field.id == "network") {
            let networks: any[] = [];
            formData["currencies"].forEach(function (curr: any) {
              networks.push(curr.currency);
            });
            value = networks;
          } else {
            value =
              typeof formData[field.id] === "number" || typeof formData[field.id] === "boolean"
                ? formData[field.id].toString()
                : formData[field.id];
          }
          Object.assign(current, { [field.name]: value });
        }
      });
      setInitialData(current);
    }
  }, []);

  const checkMaxDepositAmount = (formValues: any, formMethods: any) => {
    if (+formValues.max_deposit_amount < +formValues.min_deposit_amount) {
      setTimeout(() => {
        formMethods.setError(
          "max_deposit_amount",
          {
            type: "custom",
            message: "The max deposit amount should be greater than the min deposit amount"
          },
          100
        );
      });
    } else {
      setTimeout(() => {
        formMethods.clearErrors("max_deposit_amount");
      }, 100);
    }
  };

  const handleSubmitForm = (values: any, methods: any) => {
    console.log(values);
    const data: any = {};
    EditDepositChannelFields.forEach(function (field: any) {
      let value = null;
      if (!values[field.name] && initialData) {
        value = initialData[field.name];
      } else {
        if (field.name == "status") {
          value = values[field.name] === "true";
        } else {
          value = values[field.name];
        }
      }
      if (value || typeof value === "boolean") {
        const key = field.id == "network" ? field.name : field.id;
        Object.assign(data, { [key]: value });
      }
    });

    var chargesValue = data.charges.map(function (item: any) {
      return item.ruleKey;
    });
    var isDuplicateCharges = chargesValue.some(function (item: any, idx: any) {
      return chargesValue.indexOf(item) != idx;
    });
    if (isDuplicateCharges) {
      methods.setError("charges", {
        type: "custom",
        message: "Each group can only be added once"
      });
      return;
    }

    const newData = {
      ...data,
      charges: data.charges.map((el: any) => ({
        ...el,
        unit: data.serviceFeeUnit
      }))
    };

    values.charges.forEach(({ value }: any) => {
      if (value === null || value === "") {
        methods.setError("charges", {
          type: "custom",
          message: "At least input a number in order to proceed"
        });
        return;
      }
    });
    if (values?.charges?.some(({ value }: any) => value === null || value === "")) return;

    updateDepositChannel(
      { id: formData.id, payload: { ...newData, requestBy: account } },
      {
        onSuccess: (data) => {
          push(`/${feature}/management`);
        }
      }
    );
  };

  return (
    <DetailContentForm
      formData={formData}
      initialData={initialData}
      fields={formFields}
      validations={formValidations}
      onSubmit={handleSubmitForm}
      onCancel={() => setFormData(null)}
      parentCallback={(func: any, values: any, methods: any) => {
        eval(func);
      }}
    />
  );
};

export default DepositChannelForm;
