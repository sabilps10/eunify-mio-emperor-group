import { FC, useCallback, useEffect, useState } from "react";
import { useQuery } from "react-query";
import TableList from "@/components/TableList";
import { BLACKLIST_ACTION } from "@/config/tables/blacklist";
import { getEntityId } from "@/services/common-services";
import { getBlacklistChannelList, getDepositChannelList } from "@/services/mio-services";

export const BLACKLIST_FILTER: any = [
  {
    id: "entity",
    name: "entity",
    options: [],
    type: "dropdown"
  },
  {
    id: "trading_acc_no",
    name: "trading_acc_no",
    type: "text"
  },
  {
    name: "dummy_account",
    id: "dummy_account",
    type: "dropdown",
    options: [
      { value: true, label: "Include" },
      { value: false, label: "Exclude" }
    ]
  }
];

const BlacklistListing: FC = () => {
  const [, updateState] = useState<any>();
  const forceUpdate = useCallback(() => updateState({}), []);
  const [exportHeader, setExportheader] = useState<any>([]);
  const [header, setHeader] = useState<any>([]);
  const [loadingHeader, setLoadingHeader] = useState<boolean>(false);
  const [depositList, setDepositList] = useState<any>([]);
  const [blacklistData, setBlacklistData] = useState<any>([]);
  const [filterData, setFilterData] = useState({
    entityId: null,
    tradingAccountNo: null,
    includeDummyAccount: false
  });
  const { data: entityData } = useQuery(["get-entity"], getEntityId);

  const { data: depositChannel } = useQuery(["get-deposit-channel"], getDepositChannelList);

  const { data: blacklist, isFetching: fetchingRecord } = useQuery(
    ["get-blacklist-channel", filterData],
    () => getBlacklistChannelList(filterData),
    {
      enabled: !!filterData
    }
  );

  useEffect(() => {
    if (entityData) {
      let items: { label: string; value: any }[] = [];
      entityData?.response[0].items.forEach(function (val: any) {
        let item = { label: "", value: "" };
        item.label = val.codeCode;
        item.value = val.codeCode;
        items.push(item);
      });
      BLACKLIST_FILTER[0].options = items;
      forceUpdate();
    }
  }, [entityData]);

  useEffect(() => {
    if (depositChannel) {
      setDepositList(depositChannel.response);
    }
  }, [depositChannel]);

  useEffect(() => {
    setLoadingHeader(true);
    const temp: any = [
      {
        id: "no",
        numeric: false,
        disablePadding: false,
        label: "No.",
        isSort: true
      },
      {
        id: "entityId",
        numeric: false,
        disablePadding: false,
        label: "Entity",
        isSort: true
      },
      {
        id: "tradingAccountName",
        numeric: false,
        disablePadding: false,
        label: "Trading Acc Name",
        isSort: true
      },
      {
        id: "tradingAccountNo",
        numeric: false,
        disablePadding: false,
        label: "Trading Acc Number",
        isSort: true
      }
    ];
    depositList?.map((el: any) => {
      temp.push({
        id: el.id,
        numeric: false,
        disablePadding: false,
        label: el.gatewayNameEn,
        isSort: true,
        type: "checkbox"
      });
    });
    temp.push({
      id: "action",
      numeric: false,
      disablePadding: false,
      label: "Action",
      key_value: "tradingAccountNo",
      isSort: true
    });
    setHeader(temp);
    setLoadingHeader(false);
    forceUpdate();
  }, [depositList]);

  useEffect(() => {
    if (header.length > 0) {
      setLoadingHeader(true);
      const heading = header.map((el: any, idx: number) => ({
        name: el.id,
        label: el.label,
        width: 20,
        position: 1 + idx,
        isSort: true
      }));
      heading.pop();
      heading.shift();
      setExportheader(heading);
      setLoadingHeader(false);
      forceUpdate();
    }
  }, [header]);

  useEffect(() => {
    if (blacklist) {
      setLoadingHeader(true);
      const row: any = [];
      let rowData: any = {};

      blacklist.response.map((el: any) => {
        rowData = {
          entityId: el.entityId,
          tradingAccountNo: el.tradingAccountNo,
          tradingAccountName: el.list[0].clientName
        };
        depositList?.forEach((e: any) => {
          rowData[e.gatewayNameEn.replace(/\s/g, "")] = false;
        });
        el?.list?.map((list: any) => {
          const findChannel: any = depositList?.find((el: any) => el.id === list.paymentChannelId);
          rowData[findChannel?.id] = list.blackList;
        });

        row.push(rowData);
      });

      setBlacklistData(row);
      setLoadingHeader(false);
      forceUpdate();
    }
  }, [blacklist, header]);

  const handleFilterData = (values: any) => {
    setFilterData({
      entityId: values.entity ? values.entity : null,
      tradingAccountNo: values.trading_acc_no ? values.trading_acc_no : null,
      includeDummyAccount: values.dummy_account === true ? true : false
    });
  };

  if (!loadingHeader) {
    return (
      <TableList
        header={header}
        filter={BLACKLIST_FILTER}
        action={BLACKLIST_ACTION}
        data={blacklistData}
        isLoading={fetchingRecord}
        onFilterData={handleFilterData}
        exportDataObject={{ heading: exportHeader }}
      />
    );
  }
  return <></>;
};

export default BlacklistListing;
