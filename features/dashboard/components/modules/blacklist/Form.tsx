import { FC, useEffect, useState, useCallback } from "react";
import { object, string, any, array } from "zod";
import { useRouter } from "next/router";
import { useQuery, useMutation } from "react-query";
import debounce from "lodash.debounce";

import { getEntityId } from "@/services/common-services";
import { getDepositChannelList, updateBlacklistChannel } from "@/services/mio-services";
import { getAccounts } from "@/services/common-services";

import useFormContent from "@/features/dashboard/hooks/useFormContent";
import BlacklistDetail from "../../BlacklistDetails";
import useFormData from "@/hooks/useFormData";
import useLocalStorage from "@/hooks/useLocalStorage";
import { LOCALSTORAGE_KEY } from "@/config/constants";

const BlacklistFields: any = [
  {
    id: "entity",
    name: "entity",
    type: "select",
    options: [],
    disabled: false
  },
  {
    id: "trading_acc_no",
    name: "trading_acc_no",
    type: "text",
    disabled: false
  },
  { id: "trading_acc_name", name: "trading_acc_name", type: "text", disabled: false },
  {
    id: "blacklist_channel",
    name: "blacklist_channel",
    type: "checkbox",
    multiple_select: true,
    options: [
      { name: "edda", label: "eDDA", value: 42 },
      { name: "bank_transfer", label: "Bank Transfer", value: 1 },
      { name: "fps", label: "FPS", value: 2 },
      { name: "ccba_alipay", label: "CCBA Alipay", value: 3 },
      { name: "wechatpay", label: "WeChat Pay", value: 4 }
    ],
    disabled: false
  }
];

const BlacklistSchema = object({
  entity: string().min(1, { message: "Entity is required" }),
  trading_acc_no: string().min(1, { message: "Trading Account No. is required" })
  // trading_acc_name: string().min(1, { message: "Trading Account Name is required" }),
  // blacklist_channel: array(object({}))
});

const BlacklistForm: FC = () => {
  const {
    push,
    query: { page, feature, trading_acc_no, entity }
  } = useRouter();
  const { getItem } = useLocalStorage();

  const [, updateState] = useState<any>();
  const forceUpdate = useCallback(() => updateState({}), []);

  const { data: entityId } = useQuery(["get-entity"], getEntityId);
  const { data: depositChannel } = useQuery(["get-deposit-channel"], getDepositChannelList);

  const { mutate: fetchAccountDetail, isLoading: fetchingAccount } = useMutation((payload: any) =>
    getAccounts(payload)
  );

  const { mutate } = useMutation((payload: any) => updateBlacklistChannel(payload));

  const { formFields, formValidations, setFormValue } = useFormContent();
  const { data: formData, setFormData } = useFormData();
  const [entityList, setEntityList] = useState<any>([]);
  const [depositList, setDepositList] = useState<any>([]);
  const [initialData, setInitialData] = useState<any>(null);

  useEffect(() => {
    if (entityId) {
      let items: { label: string; value: any }[] = [];
      entityId.response[0].items.forEach(function (val: any) {
        let item = { label: "", value: "" };
        item.label = val.codeCode;
        item.value = val.codeCode;
        items.push(item);
      });
      setEntityList(items);
      forceUpdate();
    }
  }, [entityId]);

  useEffect(() => {
    if (depositChannel) {
      const options = depositChannel.response.map((el: any) => ({
        label: el.gatewayNameEn,
        value: el.id,
        entity: el.entityId
      }));
      setDepositList(options);
      forceUpdate();
    }
  }, [depositChannel]);

  const getAccountDetails = debounce((formValues: any, formMethods: any) => {
    fetchAccountDetail(
      {
        entityId: formValues.entity,
        tradingAccountNo: formValues.trading_acc_no
      },
      {
        onSuccess: (res) => {
          if (!res.result) {
            formMethods.setError("trading_acc_no", {
              type: "custom",
              message: res.errorDescription
            });
          } else {
            setFormData({
              ...formData,
              trading_acc_name: res?.response?.realName
            });
            setInitialData({
              ...initialData,
              trading_acc_name: res?.response?.realName
            });
          }
        }
      }
    );
  }, 1000);

  useEffect(() => {
    if (formData) {
      const isNumeric = (value: any) => {
        return /^-?\d+$/.test(value);
      };

      if (page === "detail") {
        const channel: any = [];
        const newList: any = depositList;
        for (let key in formData) {
          if (isNumeric(key)) {
            const find = depositList?.find((el: any) => el.value == key);
            const index = depositList.findIndex((el: any) => el.value == key);
            channel.push({ ...find, checked: formData[key] });
            newList[index] = { ...find, checked: formData[key] };
          }
        }

        setInitialData({
          ...initialData,
          trading_acc_name: formData?.trading_acc_name,
          trading_acc_no: formData.tradingAccountNo,
          entity: formData.entityId,
          blacklist_channel: channel
        });
        setDepositList(newList);
        forceUpdate();
      } else {
        setInitialData({
          ...initialData,
          ...formData
        });
      }
    }
  }, [formData, depositList]);

  useEffect(() => {
    forceUpdate();
  }, [initialData]);

  useEffect(() => {
    if (!formFields) {
      if (page == "detail") {
        BlacklistFields[0].disabled = true;
        BlacklistFields[1].disabled = true;
        BlacklistFields[2].disabled = true;
      }
      setFormValue("fields", BlacklistFields);
      setFormValue("validations", BlacklistSchema);
    }
  }, []);

  const handleSubmit = (data: any) => {
    const account = getItem(LOCALSTORAGE_KEY.ACCOUNT);
    const filterEntity =
      data.blacklist_channel.length > 0
        ? data.blacklist_channel?.filter((el: any) => el.entity === data.entity)
        : [];
    const payload = {
      entityId: data.entity,
      tradingAccountNo: +data.trading_acc_no,
      clientName: initialData?.trading_acc_name,
      paymentChannelIdList: filterEntity.filter((el: any) => el.checked).map((el: any) => el.value),
      requestBy: account
    };
    mutate(payload, {
      onSuccess: () => {
        push(`/${feature}/management`);
      }
    });
  };

  const handleCancel = () => {
    setFormData(null);
    setInitialData(null);
  };

  return (
    <BlacklistDetail
      initialData={initialData}
      validations={formValidations}
      entity={entityList}
      options={depositList}
      onCancel={handleCancel}
      onSubmit={handleSubmit}
      loadingButton={fetchingAccount}
      parentCallback={getAccountDetails}
    />
  );
};

export default BlacklistForm;
