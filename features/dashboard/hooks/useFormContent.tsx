import { useState } from "react";
import { object } from "zod";

const useFormContent = () => {
  const [formFields, setFormFields] = useState<any>(null);
  const [formValidations, setFormValidations] = useState(object({}));

  const [formDependencies, setFormDependencies] = useState<any>(null);

  const setFormValue = (name: string, value: any) => {
    if (name == "fields") {
      setFormFields(value);
    }
    if (name == "validations") {
      setFormValidations(value);
    }
    if (name === "dependencies") {
      setFormDependencies(value);
    }
  };
  return {
    formFields,
    formValidations,
    setFormValue,
    formDependencies,
    setFormDependencies
  };
};

export default useFormContent;
