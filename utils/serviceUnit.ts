export const ServiceUnit = (type: string, currency: any, value: any) => {
  switch (type) {
    case "P":
      return value + " %";
      break;
    case "D":
      return currency + " " + value;
    default:
      return "";
      break;
  }
};
