import Cookies from "universal-cookie";

const TOKEN_KEY = "TOKEN_KEY";
const cookies = new Cookies();

export const getToken = () => {
  return cookies.get(TOKEN_KEY);
};
