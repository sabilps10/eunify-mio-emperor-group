import type { FC, PropsWithChildren, ReactNode } from "react";
import Box from "@mui/material/Box";
import Container from "@mui/material/Container";
import Stack from "@mui/material/Stack";
import IconButton from "@mui/material/IconButton";
import Typography from "@mui/material/Typography";
import { IoChevronBack as BackIcon } from "react-icons/io5";

import AppBar from "@/components/AppBar";

export type PageLayoutProps = PropsWithChildren<{
  withMenus?: boolean;
  toolbar?: ReactNode;
  title?: string;
  titlePanels?: ReactNode;
  onPressBack?: () => void;
  color?: "yellow" | "blue";
}>;

const PageLayout: FC<PageLayoutProps> = ({
  children,
  toolbar,
  title = "Untitled Page",
  titlePanels,
  withMenus = true,
  onPressBack = () => null,
  color = "blue"
}) => {
  const backgroundColor = color === "blue" ? "brandBlue.500" : "brandYellow.500";
  const textColor = color === "blue" ? "neutrals.white" : "neutrals.text";

  return (
    <Box className="layout">
      <AppBar
        elevation={0}
        sx={{
          backgroundColor
        }}
      >
        {onPressBack && (
          <IconButton edge="start" sx={{ color: textColor }} onClick={onPressBack}>
            <BackIcon />
          </IconButton>
        )}
        {toolbar && (
          <Box width="100%" display={{ xs: "block", sm: "none" }}>
            {toolbar}
          </Box>
        )}
      </AppBar>

      <Container
        maxWidth={false}
        component="section"
        className="title"
        sx={{
          paddingY: {
            xs: 1.25,
            md: 3
          },
          paddingX: 2,
          paddingBottom: {
            xs: 4,
            md: 5
          },
          backgroundColor
        }}
      >
        <Stack
          direction="row"
          alignItems={{
            xs: "flex-start",
            sm: "center"
          }}
          justifyContent="space-between"
          sx={{
            color: textColor
          }}
        >
          <Stack spacing={1}>
            <Typography
              variant="h2"
              className="title-text"
              sx={{
                color: textColor
              }}
            >
              {title}
            </Typography>
            {titlePanels && <Box className="title-panels">{titlePanels}</Box>}
          </Stack>

          <Box
            display={{
              xs: "none",
              sm: "block"
            }}
          >
            {toolbar}
          </Box>
        </Stack>
      </Container>

      <Container
        maxWidth={false}
        component="main"
        className="main"
        sx={(theme) => ({
          paddingY: 4,
          paddingX: 2,
          borderTopLeftRadius: theme.spacing(1),
          borderTopRightRadius: theme.spacing(1),
          marginTop: -2,
          backgroundColor: "neutrals.white"
        })}
      >
        {children}
      </Container>
    </Box>
  );
};

export default PageLayout;
