import type { FC, PropsWithChildren } from "react";
import Box from "@mui/material/Box";
import IconButton from "@mui/material/IconButton";
import Container, { ContainerProps } from "@mui/material/Container";
import Typography from "@mui/material/Typography";
import Stack from "@mui/material/Stack";
import { IoChevronBack as BackIcon } from "react-icons/io5";

import AppBar from "@/components/AppBar";
import { spacing } from "@themes/default/spacing";

export type SubPageLayoutProps = PropsWithChildren<{
  title?: string;
  onPressBack?: () => void;
  backgroundColor?: string;
  containerProps?: ContainerProps;
}>;

const SubPageLayout: FC<SubPageLayoutProps> = ({
  children,
  title,
  onPressBack,
  backgroundColor,
  containerProps = {},
}) => {
  return (
    <Box className="layout">
      <AppBar
        elevation={0}
        position="fixed"
        sx={{ backgroundColor: "neutrals.white" }}
      >
        {onPressBack && (
          <IconButton
            edge="start"
            sx={{ color: "neutrals.text" }}
            onClick={onPressBack}
          >
            <BackIcon />
          </IconButton>
        )}

        <Box>
          <Typography variant="h4" sx={{ color: "brandYellow.800" }}>
            {title}
          </Typography>
        </Box>
      </AppBar>

      <Container
        maxWidth={false}
        component="main"
        className="main"
        {...containerProps}
        sx={{
          paddingLeft: 0,
          paddingRight: 0,
          paddingTop: {
            xs: spacing(6),
            sm: spacing(8),
            md: 0,
          },
          paddingX: {
            sm: 0,
            md: 0,
            lg: 0,
            xl: 0,
          },
          backgroundColor: backgroundColor ? backgroundColor : "brandGrey.500",
          ...containerProps?.sx,
        }}
      >
        <Stack gap={1.5}>{children}</Stack>
      </Container>
    </Box>
  );
};

export default SubPageLayout;
