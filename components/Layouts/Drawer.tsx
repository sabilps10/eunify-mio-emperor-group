import { FC, useState, useEffect, useCallback } from "react";
import { styled } from "@mui/material/styles";

import MuiDrawer from "@mui/material/Drawer";
import List from "@mui/material/List";
import Divider from "@mui/material/Divider";
import Toolbar from "@mui/material/Toolbar";
import { DRAWER_WIDTH, LOCALSTORAGE_KEY } from "@/config/constants";
import { ListMenu } from "@/config/dashboard-menu";
import {
  ListItemButton,
  Typography,
  ListItemIcon,
  ListItemText,
  Stack,
  Box,
  Skeleton,
  Button
} from "@mui/material";
import Collapse from "@mui/material/Collapse";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import ExpandLessIcon from "@mui/icons-material/ExpandLess";
import DashboardIcon from "@mui/icons-material/Dashboard";
import AccountCircleIcon from "@mui/icons-material/AccountCircle";
import useDrawerStore from "@/store/drawerStore";
import { useRouter } from "next/router";
import useMounted from "@/hooks/useMounted";
import useLocalStorage from "@/hooks/useLocalStorage";
import useAmazonCognito from "@/hooks/useAmazonCognito";
import securityStore, { SecurityStoreTypes } from "@/store/securityStore";
import convertNameToUrl from "@/utils/convertToPathUrl";
import shallow from "zustand/shallow";
import useAuditLog from "@/hooks/useAuditLog";

type DrawerComponentProps = {
  isLoading: boolean;
  menus: any[];
};

const DrawerComponent: FC<DrawerComponentProps> = ({ isLoading = false, menus }) => {
  const isDrawerOpen = useDrawerStore((state) => state.isDrawerOpen);
  const { isMounted } = useMounted();
  const { logout } = useAmazonCognito();
  const activeMenu = useDrawerStore((state) => state.activeMenu);
  const setActiveMenu = useDrawerStore((state) => state.setActiveMenu);
  const { client_functions, role_code } = securityStore((state) => state, shallow) as SecurityStoreTypes;
  const { handleLogFunction } = useAuditLog();
  let entities = Object.keys(client_functions);

  const [_, updateState] = useState<any>();
  const forceUpdate = useCallback(() => updateState({}), []);

  const { getItem } = useLocalStorage();
  const account = getItem(LOCALSTORAGE_KEY.ACCOUNT);

  useEffect(() => {
    forceUpdate();
  }, [client_functions]);

  const { push, query } = useRouter();

  const toggleMenu = async (item: any, link: any, code: string) => {
    handleLogFunction(code, link);
    let readAccess = 0;

    entities.map((el: any) => {
      const access = client_functions[el].find(
        (x: any) => convertNameToUrl(x.funcNameEn) == link.split("/")[1]
      )?.readAccess;
      if (access) {
        readAccess++;
      }
    });

    if (readAccess > 0) {
      if (link) push(link);
    }
    setActiveMenu(item, code);
  };

  const Drawer = styled(MuiDrawer, { shouldForwardProp: (prop) => prop !== "open" })(({ theme }) => {
    return {
      "& .MuiDrawer-paper": {
        position: "relative",
        whiteSpace: "nowrap",
        width: DRAWER_WIDTH,
        maxHeight: "100vh",
        overflow: "scroll",
        boxSizing: "border-box",
        [theme.breakpoints.down("md")]: {
          width: theme.spacing(9)
        }
      }
    };
  });

  const RenderListMenu: FC<{ items: any }> = ({ items = [] }) => {
    if (!isMounted) return <></>;
    if (isLoading) return <RenderSkeletonLoading />;

    const convertNameToPath = (value: string) => {
      value = value.split("/").join("-");
      return value.toLocaleLowerCase().split(" ").join("-");
    };

    const convertNameToUrl = (value: string) => {
      value = value.split("/").join("-");
      return "/" + value.toLocaleLowerCase().split(" ").join("-") + "/management";
    };

    items = items?.sort((a: any, b: any) => (a.displayOrder > b.displayOrder ? 1 : -1));

    return (
      <List component="nav">
        {items?.map((value: any, key: number) => {
  
          let pl = 3;
          if (value.parentFuncId === 0) pl = 5;
          if (value.parentFuncId > 0) pl = 7;

          let readAccess = 0;

          entities.map((el: any) => {
            const access = client_functions[el].find(
              (x: any) => x.funcNameEn == value.funcNameEn
            )?.readAccess;
            if (access) {
              readAccess++;
            }
          });

          if (!value?.child || value?.child?.length === 0) {
            if (readAccess == 0 || value.isLeftMenu == false) {
              return null;
            } else {
              return (
                <ListItemButton
                  key={key}
                  sx={(theme) => ({
                    pl,
                    whiteSpace: "normal",
                    backgroundColor:
                      convertNameToPath(value.funcNameEn) == query.feature
                        ? theme.palette.brandYellow[300]
                        : ""
                  })}
                  onClick={() =>
                    toggleMenu(
                      convertNameToPath(value.funcNameEn),
                      convertNameToUrl(value.funcNameEn),
                      value.funcCode
                    )
                  }
                >
                  <ListItemText primary={<Typography variant="h4">{value.funcNameEn}</Typography>} />
                </ListItemButton>
              );
            }
          }
          return (
            <Stack key={key}>
              <ListItemButton
                onClick={() =>
                  toggleMenu(
                    convertNameToPath(value.funcNameEn),
                    value.child.length > 0
                      ? convertNameToUrl(value.child[0].funcNameEn)
                      : convertNameToUrl(value.funcNameEn),
                    value.funcCode
                  )
                }
                sx={{ pl, whiteSpace: "normal" }}
              >
                <ListItemText primary={<Typography variant="h4">{value.funcNameEn}</Typography>} />
                {activeMenu[convertNameToPath(value.funcNameEn)] ? <ExpandLessIcon /> : <ExpandMoreIcon />}
              </ListItemButton>
              <Collapse in={activeMenu[convertNameToPath(value.funcNameEn)]} timeout="auto" unmountOnExit>
                <Stack>
                  <RenderListMenu items={value.child} />
                </Stack>
              </Collapse>
            </Stack>
          );
        })}
      </List>
    );
  };

  return (
    <Drawer variant="permanent" open={isDrawerOpen}>
      <Toolbar
        sx={{
          display: "flex",
          alignItems: "center",
          justifyContent: "flex-start"
        }}
      >
        <Stack direction={"row"} gap={2} alignItems="center">
          <AccountCircleIcon />
          <Typography variant="h4">{account}</Typography>
        </Stack>
      </Toolbar>
      <Divider />

      <RenderListMenu items={menus} />

      <Stack sx={{ marginTop: 5, marginLeft: 5, marginBottom: 5 }}>
        <Typography sx={{ color: "#adadad" }} variant={"body"}>
          {process.env.NEXT_PUBLIC_ENVIRONMENT}: v{process.env.NEXT_PUBLIC_VERSION}
        </Typography>
      </Stack>
    </Drawer>
  );
};

const RenderSkeletonLoading = () => {
  return (
    <Stack gap={2} sx={{ mt: 2 }}>
      <Skeleton variant="rectangular" width={220} height={20} sx={{ borderRadius: 1, ml: 3 }} />
      <Skeleton variant="rectangular" width={220} height={20} sx={{ borderRadius: 1, ml: 3 }} />
      <Skeleton variant="rectangular" width={180} height={20} sx={{ borderRadius: 1, ml: 10 }} />
      <Skeleton variant="rectangular" width={180} height={20} sx={{ borderRadius: 1, ml: 10 }} />
      <Skeleton variant="rectangular" width={180} height={20} sx={{ borderRadius: 1, ml: 10 }} />
      <Skeleton variant="rectangular" width={220} height={20} sx={{ borderRadius: 1, ml: 3 }} />
      <Skeleton variant="rectangular" width={220} height={20} sx={{ borderRadius: 1, ml: 3 }} />
      <Skeleton variant="rectangular" width={220} height={20} sx={{ borderRadius: 1, ml: 3 }} />
      <Skeleton variant="rectangular" width={220} height={20} sx={{ borderRadius: 1, ml: 3 }} />
      <Skeleton variant="rectangular" width={180} height={20} sx={{ borderRadius: 1, ml: 10 }} />
      <Skeleton variant="rectangular" width={220} height={20} sx={{ borderRadius: 1, ml: 3 }} />
      <Skeleton variant="rectangular" width={220} height={20} sx={{ borderRadius: 1, ml: 3 }} />
    </Stack>
  );
};

export default DrawerComponent;
