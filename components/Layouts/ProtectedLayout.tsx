import type { FC, PropsWithChildren } from "react";

const ProtectedLayout: FC<PropsWithChildren> = ({ children }) => (
  <>{children}</>
);

export default ProtectedLayout;
