import { FC, PropsWithChildren, useEffect, useState, useCallback } from "react";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import CssBaseline from "@mui/material/CssBaseline";

import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import Container from "@mui/material/Container";
import Grid from "@mui/material/Grid";
import AppBarComponent from "./AppBar";
import DrawerComponent from "./Drawer";
import { useRouter } from "next/router";
import useSecurity from "@/hooks/useSecurity";
import { useMutation, useQuery } from "react-query";
import { getClientFunctions, getUserAssignment, getRoleAssignments } from "@/services/security-services";
import securityStore, { SecurityStoreTypes } from "@/store/securityStore";
import shallow from "zustand/shallow";
import { LOCALSTORAGE_KEY } from "@/config/constants";
import useLocalStorage from "@/hooks/useLocalStorage";
import useMounted from "@/hooks/useMounted";
import axios from "axios";
import { Toaster } from "react-hot-toast";
import useAmazonCognito from "@/hooks/useAmazonCognito";

const BaseLayout: FC<PropsWithChildren> = ({ children }) => {
  const { pathname, ...props } = useRouter();
  const isLoginPageOr404 = pathname == "/login" || props.route == "/404";
  const { isMounted } = useMounted();

  const { getItem } = useLocalStorage();
  const account = getItem(LOCALSTORAGE_KEY.ACCOUNT);
  const token = getItem(LOCALSTORAGE_KEY.TOKEN);

  const fetchClientFunctions = useQuery(
    ["get-client-function", account],
    async () => {
      const payload = { account: account, appCode: "MIO_ADMIN_PORTAL" };
      const { response } = await getClientFunctions(payload);
      return response;
    },
    { keepPreviousData: true, enabled: !!account }
  );

  const {
    data: userAssignment,
    isLoading: userAssignmentLoading,
    isFetching
  } = useQuery(
    ["get-user-assignment", account],
    () =>
      getUserAssignment({
        userName: account,
        status: true
      }),
    {
      enabled: !!account
    }
  );

  const SecurityStore = securityStore((state) => state, shallow) as SecurityStoreTypes;
  const [_, updateState] = useState<any>();
  const forceUpdate = useCallback(() => updateState({}), []);

  const { data, isLoading: isDrawerLoading } = fetchClientFunctions;
  const [loadingRole, setLoadingRole] = useState<boolean>(false);
  const { client_functions, role_code, setClientFunction } = SecurityStore;
  const { logout } = useAmazonCognito();

  useEffect(() => {
    if (userAssignment) {
      fetchAssignment();
    }
    if (!account || !token) {
      console.log("logout");
      logout();
    }
  }, [userAssignment, account, token]);

  const fetchAssignment = async () => {
    const roleList = userAssignment?.response && userAssignment?.response[0]?.roleList;
    let accessByRole: any = {};
    let roleCode: any = "";
    const rolePromises = roleList?.map(async (entity: any) => {
      setLoadingRole(true);
      const data = await getRoleAssignments({ roleId: entity?.roleId });
      Object.assign(accessByRole, { [entity.entityId]: data?.response[0]?.funcList });
      roleCode = data?.response[0]?.roleCode;
      setLoadingRole(false);
    });
    if (rolePromises) {
      await Promise.all(rolePromises);
    }

    setClientFunction(accessByRole, roleCode);
    forceUpdate();
  };

  if (!isMounted) return <></>;
  return (
    <Box sx={{ display: "flex" }}>
      <Toaster />
      <AppBarComponent isLoginPageOr404={isLoginPageOr404} />
      {isLoginPageOr404 == false && (
        <DrawerComponent menus={data} isLoading={isDrawerLoading || userAssignmentLoading || loadingRole} />
      )}
      <Box
        component="main"
        sx={{
          backgroundColor: (theme) => theme.palette.grey[100],
          flexGrow: 1,
          height: "100vh",
          overflow: "auto",
          pt: 3
        }}
      >
        <Toolbar />
        <Container maxWidth={false}>
          <Grid container>{children}</Grid>
        </Container>
      </Box>
    </Box>
  );
};

export default BaseLayout;
