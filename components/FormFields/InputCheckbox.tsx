import { FC, useEffect, useMemo } from "react";

import {
  FormLabel,
  Typography,
  FormControl,
  FormControlLabel,
  Checkbox,
  Stack
} from "@mui/material";

import { withStyles } from "@mui/styles";
import { Controller, useFormContext } from "react-hook-form";

export type InputTextProps = {
  label?: string;
  fullWidth?: boolean;
  required?: boolean;
  name: string;
  sx?: any;
  options: any;
  isMultiple?: boolean;
  disabled?: boolean;
  defaultValue?: any;
};

const StyledCheckbox = withStyles({
  root: {
    "&$disabled": {
      color: "hsla(189, 100%, 41%, 1)"
    }
  },
  checked: {},
  disabled: {}
})(Checkbox);

const InputCheckbox: FC<InputTextProps> = ({
  label,
  fullWidth,
  required,
  name,
  sx,
  options,
  disabled = false,
  isMultiple = true,
  defaultValue = null
}) => {
  const {
    control,
    formState: { errors },
    getValues,
    setValue
  } = useFormContext();

  useEffect(() => {
    if (!getValues(name)) {
      setValue(name, []);
    }
  }, [getValues]);

  const handleCheck = (checked: any) => {
    const { [name]: value } = getValues();

    const newValue = value?.includes(checked)
      ? value?.filter((val: any) => val !== checked)
      : [...(value ?? []), checked];
    return newValue;
  };

  const error = errors[name] ? errors[name]?.message : "";
  return (
    <Controller
      control={control}
      name={name}
      defaultValue={defaultValue}
      render={({ field: { onChange, value, ...field } }) => {
        return (
          <>
            <FormControl
              sx={{ gap: 1, ...sx }}
              required={required}
              fullWidth={fullWidth}
              error={!!errors[name]}
            >
              {label && <FormLabel color="secondary">{label}</FormLabel>}
              <Stack direction="row" alignItems="center" sx={{ flexWrap: "wrap" }} gap={2}>
                {options.map((val: any, key: number) => {
                  return (
                    <FormControlLabel
                      label={val.label}
                      key={key}
                      control={
                        <StyledCheckbox
                          disabled={disabled}
                          checked={value.includes(val.value)}
                          onChange={() => onChange(handleCheck(val.value))}
                          {...field}
                        />
                      }
                    />
                  );
                })}
              </Stack>
              <Typography variant="body" color="brandRed.500">{`${error}`}</Typography>
            </FormControl>
          </>
        );
      }}
    ></Controller>
  );
};

export default InputCheckbox;
