import * as React from "react";
import NumberFormat from "react-number-format";

interface CustomNumberFormatProps {
  onChange: (event: { target: { name: string; value: any } }) => void;
  name: string;
}

const CustomNumberFormat = React.forwardRef<NumberFormat<any>, CustomNumberFormatProps>(
  function NumberFormatCustom(props, ref) {
    const { onChange, ...other } = props;

    return (
      <NumberFormat
        {...other}
        allowNegative={false}
        decimalScale={2}
        getInputRef={ref}
        onValueChange={(values) => {
          onChange({
            target: {
              name: props.name,
              value: values.value.toString()
            }
          });
        }}
        thousandSeparator
      />
    );
  }
);

export default CustomNumberFormat;
