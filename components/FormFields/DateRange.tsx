import { FC } from "react";
import { DateTimePicker } from "@mui/x-date-pickers/DateTimePicker";
import { LocalizationProvider } from "@mui/x-date-pickers";
import { AdapterDateFns } from "@mui/x-date-pickers/AdapterDateFns";
import { Typography, Stack, TextField } from "@mui/material";

import { Controller, useFormContext } from "react-hook-form";

import useTranslate from "@/hooks/useTranslate";

interface DateRangeProps {
  start_name: string;
  end_name: string;
}

const DateRange: FC<DateRangeProps> = ({ start_name, end_name }) => {
  const { control } = useFormContext();
  const { translate } = useTranslate();

  return (
    <LocalizationProvider dateAdapter={AdapterDateFns}>
      <Stack direction="column" gap={1}>
        <Typography variant="h4">{translate(`dashboard.forms.labels.period`)}</Typography>
        <Stack direction="row" alignItems="center" gap={2}>
          <Controller
            name={start_name}
            control={control}
            defaultValue={null}
            render={({ field, ...props }) => {
              return (
                <DateTimePicker
                  value={field.value}
                  onChange={(value: Date | null) => {
                    field.onChange(value);
                  }}
                  renderInput={(params) => <TextField size="small" {...params} />}
                />
              );
            }}
          />

          {"-"}
          <Controller
            name={end_name}
            control={control}
            defaultValue={null}
            render={({ field, ...props }) => {
              return (
                <DateTimePicker
                  value={field.value}
                  onChange={(value: Date | null) => {
                    field.onChange(value);
                  }}
                  renderInput={(params) => <TextField size="small" {...params} />}
                />
              );
            }}
          />
        </Stack>
      </Stack>
    </LocalizationProvider>
  );
};

export default DateRange;
