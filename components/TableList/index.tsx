import * as React from "react";
import { useRouter } from "next/router";
import { useForm } from "react-hook-form";
import { useQuery } from "react-query";
import moment from "moment";
import { getBankEddaCode } from "@/services/common-services";
import Box from "@mui/material/Box";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TablePagination from "@mui/material/TablePagination";
import TableRow from "@mui/material/TableRow";
import TableSortLabel from "@mui/material/TableSortLabel";
import ClickAwayListener from "@mui/material/ClickAwayListener";
import Tooltip from "@mui/material/Tooltip";
import Typography from "@mui/material/Typography";
import Paper from "@mui/material/Paper";
import { visuallyHidden } from "@mui/utils";
import parse from "html-react-parser";

import {
  Autocomplete,
  Button,
  CircularProgress,
  Divider,
  Stack,
  TextField,
  Grid,
  Checkbox,
  Select,
  MenuItem,
  Radio,
  FormControlLabel,
  Alert,
  AlertTitle,
  Snackbar,
  IconButton,
  Modal,
  DialogContent
} from "@mui/material";
import { DateTimePicker } from "@mui/x-date-pickers/DateTimePicker";
import { DesktopDatePicker, LocalizationProvider } from "@mui/x-date-pickers";
import { AdapterDateFns } from "@mui/x-date-pickers/AdapterDateFns";
import { BsCheckLg as ChecklistIcon } from "react-icons/bs";
import { MdClear as ClearIcon } from "react-icons/md";

import { getWithdrawalBalanceCheck } from "@/services/mio-services";
import useTranslate from "@/hooks/useTranslate";
import ModalDialog from "../ModalDialog";
import useDialog from "@/hooks/useDialog";
import useExportFile from "@/hooks/useExportFile";
import useFormData from "@/hooks/useFormData";
import { RowingOutlined } from "@mui/icons-material";
import DateRange from "../FormFields/DateRange";
import { ServiceUnit } from "@/utils/serviceUnit";
import useLocalStorage from "@/hooks/useLocalStorage";
import { LOCALSTORAGE_KEY } from "@/config/constants";
import { makeStyles } from "@mui/styles";
import convertNameToUrl from "@/utils/convertToPathUrl";
import securityStore, { SecurityStoreTypes } from "@/store/securityStore";
import shallow from "zustand/shallow";
import { formatNumber, HumanizedDifference2Objects } from "@/config/utilities";
import { FilterPagination, Pagination, SortParams } from "@/types";
import RejectModal from "@/features/dashboard/components/RejectModal";
import InputText from "@/components/FormFields/InputText";
import useDrawerStore from "@/store/drawerStore";
import useAuditLog from "@/hooks/useAuditLog";
import Dialog from "@mui/material/Dialog";

function descendingComparator(a: any, b: any, orderBy: keyof any) {
  let newA = typeof a[orderBy] === "string" ? a[orderBy]?.toLowerCase() : a[orderBy];
  let newB = typeof b[orderBy] === "string" ? b[orderBy]?.toLowerCase() : b[orderBy];

  if (newB < newA) {
    return -1;
  }
  if (newB > newA) {
    return 1;
  }
  return 0;
}

type Order = "asc" | "desc";

const useStyles = makeStyles({
  sticky: {
    position: "sticky",
    right: 0,
    background: "white"
  }
});

function getComparator<Key extends keyof any>(
  order: Order,
  orderBy: Key
): (a: { [key in Key]: any }, b: { [key in Key]: any }) => number {
  return order === "desc"
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function tableSort<T>(array: readonly T[], comparator: (a: T, b: T) => number) {
  const stabilizedThis = array?.map((el, index) => [el, index] as [T, number]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) {
      return order;
    }
    return a[1] - b[1];
  });
  return stabilizedThis.map((el) => el[0]);
}

function stableSort<T>(array: readonly T[], comparator: (a: T, b: T) => number) {
  const stabilizedThis = array.map((el, index) => [el, index] as [T, number]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) {
      return order;
    }
    return a[1] - b[1];
  });

  return stabilizedThis.map((el) => el[0]);
}

interface HeaderTableProps {
  rowCount: number;
  onRequestSort: (event: React.MouseEvent<unknown>, property: string) => void;
  header: any;
}

interface FilterTableProps {
  type: any;
  id: string;
  name: string;
  options?: any;
}

interface TableListProps {
  header: any;
  filter?: any;
  action?: any;
  callbackAction?: (name: string, value: any) => void;
  data: any;
  isLoading?: boolean;
  useCheckbox?: boolean;
  filterData?: any;
  onFilterData?: (value: any) => void;
  rowActionCallback?: (value: any) => void;
  onSelectRow?: (value: boolean, row: any, headerName?: any) => void;
  selectedData?: any[];
  exportDataObject?: any;
  actionButtonLoading?: boolean;
  rowRetry?: any;
  isAccess?: boolean;
  isPagination?: boolean;
  pagination?: Pagination | null;
  handlePagination?: (val: FilterPagination) => void;
}

const TableList: React.FC<TableListProps> = ({
  header,
  filter = [],
  action = [],
  callbackAction = () => null,
  data = [],
  isLoading = false,
  useCheckbox = false,
  filterData,
  onFilterData = () => null,
  rowActionCallback = () => null,
  onSelectRow,
  selectedData = [],
  exportDataObject = null,
  actionButtonLoading = false,
  rowRetry = "",
  isAccess = false,
  pagination = null,
  isPagination = false,
  handlePagination = () => null
}) => {
  const { client_functions } = securityStore((state) => state, shallow) as SecurityStoreTypes;
  const access = client_functions;

  const { handleSubmit, getValues, register, setValue, watch } = useForm();
  const { setFormData } = useFormData();
  const [tooltipOpen, setTooltipOpen] = React.useState<any>(null);
  const [order, setOrder] = React.useState<Order>("asc");
  const [orderBy, setOrderBy] = React.useState<string>("null");
  const [page, setPage] = React.useState(0);
  const [generatedFileHeader, setGeneratedFileHeader] = React.useState<any[]>([]);
  const [generatedFileData, setGeneratedFileData] = React.useState<any[]>([]);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const { onGenerateFile } = useExportFile();
  const [rejectData, setRejectData] = React.useState<any>(null);

  const [paginationValue, setPagination] = React.useState<FilterPagination>({
    page: 0,
    size: 10,
    sort: []
  });

  const checkAccess = (row: any, permission: "writeAccess" | "readAccess" | "approveAccess") => {
    if (access) {
      if (row.entityId) {
        const rowAccess = access[row.entityId]?.find(
          (menu: any) => convertNameToUrl(menu.funcNameEn) == feature
        );
        return rowAccess ? rowAccess[permission] : false;
      } else {
        return true;
      }
    }
    return true;
  };

  const dataWithAccess = (arr: any) => {
    const newData: any = [];

    arr?.map((row: any) => {
      if (checkAccess(row, "readAccess")) {
        newData.push(row);
      }
    });

    if (access) {
      return newData;
    } else {
      return arr;
    }
  };

  const { translate } = useTranslate();
  const classes = useStyles();
  const {
    push,
    query: { feature, tab }
  } = useRouter();

  const { data: bankEdda } = useQuery(["get-edda-bank-code"], getBankEddaCode, {
    enabled: feature == "local-bank-list-edda-mapping"
  });
  React.useEffect(() => {
    if (data?.length && !isLoading && exportDataObject) {
      generateCsvResource();
    }
  }, [data, isLoading, exportDataObject?.data, exportDataObject?.loading]);

  React.useEffect(() => {
    if (isPagination) {
      handlePagination(paginationValue);
    }
  }, [isPagination, paginationValue]);

  const functionCode = useDrawerStore((state) => state.functionCode);
  const { handleLogFunction } = useAuditLog();

  const generateCsvResource = () => {
    let cellHeading: any[] = [];
    let cellData: any[] = [];
    exportDataObject.heading.forEach(function (val: any) {
      if (val.label) {
        cellHeading.push({ col: val.label, width: val.width, name: val.name });
      }
    });

    (exportDataObject.data ? dataWithAccess(exportDataObject.data) : dataWithAccess(data)).forEach(function (
      data: any,
      index: number
    ) {
      let dataItem: any = {};
      exportDataObject.heading.forEach(function (heading: any) {
        if (heading.name !== "no" && heading.type != "custom-columns") {
          if (heading.type == "custom-value") {
            heading.custom_values.forEach(function (custom_value: any) {
              if (custom_value.value == data[heading.name])
                dataItem[heading.position.toString()] = custom_value.label;
            });
          } else if (heading.name === "gatewayCurrency") {
            dataItem[heading.position.toString()] = data?.currencies[0]?.currency;
          } else {
            dataItem[heading.position.toString()] = data[heading.name] || "N/A";
          }
        }
        if (heading.type === "custom-columns-user") {
          if (heading.name === "isActive") {
            dataItem[heading.position.toString()] = data[heading.name] === true ? "Active" : "InActive";
          }

          if (heading.name === "roleList") {
            let idx = 4;
            data[heading.name].map((value: any, dataindex: number) => {
              if (!cellHeading.find((item: any) => item.col === value.entityId)) {
                cellHeading.push({
                  col: value.entityId,
                  width: 30,
                  name: value.entityId
                });
              }
              dataItem[(idx + dataindex).toString()] = value.roleNameEn;
            });
          }
        }
        if (heading.type == "custom-columns-0") {
          if (heading.name == "currencies" && heading.key == "currency") {
            dataItem[heading.position.toString()] = data[heading.name][0]["cryptoCurrency"]
              ? data[heading.name][0]["cryptoCurrency"]
              : data[heading.name][0][heading.key];
          } else if (heading.name == "serviceFee") {
            let unit = "";
            if (data[heading.key] === "D") {
              unit = data?.currencies[0]["cryptoCurrency"]
                ? data?.currencies[0]["cryptoCurrency"]
                : data?.currencies[0]["currency"];
            } else {
              unit = "%";
            }

            dataItem[heading.position.toString()] = `${data[heading.name]} ${unit}`;
          } else {
            if (data[heading.name].length > 1) {
              let value = "";
              data[heading.name].map((el: any, index: number) => {
                value += index == 0 ? el[heading.key] : `, ${el[heading.key]}`;
              });
              dataItem[heading.position.toString()] = value;
            } else {
              dataItem[heading.position.toString()] = data[heading.name][0][heading.key];
            }
          }
        }

        if (heading.type == "custom-columns") {
          let pos = 0;
          data[heading.name].map((value: any, dataindex: number) => {
            heading.columns.map((col: any) => {
              if (!cellHeading.find((item: any) => item.col === col.label.replace(":count", dataindex + 1))) {
                cellHeading.push({
                  col: col.label.replace(":count", dataindex + 1),
                  width: col.width,
                  name: col.name
                });
              }
              if (col.name == "unit") {
                dataItem[col.position + dataindex * heading.columns.length] =
                  value[col.name].toLowerCase() === "p"
                    ? "%"
                    : data["currencies"][0]["cryptoCurrency"]
                    ? data["currencies"][0]["cryptoCurrency"]
                    : data["currencies"][0]["currency"];
              } else {
                dataItem[col.position + dataindex * heading.columns.length] = value[col.name];
              }
            });
          });
        }

        if (heading.type == "custom-data") {
          const isDeposit = data["tradeType"].toLowerCase().includes("mi");
          const isWithdraw = data["tradeType"].toLowerCase().includes("mo");

          if (heading.key == "exchange-rate") {
            if (data["actExchangeRate1"] && data["actExchangeRate2"]) {
              dataItem[heading.position.toString()] = (
                data["actExchangeRate1"] * data["actExchangeRate2"]
              ).toString();
            } else {
              dataItem[heading.position.toString()] = (
                data["tmpExchangeRate1"] * data["tmpExchangeRate2"]
              ).toString();
            }
          }

          if (heading.key == "actual-amount") {
            if (isDeposit) {
              if (data["actAmount"]) {
                dataItem[heading.position.toString()] = data["actAmount"];
              } else {
                dataItem[heading.position.toString()] = data["tmpAmount"];
              }
            }
            if (isWithdraw) {
              if (data["actDepositAmount"]) {
                dataItem[heading.position.toString()] = data["actDepositAmount"];
              } else {
                dataItem[heading.position.toString()] = data["tmpDepositAmount"];
              }
            }
          }

          if (heading.key == "actual-currency") {
            if (isDeposit) {
              dataItem[heading.position.toString()] = data["currency"];
            }
            if (isWithdraw) {
              dataItem[heading.position.toString()] = data["settleCurrency"];
            }
          }

          if (heading.key == "platform-currency") {
            dataItem[heading.position.toString()] = "USD";
          }

          if (heading.key == "platform-amount") {
            if (isDeposit) {
              dataItem[heading.position.toString()] = data["actDepositAmount"]
                ? data["actDepositAmount"]
                : data["tmpDepositAmount"];
            }
            if (isWithdraw) {
              dataItem[heading.position.toString()] = data["actAmount"]
                ? data["actAmount"]
                : data["tmpAmount"];
            }
          }

          if (heading.key == "service-charge") {
            dataItem[heading.position.toString()] = data["actCharge"] ? data["actCharge"] : data["tmpCharge"];
          }

          if (heading.key == "calculated-actual-deposit") {
            let actualAmount = 0;
            const serviceCharge = data["actCharge"] ? data["actCharge"] : data["tmpCharge"];
            if (isWithdraw) {
              actualAmount = data["actDepositAmount"] ? data["actDepositAmount"] : data["tmpDepositAmount"];
            }
            if (isDeposit) {
              actualAmount = data["actAmount"] ? data["actAmount"] : data["tmpAmount"];
            }
            if (data["tradeType"].toLowerCase() == "mi" || data["tradeType"].toLowerCase() == "mia") {
              dataItem[heading.position.toString()] = actualAmount - serviceCharge;
            } else {
              dataItem[heading.position.toString()] = "N/A";
            }
          }

          if (heading.key == "platform-currency") {
            dataItem[heading.position.toString()] = "USD";
          }
          if (heading.key == "bank-location") {
            dataItem[heading.position.toString()] = data.bankLocation?.nameEn || "N/A";
          }
          if (heading.key == "bank-province") {
            dataItem[heading.position.toString()] = data.bankProvince?.nameEn || "N/A";
          }
          if (heading.key == "bank-region") {
            dataItem[heading.position.toString()] = data.bankRegion?.nameEn || "N/A";
          }
          if (heading.key == "bank-city") {
            dataItem[heading.position.toString()] = data.bankCity?.nameEn || "N/A";
          }
        }
      });
      cellData.push(dataItem);
    });
    setGeneratedFileHeader(cellHeading);
    setGeneratedFileData(cellData);
  };

  const handleChangePage = (event: unknown, newPage: number) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement>) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const emptyRows =
    page > 0
      ? Math.max(0, (1 + page) * rowsPerPage - (access ? dataWithAccess(data).length : data.length))
      : 0;

  const handleSearch = () => {
    const queryFilter = getValues();
    setPage(0);
    setPagination({ ...paginationValue, page: 0 });
    onFilterData(queryFilter);
  };

  function RenderTableHead(props: HeaderTableProps) {
    const { header, onRequestSort } = props;

    const createSortHandler = (property: any) => (event: React.MouseEvent<unknown>) => {
      onRequestSort(event, property);
    };

    return (
      <TableHead>
        <TableRow>
          {header.map((headCell: any, key: number) => {
            let id = headCell?.sortId ? headCell?.sortId : headCell.id;

            const hasSort = headCell.isSort;
            const isActive = paginationValue.sort.find((e: SortParams) => e.field === id) as SortParams;

            let direction: any = "asc" || undefined;
            let sortDirection: any = "asc";
            let active: boolean = false;

            if (!isPagination) {
              sortDirection = orderBy === id ? "desc" : "asc" || "asc";
              active = orderBy === id;
              direction = orderBy === id ? order : "asc" || "asc";
            } else {
              direction = isActive?.ascOrder ? "desc" : "asc" || "asc";
              sortDirection = isActive?.ascOrder ? "desc" : "asc" || "asc";
              active = isActive?.field === id;
            }

            return (
              <TableCell
                key={key}
                align="center"
                sortDirection={hasSort ? sortDirection : undefined}
                className={headCell.sticky ? classes.sticky : ""}
              >
                <TableSortLabel
                  sx={{
                    "&.MuiTableSortLabel-root": {
                      cursor: hasSort ? "pointer" : "unset",
                      "&:hover": {
                        cursor: hasSort ? "pointer" : "unset",
                        color: "unset",
                        ">.MuiSvgIcon-root": {
                          visibility: hasSort ? "initial" : "hidden"
                        }
                      }
                    }
                  }}
                  active={hasSort ? active : false}
                  direction={hasSort ? direction : undefined}
                  onClick={hasSort ? createSortHandler(id) : undefined}
                >
                  <Typography variant="body" fontWeight={700}>
                    {headCell.label}
                  </Typography>
                  {orderBy === id ? (
                    <Box component="span" sx={visuallyHidden}>
                      {order === "desc" ? "sorted descending" : "sorted ascending"}
                    </Box>
                  ) : null}
                </TableSortLabel>
              </TableCell>
            );
          })}
        </TableRow>
      </TableHead>
    );
  }

  const RenderFilterOption: React.FC<FilterTableProps> = ({ id, name, type, options }) => {
    /* note: add every possibly type of filter here */
    let defaultValue = "";

    if (filterData) {
      if (typeof filterData[name] == "boolean") {
        defaultValue = filterData[name].toString();
      } else if (typeof filterData[name] == "number") {
        defaultValue = getValues(name);
      } else if (filterData[name]) {
        defaultValue = filterData[name].toString();
      } else {
        defaultValue = getValues(name) || "";
      }
    } else {
      defaultValue =
        typeof getValues(name) === "boolean" || getValues(name) === 0 || getValues(name)
          ? getValues(name)
          : "";
    }

    switch (type) {
      case "radio":
        return (
          <Stack direction="column" gap={1}>
            <Typography variant="body">{translate(`dashboard.labels.${id}`)}</Typography>
            <Stack direction="row">
              {options &&
                options?.map((option: any, key: number) => (
                  <FormControlLabel key={key} value={option} control={<Radio />} label={option} />
                ))}
            </Stack>
          </Stack>
        );
      case "dropdown":
        return (
          <Stack direction="column" gap={1}>
            <Typography variant="body">{translate(`dashboard.labels.${id}`)}</Typography>
            <Autocomplete
              {...register(name)}
              disablePortal
              id="entity"
              size="small"
              defaultValue={options?.find((v: any) => v.value === defaultValue) || null}
              options={options}
              sx={{ width: "100%" }}
              onChange={(_, value: any) => setValue(name, value?.value)}
              renderInput={(params) => <TextField {...params} inputProps={{
                ...params.inputProps,
                onKeyPress: (e) => {
                  if(e.key.match(/[`#$%^&*"_+=\[\]{};':"\\|<>\/?~]/g)){
                    e.preventDefault();
                  }
                },
              }}/>}
            />
          </Stack>
        );
      case "time-range":
        return (
          <LocalizationProvider dateAdapter={AdapterDateFns}>
            <Stack direction="column" gap={1}>
              <Typography variant="body">{translate(`dashboard.labels.${id}`)}</Typography>
              <Stack direction="row" alignItems="center" gap={2}>
                <Stack direction="row" alignItems="center" gap={1}>
                  <DateTimePicker
                    inputFormat="yyyy-MM-dd HH:mm:ss"
                    views={["year", "day", "hours", "minutes", "seconds"]}
                    value={watch(`${name}_start`) || null}
                    onAccept={(value) => {
                      setValue(`${name}_start`, value);
                    }}
                    onChange={() => null}
                    renderInput={(params) => (
                      <TextField
                        size="small"
                        {...params}
                        inputProps={{
                          ...params.inputProps,
                          placeholder: "Select Date Time"
                        }}
                      />
                    )}
                  />
                  {watch(`${name}_start`) && (
                    <IconButton>
                      <ClearIcon
                        style={{ cursor: "pointer" }}
                        onClick={() => {
                          setValue(`${name}_start`, null);
                        }}
                      />
                    </IconButton>
                  )}
                </Stack>
                <Stack direction="row" alignItems="center" gap={1}>
                  <DateTimePicker
                    inputFormat="yyyy-MM-dd HH:mm:ss"
                    views={["year", "day", "hours", "minutes", "seconds"]}
                    value={watch(`${name}_end`) || null}
                    onAccept={(value) => {
                      setValue(`${name}_end`, value);
                    }}
                    onChange={() => null}
                    renderInput={(params) => (
                      <TextField
                        size="small"
                        {...params}
                        inputProps={{
                          ...params.inputProps,
                          placeholder: "Select Date Time"
                        }}
                      />
                    )}
                  />
                  {watch(`${name}_end`) && (
                    <IconButton>
                      <ClearIcon
                        style={{ cursor: "pointer" }}
                        onClick={() => setValue(`${name}_end`, null)}
                      />
                    </IconButton>
                  )}
                </Stack>
              </Stack>
            </Stack>
          </LocalizationProvider>
        );
      default:
        return (
          <Stack direction="column" gap={1}>
            <Typography variant="body">{translate(`dashboard.labels.${id}`)}</Typography>
            <TextField
              {...register(name)}
              size="small"
              onChange={(e) => {
                let val = e.target.value.replaceAll(/[`#$%^&*"_+=\[\]{};':"\\|<>\/?~]/g, "");
                setValue(name, val);
              }}
              sx={{ width: "100%" }}
            />
          </Stack>
        );
    }

    /* end of note */
    return <></>;
  };

  const TableToolbar = () => {
    const [currencies, setCurrencies] = React.useState<any>({});

    React.useEffect(() => {
      if (selectedData.length > 0) {
        selectedData.map((el: any) => {
          if (currencies[el.currency]) {
            setCurrencies({
              ...currencies,
              [el.currency]: el.actAmount + currencies[el.currency]
            });
          } else {
            setCurrencies({ ...currencies, [el.currency]: el.actAmount });
          }
        });
      }
    }, [selectedData]);

    const handleTableAction = (action: any) => {
      if (action.type == "link") {
        handleLogFunction(functionCode, `/${feature}${action.value}`);
        push(`/${feature}${action.value}`);
      }
      if (action.type == "callback_action") {
        callbackAction(action.value, selectedData);
      }
      if (action.type == "export_csv") {
        handleLogFunction(functionCode, `/${feature}/export`);
        if (
          (data || exportDataObject.data.length) &&
          (dataWithAccess(data).length || dataWithAccess(exportDataObject.data).length)
        ) {
          onGenerateFile(generatedFileData, generatedFileHeader, feature + "_" + new Date(), ".xlsx");
        } else {
          alert("Data not ready yet!");
        }
      }
    };

    let amountHKD = 0;
    let amountRMB = 0;
    let amountUSD = 0;
    let amountUSDTERC20 = 0;
    let amountUSDTTRC20 = 0;

    if (feature == "payment-management" && useCheckbox) {
      selectedData.forEach((data: any) => {
        let currency = "";
        let amount = null;
        if (data.tradeType.toLowerCase().includes("mi")) {
          currency = data.currency;
          amount = data.actAmount ? data.actAmount : data.tmpAmount;
        }
        if (data.tradeType.toLowerCase().includes("mo")) {
          currency = data.settleCurrency;
          amount = data.actDepositAmount ? data.actDepositAmount : data.tmpDepositAmount;
        }
        if (currency.toLowerCase() == "hkd") {
          amountHKD += amount;
        }
        if (currency.toLowerCase() == "rmb") {
          amountRMB += amount;
        }
        if (currency.toLowerCase() == "usd") {
          amountUSD += amount;
        }
        if (currency.toLowerCase() == "usdterc20") {
          amountUSDTERC20 += amount;
        }
        if (currency.toLowerCase() == "usdttrc20") {
          amountUSDTTRC20 += amount;
        }
      });
    }

    return (
      <Stack>
        <Stack sx={{ width: "100%" }}>
          <Stack gap={3} direction="row" justifyContent="space-between">
            <Grid container spacing={2} padding={2}>
              {filter.map((value: any, key: number) => {
                return (
                  <Grid item md={value.type === "time-range" ? 6 : 4} key={key}>
                    <RenderFilterOption
                      type={value.type}
                      id={value.id}
                      name={value.name}
                      options={value.options}
                    />
                  </Grid>
                );
              })}
            </Grid>
          </Stack>
          <Divider />
          <Stack alignItems="flex-end">
            <Stack direction="row" alignItems="flex-end" gap={2} padding={2}>
              {filter.length ? (
                <Button size="small" sx={{ height: 35 }} onClick={handleSearch}>
                  <Typography variant="body" fontWeight={700}>
                    {translate("dashboard.buttons.search")}
                  </Typography>
                </Button>
              ) : null}

              {action &&
                action.map((value: any, key: any) => {
                  if (value.name == "export" && !dataWithAccess(data).length) {
                    return null;
                  }
                  return (
                    <Stack key={key}>
                      <Button
                        onClick={() => handleTableAction(value.action)}
                        size="small"
                        variant="outlined"
                        sx={{ height: 35 }}
                        disabled={
                          value.name != "export"
                            ? false
                            : exportDataObject.loading
                            ? exportDataObject.loading
                            : false
                        }
                      >
                        <Typography variant="body" fontWeight={700}>
                          {translate(`dashboard.buttons.${value.name}`)}
                        </Typography>
                      </Button>
                    </Stack>
                  );
                })}
            </Stack>
          </Stack>

          {useCheckbox && (
            <>
              <Divider />
              <Stack direction="row" gap={2} padding={2}>
                {useCheckbox && <Typography>Total Selected records: {selectedData.length}</Typography>}
                {useCheckbox && <Typography>Total Amount: </Typography>}
                {useCheckbox && <Typography>HKD {formatNumber("HKD", amountHKD)}</Typography>}
                {useCheckbox && <Typography>RMB {formatNumber("RMB", amountRMB)}</Typography>}
                {useCheckbox && <Typography>USD {formatNumber("USD", amountUSD)}</Typography>}
                {useCheckbox && (
                  <Typography>USDTERC20 {formatNumber("USDTERC20", amountUSDTERC20)}</Typography>
                )}
                {useCheckbox && (
                  <Typography>USDTTRC20 {formatNumber("USDTTRC20", amountUSDTTRC20)}</Typography>
                )}
              </Stack>
            </>
          )}
        </Stack>
      </Stack>
    );
  };

  const RenderRowAction = ({
    feature,
    row,
    action_key,
    id,
    rowRetry
  }: {
    feature: any;
    row: any;
    action_key: any;
    id: any;
    rowRetry?: any;
  }) => {
    const { getItem } = useLocalStorage();
    const account = getItem(LOCALSTORAGE_KEY.ACCOUNT);
    const isOwnData = account == row.requestBy;

    const isShowRetry =
      rowRetry !== null && rowRetry?.row === row.id && rowRetry?.status?.toLowerCase() === "pass";
    /* note: add every possibly feature action here */
    switch (feature) {
      case "beneficiary-accounts":
      case "list-of-key-term-naming":
      case "prc-province":
      case "prc-region-district":
      case "prc-city":
      case "blacklist":
      case "withdrawal-channel":
      case "deposit-channel":
        if (checkAccess(row, "writeAccess")) {
          return (
            <Button
              sx={{ height: 30 }}
              onClick={function () {
                setFormData(row);
                handleLogFunction(functionCode, `/${feature}/detail?id=${id}`);
                push({ pathname: `/${feature}/detail`, query: { id: action_key } });
              }}
            >
              <Typography variant="body" fontWeight="bold">
                {translate("dashboard.buttons.edit")}
              </Typography>
            </Button>
          );
        } else {
          return null;
        }

      case "transaction-records":
        return (
          <Button
            sx={{ height: 30 }}
            onClick={() => {
              setFormData(row);
              handleLogFunction(functionCode, `/${feature}/detail?id=${row.orderNo}`);
              push({ pathname: `/${feature}/detail`, query: { id: action_key } });
            }}
          >
            <Typography variant="body" fontWeight="bold">
              {translate("dashboard.buttons.details")}
            </Typography>
          </Button>
        );
      case "withdrawal-management":
      case "deposit-management":
        return (
          <Stack direction="row" gap={1}>
            <Button
              sx={{ height: 45 }}
              disabled={actionButtonLoading}
              onClick={() => {
                setFormData(row);
                handleLogFunction(functionCode, `/${feature}/detail?id=${row.orderNo}`);
                push({ pathname: `/${feature}/detail`, query: { id: action_key } });
              }}
            >
              <Typography variant="body" fontWeight="bold">
                {translate("dashboard.buttons.details")}
              </Typography>
            </Button>
            {checkAccess(row, "approveAccess") && row.status == 0 && !isOwnData && (
              <>
                <Button
                  sx={{ height: 45 }}
                  disabled={
                    actionButtonLoading ||
                    ((!row.isValidAml && !(row.tradeType.toLowerCase() === "moa" || row.tradeType.toLowerCase() === "mia")) &&
                      !(rowRetry?.row === row.id && rowRetry?.status?.toLowerCase() === "pass"))
                  }
                  onClick={() => rowActionCallback({ data: row, action: "approve" })}
                >
                  <Typography variant="body" fontWeight="bold">
                    {translate("dashboard.buttons.approve")}
                  </Typography>
                </Button>
                <Button sx={{ height: 45 }} disabled={actionButtonLoading} onClick={() => setRejectData(row)}>
                  <Typography variant="body" fontWeight="bold">
                    {translate("dashboard.buttons.reject")}
                  </Typography>
                </Button>
              </>
            )}
            {checkAccess(row, "approveAccess") && !row.isValidAml && row.status === 0 && !isShowRetry && (
              <>
                <Button
                  sx={{ height: 45, width: 130 }}
                  disabled={actionButtonLoading}
                  onClick={() => rowActionCallback({ data: row, action: "retry-aml" })}
                >
                  <Typography variant="body" fontWeight="bold">
                    {translate("dashboard.buttons.retry_aml")}
                  </Typography>
                </Button>
                {rowRetry?.row === row.id && rowRetry?.status?.toLowerCase() !== "pass" && (
                  <Box display="flex" alignItems="center" color="red">
                    Retried
                  </Box>
                )}
              </>
            )}
          </Stack>
        );

      case "payment-management":
        return (
          <Stack
            direction="row"
            gap={1}
            sx={{ width: tab === "unclaimed_deposit" && checkAccess(row, "writeAccess") ? 270 : 100 }}
          >
            <Button
              sx={{ height: 45 }}
              onClick={() => {
                setFormData(row);
                handleLogFunction(functionCode, `/${feature}/detail?id=${id}`);
                push({ pathname: `/${feature}/detail`, query: { id: action_key } });
              }}
            >
              <Typography variant="body" fontWeight="bold">
                {translate("dashboard.buttons.details")}
              </Typography>
            </Button>
            {tab === "unclaimed_deposit" && checkAccess(row, "writeAccess") && (
              <Button
                sx={{ height: 45 }}
                onClick={() => {
                  setFormData(row);
                  handleLogFunction(functionCode, `/ccba-bank-records/detail?id=${id}`);
                  push({ pathname: `/ccba-bank-records/detail`, query: { id: action_key } });
                }}
              >
                <Typography variant="body" fontWeight="bold">
                  {translate("dashboard.buttons.manual_deposit")}
                </Typography>
              </Button>
            )}
          </Stack>
        );
      case "dummy-account-list":
        return (
          <Button sx={{ height: 45 }} onClick={() => rowActionCallback(row)}>
            <Typography variant="body" fontWeight="bold">
              {translate("dashboard.buttons.remove")}
            </Typography>
          </Button>
        );
      case "bank-&-credit-card-record":
        if (row.status !== 1 && row.bankType.toLowerCase() == "cc") return <></>;
        if (checkAccess(row, "writeAccess")) {
          return (
            <Button
              sx={{ height: 45 }}
              onClick={() => {
                setFormData(row);
                handleLogFunction(functionCode, `/${feature}/detail?id=${id}`);
                push({ pathname: `/${feature}/detail`, query: { id: action_key } });
              }}
            >
              <Typography variant="body" fontWeight="bold">
                {translate("dashboard.buttons.edit")}
              </Typography>
            </Button>
          );
        } else {
          return null;
        }

      case "exceptions-management":
        if (checkAccess(row, "writeAccess")) {
          return (
            <Button
              disabled={actionButtonLoading}
              sx={{ height: 45, width: 200 }}
              onClick={() => rowActionCallback(row)}
            >
              <Typography variant="body" fontWeight="bold" sx={{ fontSize: "12px" }}>
                {row.tradeType.toLowerCase().includes("mi") &&
                  translate("dashboard.buttons.retry_auto_deposit")}
                {row.tradeType.toLowerCase().includes("mo") &&
                  translate("dashboard.buttons.retry_auto_withdrawal")}
              </Typography>
            </Button>
          );
        } else {
          return null;
        }

      case "ccba-bank-records":
        if (checkAccess(row, "writeAccess") && (row.paymentStatus == 9 || row.paymentStatus == 21)) {
          return (
            <Button
              sx={{ height: 45, width: 165 }}
              onClick={() => {
                setFormData(row);
                handleLogFunction(functionCode, `/${feature}/detail?id=${id}`);
                push({ pathname: `/${feature}/detail`, query: { id: action_key } });
              }}
            >
              <Typography variant="body" fontWeight="bold">
                {translate("dashboard.buttons.manual_deposit")}
              </Typography>
            </Button>
          );
        } else {
          return null;
        }

      case "operator-notifications":
        return (
          <Stack direction="row" gap={1}>
            <Button
              sx={{ height: 45 }}
              onClick={function () {
                setFormData(row);
                handleLogFunction(functionCode, `/${feature}/detail?id=${id}`);
                push({ pathname: `/${feature}/detail`, query: { id: action_key } });
              }}
            >
              <Typography variant="body" fontWeight="bold">
                {translate("dashboard.buttons.edit")}
              </Typography>
            </Button>
            <Button
              sx={{ height: 45 }}
              onClick={function () {
                setFormData(row);
                handleLogFunction(functionCode, `/${feature}/notification-preview?id=${id}`);
                push({ pathname: `/${feature}/notification-preview`, query: { id: action_key } });
              }}
            >
              <Typography variant="body" fontWeight="bold">
                {translate("dashboard.buttons.preview")}
              </Typography>
            </Button>
          </Stack>
        );
      default:
        return (
          <Button
            sx={{ height: 45 }}
            onClick={function () {
              setFormData(row);
              handleLogFunction(functionCode, `/${feature}/detail?id=${id}`);
              push({ pathname: `/${feature}/detail`, query: { id: action_key } });
            }}
          >
            <Typography variant="body" fontWeight="bold">
              {translate("dashboard.buttons.edit")}
            </Typography>
          </Button>
        );
    }
    /* end of note */
  };

  const RenderRowData = ({ row, index }: { row: any; index: number }) => {
    const [showModal, setShowModal] = React.useState(false);
    const {
      data: balance,
      refetch: fetchBalance,
      isFetching: fetchingBalance
    } = useQuery(
      ["check-balance-withdrawal", row.instRequestId],
      () =>
        getWithdrawalBalanceCheck({
          tradingAccountNo: row.tradingAccountNo,
          entityId: row.entityId
        }),
      {
        enabled: false
      }
    );

    return (
      <>
        {header.map((val: any, key: number) => {
          let rowValue = typeof row[val.id] == "boolean" ? row[val.id].toString() : row[val.id];
          let show = false;
          const arrayStatus = ["isActive", "isValidAml", "active", "status", "tradeType"];

          if (feature === "local-bank-list-edda-mapping" && val.label === "Client Bank") {
            rowValue += ` ${row.bankNameSys}`;
          }

          if (feature === "deposit-channel" && val.label === "Service Fee") {
            rowValue = ServiceUnit(
              row.serviceFeeUnit,
              row?.currencies[0]?.cryptoCurrency
                ? row?.currencies[0]?.cryptoCurrency
                : row?.currencies[0]?.currency,
              row[val.id]
            );
          }

          if (feature === "withdrawal-channel" && val.label === "Service Fee") {
            rowValue = ServiceUnit(
              row.serviceFeeUnit,
              row?.currencies[0]?.cryptoCurrency
                ? row?.currencies[0]?.cryptoCurrency
                : row?.currencies[0]?.currency,
              row[val.id]
            );
          }

          if (val.condition) {
            if (row[val.condition.name] == val.condition.value) {
              show = true;
            } else {
              show = false;
            }
          } else {
            show = true;
          }
          if (val.id === "no") {
            return (
              <TableCell align="center" key={Math.random()}>
                {index + 1}
              </TableCell>
            );
          }
          if (val.id === "checkbox" && useCheckbox) {
            let selected: any = [];
            let isCheked: boolean = false;

            selected = selectedData.filter((el) => el.id === row.id);
            isCheked = selected.length > 0 ? true : false;

            if (checkAccess(row, "approveAccess")) {
              return (
                <TableCell key={Math.random()} align="center">
                  <Checkbox
                    checked={isCheked}
                    onChange={(e: any) => (onSelectRow ? onSelectRow(e.target.checked, row, val) : null)}
                  />
                </TableCell>
              );
            } else {
              return <Typography> </Typography>;
            }
          }
          if (val.id === "latest_info") {
            return (
              <ClickAwayListener onClickAway={() => setTooltipOpen(null)} key={Math.random()}>
                <TableCell align="left">
                  <Tooltip
                    arrow
                    PopperProps={{
                      disablePortal: true
                    }}
                    onClose={() => setTooltipOpen(null)}
                    open={row.instRequestId === tooltipOpen}
                    disableFocusListener
                    disableHoverListener
                    disableTouchListener
                    title={
                      fetchingBalance ? (
                        "Loading..."
                      ) : (
                        <span style={{ whiteSpace: "pre-line", textAlign: "left" }}>
                          Balance: USD {formatNumber("USD", balance?.response?.tradingAccountBalance)} <br />
                          Credit: USD {formatNumber("USD", balance?.response?.credit)}
                          <br />
                          Floating: USD {formatNumber("USD", balance?.response?.floating)} <br />
                          Margin: USD {formatNumber("USD", balance?.response?.margin)} <br />
                          Equity: USD {formatNumber("USD", balance?.response?.equity)} <br />
                          Usable Margin: USD {formatNumber("USD", balance?.response?.usableMargin)} <br />
                          Withdrawal Balance: USD {formatNumber("USD", balance?.response?.balance)} <br />
                        </span>
                      )
                    }
                  >
                    <Button
                      onClick={() => {
                        setTooltipOpen(row.instRequestId);
                        fetchBalance();
                      }}
                      sx={{ width: 130 }}
                    >
                      <Typography variant="body" fontWeight={700}>
                        Latest info
                      </Typography>
                    </Button>
                  </Tooltip>
                </TableCell>
              </ClickAwayListener>
            );
          }

          if (val.id == "action") {
            return (
              <TableCell align="center" key={Math.random()} className={val.sticky ? classes.sticky : ""}>
                <RenderRowAction
                  rowRetry={rowRetry}
                  feature={feature}
                  row={row}
                  action_key={row[val.key_value]}
                  id={row[val.idValue]}
                />
              </TableCell>
            );
          }
          if (arrayStatus?.includes(val.id)) {
            return <RenderStatusData status={row[val.id]} id={val.id} key={key} />;
          }
          if (val.type === "checkbox") {
            if (row[val.id]) {
              return (
                <TableCell align="center" key={Math.random()} sx={{ color: "#111" }}>
                  <ChecklistIcon key={Math.random()} />
                </TableCell>
              );
            } else {
              return (
                <TableCell align="center" key={Math.random()} sx={{ color: "#111" }}>
                  <Typography sx={{ fontWeight: 700 }}>-</Typography>
                </TableCell>
              );
            }
          }
          if (val.row_condition) {
            val.row_condition.forEach(function (v: any) {
              if (v.type == "contain-string") {
                if (rowValue?.includes(v.key)) rowValue = v.value;
              }
            });
          }
          if (feature === "local-bank-list-edda-mapping" && val.id == "edda") {
            let eDDA = "";
            bankEdda?.response[0]?.items.forEach((val: any) => {
              if (row.eddaBankCode == val.codeCode) eDDA = val.name;
            });
            return (
              <TableCell align="center" key={Math.random()} sx={{ color: "#111" }}>
                {eDDA}
              </TableCell>
            );
          }
          if (feature === "audit-log") {
            const parsedObject = JSON.parse(row.content);
            if (val.id == "content") {
              return (
                <TableCell align="center" key={Math.random()} sx={{ color: "#111" }}>
                  {parsedObject.description || "-"}
                </TableCell>
              );
            }
            if (val.id == "detail") {
              return (
                <TableCell align="center" key={Math.random()} sx={{ color: "#111" }}>
                  <Button sx={{ height: 45 }} onClick={() => setShowModal(true)}>
                    <Typography variant="body" fontWeight="bold">
                      {translate("dashboard.buttons.details")}
                    </Typography>
                  </Button>
                  <Dialog onClose={() => setShowModal(false)} open={showModal} maxWidth={"sm"}>
                    <DialogContent>
                      <Stack sx={{ mb: 1 }}>
                        {parsedObject.action} {parsedObject.table}
                      </Stack>
                      {parse(
                        HumanizedDifference2Objects(
                          parsedObject.action,
                          parsedObject.before,
                          parsedObject.after
                        )
                      )}
                    </DialogContent>
                  </Dialog>
                </TableCell>
              );
            }
          }
          if (
            feature === "transaction-records" ||
            feature === "payment-management" ||
            feature === "ccba-bank-records" ||
            feature === "deposit-management" ||
            feature === "withdrawal-management" ||
            feature === "exceptions-management"
          ) {
            if (val.name === "actual_currency") {
              if (row.tradeType.toLowerCase().includes("mi")) {
                return (
                  <TableCell align="center" key={Math.random()} sx={{ color: "#111" }}>
                    {row.currency}
                  </TableCell>
                );
              }
              if (row.tradeType.toLowerCase().includes("mo")) {
                return (
                  <TableCell align="center" key={Math.random()} sx={{ color: "#111" }}>
                    {row.settleCurrency}
                  </TableCell>
                );
              }
            }

            if (val.name === "balance") {
              return (
                <TableCell align="center" key={Math.random()} sx={{ color: "#111" }}>
                  USD{" "}
                  {row.balance == null ? formatNumber("USD", row.balance) : formatNumber("USD", row.balance)}
                </TableCell>
              );
            }

            if (val.name === "platform_amount") {
              if (row.tradeType.toLowerCase().includes("mi"))
                return (
                  <TableCell align="center" key={Math.random()} sx={{ color: "#111" }}>
                    USD{" "}
                    {row.actDepositAmount == null
                      ? formatNumber("USD", row.tmpDepositAmount)
                      : formatNumber("USD", row.actDepositAmount)}
                  </TableCell>
                );
              if (row.tradeType.toLowerCase().includes("mo"))
                return (
                  <TableCell align="center" key={Math.random()} sx={{ color: "#111" }}>
                    USD{" "}
                    {row.actAmount == null
                      ? formatNumber("USD", row.tmpAmount)
                      : formatNumber("USD", row.actAmount)}
                  </TableCell>
                );
            }
            if (val.name === "actual_amount") {
              if (row.tradeType.toLowerCase().includes("mi")) {
                return (
                  <TableCell align="center" key={Math.random()} sx={{ color: "#111" }}>
                    {row.actAmount == null
                      ? formatNumber(row.currency, row.tmpAmount)
                      : formatNumber(row.currency, row.actAmount)}
                  </TableCell>
                );
              }
              if (row.tradeType.toLowerCase().includes("mo")) {
                return (
                  <TableCell align="center" key={Math.random()} sx={{ color: "#111" }}>
                    {row.actDepositAmount == null
                      ? formatNumber(row.settleCurrency, row.tmpDepositAmount)
                      : formatNumber(row.settleCurrency, row.actDepositAmount)}
                  </TableCell>
                );
              }
            }
            if (val.name === "service_fee") {
              if (row.tradeType.toLowerCase().includes("mi")) {
                return (
                  <TableCell align="center" key={Math.random()} sx={{ color: "#111" }}>
                    {row.cryptoCurrency ? row.cryptoCurrency : row.currency}{" "}
                    {row.actCharge == null ? row.tmpCharge : row.actCharge}
                  </TableCell>
                );
              }
              if (row.tradeType.toLowerCase().includes("mo")) {
                return (
                  <TableCell align="center" key={Math.random()} sx={{ color: "#111" }}>
                    {row.cryptoCurrency ? row.cryptoCurrency : row.settlementAccCurrency}{" "}
                    {row.actCharge == null ? row.tmpCharge : row.actCharge}
                  </TableCell>
                );
              }
            }
            if (val.name === "platform_currency") {
              return (
                <TableCell align="center" key={Math.random()} sx={{ color: "#111" }}>
                  USD
                </TableCell>
              );
            }
          }

          return (
            <TableCell align="center" key={Math.random()} sx={{ color: rowValue ? "#111" : "#a1a1a1" }}>
              {show ? (rowValue ? rowValue : "N/A") : "N/A"}
            </TableCell>
          );
        })}
      </>
    );
  };

  const RenderStatusData = ({ status, id }: { status: any; id?: any }) => {
    if (
      feature == "deposit-channel" ||
      feature == "withdrawal-channel" ||
      feature == "user-management" ||
      feature == "local-bank-list-edda-mapping"
    ) {
      if (status == true) return <TableCell align="center">Active</TableCell>;
      if (status == false) return <TableCell align="center">Inactive</TableCell>;
      return <TableCell align="center">-</TableCell>;
    }
    if (feature == "deposit-management" || feature === "withdrawal-management") {
      if (id === "isValidAml") {
        if (status == true) return <TableCell align="center">Pass</TableCell>;
        if (status == false) return <TableCell align="center">Fail</TableCell>;
      }
      if (id === "status") {
        if (status == 0) {
          return <TableCell align="center">Processing</TableCell>;
        }
        if (status == 1) {
          return <TableCell align="center">Approved</TableCell>;
        }
        if (status == 2) {
          return <TableCell align="center">Rejected</TableCell>;
        }
      }
    }
    if (feature == "edda-record") {
      if (status == 0) {
        return <TableCell align="center">Pending</TableCell>;
      }
      if (status == 1) {
        return <TableCell align="center">Success</TableCell>;
      }
      if (status == 2) {
        return <TableCell align="center">Failed</TableCell>;
      }
    }
    if (feature == "manual-adjustments-types") {
      return <TableCell align="center">{status === true ? "Active" : "Inactive"}</TableCell>;
    }

    if (feature === "transaction-records") {
      if (id === "tradeType") {
        if (status.toLowerCase() === "mi") return <TableCell align="center">Deposit</TableCell>;
        if (status.toLowerCase() === "mo") return <TableCell align="center">Withdrawal</TableCell>;
        if (status.toLowerCase() === "mia") return <TableCell align="center">Deposit Adjustment</TableCell>;
        if (status.toLowerCase() === "moa")
          return <TableCell align="center">Withdrawal Adjustment</TableCell>;
      }
    }

    return (
      <TableCell align="center" sx={{ color: status ? "#111" : "#a1a1a1" }}>
        {status ? status : "N/A"}
      </TableCell>
    );
  };

  const handleRequestSort = (event: React.MouseEvent<unknown>, property: string) => {
    const isAsc = orderBy === property && order === "asc";

    let sortList = [...paginationValue.sort];
    const sortIndex = sortList.findIndex((e) => e.field === property);

    if (isPagination) {
      if (sortIndex === -1) {
        sortList = [{ field: property, ascOrder: isAsc }];
      } else {
        sortList[sortIndex].ascOrder = !sortList[sortIndex].ascOrder;
      }

      setPagination({
        ...paginationValue,
        sort: sortList
      });
    }

    setOrder(isAsc ? "desc" : "asc");
    if (!isPagination) {
      setOrderBy(property);
    }
  };

  return (
    <Box sx={{ width: "100%" }}>
      <Paper sx={{ width: "100%", mb: 2 }}>
        {action.length ? <TableToolbar /> : null}
        {action.length ? <Divider /> : null}
        <TableContainer>
          <Table>
            <RenderTableHead header={header} rowCount={data?.length} onRequestSort={handleRequestSort} />
            <TableBody>
              {isLoading && (
                <TableRow>
                  <TableCell colSpan={header.length} align="center">
                    <CircularProgress color="inherit" size={25} />
                  </TableCell>
                </TableRow>
              )}
              {!isLoading
                ? isPagination
                  ? stableSort(isAccess ? data : dataWithAccess(data), getComparator(order, orderBy)).map(
                      (row: any, index: number) => {
                        return (
                          <TableRow tabIndex={-1} key={index}>
                            <RenderRowData key={index} row={row} index={index} />
                          </TableRow>
                        );
                      }
                    )
                  : stableSort(isAccess ? data : dataWithAccess(data), getComparator(order, orderBy))
                      .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                      .map((row: any, index: number) => {
                        return (
                          <TableRow tabIndex={-1} key={index}>
                            <RenderRowData key={index} row={row} index={index} />
                          </TableRow>
                        );
                      })
                : null}
              {(isAccess ? !data?.length : !dataWithAccess(data).length) && !isLoading && (
                <TableRow>
                  <TableCell colSpan={header.length} align="center" sx={{ background: "#ededed" }}>
                    No Data Available
                  </TableCell>
                </TableRow>
              )}
              {emptyRows > 0 && (
                <TableRow>
                  <TableCell colSpan={header.length} align="center" sx={{ background: "#ededed" }}>
                    End of row
                  </TableCell>
                </TableRow>
              )}
            </TableBody>
          </Table>
        </TableContainer>
        {!isPagination && data?.length ? (
          <TablePagination
            rowsPerPageOptions={[5, 10, 25]}
            component="div"
            count={isAccess ? data.length : dataWithAccess(data).length}
            rowsPerPage={rowsPerPage}
            page={page}
            onPageChange={handleChangePage}
            onRowsPerPageChange={handleChangeRowsPerPage}
          />
        ) : null}
        {!isLoading && isPagination && pagination ? (
          <TablePagination
            rowsPerPageOptions={[5, 10, 25]}
            component="div"
            count={pagination?.totalElements}
            rowsPerPage={paginationValue?.size}
            page={paginationValue.page}
            onPageChange={(val, newVal) => {
              setPagination({ ...paginationValue, page: newVal });
            }}
            onRowsPerPageChange={(event) => {
              setPagination({ ...paginationValue, size: parseInt(event.target.value), page: 0 });
            }}
          />
        ) : null}
      </Paper>
      <RejectModal
        isOpen={!!rejectData}
        data={rejectData}
        onClose={() => setRejectData(null)}
        onSubmit={rowActionCallback}
      />
    </Box>
  );
};

export default TableList;
