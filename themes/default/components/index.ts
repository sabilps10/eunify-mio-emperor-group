import { createTheme } from "@mui/material/styles";

import MuiButton from "./MuiButton";
import MuiTextField from "./MuiTextField";
import { MuiFormLabel } from "./MuiForm";
import { MuiCard } from "./MuiCard";
import MuiSelect from "./MuiSelect";
import MuiRadio from "./MuiRadio";

// // When using TypeScript 4.x and above
import type {} from "@mui/x-date-pickers/themeAugmentation";

// // When using TypeScript 3.x and below
// import "@mui/x-date-pickers/themeAugmentation";

const componentsTheme = createTheme({
  components: {
    MuiButton,
    MuiSelect,
    MuiFormLabel,
    MuiRadio,
    MuiCard,
    MuiTextField,
    MuiDatePicker: {
      styleOverrides: {
        root: {
          backgroundColor: "red"
        }
      }
    }
  }
});

export default componentsTheme;
