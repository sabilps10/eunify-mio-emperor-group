export type SortParams = {
  field: string;
  ascOrder: boolean;
};

export type FilterPagination = {
  page: number;
  size: number;
  sort: SortParams[];
};

export type Pagination = {
  empty: boolean;
  first: boolean;
  last: boolean;
  number: number;
  numberOfElement: number;
  pageable: {
    offset: number;
    pageNumber: number;
    pageSize: number;
    paged: true;
    sort: {
      empty: boolean;
      sorted: boolean;
      unsorted: boolean;
    };
  };
  size: number;
  sort: {
    empty: boolean;
    sorted: boolean;
    unsorted: boolean;
  };
  totalElements: number;
  totalPages: number;
};

export type PaginationState = (Pagination & {}) | null;
